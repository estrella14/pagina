<?php
include 'Vista/inc/cab.php';
?>
      <!-- Main Content -->
      <div id="content">

        <?php
          include 'Vista/inc/barra.php';
        ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
           
          </div>

          <!-- Content Row -->
          <div class="row">

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-4 col-md-6 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Leads Hestia</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800" id="res3"><?=$res1['cantidad']?></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-calendar fa-2x text-gray-300 cal" tipo="3"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-4 col-md-6 mb-4">
              <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Leads FGC</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800" id="res2"><?=$res2['cantidad']?></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-calendar fa-2x text-gray-300 cal" tipo="2"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-4 col-md-6 mb-4">
              <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Plataformas Especiales</div>
                      <div class="row no-gutters align-items-center">
                        <div class="col-auto">
                          <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800" id="res4"><?=$res3['cantidad']?></div>
                        </div>
                        
                      </div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-calendar fa-2x text-gray-300 cal" tipo="4"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            
          </div>

          <!-- Content Row -->

          <div class="row">

            <!-- Area Chart -->
            <div class="col-xl-8 col-lg-7">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Registros</h6>
                  <div class="dropdown no-arrow">
                  <i class="fas fa-calendar fa-sm fa-fw text-gray-400" id="graf1"></i>
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                      <div class="dropdown-header">Buscar por:</div>
                      <a class="dropdown-item" onClick="grafica(2)">Mes</a>
                      <a class="dropdown-item" onClick="grafica(1 )">Semana</a>
                    </div>
                  </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <div class="chart-area">
                    <canvas id="myAreaChart"></canvas>
                  </div>
                </div>
              </div>
            </div>

            <!-- Pie Chart -->
            <div class="col-xl-4 col-lg-5">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Flujo de origen</h6>
                  <div class="dropdown no-arrow">
                  <i class="fas fa-calendar fa-sm fa-fw text-gray-400" id="graf2"></i>
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                      <div class="dropdown-header">Dropdown Header:</div>
                      <a class="dropdown-item" href="#">Action</a>
                      <a class="dropdown-item" href="#">Another action</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                  </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <div class="chart-pie pt-4 pb-2">
                    <canvas id="myPieChart"></canvas>
                  </div>
                  <div class="mt-4 text-center small">
                    <span class="mr-2">
                      <i class="fas fa-circle text-primary"></i> Direct
                    </span>
                    <span class="mr-2">
                      <i class="fas fa-circle text-success"></i> Social
                    </span>
                    <span class="mr-2">
                      <i class="fas fa-circle text-info"></i> Referral
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>

          

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->
<?php

include 'Vista/inc/pie.php';

?>

<script>
Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#858796';
var labelsdata = 
	[
		<?php foreach($resRef as $row) 
			echo "'".$row['prop1']."' ," 
			?>
	]
;


var labelLineData = 	[
			<?php foreach($res5 as $row) 
			echo "'".$row['dia']."' ," 
			?>
			];
	var dataLineData =  [
		  <?php foreach($res5 as $row) 
			echo "".$row['cantidad']." ," 
			?>
	  ];
var config2 = {
  type: 'doughnut',
  data: {
    labels: labelsdata,
    datasets: [{
      data: [<?php foreach($resRef as $row) 
			echo "'".$row['cantidad']."' ," 
			?>],
      backgroundColor: ['#4e73df', '#1cc88a', '#36b9cc','#f6c23e'],
      hoverBackgroundColor: ['#2e59d9', '#17a673', '#2c9faf','#e6af24'],
      hoverBorderColor: "rgba(234, 236, 244, 1)",
    }],
  },
  options: {
    maintainAspectRatio: false,
    tooltips: {
      backgroundColor: "rgb(255,255,255)",
      bodyFontColor: "#858796",
      borderColor: '#dddfeb',
      borderWidth: 1,
      xPadding: 15,
      yPadding: 15,
      displayColors: false,
      caretPadding: 10,
    },
    legend: {
      display: false
    },
    cutoutPercentage: 80,
  },
};
var config = {
  type: 'line',
  data: {
    labels: labelLineData,
    datasets: [{
      label: "Prospectos",
      lineTension: 0.3,
      backgroundColor: "rgba(78, 115, 223, 0.05)",
      borderColor: "rgba(78, 115, 223, 1)",
      pointRadius: 3,
      pointBackgroundColor: "rgba(78, 115, 223, 1)",
      pointBorderColor: "rgba(78, 115, 223, 1)",
      pointHoverRadius: 3,
      pointHoverBackgroundColor: "rgba(78, 115, 223, 1)",
      pointHoverBorderColor: "rgba(78, 115, 223, 1)",
      pointHitRadius: 10,
      pointBorderWidth: 2,
      data:dataLineData,
    }],
  },
  options: {
    maintainAspectRatio: false,
    layout: {
      padding: {
        left: 10,
        right: 25,
        top: 25,
        bottom: 0
      }
    },
    scales: {
      xAxes: [{
        time: {
          unit: 'date'
        },
        gridLines: {
          display: false,
          drawBorder: false
        },
        ticks: {
          maxTicksLimit: 7
        }
      }],
      yAxes: [{
        ticks: {
          maxTicksLimit: 5,
          padding: 10,
          // Include a dollar sign in the ticks
          callback: function(value, index, values) {
            return "Registros: " +number_format(value);
          }
        },
        gridLines: {
          color: "rgb(234, 236, 244)",
          zeroLineColor: "rgb(234, 236, 244)",
          drawBorder: false,
          borderDash: [2],
          zeroLineBorderDash: [2]
        }
      }],
    },
    legend: {
      display: false
    },
    tooltips: {
      backgroundColor: "rgb(255,255,255)",
      bodyFontColor: "#858796",
      titleMarginBottom: 10,
      titleFontColor: '#6e707e',
      titleFontSize: 14,
      borderColor: '#dddfeb',
      borderWidth: 1,
      xPadding: 15,
      yPadding: 15,
      displayColors: false,
      intersect: false,
      mode: 'index',
      caretPadding: 10,
      callbacks: {
        label: function(tooltipItem, chart) {
          var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
          return datasetLabel + ': ' + number_format(tooltipItem.yLabel);
        }
      }
    }
  }
};

	function getNewChart(canvas, config) {
		        return new Chart(canvas, config);
				}
	
	 window.onload = function() {
      var ctx = document.getElementById("myAreaChart").getContext("2d");
		 var ctx2 = document.getElementById("myPieChart");
      window.myLine = getNewChart(ctx, config);
	  window.myLine2 = getNewChart(ctx2, config2);
    };
	
	

$('.cal').daterangepicker();
$('.cal').on('apply.daterangepicker', function(ev, picker) {
  var fecha1  = picker.startDate.format('YYYY-MM-DD');
  var fecha2 = picker.endDate.format('YYYY-MM-DD');
		var tipo = $(this).attr("tipo");
		$.ajax({
			url: '?evento=registros-en-rango',
			type:'post',
			data: {fecha1:fecha1,fecha2:fecha2,tipo:tipo},
			success:function(res){
				$("#res"+tipo).html(res);
			}
		});
});
$('#graf1').daterangepicker();
	$('#graf1').on('apply.daterangepicker', function(ev, picker) {
  var fecha1  = picker.startDate.format('YYYY-MM-DD');
  var fecha2 = picker.endDate.format('YYYY-MM-DD');

		$.ajax({
			url: '?evento=grafica-rango',
			type:'post',
			data: {fecha1:fecha1,fecha2:fecha2},
			success:function(res){
				console.log(res);
				var res =  JSON.parse(res);
				var labels = res.map(function(e) {
				   return e.dia;
				});
				var datass = res.map(function(e) {
				   return e.cantidad;
				});
				for (var index = 0; index = config.data.labels.length ; ++index) {
					//console.log(index);
					config.data.labels.splice(-1, 1); // remove the label first
					  config.data.datasets.forEach(function(dataset, datasetIndex) {
						dataset.data.pop();
					  });
				}			
					
				for (var index = 0; index < datass.length; ++index) {
					config.data.labels.push(labels[index]);
					config.data.datasets.forEach(function(dataset) {
					  dataset.data.push(datass[index]);
					});
				}
      			window.myLine.update();
			}
		});
});
function grafica2(tipo){
		$.ajax({
			url:'?evento=semana-o-mes',
			type:'post',
			data:{tipo:tipo},
			success:function(res){
				
				var res =  JSON.parse(res);
				var labels = res.map(function(e) {
				   return e.prop1;
				});
				var datass = res.map(function(e) {
				   return e.cantidad;
				});
				
				for (var index = 0; index = config2.data.labels.length ; ++index) {
					console.log(index);
					config2.data.labels.splice(-1, 1); // remove the label first
					  config2.data.datasets.forEach(function(dataset, datasetIndex) {
						dataset.data.pop();
					  });
				}			
					
				for (var index = 0; index < datass.length; ++index) {
					config2.data.labels.push(labels[index]);
					config2.data.datasets.forEach(function(dataset) {
					  dataset.data.push(datass[index]);
					});
				}
      			window.myLine2.update();
			}
		});
		
  }
  $('#graf1').daterangepicker();
	$('#graf1').on('apply.daterangepicker', function(ev, picker) {
  var fecha1  = picker.startDate.format('YYYY-MM-DD');
  var fecha2 = picker.endDate.format('YYYY-MM-DD');

		$.ajax({
			url: '?evento=rango-grafica',
			type:'post',
			data: {fecha1:fecha1,fecha2:fecha2},
			success:function(res){
				console.log(res);
				var res =  JSON.parse(res);
				var labels = res.map(function(e) {
				   return e.dia;
				});
				var datass = res.map(function(e) {
				   return e.cantidad;
				});
				for (var index = 0; index = config.data.labels.length ; ++index) {
					//console.log(index);
					config.data.labels.splice(-1, 1); // remove the label first
					  config.data.datasets.forEach(function(dataset, datasetIndex) {
						dataset.data.pop();
					  });
				}			
					
				for (var index = 0; index < datass.length; ++index) {
					config.data.labels.push(labels[index]);
					config.data.datasets.forEach(function(dataset) {
					  dataset.data.push(datass[index]);
					});
				}
      			window.myLine.update();
			}
		});
});
  $('#graf2').daterangepicker();
	$('#graf2').on('apply.daterangepicker', function(ev, picker) {
  var fecha1  = picker.startDate.format('YYYY-MM-DD');
  var fecha2 = picker.endDate.format('YYYY-MM-DD');
		
		$.ajax({
			url: '?evento=rango-grafica-cir',
			type:'post',
			data: {fecha1:fecha1,fecha2:fecha2},
			success:function(res){
				console.log(res);
				var res =  JSON.parse(res);
				var labels = res.map(function(e) {
				   return e.prop1;
				});
				var datass = res.map(function(e) {
				   return e.cantidad;
				});
				
				for (var index = 0; index = config2.data.labels.length ; ++index) {
					//console.log(index);
					config2.data.labels.splice(-1, 1); // remove the label first
					  config2.data.datasets.forEach(function(dataset, datasetIndex) {
						dataset.data.pop();
					  });
				}			
					
				for (var index = 0; index < datass.length; ++index) {
					config2.data.labels.push(labels[index]);
					config2.data.datasets.forEach(function(dataset) {
					  dataset.data.push(datass[index]);
					});
				}
      			window.myLine2.update();
			}
		});
});
</script>