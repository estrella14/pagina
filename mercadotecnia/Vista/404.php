<?php
include 'Vista/inc/cab.php';
?>

      <!-- Main Content -->
      <div id="content">

        <?php
          include 'Vista/inc/barra.php';
        ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- 404 Error Text -->
          <div class="text-center">
            <div class="error mx-auto" data-text="404">404</div>
            <p class="lead text-gray-800 mb-5">Página No Encontrada</p>
            <p class="text-gray-500 mb-0">¡ocurrio un error!...</p>
            <a href="?evento=inicio">&larr; regresar a Dashboard</a>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

<?php 
include 'Vista/inc/pie.php';

?>