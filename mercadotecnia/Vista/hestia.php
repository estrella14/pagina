<?php
include 'Vista/inc/cab.php';
?>
<div id="content">
    <?php
    include 'Vista/inc/barra.php';
    ?>
    <div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Plataforma Digital Fiduciaria</h1>
<p class="mb-4">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsam incidunt iure recusandae voluptate nobis ui? Beatae illum alias eos impedit nemo?</p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">Información de contacto</h6>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>Nombre</th>
            <th>Correo</th>
            <th>Teléfono</th>
            <th>Mensaje</th>
            <th>Fecha</th>
            <th>Prop1</th>
            <th>Acciones</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
          <th>Nombre</th>
            <th>Correo</th>
            <th>Teléfono</th>
            <th>Mensaje</th>
            <th>Fecha</th>
            <th>Prop1</th>
            <th>Acciones</th>
          </tr>
        </tfoot>
        <tbody>
          <?php foreach ($res as $row) {?>
          <tr>
            <td><?=$row['nombre']?></td>
            <td><?=$row['correo']?></td>
            <td><?=$row['tel']?></td>
            <td><?=$row['msg']?></td>
            <td><?=$row['fecha']?></td>
            <td><?=$row['prop1']?></td>
            <td><button class="btn btn-sm btn-warning">Borrar</button></td>
          </tr>
          <?php } ?>
         
        </tbody>
      </table>
    </div>
  </div>
</div>

</div>
</div>

<?php
include 'Vista/inc/pie.php';
?>