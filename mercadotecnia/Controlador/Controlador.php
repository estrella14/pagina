<?php
session_start();
include 'funciones/config.php';

include 'Modelo/Modelo.php';
class Controlador  
{
    function handler($evento){
        if($evento != "post-login"){
            if(!isset($_SESSION['usr-credix-thrust'])){
                $evento = 'login';
            }
        }
        
        $inicio = set_obj();
        switch ($evento) {
            case 'inicio':
               /* $res = $inicio->getArray("*","cdx_thrust");
                print_r($res);*/
                $res1 = $inicio->get("COUNT(id) as cantidad","cdx_thrust","tabla = '3'");
				$res2 = $inicio->get("COUNT(id) as cantidad","cdx_thrust","tabla = '2'");
                $res3 = $inicio->get("COUNT(id) as cantidad","cdx_thrust","tabla = '4'");
                $mes = date("n");
				$res5 = $inicio->getArray(" DAY(fecha) as dia ,MONTH(fecha) as mes, COUNT(*) as cantidad ","cdx_thrust","MONTH( fecha ) = '$mes'","GROUP BY DAY(fecha) , MONTH(fecha)");
                $resRef = $inicio->getArray("prop1, COUNT( prop1 ) as cantidad","cdx_thrust GROUP BY prop1 HAVING COUNT( prop1 ) >1");
                include 'Vista/index.php';
                break;
            case 'login':
                include 'Vista/login.php';
            break;
            case 'post-login':
                if($_POST['usr']=="admin-ct" && $_POST['psw']=="CredixThrust2020@"){
                     $_SESSION['usr-credix-thrust'] = "check";
                     header("Location: ?evento=inicio");   
                }else{
                    header('Location: ?evento=login&error=1');
                }
            break;
            case 'plataformas-especiales':
                $res = $inicio->getArray("*","cdx_thrust","tabla = '4' and borrado = '0'","order by id desc");
                include 'Vista/ct.php'; 
            break;
            case 'hestia':
                $res = $inicio->getArray("*","cdx_thrust","tabla = '3' and borrado = '0'","order by id desc");
                include 'Vista/hestia.php';
            break;
            case 'fideicomiso-de-garantía-de-credito':
                $res = $inicio->getArray("*","cdx_thrust","tabla = '2' and borrado = '0'","order by id desc");
                include 'Vista/fgc.php';
            break;
            case 'salir-cuenta':
                session_destroy();
                header('Location: ?evento=login');
            break;
            case 'borrar-post':
                $id = $_POST['id'];
				//print_r($datos);
				$ins = $inicio->update("cdx_thrust","borrado = 1", "id = '$id'");
			    //echo $ins;
				$ref = $_SERVER['HTTP_REFERER'];
				header('Location: '.$ref.' ');
				break;
            break;
            case 'registros-en-rango':
                $fecha1 = $_POST['fecha1'];
				$fecha2 = $_POST['fecha2'];
				$fechafinal = date("Y-m-d",strtotime($fecha2."+ 1 days"));//sumamos un dia para mysql detecte los dias exepto el ultimo
				$tabla = $_POST['tipo'];
				$res1 = $inicio->get("COUNT(id) as cantidad","cdx_thrust","tabla = '$tabla' and  fecha BETWEEN '$fecha1' AND '$fechafinal'");
				echo $res1['cantidad'];
            break;
            case 'grafica-rango':
				$fecha1 = $_POST['fecha1'];
				$fecha2 = $_POST['fecha2'];
				$fechafinal = date("Y-m-d",strtotime($fecha2."+ 1 days"));
				$res5 = $inicio->getArray(" DAY(fecha) as dia ,MONTH(fecha) as sem, COUNT(*) as cantidad ","cdx_thrust"," fecha BETWEEN '$fecha1' AND '$fechafinal'","GROUP BY DAY(fecha) , MONTH(fecha)");
				echo json_encode($res5);
            break;
            case 'semana-o-mes':
                $tipo = $_POST['tipo'];
				switch($tipo){
					case '1': //semana
						$sem = date('W');
						$sem -=1;
						$resRef = $inicio->getArray("prop1, COUNT( prop1 ) as cantidad","cdx_thrust ","WEEK( fecha ) = '$sem'","GROUP BY prop1 HAVING COUNT( prop1 ) >1");
						break;
					case '2': //mes
						$mes = date("n");
						$resRef = $inicio->getArray("prop1, COUNT( prop1 ) as cantidad","cdx_thrust ","MONTH( fecha ) = '$mes'","GROUP BY prop1 HAVING COUNT( prop1 ) >1");
						break;
				}
				echo json_encode($resRef);
            break;
            case 'rango-grafica':
				$fecha1 = $_POST['fecha1'];
				$fecha2 = $_POST['fecha2'];
				$fechafinal = date("Y-m-d",strtotime($fecha2."+ 1 days"));
				$res5 = $inicio->getArray(" DAY(fecha) as dia ,MONTH(fecha) as sem, COUNT(*) as cantidad ","cdx_thrust"," fecha BETWEEN '$fecha1' AND '$fechafinal'","GROUP BY DAY(fecha) , MONTH(fecha)");
				echo json_encode($res5);
			break;
            case 'rango-grafica-cir':
				$fecha1 = $_POST['fecha1'];
				$fecha2 = $_POST['fecha2'];
				$fechafinal = date("Y-m-d",strtotime($fecha2."+ 1 days"));
				$resRef = $inicio->getArray("prop1, COUNT( prop1 ) as cantidad","cdx_thrust ","fecha BETWEEN '$fecha1' AND '$fechafinal'","GROUP BY prop1 HAVING COUNT( prop1 ) >1");
				echo json_encode($resRef);
			break;
            default:
                include 'Vista/404.php';
            break;
            

        }
    }
}
function set_obj()
		{
		    $obj = new Modelo();
		    return $obj;
		}


?>