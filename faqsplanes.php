<?php
$pageLink = "Preguntas frecuentes";
include 'inc/cab.php';
?>
<style>
    .buscador-faqs {
        display: flex;
        justify-content: center;
        margin-bottom: 3rem;
    }

    .inputbuscar {
        display: flex;
        box-shadow: 0px 0px 10px 1px #00000052;
        align-items: center;
        background-color: #fff;
        border-radius: 15px;
        width: 50%;
    }

    .inputbuscar input {
        border: none;
        padding: 1.3rem 0.5rem;
        border-radius: 15px;
        width: 100%;
    }

    .inputbuscar input:focus {
        outline: none;
    }

    .inputbuscar .iconobuscar {
        margin: 0 0.2rem 0 1rem;
    }

    .lateral_faq {
        height: 100vh;
        box-shadow: 4px 0 4px -2px #00000054;
        padding: 0 15px;
        position: fixed;
        left: 0;
        width: 17.7%;
        background-color: #fff;
        font-size: .9375rem;
    }

    .item_nav_faq {
        display: flex;
        padding: 1rem 1.2rem;
        border-radius: 15px;
        margin-bottom: 10px;
    }

    .item_nav_faq.active a div span {
        color: #fff;
    }

    .item_nav_faq.active {
        background-color: #051c33;
        color: #fff;
    }

    .item_nav_faq.active:hover {
        background-color: #051c33;
        cursor: auto;
    }

    .item_nav_faq .icono_nav {
        padding: 0 1.2rem 0 0rem;
    }

    .item_nav_faq:hover {
        background-color: #9ac5f0;
        cursor: pointer;
    }

    .select_mob_faqs {
        display: none;
    }

    @media (max-width:767px) {
        .lateral_faq {
            display: none;
        }

        .select_mob_faqs {
            display: flex;
            justify-content: center;
        }

        .inputbuscar {
            width: 90%;
        }

        .faqs .container-fluid {
            padding-right: 0px;
            padding-left: 0px;
        }

        .faqs .mob-faq {
            padding-right: 15px;
            padding-left: 15px;
        }
    }
</style>
<main style="background-color:#f5f5f5">
    <section class="faqs">
        <div class="container-fluid">
            <div class="row">

                <?php
                include 'menuFaqs.php';
                ?>
                <div class="col-md-10 mt-5 contenedor-mob" style="text-align:justify">

                    <div class="ts-title text-center mt-4">
                        <h2>Preguntas Frecuentes - Nuestros Planes</h2>
                    </div>
                    <center>
                        <p id="textoselect" hidden>Seleccione alguna sección de la cual desee buscar</p>
                    </center>
                    <div class="select_mob_faqs mb-5 form-group container">
                        <select name="" id="whatfaqs" class="form-control" onchange="whatsfaqs()">
                            <option value="Covenant Documentos">Covenant Documentos</option>
                            <option value="Covenant Firmas">Covenant Firmas</option>
                            <option value="Covenant App">Covenant App</option>
                            <option value="Sello Digital de Tiempo">Sello Digital de Tiempo</option>
                            <option value="Constancia NOM-151">Constancia NOM-151</option>
                            <option value="Planes de Covenant" selected>Planes de Covenant</option>
                            <option value="Definiciones">Definiciones</option>
                        </select>
                    </div>
                    <div class="buscador-faqs">
                        <div class="inputbuscar">
                            <div class="iconobuscar">
                                <i class="fa fa-search"></i>
                            </div>
                            <input type="input" placeholder="Escriba algo a buscar..." id="buscador">
                        </div>
                    </div>
                    <div id="accordion" class="row mob-faq justify-content-center mb-5">
                        <div id="" class="accordion col-md-11 ">
                            <div class="card">
                                <div class="card-header faq" id="headingOne">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link  pregunta_search" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            1.- ‍¿Cómo puedo obtener mi Firma Electrónica?

                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>

                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="card-body">
                                        Para más información acerca e cómo obtener tu firma electrónica avanzada visita el siguiente link <a class="Linkss" href="https://www.gob.mx/tramites/ficha/obtencion-de-e-firma/SAT137">https://www.gob.mx/tramites/ficha/obtencion-de-e-firma/SAT137</a>
                                        Encontrarás cómo obtener tu Firma Electrónica Avanzada emitida por el SAT.


                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="headingTwo">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            2.- ¿Qué hacer si deseo incluir más documentos a mi plan contratado?


                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                    <div class="card-body">
                                        Puedes cambiar tu suscripción al siguiente plan para obtener más beneficios.

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="headingThree">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            3.- ¿Conexión vía API?



                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        Podemos hacer la integración Vía API al contratar nuestros servicios con el Plan Corporativo.
                                        SERVICIOS API:
                                        Conjunto de métodos y funciones que permite la conexión con los servicios de Covenant a través de la plataforma del cliente (servicio opcional). Como requerimiento principal, la empresa debe desarrollar un cliente WebService de tipo Rest, para enviar y recibir la información, esto de acuerdo con el lenguaje compatible con su propio desarrollo.

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p4">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r4" aria-expanded="false" aria-controls="r4">
                                            4.- ¿Puedo agregar más usuarios a mi plan contratado?





                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r4" aria-expanded="false" aria-controls="r4"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r4" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        No, cada Plan contiene un número de usuarios limitado, si desea agregar más usuarios, le sugerimos suscribirse a un Plan mayor que satisfaga sus necesidades.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p5">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r5" aria-expanded="false" aria-controls="r5">
                                            5.- ¿Cuál es el método de pago?


                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r5" aria-expanded="false" aria-controls="r4"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r5" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        Puede hacer el pago por suscripción mensual o bien, hacerlo de forma anual de acuerdo con su preferencia.


                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p6">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r6" aria-expanded="false" aria-controls="r6">
                                            6.- ¿Qué pasa con los documentos que no utilice durante el mes?



                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r6" aria-expanded="false" aria-controls="r6"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r6" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        Al finalizar el mes, los documentos se cancelan y no podrá hacer uso de los restantes.
                                        Al pagar la suscripción mensual renueva sus beneficios.


                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p8">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r8" aria-expanded="false" aria-controls="r8">
                                            7.- ¿Puedo adquirir más firmas de las que incluye mi plan contratado?




                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r8" aria-expanded="false" aria-controls="r8"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r8" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        Legalex puede ofrecerle las firmas que usted necesite, puede contratar paquetes de firmas, de acuerdo con sus necesidades.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p9">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r9" aria-expanded="false" aria-controls="r9">
                                            8.- ¿Qué pasa si excedo o no termino las firmas adquiridas?

                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r9" aria-expanded="false" aria-controls="r9"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r9" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        Las firmas que Legalex ofrece se reinician mensualmente, y en caso de excederse tendrá un costo adicional el cual será facturado mensualmente.

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p10">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r10" aria-expanded="false" aria-controls="r10">
                                            9.- ¿El servicio de Constancias de Conservación de Mensajes de Datos NOM-151 está disponible para todos los planes?


                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r10" aria-expanded="false" aria-controls="r10"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r10" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        El servicio de las CCMD únicamente esta disponible para el Plan Corporativo, los planes personal, negocios y empresarial únicamente incluyen el servicio de Firma Electrónica.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p11">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r11" aria-expanded="false" aria-controls="r11">
                                            10.- ¿Puedo contratar únicamente el Servicio de Constancias de Conservación de Mensajes de Datos NOM-151 y Sello Digital de Tiempo?


                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r11" aria-expanded="false" aria-controls="r11"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r11" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        Si, Legalex puede ofrecer el Servicio que usted requiere de forma individual, tendrá que contactar a ventas vía llamada telefónica o dejandonos un correo electrónico para escoger el servicio deseado y uno de nuestros asesores de ventas con gusto lo atenderá.

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p12">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r12" aria-expanded="false" aria-controls="r12">
                                            11.- ¿Tienen costo los usuarios adicionales?


                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r12" aria-expanded="false" aria-controls="r12"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r12" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        Cada plan tiene un número de usuarios determinado, si desea más usuarios deberá cambiar de plan. Para el caso del Plan Corporativo el número de usuarios es ilimitado.


                                    </div>
                                </div>
                            </div>



                        </div>
                    </div>
                    <?php
                    include 'inc/pie.php';
                    ?>
                </div>
            </div>
        </div>
    </section>
</main>

<script type="text/javascript" src="assets/js/faqs.js"></script>