<?php
$pageLink = "Preguntas frecuentes";
include 'inc/cab.php';
?>
<style>
    .buscador-faqs {
        display: flex;
        justify-content: center;
        margin-bottom: 3rem;
    }

    .inputbuscar {
        display: flex;
        box-shadow: 0px 0px 10px 1px #00000052;
        align-items: center;
        background-color: #fff;
        border-radius: 15px;
        width: 50%;
    }

    .inputbuscar input {
        border: none;
        padding: 1.3rem 0.5rem;
        border-radius: 15px;
        width: 100%;
    }

    .inputbuscar input:focus {
        outline: none;
    }

    .inputbuscar .iconobuscar {
        margin: 0 0.2rem 0 1rem;
    }

    .lateral_faq {
        height: 100vh;
        box-shadow: 4px 0 4px -2px #00000054;
        padding: 0 15px;
        position: fixed;
        left: 0;
        width: 17.7%;
        background-color: #fff;
        font-size: .9375rem;
    }

    .item_nav_faq {
        display: flex;
        padding: 1rem 1.2rem;
        border-radius: 15px;
        margin-bottom: 10px;
    }

    .item_nav_faq.active a div span {
        color: #fff;
    }

    .item_nav_faq.active {
        background-color: #051c33;
        color: #fff;
    }

    .item_nav_faq.active:hover {
        background-color: #051c33;
        cursor: auto;
    }

    .item_nav_faq .icono_nav {
        padding: 0 1.2rem 0 0rem;
    }

    .item_nav_faq:hover {
        background-color: #9ac5f0;
        cursor: pointer;
    }

    .select_mob_faqs {
        display: none;
    }

    @media (max-width:767px) {
        .lateral_faq {
            display: none;
        }

        .select_mob_faqs {
            display: flex;
            justify-content: center;
        }

        .inputbuscar {
            width: 90%;
        }

        .faqs .container-fluid {
            padding-right: 0px;
            padding-left: 0px;
        }

        .faqs .mob-faq {
            padding-right: 15px;
            padding-left: 15px;
        }
    }
</style>
<main style="background-color:#f5f5f5">
    <section class="faqs">
        <div class="container-fluid">
            <div class="row">

                <?php
                include 'menuFaqs.php';
                ?>
                <div class="col-md-10 mt-5 contenedor-mob" style="text-align:justify">

                    <div class="ts-title text-center mt-4">
                        <h2>Preguntas Frecuentes - Constancia NOM-151</h2>
                    </div>

                    <center>
                        <p id="textoselect" hidden>Seleccione alguna sección de la cual desee buscar</p>
                    </center>
                    <div class="select_mob_faqs mb-5 form-group container">
                        <select name="" id="whatfaqs" class="form-control" onchange="whatsfaqs()">
                            <option value="Covenant Documentos">Covenant Documentos</option>
                            <option value="Covenant Firmas">Covenant Firmas</option>
                            <option value="Covenant App">Covenant App</option>
                            <option value="Sello Digital de Tiempo">Sello Digital de Tiempo</option>
                            <option value="Constancia NOM-151" selected>Constancia NOM-151</option>
                            <option value="Planes de Covenant">Planes de Covenant</option>
                            <option value="Definiciones">Definiciones</option>
                        </select>
                    </div>
                    <div class="buscador-faqs">
                        <div class="inputbuscar">
                            <div class="iconobuscar">
                                <i class="fa fa-search"></i>
                            </div>
                            <input type="input" placeholder="Escriba algo a buscar..." id="buscador">
                        </div>
                    </div>
                    <div id="accordion" class="row mob-faq justify-content-center mb-5">
                        <div id="" class="accordion col-md-11 ">
                            <div class="card">
                                <div class="card-header faq" id="headingOne">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link  pregunta_search" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            1.- ‍¿Qué es?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>

                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="card-body">
                                        Una constancia de conservación de mensajes de datos conforme la NOM-151-SCFI-2016, propiamente es un documento o archivo electrónico que permite garantizar que la información que es recibida de forma electrónica (bajo una solicitud) al igual que un sello digital de tiempo, existió en un tiempo y con características específicas.
                                        <br><br>
                                        Esto permite resguardar una evidencia informática de que un archivo no ha sido modificado y se conserva de forma íntegra por un tercero de confianza. El principal beneficio de conservar un mensaje de datos conforme la NOM-151 es la validez a través del tiempo ante una autoridad dentro del territorio mexicano, otorgando certeza jurídica y valor probatorio en juicio; ya que permite demostrar su integridad, en caso de que el documento hubiera sido alterado posteriormente.





                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="headingTwo">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            2.- ¿Quién puede emitir una Constancia de Conservación de Mensajes de Datos?


                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                    <div class="card-body">
                                        ‍‍Solo un Prestador de Servicios de Certificación acreditado por la Secretaría de Economía.

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="headingThree">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            3.- ¿Para qué me sirve?


                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        Para brindar a una autoridad dentro del territorio mexicano la certeza que el documento fue creado en el momento que se realizó la operación y no fue creado en el momento que se realiza la revisión por parte de las autoridades. Así como la obtención de integridad y conservación del documento a través del tiempo.


                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p4">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r4" aria-expanded="false" aria-controls="r4">
                                            4.- ¿A cuáles documentos puedo aplicar una CCMD-NOM-151?




                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r4" aria-expanded="false" aria-controls="r4"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r4" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        Solo a archivos que tengan extensión PDF, pudiendo ser: contratos, acuerdos de confidencialidad, códigos de ética, pagares, fideicomisos, cartas instrucción, avalúos, etc.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p5">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r5" aria-expanded="false" aria-controls="r5">
                                            5.- ¿Tiene vigencia? ¿Cuánto tiempo?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r5" aria-expanded="false" aria-controls="r4"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r5" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        Al aplicar la Constancia de Conservación de Mensajes de Datos, garantiza que el documento se conserva íntegro desde la fecha en que se le aplicó la Constancia de Conservación de Mensajes de Datos y hasta por un periodo de 10 años.


                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p6">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r6" aria-expanded="false" aria-controls="r6">
                                            6.- ¿Cuánto cuesta el servicio de emisión de Constancia de Conservación de Mensajes de Datos?


                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r6" aria-expanded="false" aria-controls="r6"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r6" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        Para más información sobre nuestros servicios, paquetes o planes de venta puede contactarnos en contacto@legalexgs.com o a los teléfonos: <a class="Linkss" href="tel:4436906851p101">+52 443 690 6851 Ext.101</a> y <a class="Linkss" href="tel:4436906855">+52 443 690 6855</a>


                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p7">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r7" aria-expanded="false" aria-controls="r7">
                                            7.- ¿Qué es “fecha cierta”?



                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r7" aria-expanded="false" aria-controls="r7"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r7" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        Es un es un requisito exigible respecto de los documentos privados que se presentan a la autoridad fiscal como consecuencia del ejercicio de sus facultades de comprobación, que los contribuyentes tienen el deber de conservar para demostrar la adquisición de un bien o la realización de un contrato u operación que incida en sus actividades fiscales.
                                    </div>
                                </div>
                            </div>




                        </div>
                    </div>
                    <?php
                    include 'inc/pie.php';
                    ?>
                </div>
            </div>
        </div>
    </section>
</main>

<script type="text/javascript" src="assets/js/faqs.js"></script>