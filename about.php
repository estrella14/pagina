<?php 
$pageLink = "acerca";
include 'inc/cab.php';
?>
<head>
<link rel="stylesheet" href="assets/css/nuevo.css">
</head>
<style>

@media (max-width: 800px){
    .cards-container row {
      width: 100%;
      margin-left: 0%;
      /* width: 90%;
      margin-left: 5%; */
    }
}

</style>

    <div class="ts-content mt-5">
       
    <header id="ts-hero" class="pt-4 pb-4 bannerPrincipal  text-black" data-bg-color="#F2F2F5" >

    <div class="container-fluid covenant">
                


        <main id="ts-content">

       
            <section id="story" class="ts-block " data-animate="ts-fadeInUp" data-ts-delay=".3s" style="padding-top:0rem">
                <div class="container">
                    <div class="row">

                        <div class="col-md-4  text-center" data-animate="ts-fadeInLeft" data-ts-delay=".3s">
                            <div class="ts-title">
                                <h2>¿Qué es un prestador de servicios de certificación?</h2>
                            </div>
                        </div>

                        <div class="col-md-8 text-justify" data-animate="ts-fadeInRight" data-ts-delay=".3s" >
                            <blockquote class="blockquote" data-animate="ts-fadeInUp" data-ts-delay=".3s">
                                <h3 class="text-center">
                                El futuro es ahora, construyámoslo juntos
                                </h3>
                        <div class="blockquote-footer"></div>
                    </blockquote>
                            <p>
                            Es un auxiliar del comercio, brindando a las pequeñas, medianas y grandes empresas servicios 
                            jurídico-tecnológicos para agilizar y dar certeza en los actos comerciales celebrados entre partes. 
                            <a style="color:#ff711c;font-weight: 900;" href="http://www.firmadigital.gob.mx/">(Secretaría de Economía, 2018.)</a>
                            </p>
                            <p>
                            Brinda certeza jurídica, validez legal y seguridad informática en la celebración de actos de comercio por medios electrónicos, 
                            a través del uso de la Firma Electrónica Avanzada, Conservación de Constancias de Mensajes de Datos, 
                            Emisión de Sellos Digitales de Tiempo y Digitalización de documentos en soporte físico.
                            </p>
                            <div class="ts-group h5">
                                <a href="https://www.linkedin.com/company/legalex-gs/?originalSubdomain=mx" class="px-1" target="_blank">
                                    <i class="fab fa-linkedin-in"></i>
                                </a>
                                <a href="https://www.youtube.com/channel/UC32g56nRFbuzZRsr-1yjQcw" class="px-1" target="_blank">
                                    <i class="fab fa-youtube-square"></i>
                                </a>
                            </div>
                        </div>

                    </div>
                </div>
            </section>
            </div>

    </header>


    <section id="features" class="pt-5 pb-5 text-black text-center text-sm-left" data-bg-color="#132035" >
                <div class="container">
                
                    <div class="row">

                        <div class="col-md-4 text-center text-white" data-animate="ts-fadeInLeft" data-ts-delay=".10s">
                            <div class="ts-feature">
                                <img src="assets/img/idea.png" alt="" class="mb-5">
                                <h4>NUESTRA MISIÓN</h4>
                                <p style="text-align: justify;">Ser una de las mejores empresas Prestadoras de Servicios de Certificación, eficiente y competitiva a nivel nacional e internacional. Caracterizada por su creatividad, solidez, eficiencia y honestidad. Centrados en la satisfacción y las necesidades tecnológicas de nuestros clientes.</p>
                            </div>
                        </div>
                       
                      

                        <div class="col-md-4 text-center text-white" data-animate="ts-fadeInLeft" data-ts-delay=".10s">
                            <div class="ts-feature">
                                <img src="assets/img/agil.png" alt="" class="mb-5">
                                <h4>NUESTRA VISIÓN</h4>
                                <p style="text-align: justify;"> Consolidarnos como la mejor empresa Prestadora de Servicios de Certificación, siendo innovadores en los servicios financieros y tecnológicos, buscando siempre estar a la vanguardia del mercado.</p>
                            </div>
                        </div>
                        
                        <div class="col-md-4 text-center text-white" data-animate="ts-fadeInLeft" data-ts-delay=".10s">
                            <div class="ts-feature">
                                <img src="assets/img/cuete.png" alt="" class="mb-5">
                                <h4>NUESTROS OBJETIVOS</h4>
                                <p style="text-align: justify;">Identificar, evaluar y valorar las mejores prácticas tecnológicas del desarrollo de software. Establecer controles para el desarrollo del o los servicios de nuestra empresa como de la emisión de sello digital de tiempo y sus complementos. Mantener la seguridad e integridad de los datos e información que se maneja durante nuestros servicios sin importar que sean digitales, electrónicos o en papel.</p>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="ts-background ts-bg-black">
                    <div class="ts-background-image ts-background-repeat ts-opacity__10 ts-img-into-bg back-parallax">
                        <img src="assets/img/bg__pattern--topo-white.png" alt="">
                    </div>
                </div>
            </section>

            


            <section class="ts-block ts-background-repeat back-parallax" data-animate="ts-fadeInLeft" data-ts-delay=".3s">
            <h2 class="pt-5 text-center"><b>Nuestros Valores</b> </h2>
                <div class="container ts-promo-numbers text-center ">
                <div class="ts-title text-center "  data-animate="ts-fadeInLeft" data-ts-delay=".5s"> 
                    <div class="row justify-content-center valores">

                        <div class="col-md-3" data-animate="ts-fadeInLeft" data-ts-delay=".5s">
                            <div class="ts-feature   text-center">
                                <h4>HONESTIDAD</h4>
                                <img src="assets/img/valor1.svg" alt="" class="mb-5" style="width:95px">
                               
                               
                            </div>
                        </div>

                        <div class="col-md-3 " data-animate="ts-fadeInLeft" data-ts-delay=".5s">
                            <div class="ts-feature text-justify">
                                <p >Todas nuestras acciones se basan en esté valor. Construimos para que creas en nosotros.</p>
                            </div>
                        </div>
                        
                        <div class="col-md-3" data-animate="ts-fadeInLeft" data-ts-delay=".5s">
                            <div class="ts-feature text-center">
                                <h4>CONFIANZA</h4>
                                <img src="assets/img/confianza.svg" alt="" class="mb-5" style="width:95px">
                            </div>
                        </div>
                        <div class="col-md-3 " data-animate="ts-fadeInLeft" data-ts-delay=".5s">
                            <div class="ts-feature  text-justify">
                                <p>Nos esforzamos diariamente por cumplir altos estándares de calidad y seguridad con nuestro equipo de trabajo así como con los clientes.</p>
                            </div>
                        </div>

                        <div class="col-md-3 " data-animate="ts-fadeInLeft" data-ts-delay=".5s">
                            <div class="ts-feature  text-center">
                                <h4>RESPONSABILIDAD</h4>
                                <img src="assets/img/responsabilidad.svg" alt="" class="mb-5" style="width:95px">
                            </div>
                        </div>

                        <div class="col-md-3 " data-animate="ts-fadeInLeft" data-ts-delay=".5s">
                            <div class="ts-feature  text-justify">
                                <p>Somos una empresa que cuida cada uno de sus pasos, pensando siempre en la integridad de los clientes cada vez que usan nuestros sistemas.</p>
                            </div>
                        </div>
                        

                        <div class="col-md-3 text-justify" data-animate="ts-fadeInLeft" data-ts-delay=".5s">
                            <div class="ts-feature  text-center">
                                <h4>PROFESIONALISMO</h4>
                                <img src="assets/img/profesionalismo.svg" alt="" class="mb-5" style="width:95px">
                            </div>
                        </div>

                        <div class="col-md-3 text-justify" data-animate="ts-fadeInLeft" data-ts-delay=".5s">
                            <div class="ts-feature  text-justify">
                                <p> Fundamento esencial, trabajamos y construimos cada producto, sistema o software con los clientes como si fueran de nuestra familia.</p>
                            </div>
                        </div>

                        
                       

                    </div>
                </div>
            </section>


<section class="bannerPrincipal pt-4 pb-4"  data-bg-color="#F2F2F5"  style="margin: auto auto 30px auto; padding: 15px;">
   
     <?php
        include 'card/nuevo.html';
        ?>
    

</section>

  
   




        </main>
       

<?php
include 'inc/pie.php';
?>
