<?php 
$pageLink = "sello";
include 'inc/cab.php';
?>

    
    <div class="ts-content">

           
        <main id="ts-content">

        
            <section id="about" class="ts-block py-5">
                <div class="container pt-5">
                    <div class="row justify-content-center">

                        <div class="col-md-12" data-animate="ts-fadeInDown" data-ts-delay=".15s">
                            <div class="ts-title">
                                <h2> Politicas de Cookies</h2>
                            </div>
                        </div>

                        <div class="col-md-8" data-animate="ts-fadeInDown" data-ts-delay=".55s">
                            <p>
                            En nuestro Sitio web utilizamos "cookies". Las cookies son pequeños archivos de datos que se almacenan en el disco duro de su equipo de cómputo o del dispositivo de comunicación electrónica que usted utiliza cuando navega en nuestro Sitio. Estos archivos de datos permiten intercambiar información de estado entre nuestro Sitio web y el navegador que usted utiliza. La "información de estado" puede revelar medios de identificación de sesión, medios de autenticación o sus preferencias como usuario, así como cualquier otro dato almacenado por el navegador respecto del Sitio web. <br><br>
Las cookies nos permiten monitorear el comportamiento de un usuario en línea. Utilizamos la información que es obtenida a través de cookies para ayudarnos a optimizar su experiencia de compra. A través del uso de cookies podemos, por ejemplo, personalizar en su favor nuestra página de inicio de manera que nuestras pantallas se desplieguen de mejor manera de acuerdo a su tipo de navegador. Las cookies también nos permiten ofrecerle recomendaciones personalizadas respecto de productos, y correos electrónicos.
<br><br>
Las cookies no son software espía, y Legalex GS no recopila datos de múltiples sitios o comparte con terceros la información que obtenemos a través de cookies.
<br><br>
Como la mayoría de los sitios web, nuestros servidores registran su dirección IP, la dirección URL desde la que accedió a nuestro Sitio web, el tipo de navegador, y la fecha y hora de sus compras y otras actividades. Utilizamos esta información para la administración del sistema, solución de problemas técnicos, investigación de fraudes y para nuestras comunicaciones con usted.

                            </p>
                            
                        </div>

                    </div>
                </div>

            </section>

           
 

            
          

           
          

        </main>

<?php
include 'inc/pie.php';
?>
