<?php 
    $pageLink = "Preguntas frecuentes";
    include 'inc/cab.php';
?>
<style>
    .buscador-faqs{
        display: flex;
        justify-content: center;
        margin-bottom: 3rem;
    }
    .inputbuscar{
        display: flex;
        box-shadow: 0px 0px 10px 1px #00000052;
        align-items: center;
        background-color: #fff;
        border-radius: 15px;
        width: 50%;
    }
    .inputbuscar input{
        border:none;
        padding: 1.3rem 0.5rem;
        border-radius: 15px;
        width:100%;
    }
    .inputbuscar input:focus{
        outline:none;
    }
    .inputbuscar .iconobuscar{
        margin: 0 0.2rem 0 1rem;
    }
    .lateral_faq{
        height:100vh;
        box-shadow: 4px 0 4px -2px #00000054;
        padding: 0 15px;
        position:fixed;
        left:0;
        width: 17.7%;
        background-color:#fff;
        font-size: .9375rem;
    }
    .item_nav_faq{
        display:flex;
        padding: 1rem 1.2rem;
        border-radius:15px;
        margin-bottom:10px;
    }
    .item_nav_faq.active{
        background-color:#051c33;
        color:#fff;
    }
    .item_nav_faq.active:hover{
        background-color: #051c33;
        cursor: auto;
    }
    .item_nav_faq .icono_nav{
        padding: 0 1.2rem 0 0rem;
    }
    .item_nav_faq:hover{
        background-color:#9ac5f0;
        cursor:pointer;
    }
    .select_mob_faqs{
        display:none;
    }
    .item_nav_faq.active a div span{
        color:#fff;
    }
    @media (max-width:767px){
        .lateral_faq{
            display:none;
        }
        .select_mob_faqs{
            display:flex;
            justify-content:center;
        }
        .inputbuscar{
            width:90%;
        }
        .faqs .container-fluid{
            padding-right: 0px; 
            padding-left: 0px;
        }
        .faqs .mob-faq{
            padding-right: 15px;
            padding-left: 15px;
        }
    }
</style>
    <main style="background-color:#f5f5f5">
        <section class="faqs">
            <div class="container-fluid">
                <div class="row">
                    
                    <div class="col-md-2">
                        <div class="lateral_faq ">
                            <div class="nav_faq  pt-5">
                                <div class="titulo_nav_faq">
                                    <h2 class="pt-3 h4">Secciones</h2>
                                </div>
                                <div class="item_nav_faq active">
                                    <a href="preguntasfrecuentes.php">
                                    <div>
                                        <span>Covenant Documentos</span>
                                    </div>
                                    </a>
                                </div>
                                <div class="item_nav_faq ">
                                    <a href="preguntasfrecuentesapp.php">
                                    <div>
                                        <span>Covenant App</span>
                                    </div>
                                    </a>
                                </div>
                                <div class="item_nav_faq ">
                                    <a href="faqsSello.php">
                                    <div>
                                        <span>Sello Digital de Tiempo</span>
                                    </div>
                                    </a>
                                </div>

                                <div class="item_nav_faq">
                                    <a href="faqsnom.php">
                                    <div>
                                        <span>Constancia NOM-151</span>
                                    </div>
                                    </a>
                                </div>
                                
                                <div class="item_nav_faq">
                                    <a href="faqsPlanes.php">
                                    <div>
                                        <span>Planes de Covenant</span>
                                    </div>
                                    </a>
                                </div>
                                <div class="item_nav_faq ">
                                    <a href="faqsterminos.php">
                                    <div>
                                        <span>Definciones</span>
                                    </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-10 mt-5 contenedor-mob">
                    
                        <div class="ts-title text-center mt-4" >
                            <h2>Preguntas Frecuentes - Covenant Documentos</h2>
                        </div> 
                        <div class="select_mob_faqs mb-5 form-group container">
                            <select name="" id="" class="form-control" >
                                <option value="Covenant">Covenant</option>
                                <option value="Sello Digital de Tiempo">Sello Digital de Tiempo</option>
                                <option value="Constancia NOM-151">Constancia NOM-151</option>
                                <option value="Firma Electrónica">Firma Electrónica</option>
                                <option value="Planes de Covenant">Planes de Covenant</option>
                            </select>
                        </div>
                        <div class="buscador-faqs">
                            <div class="inputbuscar">
                                <div class="iconobuscar">
                                    <i class="fa fa-search"></i>
                                </div>
                                <input type="input" placeholder="Escriba algo a buscar..." id="buscador">
                            </div>
                        </div> 
                        <div id="accordion" class="row mob-faq justify-content-center mb-5">
                            <div id="" class="accordion col-md-11 ">
                                <div class="card">
                                    <div class="card-header faq" id="headingOne">
                                        <h5 class="mb-0">
                                        <button class="btn btn-link  pregunta_search" data-toggle="collapse" data-target="#collapseO" aria-expanded="true" aria-controls="collapseO">
                                       1.- Cómo firmar un documento en COVENANT
                                        </button>
                                        </h5>
                                        <span  data-toggle="collapse" data-target="#collapseO" aria-expanded="true" aria-controls="collapseO"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                    </div>
                                    
                                    <div id="collapseO" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                        <div class="card-body">
                                            <h4>Aprende a firmar un documento con tu e.firma paso a paso</h4>
                                            <iframe width="90%" height="350" src="https://www.youtube.com/embed/xJACkwvfApU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                             <br><br> Estas instrucciones son para firmar documentos ordinarios desde COVENANT en una computadora. 
                                             <P>Si necesitas firmar en tu smartphone, te recomendamos hacerlo con la app de <a href="preguntasfrecuentesapp.php">COVENANT</a> .</P>      
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header faq" id="headingOne">
                                        <h5 class="mb-0">
                                        <button class="btn btn-link  pregunta_search" data-toggle="collapse" data-target="#collapse2" aria-expanded="true" aria-controls="collapse2">
                                       2.- Diferencias entre la firma electrónica, la firma digital y la firma digitalizada
                                        </button>
                                        </h5>
                                        <span  data-toggle="collapse" data-target="#collapse2" aria-expanded="true" aria-controls="collapse2"><i class="  more-less fas fa-plus-circle"></i></span>
                                    </div>
                                    
                                    <div id="collapse2" class="collapse " aria-labelledby="headingOne" data-parent="#accordion">
                                        <div class="card-body">
                                       <h4>¿Qué es la firma electrónica?</h4> 
La firma electrónica son unos datos electrónicos que acompañan a una determinada información (también en formato electrónico). Al mismo tiempo, la firma electrónica es un concepto legal que equivale a la firma manuscrita y que tiene el objetivo de dar fe de la voluntad del firmante.
<br><br>
El Reglamento (UE) Nº 910/2014, conocido como eIDAS, es el marco normativo europeo que confiere validez jurídica a las firmas electrónicas. Según este la firma electrónica son los “datos en formato electrónico anejos a otros datos electrónicos o asociados de manera lógica con ellos que utiliza el firmante para firmar.”
<br><br>
La definición de firma electrónica que recoge el Reglamento es la más básica, y es el fundamento común de los tres tipos de firmas electrónicas que existen:
<br><br>
<ul>
    <li>La firma electrónica o firma electrónica simple</li>
    <li>La firma electrónica avanzada</li>
    <li>La firma electrónica cualificada</li>
</ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header faq" id="headingOne">
                                        <h5 class="mb-0">
                                        <button class="btn btn-link  pregunta_search" data-toggle="collapse" data-target="#collapse3" aria-expanded="true" aria-controls="collapse3">
                                          3.- ¿La e.firma (antes FIEL) es exclusiva para trámites fiscales y gubernamentales?
                                        </button>
                                        </h5>
                                        <span  data-toggle="collapse" data-target="#collapse3" aria-expanded="true" aria-controls="collapse3"><i class="  more-less fas fa-plus-circle"></i></span>
                                    </div>
                                    
                                    <div id="collapse3" class="collapse " aria-labelledby="headingOne" data-parent="#accordion">
                                        <div class="card-body">
                                        No. Existe una noción errónea respecto a los certificados de firma electrónica avanzada que emite el SAT (la FIEL o e.firma): que solo pueden usarse para trámites ante la autoridad fiscal que los emite y ante otras dependencias de gobierno.
<br><br>
La e.firma la emite el SAT, una Agencia Certificadora autorizada por Banxico. Los certificados privados los emiten diversos Prestadores de Servicios de Certificación acreditados para tal fin por la Secretaría de Economía.
<br><br>
Por ello algunas personas creen que para asuntos privados como firma de contratos únicamente son válidos los certificados que emiten los PSCs privados.
<br><br>
Esta idea es errónea porque la legislación vigente para asuntos privados —el Código de Comercio— no hace diferencia entre uno y otro tipo de certificados. En tanto los certificados cumplan lo estipulado en la ley y no exista una legislación o regulación que acote su uso para un fin específico, pueden usarse indistintamente.
<br><br>
En Mifiel solamente soportamos el uso de certificados de e.firma emitidos por el SAT. Todos los representantes legales cuentan con una.
<br><br>
Soportarlos además trae una ventaja: el SAT emite también certificados para personas morales. De acuerdo con el Artículo 19-A del Código Fiscal de la Federación se presume sin que se admita prueba en contrario, que los documentos firmados con FIEL de persona moral los firmó un apoderado legal con poderes amplios. Esta disposición es aplicable a asuntos privados por el principio de supletoriedad de la ley. Ningún otro PSC o Agencia Certificadora privada ni pública emite tal tipo de certificados.
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header faq" id="headingOne">
                                        <h5 class="mb-0">
                                        <button class="btn btn-link  pregunta_search" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        4.- ‍¿Por qué recibí un correo de COVENANT?
                                        </button>
                                        </h5>
                                        <span  data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><i class="  more-less fas fa-plus-circle"></i></span>
                                    </div>
                                    
                                    <div id="collapseOne" class="collapse " aria-labelledby="headingOne" data-parent="#accordion">
                                        <div class="card-body">
                                        Covenant envía correos de notificación a usuarios registrados por diferentes motivos, algunos del ellos son: activación de cuenta, documento pendiente de firma, documento firmado, recuperación y cambio de contraseña, confirmación de un documento firmado, entre otros.
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header faq" id="headingTwo">
                                        <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        5.-  ¿Qué es Covenant Firma?
                                        </button>
                                        </h5>
                                        <span  data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"><i class=" more-less fas fa-plus-circle"></i></span>
                                    </div>
                                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                        <div class="card-body">
                                        ‍‍COVENANT FIRMA es la solución tecnológica orientada para llevar a cabo de manera electrónica y segura la firma de un documento evitando la presencia física del firmante; hacemos uso de la e.firma (FIEL) que el Sistema de Administración Tributaria (SAT) provee, adicionalmente, contamos con el uso de la firma electrónica simple (firma autógrafa digital), aplicamos normas de seguridad informática para garantizar la integridad de la firma (incluimos un sello digital de tiempo por transacción).
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header faq" id="headingThree">
                                        <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                       6.- ¿Mis clientes tendrán que firmar electrónicamente a través de COVENANT?
                                        </button>
                                        </h5>
                                        <span  data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree"><i class=" more-less fas fa-plus-circle"></i></span>
                                    </div>
                                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                        <div class="card-body">
                                        Sí. Puede hacerlo vía web o utilizando nuestra aplicación móvil (la cual se encuentra disponible para descarga gratuita dentro de Play Store y App Store). <br><br>
                                        El proceso de "Activación de cuenta" es muy sencillo, solo se realiza una  ocasión; recibe mediante un correo electrónico un usuario y contraseña temporal, accede a Covenant Firma, siguiendo las indicaciones del proceso y en cuestión de minutos estarás firmando electrónicamente.

                                       </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header faq" id="p4">
                                        <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r4" aria-expanded="false" aria-controls="r4">
                                        7.- ‍¿Cuál es la seguridad jurídica aportada?
                                        </button>
                                        </h5>
                                        <span  data-toggle="collapse" data-target="#r4" aria-expanded="false" aria-controls="r4"><i class=" more-less fas fa-plus-circle"></i></span>
                                    </div>
                                    <div id="r4" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                        <div class="card-body">
                                                                                    Legalex GS al ser un Prestador de Servicios de Certificación (PSC) acreditado por la Secretaría de Economía Federal funge como un auxiliar del comercio, brindando servicios jurídico-tecnológicos para agilizar y dar certeza en los actos comerciales que se celebran entre las partes, mediante el uso de nuestros servicios. Utilizamos la Firma Electrónica Avanzada que provee el Servicio de Administración Tributaria (SAT) dentro de nuestros procesos de firmado electrónico y así ofrecer la figura del "No Repudio", además de verificar que la persona que intenta firmar es quien dice ser. Adicionalmente, incorporamos el servicio de Sellos Digitales de Tiempo en cada transacción de firma emitida y una Constancia de Conservación de Mensajes de Datos (en caso de ser requerida) a cada documento, logrando garantizar la integridad del mismo por un período de tiempo de 10 años.
                                            <br><br>
                                            "Legalex GS cuando con las acreditaciones y permisos necesarios para cumplir con las reglas y artículos necesarios para la emisión de los mensajes de datos generados y conservados mediante el uso de Covenant y así, poder brindar el mayor nivel de calidad al cumplir con los más altos estándares informáticos y gozar del más alto valor aprobatorio.

                                            Adicionalmente, el Sello Digital de Tiempo proporcionado por Covenant es un mecanismo con reconocimiento legal que ampara la fecha y hora exacta en la que ocurrio una acción, transacción o movimiento dentro de los documentos privados mercantiles.

                                            Legislación de interés: <br><br>
                                            - Reglas generales a las que deberán sujetarse los Prestadores de Servicios de Certificación en su título del primero al cuarto y del sexto al séptimo.<br>
                                            - La Norma Oficial Mexicana NOM-151-SCFI-2016.<br>
                                            - Código de Comercio en sus artículos 89, 89 bis, 95, 95 bis y del 96 al 99.<br>
                                            - Código Fiscal de la Federación en su artículo 17 sub. C al J.<br>
                                            - Ley de firma electrónica avanzada artículos 2 al 13, 15, 17 al 20, 23 al 25 y 27."<br>

                                       </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header faq" id="p5">
                                        <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r5" aria-expanded="false" aria-controls="r5">
                                        8.- ¿Qué necesidades resuelve Covenant Firma?
                                        </button>
                                        </h5>
                                        <span  data-toggle="collapse" data-target="#r5" aria-expanded="false" aria-controls="r4"><i class=" more-less fas fa-plus-circle"></i></span>
                                    </div>
                                    <div id="r5" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                        <div class="card-body">
                                            <ul>
                                                <li>Evita la presencia física del firmante, firmando desde un equipo de cómputo o desde un dispositivo móvil a través de nuestra aplicación de firma móvil, Covenant Firma.</li>
                                                <li>Seguridad tecnológica sin intervención de terceros.</li>
                                                <li>Garantiza la integridad del documento.</li>
                                                <li>Certeza de que el firmante involucrado es quien dice ser.</li>
                                                <li>Respaldo jurídico y tecnológico.</li>
                                                <li>Optimización de tiempos en la gestión y recabación de firmas.</li>
                                                <li>Ahorro en insumos administrativos.</li>
                                            </ul>
                                       </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header faq" id="p6">
                                        <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r6" aria-expanded="false" aria-controls="r6">
                                        9.- ¿Cómo puedo ser usuario de Covenant Firma?
                                        </button>
                                        </h5>
                                        <span  data-toggle="collapse" data-target="#r6" aria-expanded="false" aria-controls="r6"><i class=" more-less fas fa-plus-circle"></i></span>
                                    </div>
                                    <div id="r6" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                        <div class="card-body">
                                             1. Cuando se solicita la firma de un documento, de forma automática se crea el usuario para las personas firmantes involucradas (esto se hace a través del llenado de información cuando se va a enviar el documento a firma desde Covenant Documentos). El usuario y contraseña (de un único uso) se hace llegar a través de un correo electrónico proveniente de las cuentas oficiales de Legalex GS: contacto.psc@legalexgs.com o soporte.covenant@legalexgs.com
                                            <br><br>
                                            2. Al contratar los servicios de Covenant con Legalex GS, el Cliente contratante de nuestros servicios decide quienes son los involucrados que dará de alta para poder firmar dentro de Covenant Firma.

                                       </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header faq" id="p7">
                                        <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r7" aria-expanded="false" aria-controls="r7">
                                            10.- ¿Qué hacer si no recibo mi correo de activación o confirmación de cuenta?
                                        </button>
                                        </h5>
                                        <span  data-toggle="collapse" data-target="#r7" aria-expanded="false" aria-controls="r7"><i class=" more-less fas fa-plus-circle"></i></span>
                                    </div>
                                    <div id="r7" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                        <div class="card-body">
                                        "Reenvie manualmente su correo de activación a otra cuenta de correo electrónico: <br><br>
1) Ingrese a <a href=" https://www.legalexgs.com/covenant_firma/"> https://www.legalexgs.com/covenant_firma/</a> y de clic en la opción ""Reenviar correo de activación"". <br>
2) Aparecerá una ventana y deberá ingresar el usuario o RFC y correo electrónico con el cual lo dieron de alta (estos datos son los que usted brindo a la empresa que le envía los documentos a firma), de igual manera, deberá ingresar el correo electrónico al que quiere que le sea reenviado el correo de activación. <br>
3) Por ultimo deberá resolver el reCAPTCHA y dar clic al botón ""Enviar"".
<br><br>
En caso de que la información proporcionada sea correcta, se enviara de forma automática el correo de activación de cuenta a la dirección de correo electrónico proporcionada como ""alternativa""."





                                            <br><br>
                                            "Recomendaciones: <br><br>
1) Revisar la bandeja de SPAM o correo no deseado. <br>
2) Verificar que el correo electrónico que se proporcionó esé correctamente escrito directamente con el emisor del documento.<br>
3) Verificar que su correo no tenga bloqueado el dominio de Legalex GS para la recepción de correos.<br>
4) Verificar que la bandeja de entrada del correo no este llena o sin espacio."

<br><br>

En caso de que ninguna de las opciones anteriores resuelva el problema, contactar a Soporte Técnico a través del Chat que se encuentra en la siguiente dirección: <a href="https://www.legalexgs.com/covenant_firma/">https://www.legalexgs.com/covenant_firma/</a> 


                                       </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header faq" id="p8">
                                        <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r8" aria-expanded="false" aria-controls="r8">
                                            11.- ¿Cómo ingresar a Covenant Firma por primera vez?
                                        </button>
                                        </h5>
                                        <span  data-toggle="collapse" data-target="#r8" aria-expanded="false" aria-controls="r8"><i class=" more-less fas fa-plus-circle"></i></span>
                                    </div>
                                    <div id="r8" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                        <div class="card-body">
                                        "Primero se debe de activar la cuenta siguiendo los pasos indicados en el correo proveniente de alguna de las cuentas oficiales de Legalex GS: contacto.psc@legalexgs.com o soporte.covenant@legalexgs.com <br><br>
1) Dentro del correo electrónico vendrá el usuario (RFC), el cual será utilizado para ingresar a Covenant Firma, adicional contendra una contraseña de un único uso, la cual se utilizá para acceder por única ocasión en el proceso de confirmación de cuenta. <br>
2) Dentro del mismo correo contiene un botón que te llevara a Covenant Firma (<a href="https://www.legalexgs.com/covenant_firma/">https://www.legalexgs.com/covenant_firma/</a> ). <br>
3) Dentro de la página de inicio de Covenant Firma, deberás copiar y pegar el usuario y la contraseña que te fue proporcionada en el correo electrónico y dar clic en el botón ""Entrar"". <br>
4) Si los datos son correctos, al ingresar Covenant Firma te pedirá que cambies la contraseña por una diferente a la que llegó en el correo electrónico."



                                       </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header faq" id="p9">
                                        <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r9" aria-expanded="false" aria-controls="r9">
                                            12.- ¿Cómo obtener mis archivos de e.firma o FIEL para poder realizar la Firma Electrónica dentro de Covenant Firma? 
                                        </button>
                                        </h5>
                                        <span  data-toggle="collapse" data-target="#r9" aria-expanded="false" aria-controls="r9"><i class=" more-less fas fa-plus-circle"></i></span>
                                    </div>
                                    <div id="r9" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                        <div class="card-body">
                                        La solicitud de e.firma o FIEL se realiza ante el Sistema de Administración Tributaria (SAT), este proceso es propio del SAT y Legalex GS no interviene en el, ni en las contraseñas de firma que se generan durante este proceso. Para más información sobre la emisión de la Firma Electrónica Avanzada visita el portal oficial del SAT o da clic en el siguiente enlace: https://www.sat.gob.mx/tramites/16703/obten-tu-certificado-de-e.firma-(antes-firma-electronica) 




                                       </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header faq" id="p10">
                                        <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r10" aria-expanded="false" aria-controls="r10">
                                            13.- ¿Cuáles son los elementos que necesito para realizar una firma electrónica?

                                        </button>
                                        </h5>
                                        <span  data-toggle="collapse" data-target="#r10" aria-expanded="false" aria-controls="r10"><i class=" more-less fas fa-plus-circle"></i></span>
                                    </div>
                                    <div id="r10" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                        <div class="card-body">
                                        "Firma Autógrafa Digital (Trazo): <br><br>
1) Únicamente es necesario acceder al documento que se requiere firmar, dar clic en el botón ""Firmar"" y Covenant Firma le mostrará el espacio para realizar la firma (web o en un dispositivo móvil).
<br><br>
Firma Electrónica Avanzada (e.firma o Fiel que provee el SAT): <br>
1) Deberá tener a la mano sus archivos de firma, los cuales tienen las siguientes extensiones *.cer, *.key y su clave o contraseña privada (esta es la contraseña que designo para su certificado ante el SAT). <br><br>
2) Contar con la e.firma o FIEL vigente y actualizada.
<br><br>
*Nota: para realizar la firma en su equipo de cómputo, deberá cargar estos archivos cada vez que realice una firma. En caso de tener instalada la aplicación móvil de firma, los archivos se cargan una única vez, por lo que deberá considerar tenerlos compartidos en una ""nube"" o que los tenga guardados dentro de su dispositivo móvil."

                                       </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header faq" id="p11">
                                        <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r11" aria-expanded="false" aria-controls="r11">
                                            14.- ¿Cómo cancelo o declino mi firma en un documento?

                                        </button>
                                        </h5>
                                        <span  data-toggle="collapse" data-target="#r11" aria-expanded="false" aria-controls="r11"><i class=" more-less fas fa-plus-circle"></i></span>
                                    </div>
                                    <div id="r11" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                        <div class="card-body">
                                        Al ingresar a Covenant Firma, nos dirigimos a la sección Documentos Pendientes y después de seleccionar el o los archivos que se quieren cancelar, se deberá presionar el botón de "Firma" o "Firmar documento(s)" para que la plataforma nos carge la visualización de los documentos que se seleccionaron. Después deberemos presionar el botón de color rojo "Cancelar Documento" en la parte superior e ingresar el motivo de cancelación, al terminar deberá de dar clic el botón "Continuar".

                                       </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header faq" id="p12">
                                        <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r12" aria-expanded="false" aria-controls="r12">
                                            15.- ¿Cuáles son los medios para realizar mi firma electrónica?

                                        </button>
                                        </h5>
                                        <span  data-toggle="collapse" data-target="#r12" aria-expanded="false" aria-controls="r12"><i class=" more-less fas fa-plus-circle"></i></span>
                                    </div>
                                    <div id="r12" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                        <div class="card-body">
                                        "La firma electrónica en Covenant se puede realizar por dos medios:<br><br>
1. Vía web a través del siguiente enlace: <a href="https://www.legalexgs.com/covenant_firma/">https://www.legalexgs.com/covenant_firma/</a> <br>
2. A través de la aplicación móvil Covenant Firma, disponible para iOS y Android."

                                       </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header faq" id="p13">
                                        <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r13" aria-expanded="false" aria-controls="r13">
                                            16.- ¿Cómo puedo recuperar mi contraseña de acceso?


                                        </button>
                                        </h5>
                                        <span  data-toggle="collapse" data-target="#r13" aria-expanded="false" aria-controls="r13r7"><i class=" more-less fas fa-plus-circle"></i></span>
                                    </div>
                                    <div id="r13" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                        <div class="card-body">
                                        Si no recuerda su contraseña para acceder, puede recuperarla ingresando a <a href="https://www.legalexgs.com/covenant_firma/">https://www.legalexgs.com/covenant_firma/</a>  y deberá de seleccionar la opción "¿Olvidaste tu contraseña?", aparecerá una ventana donde ingresará el correo electrónico con el que fue registrado, posteriormente deberá responder el reCAPTCHA (No soy un robot) y por ultimo dar clic sobre el botón "Confirmar", si los datos fueron correctos, recibirá un correo electrónico con los datos y pasos para cambiar su contraseña.


                                       </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header faq" id="p14">
                                        <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r14" aria-expanded="false" aria-controls="r14">
                                            17.- ¿Cómo desbloqueo mi cuenta? ¿Por qué pasa esto?

                                        </button>
                                        </h5>
                                        <span  data-toggle="collapse" data-target="#r14" aria-expanded="false" aria-controls="r14"><i class=" more-less fas fa-plus-circle"></i></span>
                                    </div>
                                    <div id="r14" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                        <div class="card-body">
                                        Las cuentas se bloquean por tener más de 5 intentos fallidos en la contraseña al intentar ingresar, si esto llega a suceder deberás cambiar tu contraseña y una vez que se cambie la cuenta se desbloqueará autómaticamente.

                                       </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header faq" id="p15">
                                        <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r15" aria-expanded="false" aria-controls="r15">
                                            18.- ¿Cómo puedo cambiar mi contraseña?

                                        </button>
                                        </h5>
                                        <span  data-toggle="collapse" data-target="#r15" aria-expanded="false" aria-controls="r15"><i class=" more-less fas fa-plus-circle"></i></span>
                                    </div>
                                    <div id="r15" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                        <div class="card-body">
                                        Para cambiar tu contraseña deberás ingresar a <a href="https://www.legalexgs.com/covenant_firma/">https://www.legalexgs.com/covenant_firma/</a>  y dar clic en la opción "¿Olvidaste tu contraseña?", aparecerá una ventana donde ingresará el correo electrónico con el que fue registrado, posteriormente deberá responder el reCAPTCHA (No soy un robot) y por ultimo dar clic sobre el botón "Confirmar", si los datos fueron correctos, recibirá un correo electrónico con los datos y pasos para cambiar su contraseña.

                                       </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header faq" id="p16">
                                        <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r16" aria-expanded="false" aria-controls="r16">
                                            19.- ¿Puedo cambiar mi correo electrónico? ¿Cómo puedo hacerlo?

                                        </button>
                                        </h5>
                                        <span  data-toggle="collapse" data-target="#r16" aria-expanded="false" aria-controls="r16"><i class=" more-less fas fa-plus-circle"></i></span>
                                    </div>
                                    <div id="r16" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                        <div class="card-body">
                                        "Sí, es posible cambiar el correo electrónico después de activar la cuenta, sin embargo, el nuevo correo (correo que se cambia) solo será utilizado para poder recuperar la contraseña. Para realizar el cambio, deberás seguir los siguientes pasos:<br><br>
1) Ingresar con su usuario y contraseña a <a href="https://www.legalexgs.com/covenant_firma/">https://www.legalexgs.com/covenant_firma/</a><br> 
2) Una vez dentro, deberá dar clic en el icono en forma de engranes que se encuentra en la parte superior derecha y seleccionar la opción ""Mi perfil"".<br>
3) Al terminar de cargar podrá visualizar que se encuentra en la pestaña ""Información General"" (seleccionada por defecto), verá listada su información como usuario, nombre y correo, deberá dar clic en el botón azúl ""Editar"".<br>
4) Al dar clic aparecerá una ventana donde podrá cambiar el correo actual por el nuevo y finalmente dar clic sobre el botón ""Guardar""."

                                       </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header faq" id="p17">
                                        <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r17" aria-expanded="false" aria-controls="r17">
                                            20.- ¿Qué navegadores puedo utilizar para visualizar correctamente Covenant Firma?


                                        </button>
                                        </h5>
                                        <span  data-toggle="collapse" data-target="#r17" aria-expanded="false" aria-controls="r17"><i class=" more-less fas fa-plus-circle"></i></span>
                                    </div>
                                    <div id="r17" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                        <div class="card-body">
                                        "Nuestras plataformas mantienen una compatibilidad con los siguientes exploradores o navegadores web:<br><br>
1) Windows: Chrome, Firefox, Opera, Microsoft Edge (ultima actualización).<br>
2) MacOS: Chrome, Firefox, Opera y Safari.<br>
3) Linux: Chrome, Firefox, Opera.<br><br>
Así mismo, se excluye por completo el uso de nuestras plataformas mediante el navegador web de Internet Explorer."<br>


                                       </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header faq" id="p18">
                                        <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r18" aria-expanded="false" aria-controls="r18">
                                            21.- ¿Puedo ordenar mis documentos firmados en mi cuenta?
                                        </button>
                                        </h5>
                                        <span  data-toggle="collapse" data-target="#r18" aria-expanded="false" aria-controls="r18"><i class=" more-less fas fa-plus-circle"></i></span>
                                    </div>
                                    <div id="r18" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                        <div class="card-body">
                                        "Sí, dentro de Covenant Firma (web) usted podrá crear carpetas para acomodar sus documentos firmados. Para hacerlo deberá seguir los siguientes pasos:<br><br>
1) Después de ingresar y encontrarse en su listado de documentos firmados, podrá visualizar el botón ""Crear Carpeta"" en la parte media superior derecha. Al dar clic podrá escribir el nombre de la carpeta nueva que quiera crear. Al terminar deberá dar clic en el botón ""Crear"".<br>
2) Para agregar archivos a sus carpetas, deberá seleccionar los archivos con el ""check"" que aparece del lado izquierdo (dentro del listado de sus documentos) y posteriormente dar clic al botón que se encuentra ubicado en la parte media superior ""Mover a otra carpeta"", después deberá seleccionar la carpeta donde desea que se muevan los archivos seleccionados y por ultimo dar clic en el botón ""Mover"".<br>
3) En caso de querer eliminar o renombrar la carpeta (el eliminar la carpeta no elimina los archivos contenidos en ella, simplemente los regresa al listado principal), deberá buscarla dentro del listado y dar clic en la opción ""Eliminar"" o ""Renombrar"" segun se requiera, estas opciones se encuentran al final de su listado en la parte derecha."<br>

                                       </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header faq" id="p19">
                                        <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r19" aria-expanded="false" aria-controls="r19">
                                            22.- ¿Cómo descargo mi representación gráfica (archivo PDF) y el valor de la firma (archivo XML)?

                                        </button>
                                        </h5>
                                        <span  data-toggle="collapse" data-target="#r19" aria-expanded="false" aria-controls="r19"><i class=" more-less fas fa-plus-circle"></i></span>
                                    </div>
                                    <div id="r19" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                        <div class="card-body">
                                        "Para descargar la representación gráfica (XML y PDF) de un documento firmado es necesario dirigirse a la sección de documentos Firmados, donde por cada registro (documento) encontrará un botón llamado ""XML"" y otro llamado ""PDF"" en el que podrá dar clic para descargar el archivo deseado.
                                        <br><br>
Para llegar a este menú, deberá ingresar a <a href="https://www.legalexgs.com/covenant_firma/">https://www.legalexgs.com/covenant_firma/</a> con su usuario y contraseña y dentro del panel de bienvenida dar clic a la opción ""Firmados""."

                                       </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header faq" id="p20">
                                        <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r20" aria-expanded="false" aria-controls="r20">
                                            23.- ¿Cómo ingreso a firmar por segunda ocasión? 

                                        </button>
                                        </h5>
                                        <span  data-toggle="collapse" data-target="#r20" aria-expanded="false" aria-controls="r20"><i class=" more-less fas fa-plus-circle"></i></span>
                                    </div>
                                    <div id="r20" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                        <div class="card-body">
                                        Para ingresar a Covenant Firma deberá ingresar a <a href="https://www.legalexgs.com/covenant_firma/">https://www.legalexgs.com/covenant_firma/</a>  con su usuario (RFC) y la contraseña establecida en su primer ingreso.

                                       </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header faq" id="p21">
                                        <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r21" aria-expanded="false" aria-controls="r21">
                                            24.- ¿Se pueden seleccionar varios documentos de Firma Electrónica Avanzada y Firma de Trazo para firmar al mismo tiempo?

                                        </button>
                                        </h5>
                                        <span  data-toggle="collapse" data-target="#r21" aria-expanded="false" aria-controls="r21"><i class=" more-less fas fa-plus-circle"></i></span>
                                    </div>
                                    <div id="r21" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                        <div class="card-body">
                                        No, para poder firmar más de un documento al mismo tiempo debén ser del mismo tipo de firma, es decir, todos los documentos seleccionados para ser firmados con Firma Electrónica Avanzada o con Firma Autógrafa Digital (trazo).

                                       </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header faq" id="p22">
                                        <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r22" aria-expanded="false" aria-controls="r22">
                                            25.- ¿Qué es la figura del no repudio?

                                        </button>
                                        </h5>
                                        <span  data-toggle="collapse" data-target="#r22" aria-expanded="false" aria-controls="r22"><i class=" more-less fas fa-plus-circle"></i></span>
                                    </div>
                                    <div id="r22" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                        <div class="card-body">
                                        Consiste en que la firma electrónica avanzada contenida en el o los documentos electrónicos garantiza la autoría e integridad del documento y que dicha firma corresponde exclusivamente al firmante. Es decir, el no repudio evita que el emisor o el receptor niegue la firma y recepción del documento firmado. Así, cuando se envía un documento a firma, el receptor puede comprobar que el emisor envió el documento. Por otra parte, cuando se recibe un documento para ser firmado, el emisor puede verificar que el receptor recibió el documento.

                                       </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header faq" id="p23">
                                        <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r23" aria-expanded="false" aria-controls="r23">
                                            26.- ¿Qué es la firma autógrafa?

                                        </button>
                                        </h5>
                                        <span  data-toggle="collapse" data-target="#r23" aria-expanded="false" aria-controls="r23"><i class=" more-less fas fa-plus-circle"></i></span>
                                    </div>
                                    <div id="r23" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                        <div class="card-body">
                                        La Firma Autógrafa Digital (FAD), es conocida como una firma electrónica simple, con la cual se puede dar un sustento legal mínimo necesario pero se considera repudiable. Está se realiza dibujando el trazo de la firma autógrafa que comunmente se realiza en papel, es decir, se realiza la misma firma autógrafa pero en lugar de utilizar bolígrafo y papel se realiza de forma digital.

                                       </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header faq" id="p24">
                                        <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r24" aria-expanded="false" aria-controls="rr247">
                                            27.- ¿Qué es la firma electrónica avanzada?

                                        </button>
                                        </h5>
                                        <span  data-toggle="collapse" data-target="#r24" aria-expanded="false" aria-controls="r24"><i class=" more-less fas fa-plus-circle"></i></span>
                                    </div>
                                    <div id="r24" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                        <div class="card-body">
                                        Es el conjunto de datos y caracteres que permite la identificación del firmante, está ha sido creada por medios electrónicos bajo el exclusivo control del firmante, de manera que está vinculada únicamente al mismo y a los datos a los que se refiere, lo que permite que sea detectable cualquier modificación ulterior de éstos, y a su vez, esta firma produce los mismos efectos jurídicos que la firma autógrafa.

                                       </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header faq" id="p25">
                                        <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r25" aria-expanded="false" aria-controls="r25">
                                            28.- ¿Qué hacer si no puedo ingresar?

                                        </button>
                                        </h5>
                                        <span  data-toggle="collapse" data-target="#r25" aria-expanded="false" aria-controls="r25"><i class=" more-less fas fa-plus-circle"></i></span>
                                    </div>
                                    <div id="r25" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                        <div class="card-body">
                                        "Deberás verificar que se está ingresando la contraseña correctamente teniendo en cuenta espacios, el uso de mayúsculas y minúsculas. En caso de no recordar la contraseña, puede seleccionar el botón Recuperar Contraseña, ingresar su correo electrónico con el que fue registrada su cuenta y dar clic sobre el botón ""Recuperar"", después recibirá un correo electrónico con los pasos a seguir para finalizar este proceso.
                                        <br><br>
Nota: Si no recibe el correo electrónico, deberá revisar su bandeja de SPAM o correo no deseado.
<br><br>
Si no recuerda la cuenta de correo con la que fue registrada su cuenta o no tiene acceso a la misma, deberá contactar a Soporte Técnico a través del Chat que se encuentra integrado al ingresar a Covenant Firma: https://www.legalexgs.com/covenant_firma/"

                                       </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header faq" id="p26">
                                        <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r26" aria-expanded="false" aria-controls="r26">
                                            29.- ¿No reconoce la contraseña de mi certificado? 

                                        </button>
                                        </h5>
                                        <span  data-toggle="collapse" data-target="#r26" aria-expanded="false" aria-controls="r26"><i class=" more-less fas fa-plus-circle"></i></span>
                                    </div>
                                    <div id="r26" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                        <div class="card-body">
                                        En caso de que al intentar firmar un documento con la Firma Electrónica Avanza (e.firma o FIEL) que provee el Servicio de Administración Tributaria (SAT) le regrese el aviso "La contraseña de la clave privada no es correcta" deberá verificar que la clave privada que esta ingresando corresponde a los archivos de firma (archivos con extensión *.cer y *.key). Esta contraseña se designa junto con la emisión o renovación de la e.firma o FIEL que emite el SAT, ya sea desde su portal web (para renovaciones) o en sus instalaciones (emisión por primera vez), una forma de comprobar su contraseña es intentando ingresar al portal del SAT ( https://cfdiau.sat.gob.mx/nidp/wsfed/ep?id=SATUPCFDiCon&sid=0&option=credential&sid=0 ) mediante la opción de e.firma y no mediante el acceso por contraseña (usuario (RFC) y contraseña).

                                       </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header faq" id="p27">
                                        <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r27" aria-expanded="false" aria-controls="r27">
                                            30.- ¿Puedo firmar sin subir certificados? 

                                        </button>
                                        </h5>
                                        <span  data-toggle="collapse" data-target="#r27" aria-expanded="false" aria-controls="r27"><i class=" more-less fas fa-plus-circle"></i></span>
                                    </div>
                                    <div id="r27" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                        <div class="card-body">
                                        "Para realizar la firma desde Covenant Firma en un ambiente web, no. Cada que se realice una transacción de firma deberá cargar sus archivos de la e.firma o FIEL que emite el SAT. En el caso de Legalex GS y sus productos como Covenant, los archivos que se utilizan para la firma electrónica no son guardados o resguardados.
<br><br>
Para las firmas realizadas desde la aplicación móvil de Covenant Firma (disponible para dispositivos con iOS y Android), la aplicación esta diseñada para que el usuario le indique en donde se encuentran los archivos de firma (e.firma o FIEL) y que no sea necesario cargar todos los archivos en cada firma, sin embargo, esto no quiere decir que se resguarden en la aplicación, lo unico que se guarda es la ubicación donde se consultará la información."

                                       </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header faq" id="p28">
                                        <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r28" aria-expanded="false" aria-controls="r28">
                                            31.- ¿Mis datos de firma son guardados dentro de Covenant o por Legalex GS?

                                        </button>
                                        </h5>
                                        <span  data-toggle="collapse" data-target="#r28" aria-expanded="false" aria-controls="r28"><i class=" more-less fas fa-plus-circle"></i></span>
                                    </div>
                                    <div id="r28" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                        <div class="card-body">
                                        No, los archivos que se utilizan para la firma electrónica no son guardados por Legalex GS ni a través de sus productos. La firma en el ambiente web, es realizada bajo un proceso sobre la caché del equipo donde se encuentra la persona firmando, una vez que el proceso de firma termina, esta información también es eliminada para evitar el robo de información.

                                       </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header faq" id="p29">
                                        <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r29" aria-expanded="false" aria-controls="r29">
                                            32.- ¿El Servicio de Administración Tributaria (SAT) sabe qué estoy firmando?

                                        </button>
                                        </h5>
                                        <span  data-toggle="collapse" data-target="#r29" aria-expanded="false" aria-controls="r29"><i class=" more-less fas fa-plus-circle"></i></span>
                                    </div>
                                    <div id="r29" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                        <div class="card-body">
                                        No, el Servicio de Administración Tributaria (SAT) no tiene control ni conocimiento sobre la documentación digital que se esta firmando. Los servicios proveídos por Legalex GS para la firma electrónica solo realizán la consulta en tiempo real ante el SAT para verificar que los archivos de firma con los cuales se intenta realizar el proceso de firma son veridicos, actuales y no se encuentran revocados.

                                       </div>
                                    </div>
                                </div>
                               
                                

                            </div>
                        </div>
                        <?php
    include 'inc/pie.php';
?>
                    </div>
                </div>
            </div>
        </section>
    </main>

<script>
$(document).ready(function(){
    $('#buscador').keyup(function(){
     var nombres = $('.pregunta_search');
     var buscando = $(this).val();
     var item='';
     for( var i = 0; i < nombres.length; i++ ){
         item = $(nombres[i]).html().toLowerCase();
          for(var x = 0; x < item.length; x++ ){
              if( buscando.length == 0 || item.indexOf( buscando ) > -1 ){
                  $(nombres[i]).parents('.card').show(); 
              }else{
                   $(nombres[i]).parents('.card').hide();
              }
          }
     }
  });
});
</script>