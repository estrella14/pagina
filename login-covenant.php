<?php 
$pageLink = "Iniciar Sesión Covenant";
    include 'inc/cab.php';
?>
    <section class="banner-recursos container">
        <div class="header-recursos text-center">
            <h2 >Iniciar Sesión Covenant</h2>
        </div>
        <hr>
       <div class="section-login">
                <div class="text-center mb-5">
                    <img src="assets/img/logo_covenant_firma.png" style="width:150px" alt="">
                </div>
                <div class="row justify-content-center">
                <div class="col-md-6 col-sm-6">
                    <div class="form-group">
                        <label for="form-contact-name">Ingrese su RFC *</label>
                        <input type="text" class="form-control" id="form-contact-name" name="name" placeholder="RFC completo" required>
                    </div>
                    <div class="form-group">
                        <label for="form-contact-name">Correo *</label>
                        <input type="password" class="form-control" id="form-contact-name" name="name" placeholder="Contraseña" required>
                    </div>
                    <div class="form-group flex-between" style="display:flex;justify-content: space-between;">
                        <a href="#">¿Olvidaste tu contraseña?</a> <br>
                        <a href="#">Reenviar correo de activación</a>
                    </div>
                    <div class="form-group text-center ">
                        <button type="submit" class="btn btn-naranja btn-sm">Entrar</button>
                        <div id="btn-gracias-cont" data-toggle="modal" data-target="#GraciasModal"></div>
                    </div>
                   

                </div>
                
            </div>
       </div>
    </section>
<?php
    include 'inc/pie.php';
?>