<?php

$pageLink = "Válidar SDT";

include 'inc/cab.php';

?>

   
    <div class="ts-content" style="background-color:#f7f8f9">
      

       
        <main id="ts-content" >
            <section class="pt-5 mt-4 mb-5  container">
                <div class="mb-5 text-center">
                <h2>Constancia de Conservación de Mensajes de Datos NOM-151</h2>
                <p>Sube el archivo original y los archivos .ltsq y .ltsr. Nos encargaremos de verificar su validez.</p>
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-7 carta-req pb-5 pt-3" >
                        <div class="cuerpo-titulo">
                            <h3 class="mb-3">CONSTACIA NOM-151</h3>
                            <p>Ahora sube tus archivos para poder validar la información.</p>
                        </div>
                        <div class="carta-cuerpo">
                            <div class="text-inp">
                                <p class="mb-0 bold">Subir archivo .LTSQ</p>
                                <small id="resFile1">archivo.pdf</small>
                            </div>
                            <div class="inp-inp">
                                <label class="btn-submit-file" for="1" id="ltsq"><i class="fas fa-upload"></i> <span> Subir archivo </span>.TSQ </label>
                                <input type="file" id="1">
                            </div>
                        </div>
                    
                        <div class="carta-cuerpo">
                            <div class="text-inp">
                                <p class="mb-0 bold">Subir archivo .LTSR</p>
                                <small  id="resFile2">archivo.pdf</small>
                            </div>
                            <div class="inp-inp">
                                <label class="btn-submit-file" for="2" id="ltsr"><i class="fas fa-upload"></i> <span> Subir archivo </span> .TSR</label>
                                <input type="file" id="2">
                            </div>
                            </div>
                        <div>
                        <div class="carta-cuerpo">
                            <div  class="text-inp">
                                <p class="mb-0 bold">Subir el documento</p>
                                <small  id="resFile3">archivo.pdf</small>
                            </div>
                            <div class="inp-inp">
                                <label class="btn-submit-file" for="3" id="ldoc-file"><i class="fas fa-upload"></i> <span> Seleccionar</span> </label>
                                <input type="file" id="3">
                            </div>
                        </div>
                        <div class="text-center">
                        <button onclick="validar()" class="btn btn-outline-naranja mt-4">Validar </button>
                        </div>
                        <div class="cargando hide" >
                            <img src="assets/img/logolg.png" alt="">
                            <small>Cargando...</small>
                        </div>
                    </div>      
                </div>
               
                    </div>
                    <hr class="mt-5 mb-5">
                <div id="resultados" class="hide">
                    <h2>Estos son los resultados de la validación</h2><br>
                    <div class="tabla-validar-row">
                        <div class="validar-col">
                            <h5>Número de serie:</h5>
                            <p>sdf4-ht4f-h456-456h-45f5</p>
                        </div>
                        <div class="validar-col">
                            <h5>Exactitud:</h5>
                            <p>sdf4-ht4f-h456-456h-45f5</p>
                        </div>
                        <div class="validar-col">
                            <h5>Tiempo:</h5>
                            <p>sdf4-ht4f-h456-456h-45f5</p>
                        </div>
                    </div>
                    <div class="tabla-validar-row">
                        <div class="validar-col">
                            <h5>Algoritmo:</h5>
                            <p>sdf4-ht4f-h456-456h-45f5</p>
                        </div>
                        <div class="validar-col">
                            <h5>Versión:</h5>
                            <p>sdf4-ht4f-h456-456h-45f5</p>
                        </div>
                        <div class="validar-col">
                            <h5>Policy:</h5>
                            <p>njskfdnlsdjnk nasfndjk fnms njk jk njk</p>
                        </div>
                    </div>
                    <div class="tabla-validar-row">
                        <div class="validar-col">
                            <h5>Hash:</h5>
                            <p>sdf4-ht4f-h456-456h-45f5</p>
                        </div>
                    </div>
                    <div class="info-validacion">
                    <small>Esta puede ser una nota para la validación: Lorem ipsum dolor sit amet consectetur adipisicing elit. Vel iure officia deserunt ipsum provident qui quidem atque.</small>
                </div>
                </div>
                
            </section>
<?php
include 'inc/pie.php';
?>
<script>
    
    function validar(){
        
        $(".cargando").removeClass("hide");
        setTimeout(() => {
            $(".cargando").addClass("hide");
            $("#resultados").removeClass("hide");
            document.getElementById("resultados").scrollIntoView({
    behavior:"smooth"
});
           
        }, 3000);
    }
        $('input[type=file]').change(function(){
        
        var filename = $(this).val().split('\\').pop();
        var idname = $(this).attr('id');
        var lname = "l"+idname;
                //Archivo Válido
            $("#"+lname+"").addClass('file-cargado');
            $("#resFile"+idname+"").html('<i class="fas fa-check"></i> <span>'+filename+' </span>');

            //Archivo invalido
            /*
            $("#"+lname+"").addClass('file-cargado-mal');
            $("#"+lname+"").html('<i class="fas fa-times"></i> <span>Archivo Invalido </span>');
            */
        });
</script>