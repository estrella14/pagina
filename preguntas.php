<?php
$pageLink = "Preguntas frecuentes";
include 'inc/cab.php';
?>
<style>
     .text {
    display: none;
    }
    .buscador-faqs {
        display: flex;
        justify-content: center;
        margin-bottom: 3rem;
    }

    .inputbuscar {
        display: flex;
        box-shadow: 0px 0px 10px 1px #00000052;
        align-items: center;
        background-color: #fff;
        border-radius: 15px;
        width: 30%;
        height: 47px;
    }

    .inputbuscar input {
        border: none;
        padding: 1.3rem 0.5rem;
        border-radius: 15px;
        width: 80%;
        height: 47px;
    }

    .inputbuscar input:focus {
        outline: none;
    }

    .inputbuscar .iconobuscar {
        margin: 0 0.2rem 0 1rem;
    }

    .lateral_faq {
        height: 100vh;
        box-shadow: 4px 0 4px -2px #00000054;
        padding: 0 15px;
        position: fixed;
        left: 0;
        width: 17.7%;
        background-color: #fff;
        font-size: .9375rem;
    }

    .item_nav_faq {
        display: flex;
        padding: 1rem 1.2rem;
        border-radius: 15px;
        margin-bottom: 10px;
    }

    .item_nav_faq.active a div span {
        color: #fff;
    }

    .item_nav_faq.active {
        background-color: #051c33;
        color: #fff;
    }

    .item_nav_faq.active:hover {
        background-color: #051c33;
        cursor: auto;
    }

    .item_nav_faq .icono_nav {
        padding: 0 1.2rem 0 0rem;
    }

    .item_nav_faq:hover {
        background-color: #9ac5f0;
        cursor: pointer;
    }

    .select_mob_faqs {
        display: none;
    }
   

@media (max-width:767px) {
        .lateral_faq {
            display: none;
        }

        .select_mob_faqs {
            display: flex;
            justify-content: center;
        }
        
        
        .text {
            display: flex;
            justify-content: center;
        }
        
        .inputbuscar {
            width: 90%;
        }

        .faqs .container-fluid {
            padding-right: 0px;
            padding-left: 0px;
        }

        .faqs .mob-faq {
            padding-right: 15px;
            padding-left: 15px;
        }
        .item_nav_faq.active{
        background-color:#051c33;
        color:#fff;
    }
    .item_nav_faq.active:hover{
        background-color: #051c33;
        cursor: auto;
    }
    .item_nav_faq .icono_nav{
        padding: 0 1.2rem 0 0rem;
    }
    .item_nav_faq:hover{
        background-color:#9ac5f0;
        cursor:pointer;
    }
   
    .item_nav_faq.active a div span{
        color:#fff;
    }
}



    .accordion1 .card-header {
  
    display: flex;
    justify-content: space-between;
    align-items: center;
    }
    .accordion2 .card-header {
  
  display: flex;
  justify-content: space-between;
  align-items: center;
    }
    .accordion3 .card-header {
    
    display: flex;
    justify-content: space-between;
    align-items: center;
    }
    .accordion4 .card-header {
    
    display: flex;
    justify-content: space-between;
    align-items: center;
    }
    .accordion5 .card-header {
    
    display: flex;
    justify-content: space-between;
    align-items: center;
    }
    .accordion6 .card-header {
    
    display: flex;
    justify-content: space-between;
    align-items: center;
    }
    .accordion7 .card-header {
    
    display: flex;
    justify-content: space-between;
    align-items: center;
}


</style>
<main style="background-color:#f5f5f5">
    <section class="faqs">
        <div class="container-fluid">
            <div class="row">

                <?php
                include 'menupre.php';
                ?>
                <div class="col-md-10 mt-5 contenedor-mob" style="text-align:justify">

                    <div class="ts-title text-center mt-4">
                        <h2>Preguntas Frecuentes</h2>
                    </div>

                    <div class="text" align="center">
                        <p id="textoselect "  >Seleccione alguna sección la cual desee buscar</p>
                    </div>
                        
                  
                   
                    
                    <div class="select_mob_faqs mb-5 form-group container">
                        <select name="" id="whatfaqs" class="form-control" onchange="whatsfaqs()">
                        <option  class="bold" style="color: #c56122;" disabled selected>Seleccione un Módulo</option>
                            <option value="Covenant Documentos">&nbsp;&nbsp;&nbsp;&nbsp;Covenant Documentos</option>
                            <option value="Covenant Firmas">&nbsp;&nbsp;&nbsp;&nbsp;Covenant Firmas</option>
                            <option value="Covenant App">&nbsp;&nbsp;&nbsp;&nbsp;Covenant App</option>
                            <option value="Sello Digital de Tiempo">&nbsp;&nbsp;&nbsp;&nbsp;Sello Digital de Tiempo</option>
                            <option value="Constancia NOM-151">&nbsp;&nbsp;&nbsp;&nbsp;Constancia NOM-151</option>
                            <option value="Planes de Covenant">&nbsp;&nbsp;&nbsp;&nbsp;Planes de Covenant</option>
                            <option value="Definiciones" >&nbsp;&nbsp;&nbsp;&nbsp;Definiciones</option>
                        </select>
                    </div>
                    <!--div class="buscador-faqs">
                        <div class="inputbuscar">
                            <div class="iconobuscar">
                                <i class="fa fa-search"></i>
                            </div>
                            <input type="input" placeholder="Escriba algo a buscar..." id="buscador">
                        </div>
                    </div-->
                    
                    
                    <?php
                        include 'buscador/buscador.html';
                    ?>
                    
<!---------------------------------------------------------------------------------D O C U M E N T O S------------------------------------------------------------------------>

<div id="accordion1" class="row mob-faq justify-content-center mb-5" >
                        <div id="" class="accordion1 col-md-11 ">
                            
                            <div class="ts-title  mt-4">
                                <h3 class="bold" style="font-size:2rem; color:#c56122"><b>Covenant Documentos</b></h3>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="headingOne">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link  pregunta_search" data-toggle="collapse" data-target="#collapseO" aria-expanded="true" aria-controls="collapseO">
                                            1.- ¿Qué es Covenant Documentos?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#collapseO" aria-expanded="true" aria-controls="collapseO"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>

                                <div id="collapseO" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion1">
                                    <div class="card-body">
                                        <h4>COVENANT DOCUMENTOS es una solución informática que permite la creación, gestión y resguardo de los documentos digitales.</h4>
                                        Mediante el uso de la Firma Electrónica Avanzada, Sello Digital de Tiempo y NOM-151, se da garantía legal de:
                                        <dl class="vinetas">
                                            <li>Quién ﬁrma es quien dice ser.</li>
                                            <li>Tiempo exacto de la acción de ﬁrma.</li>
                                            <li>Garantizar la Integridad de la información.</li>
                                        </dl>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="headingOne">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link  pregunta_search" data-toggle="collapse" data-target="#collapse2" aria-expanded="true" aria-controls="collapse2">
                                            2.- Ventajas de utilizar Covenant Documentos
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#collapse2" aria-expanded="true" aria-controls="collapse2"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>

                                <div id="collapse2" class="collapse " aria-labelledby="headingOne" data-parent="#accordion1">
                                    <div class="card-body">
                                        <p>COVENANT DOCUMENTOS te permitirá innovar de manera administrativa y tecnológica en el diseño, creación y gestión de todos los documentos digitales desde su origen de manera organizada y en tiempo real para su posterior envío a firma con las partes involucradas. Entre otras de las ventajas que obtiene al utilizar Covenant, se encuentran:</p>
                                        1) Reducción del riesgo de falsificación, ya que todos los documentos electrónicos contienen bloques de seguridad tecnológica para garantizar su integridad.
                                        <br><br>
                                        2) Los firmantes que intervienen pueden realizar la firma desde casi cualquier dispositivo móvil o de escritorio y desde cualquier lugar.
                                        <br><br>
                                        3) Certeza jurídica y validez legal de todos los documentos que pasan por el proceso de firma electrónica avanzada.
                                        <br><br>
                                        4) Reducción de costos, tiempos y asociados con la intervención desde la elaboración de un documento o contrato hasta la recolecta de la firma de las partes.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="headingOne">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link  pregunta_search" data-toggle="collapse" data-target="#collapse3" aria-expanded="true" aria-controls="collapse3">
                                            3.- ¿Qué son los servicios API de Covenant?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#collapse3" aria-expanded="true" aria-controls="collapse3"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>

                                <div id="collapse3" class="collapse " aria-labelledby="headingOne" data-parent="#accordion1">
                                    <div class="card-body">
                                        Es el conjunto de métodos y funciones que permiten la comunicación entre plataformas (Covenant y los servicios de un cliente) a nivel servidor por medio de webservices (WS) para lograr un proceso transparente y un flujo continuo entre los usuarios finales. Este servicio se puede incluir o no, conforme las necesidades de nuestros clientes.

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="headingOne">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link  pregunta_search" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            4.- ¿Qué necesidades resuelve Covenant Documentos?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><i class="expanded   more-less fas fa-plus-circle"></i></span>
                                </div>

                                <div id="collapseOne" class="collapse " aria-labelledby="headingOne" data-parent="#accordion1">
                                    <div class="card-body">
                                    <dl class="vinetas">
                                            <li>Disminución en costos de oficina.</li>
                                            <li>Ahorro en espacio físico de almacenamiento.</li>
                                            <li>Integridad en la elaboración de documentos.</li>
                                            <li>Respaldo seguro de su información.</li>
                                            <li>Reducción de costos y tiempos en recolección de firmas.</li>
                                            <li>Reducción de suplantación en la identidad de los firmantes.</li>
                                            <li>Reducción de la eficacia probatoria y actos fraudulentos en juicios legales.</li>
                                        </dl>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="headingTwo">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            5.- ¿Cómo puedo ser usuario de Covenant Documentos?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion1">
                                    <div class="card-body">
                                    Contratando nuestros servicios puede adquirir el acceso para usted, su empresa y colaboradores, así como, todos los firmantes que requiera dentro de su documentación de una forma muy sencilla. Para obtener más información sobre nuestros servicios, paquetes o planes de venta puede contactarnos en <a class="Linkss" href="mailto: contacto@legalexgs.com">contacto@legalexgs.com</a> o a los teléfonos: <a class="Linkss" href="tel: +52 443 690 6851 ">  (+52) 443 690 6851</a>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="headingThree">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            6.- ¿Qué hacer si no recibo mi correo de bienvenida o confirmación de cuenta?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion1">
                              
                                    <div class="card-body">
                                   
                                        Recupere su contraseña manualmente:<br>
                                        1) Ingrese a su Covenant Documentos y de clic en la opción ¿Olvidó su contraseña?.<br>
                                        2) Aparecerá una ventana y deberá ingresar el correo electrónico con el cual dieron de alta su cuenta.<br>
                                        3) Por ultimo deberá resolver el reCAPTCHA y dar clic al botón "Enviar".
                                        <br><br>
                                        Recomendaciones:<br>
                                        1) Revisar la bandeja de SPAM o correo no deseado.<br>
                                        2) Verificar que el correo electrónico que se proporcionó este correctamente escrito directamente con el administrador de la plataforma (en caso de no tener uno, acuda directamente a soporte técnico de Legalex GS).<br>
                                        3) Verificar que su correo no tenga bloqueado el dominio de Legalex GS para la recepción de correos.<br>
                                        4) Verificar que la bandeja de entrada del correo no este llena o sin espacio.
                                        <br><br>
                                        En caso de que ninguna de las opciones anteriores resuelva el problema, contactar a Soporte Técnico a través de los siguientes medios:<br>
                                        Correo electrónico para soporte Covenant Documentos: <a class="Linkss" href="mailto: soporte@legalexgs.com"> soporte@legalexgs.com</a>
                                    
                                    </div>
                                   
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p4">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r4" aria-expanded="false" aria-controls="r4">
                                            7.- ‍¿Qué pasa si no terminé de elaborar mi documento y la sesión se cerró?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r4" aria-expanded="false" aria-controls="r4"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r4" class="collapse" aria-labelledby="headingThree" data-parent="#accordion1">
                                    <div class="card-body">
                                        En el caso de los documentos que son realizados en base a plantillas, cerró la sesión cuando se estaba capturando el documento sin haber dado guardar anteriormente, éste tendrá que ser rellenado nuevamente. Ya que cuando se captura información sin ser guardada el avance se pierde.
                                        <br><br>
                                        Para los documentos que son cargados en la plataforma mediante el uso de archivos con extensión PDF, existe el guardado parcial. Si después de cargar el documento PDF dio clic en "Guardar archivo", podrá regresar al documento y continuar con el proceso de agregar a los firmantes y de igual manera si ya había agregado algún firmante y dio clic en el botón "guardar firmantes" podrá regresar al ultimo estatus de guardado donde lo dejó. Si el documento PDF nunca fue guardado desde la primer captura de información, este tendrá que subirse nuevamente.
                                        <br> <br>

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p5">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r5" aria-expanded="false" aria-controls="r5">
                                            8.- ¿Cómo se agrega a un nuevo firmante?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r5" aria-expanded="false" aria-controls="r4"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r5" class="collapse" aria-labelledby="headingThree" data-parent="#accordion1">
                                    <div class="card-body">
                                        Los firmantes se agregan de forma automática al ingresar la información de cada uno de los participantes que estarán involucrados en la firma del documento, Covenant "recuerda" a aquellos que ya se les solicito anteriormente la firma para poder precargar su información, cuando esta no se precargue significa que estará agregando la información de un nuevo firmante.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p6">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r6" aria-expanded="false" aria-controls="r6">
                                            9.- ¿Guardan mi información personal?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r6" aria-expanded="false" aria-controls="r6"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r6" class="collapse" aria-labelledby="headingThree" data-parent="#accordion1">
                                    <div class="card-body">
                                        La información que es guardada dentro de Covenant es: <br>
                                        <dl class="vinetas">
                                        -Información miníma necesaria y requerida para realizar el envío del documento a los firmantes involucrados.<br>
                                        -Copia del documento que se envío a firma (esta copia solo es accedible por el emisor del documento y los firmantes).<br>
                                        -Información de alta de usuarios, como correo electrónico, nombre, RFC y en el caso de uso de plantillas la información que se registra para las variables.
                                        </dl>

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p7">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r7" aria-expanded="false" aria-controls="r7">
                                            10.- ¿Dónde tienen sus centros de datos?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r7" aria-expanded="false" aria-controls="r7"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r7" class="collapse" aria-labelledby="headingThree" data-parent="#accordion1">
                                    <div class="card-body">
                                        Nuestros Centros de Datos están coubicados en uno de los centros más importantes de Latinoamerica, TRIARA.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p8">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r8" aria-expanded="false" aria-controls="r8">
                                            11.- ¿Qué es la Firma Autógrafa Digital (FAD) o de Trazo?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r8" aria-expanded="false" aria-controls="r8"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r8" class="collapse" aria-labelledby="headingThree" data-parent="#accordion1">
                                    <div class="card-body">
                                    La Firma Autógrafa Digital (FAD), es conocida como una firma electrónica simple, con la cual se puede dar un sustento legal mínimo necesario pero se considera repudiable. Esta se realiza dibujando el trazo de la firma autógrafa que comunmente se realiza en papel, es decir, se realiza la misma firma autógrafa pero en lugar de utilizar bolígrafo y papel se realiza de forma digital.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p9">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r9" aria-expanded="false" aria-controls="r9">
                                            12.- ¿Qué es la firma electrónica avanzada?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r9" aria-expanded="false" aria-controls="r9"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r9" class="collapse" aria-labelledby="headingThree" data-parent="#accordion1">
                                    <div class="card-body">
                                        Es el conjunto de datos y caracteres que permite la identificación del firmante, está ha sido creada por medios electrónicos bajo el exclusivo control del firmante, de manera que está vinculada únicamente al mismo y a los datos a los que se refiere, lo que permite que sea detectable cualquier modificación ulterior de éstos, y a su vez, esta firma produce los mismos efectos jurídicos que la firma autógrafa.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p10">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r10" aria-expanded="false" aria-controls="r10">
                                            13.- ¿Cuál es el fundamento legal de la Firma Electrónica Avanzada?

                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r10" aria-expanded="false" aria-controls="r10"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r10" class="collapse" aria-labelledby="headingThree" data-parent="#accordion1">
                                    <div class="card-body">
                                        La Firma electrónica se fundamenta principalmente en la Ley de Firma Electrónica Avanzada publicada en el Diario Oficial de la Federación el 11 de enero de 2012, así como en el Código de Comercio Federal Art. 89, 96 y 97.<br><br>


                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p11">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r11" aria-expanded="false" aria-controls="r11">
                                            14.- ¿Es necesario tener una cuenta para firmar un documento?

                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r11" aria-expanded="false" aria-controls="r11"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r11" class="collapse" aria-labelledby="headingThree" data-parent="#accordion1">
                                    <div class="card-body">
                                        Sí.

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p12">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r12" aria-expanded="false" aria-controls="r12">
                                            15.- ¿Cuántos firmantes puede tener un documento?

                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r12" aria-expanded="false" aria-controls="r12"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r12" class="collapse" aria-labelledby="headingThree" data-parent="#accordion1">
                                    <div class="card-body">
                                        Al contratar Covenant con Legalex GS, no existe limitantes en cuanto al número de firmantes incluidos en un mismo documento, pueden incorporar los que sean necesarios.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p13">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r13" aria-expanded="false" aria-controls="r13">
                                            16.- ¿Cuáles son las ventajas de utilizar Covenant Documentos?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r13" aria-expanded="false" aria-controls="r13r7"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r13" class="collapse" aria-labelledby="headingThree" data-parent="#accordion1">
                                    <div class="card-body">
                                    <dl class="vinetas">
                                            <li>Generar plantillas de acuerdo a tus necesidades.</li>
                                            <li>Crear flujos de revisión o administración en la documentación digital basada en plantillas. </li>
                                            <li>Garantiza que solo la persona autorizada pueda realizar cambios al contenido del documento. </li>
                                            <li>Asignar contenido especifico a las diferentes áreas de su empresa para que puedan utilizarlas sin realizar cambios en el contenido del documento.</li>
                                            <li>Cancelación o declinación de los documentos enviados a firma con un solo clic.</li>
                                            <li>Fácil envio a firma y administración del seguimiento del documento y de los firmantes en tiempo real.</li>

                                        </dl>

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p14">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r14" aria-expanded="false" aria-controls="r14">
                                            17.- ¿Qué es una plantilla?

                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r14" aria-expanded="false" aria-controls="r14"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r14" class="collapse" aria-labelledby="headingThree" data-parent="#accordion1">
                                    <div class="card-body">
                                        Es un formato o "machote" donde se define la estructura de un documento, como puede ser el contenido estático, diseño y posteriormente se agregan variables para que estas sean rellenadas sin la necesidad de que alguien intervenga en la edición del contenido estático o principal del documento.

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p15">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r15" aria-expanded="false" aria-controls="r15">
                                            18.- ¿Existen validadores al generar una plantilla?

                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r15" aria-expanded="false" aria-controls="r15"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r15" class="collapse" aria-labelledby="headingThree" data-parent="#accordion1">
                                    <div class="card-body">
                                    Sí, el usuario que realiza está acción es llamado "Checker" y su función principal es aprobar las plantillas que son creadas en Covenant así como los documentos capturados que pasen por un proceso de verificación o aprobación.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p16">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r16" aria-expanded="false" aria-controls="r16">
                                            19.- Una vez que el documento haya sido firmado, ¿Se puede modificar?

                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r16" aria-expanded="false" aria-controls="r16"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r16" class="collapse" aria-labelledby="headingThree" data-parent="#accordion1">
                                    <div class="card-body">
                                        No, una vez que un documento es enviado a firma solo puede ser cancelado, ya que cualquier modificación afectaría la integridad del documento y no lo haría confiable.

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p17">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r17" aria-expanded="false" aria-controls="r17">
                                            20.- ¿Puedo cancelar un documento si ya fue firmado parcial o totalmente?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r17" aria-expanded="false" aria-controls="r17"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r17" class="collapse" aria-labelledby="headingThree" data-parent="#accordion1">
                                    <div class="card-body">
                                        El emisor del documento puede cancelarlo siempre y cuando el documento no se encuentre firmado en su totalidad.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p18">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r18" aria-expanded="false" aria-controls="r18">
                                            21.- ¿Por qué es importante ingresar el RFC para el alta de usuarios?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r18" aria-expanded="false" aria-controls="r18"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r18" class="collapse" aria-labelledby="headingThree" data-parent="#accordion1">
                                    <div class="card-body">
                                        El RFC se vuelve un identificador único para los usuarios, con esto garantizamos que no existan usuarios iguales y de igual forma se utiliza para comparar la información relacionada con la firma y verificar que solo el usuario propietario de la cuenta es quién decide hacer la firma dentro de nuestra plataforma.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p19">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r19" aria-expanded="false" aria-controls="r19">
                                            22.- ¿Puedo combinar los tipos de firma en un mismo documento (Firma Autógrafa y Firma Electrónica Avanzada)?

                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r19" aria-expanded="false" aria-controls="r19"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r19" class="collapse" aria-labelledby="headingThree" data-parent="#accordion1">
                                    <div class="card-body">
                                        No, los documentos estan diseñados para llevar el mismo de tipo de firma para todas las firmas involucradas, esto nos ayuda a mantener un mejor control de la integridad del documento y que al mismo tiempo obtenga unicidad en el proceso de integración tecnológica y posteriormente que el documento completo pueda obtener el no repudio.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p20">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r20" aria-expanded="false" aria-controls="r20">
                                            23.- ¿Qué es la figura del no repudio?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r20" aria-expanded="false" aria-controls="r20"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r20" class="collapse" aria-labelledby="headingThree" data-parent="#accordion1">
                                    <div class="card-body">
                                        Consiste en que la firma electrónica avanzada contenida en el o los documentos electrónicos garantiza la autoría e integridad del documento y que dicha firma corresponde exclusivamente al firmante. Es decir, el no repudio evita que el emisor o el receptor nieguen la firma y recepción del documento firmado. Así, cuando se envía un documento a firma, el receptor puede comprobar que, el emisor envió el documento. Por otra parte, cuando se recibe un documento para ser firmado, el emisor puede verificar que, el receptor recibió el documento.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p21">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r21" aria-expanded="false" aria-controls="r21">
                                            24.- ¿Qué hacer si no puedo ingresar?

                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r21" aria-expanded="false" aria-controls="r21"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r21" class="collapse" aria-labelledby="headingThree" data-parent="#accordion1">
                                    <div class="card-body">
                                        Deberás de verificar que se está ingresando la contraseña correctamente teniendo en cuenta espacios, el uso de mayúsculas y minúsculas. En caso de no recordar la contraseña, puede seleccionar el botón Recuperar Contraseña, ingresar su correo electrónico con el que fue registrada su cuenta y dar clic sobre el botón "Recuperar", después recibirá un correo electrónico con los pasos a seguir para finalizar este proceso.
                                        <br><br>
                                        Nota: Si no recibe el correo electrónico, deberá revisar su bandeja de SPAM o correo no deseado.
                                        <br><br>
                                        Si no recuerda la cuenta de correo con la que fue registrada su cuenta o no tiene acceso a la misma, deberá contactar a Soporte Técnico a través del correo electrónico <a class="Linkss" href="mailto: soporte@legalexgs.com">soporte@legalexgs.com</a>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

<br>
<!---------------------------------------------------------------------------------F I R M A ---------------------------------------------------------------->

<div id="accordion2" class="row mob-faq justify-content-center mb-5">
                        <div id="" class="accordion2 col-md-11 ">
                            <div class="ts-title  mt-4">
                                <h3 class="bold" style="font-size:2rem; color:#c56122"><b>Covenant Firma</b></h3>
                            </div>
                       
                            <div class="card">
                                <div class="card-header faq" id="headingOne">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link  pregunta_search" data-toggle="collapse" data-target="#collapseO" aria-expanded="true" aria-controls="collapseO">
                                            1.- ¿Cómo firmar un documento en COVENANT?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#collapseO" aria-expanded="true" aria-controls="collapseO"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>

                                <div id="collapseO" class="collapse " aria-labelledby="headingOne" data-parent="#accordion2">
                                    <div class="card-body">
                                        <h4>Para firmar un documento se debe seguir los siguientes pasos:</h4>

                                        <P>1. Primero se debe ingresar al portal de Covenant firma en el siguiente link: <a class="Linkss" href="https://www.legalexgs.com/covenant_firma/." target="_blank">https://www.legalexgs.com/covenant_firma/</a> .</P>
                                        <P>2. Inicia sesión con tu usuario (RFC) y contraseña.</P>
                                        <P>3. Una vez que se haya iniciado sesión, debes dar clic en la sección “Pendientes de Firma” y a continuación debes de seleccionar el documento a firmar dar clic en el botón “Firmar” .</P>
                                        <P>4. Una vez que se visualice el documento a firmar, se deberá seleccionar los archivos de la e.firma e ingresar la firma para comenzará el proceso de firma .</P>
                                        <P>5. En caso de que el documento se haya firmado con éxito, aparecerá un mensaje donde se indique que la firma se ha realizado y el documento ahora aparecerá en el.</P>

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="headingOne">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link  pregunta_search" data-toggle="collapse" data-target="#collapse2" aria-expanded="true" aria-controls="collapse2">
                                            2.- Diferencias entre la firma electrónica, la firma digital y la firma digitalizada
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#collapse2" aria-expanded="true" aria-controls="collapse2"><i class="expanded   more-less fas fa-plus-circle"></i></span>
                                </div>

                                <div id="collapse2" class="collapse " aria-labelledby="headingOne" data-parent="#accordion2">
                                    <div class="card-body">
                                        <h4>¿Qué es la firma electrónica?</h4>
                                        La firma electrónica son unos datos electrónicos que acompañan a una determinada información (también en formato electrónico). Al mismo tiempo, la firma electrónica es un concepto legal que equivale a la firma manuscrita y que tiene el objetivo de dar fe de la voluntad del firmante.
                                        <br><br>
                                        El Reglamento (UE) Nº 910/2014, conocido como eIDAS, es el marco normativo europeo que confiere validez jurídica a las firmas electrónicas. Según este la firma electrónica son los “datos en formato electrónico anejos a otros datos electrónicos o asociados de manera lógica con ellos que utiliza el firmante para firmar.”
                                        <br><br>
                                        La definición de firma electrónica que recoge el Reglamento es la más básica, y es el fundamento común de los tres tipos de firmas electrónicas que existen:
                                        <br><br>
                                        <dl class="vinetas">
                                            <li>La firma electrónica o firma electrónica simple</li>
                                            <li>La firma electrónica avanzada</li>
                                            <li>La firma electrónica cualificada</li>
                                        </dl>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="headingOne">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link  pregunta_search" data-toggle="collapse" data-target="#collapse3" aria-expanded="true" aria-controls="collapse3">
                                            3.- ¿La e.firma (antes FIEL) es exclusiva para trámites fiscales y gubernamentales?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#collapse3" aria-expanded="true" aria-controls="collapse3"><i class="expanded   more-less fas fa-plus-circle"></i></span>
                                </div>

                                <div id="collapse3" class="collapse " aria-labelledby="headingOne" data-parent="#accordion2">
                                    <div class="card-body">
                                        No. Existe una noción errónea respecto a los certificados de firma electrónica avanzada que emite el SAT (la FIEL o e.firma): que solo pueden usarse para trámites ante la autoridad fiscal que los emite y ante otras dependencias de gobierno.
                                        <br><br>
                                        La e.firma la emite el SAT, una Agencia Certificadora autorizada por Banxico. Los certificados privados los emiten diversos Prestadores de Servicios de Certificación acreditados para tal fin por la Secretaría de Economía.
                                        <br><br>
                                        Por ello algunas personas creen que para asuntos privados como firma de contratos únicamente son válidos los certificados que emiten los PSCs privados.
                                        <br><br>
                                        Esta idea es errónea porque la legislación vigente para asuntos privados —el Código de Comercio— no hace diferencia entre uno y otro tipo de certificados. En tanto los certificados cumplan lo estipulado en la ley y no exista una legislación o regulación que acote su uso para un fin específico, pueden usarse indistintamente.
                                        <br><br>
                                        En Mifiel solamente soportamos el uso de certificados de e.firma emitidos por el SAT. Todos los representantes legales cuentan con una.
                                        <br><br>
                                        Soportarlos además trae una ventaja: el SAT emite también certificados para personas morales. De acuerdo con el Artículo 19-A del Código Fiscal de la Federación se presume sin que se admita prueba en contrario, que los documentos firmados con FIEL de persona moral los firmó un apoderado legal con poderes amplios. Esta disposición es aplicable a asuntos privados por el principio de supletoriedad de la ley. Ningún otro PSC o Agencia Certificadora privada ni pública emite tal tipo de certificados.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="headingOne">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link  pregunta_search" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            4.- ‍¿Por qué recibí un correo de COVENANT?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><i class="expanded   more-less fas fa-plus-circle"></i></span>
                                </div>

                                <div id="collapseOne" class="collapse " aria-labelledby="headingOne" data-parent="#accordion2">
                                    <div class="card-body">
                                        Covenant envía correos de notificación a usuarios registrados por diferentes motivos, algunos del ellos son: activación de cuenta, documento pendiente de firma, documento firmado, recuperación y cambio de contraseña, confirmación de un documento firmado, entre otros.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="headingTwo">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            5.- ¿Qué es Covenant Firma?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion2">
                                    <div class="card-body">
                                        ‍‍‍COVENANT FIRMA es la solución tecnológica orientada para llevar a cabo de manera electrónica y segura la firma de un documento evitando la presencia física del firmante; hacemos uso de la e.firma (FIEL) que el Sistema de Administración Tributaria (SAT) provee, adicionalmente, contamos con el uso de la firma electrónica simple (firma autógrafa digital), aplicamos normas de seguridad informática para garantizar la integridad de la firma (incluimos un sello digital de tiempo por transacción).
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="headingThree">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            6.- ¿Mis clientes tendrán que firmar electrónicamente a través de COVENANT?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion2">
                                    <div class="card-body">
                                        Sí. Puede hacerlo vía web o utilizando nuestra aplicación móvil, la cual se encuentra disponible para descarga gratuita dentro de Play Store y App Store.

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p4">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r4" aria-expanded="false" aria-controls="r4">
                                            7.- ‍¿Cuál es la seguridad jurídica aportada?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r4" aria-expanded="false" aria-controls="r4"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r4" class="collapse" aria-labelledby="headingThree" data-parent="#accordion2">
                                    <div class="card-body">
                                        Legalex GS al ser un Prestador de Servicios de Certificación (PSC) acreditado por la Secretaría de Economía Federal, funge como un auxiliar del comercio, brindando servicios jurídico-tecnológicos para agilizar y dar certeza en los actos comerciales que se celebran entre las partes, mediante el uso de nuestros servicios. Utilizamos la Firma Electrónica Avanzada que provee el Servicio de Administración Tributaria (SAT) dentro de nuestros procesos de firmado electrónico y así ofrecer la figura del "No Repudio", además de verificar que la persona que intenta firmar es quien dice ser. Adicionalmente, incorporamos el servicio de Sellos Digitales de Tiempo en cada transacción de firma emitida y una Constancia de Conservación de Mensajes de Datos (en caso de ser requerida) a cada documento, logrando garantizar la integridad del mismo por un período de tiempo de 10 años.
                                        <br><br>
                                        Legalex GS cuenta con las acreditaciones y permisos necesarios para cumplir con las reglas y artículos necesarios para la emisión de los mensajes de datos generados y conservados mediante el uso de Covenant y así, poder brindar el mayor nivel de calidad al cumplir con los más altos estándares informáticos y gozar del más alto valor aprobatorio.
                                        <br> <br>
                                        Adicionalmente, el Sello Digital de Tiempo proporcionado por Covenant es un mecanismo con reconocimiento legal que ampara la fecha y hora exacta en la que ocurrio una acción, transacción o movimiento dentro de los documentos privados mercantiles.
                                        <br><br>
                                        Legislación de interés: <br>
                                        - Reglas generales a las que deberán sujetarse los Prestadores de Servicios de Certificación en su título del primero al cuarto y del sexto al séptimo. <br>
                                        - La Norma Oficial Mexicana NOM-151-SCFI-2016. <br>
                                        - Código de Comercio en sus artículos 89, 89 bis, 95, 95 bis y del 96 al 99. <br>
                                        - Código Fiscal de la Federación en su artículo 17 sub. C al J. <br>
                                        - Ley de firma electrónica avanzada artículos 2 al 13, 15, 17 al 20, 23 al 25 y 27.

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p5">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r5" aria-expanded="false" aria-controls="r5">
                                            8.- ¿Qué necesidades resuelve Covenant Firma?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r5" aria-expanded="false" aria-controls="r4"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r5" class="collapse" aria-labelledby="headingThree" data-parent="#accordion2">
                                    <div class="card-body">
                                    <dl class="vinetas">
                                            <li>Evita la presencia física del firmante, firmando desde un equipo de cómputo o desde un dispositivo móvil a través de nuestra aplicación de firma móvil, Covenant Firma.</li>
                                            <li>Seguridad tecnológica sin intervención de terceros.</li>
                                            <li>Garantiza la integridad del documento.</li>
                                            <li>Certeza de que el firmante involucrado es quien dice ser.</li>
                                            <li>Respaldo jurídico y tecnológico.</li>
                                            <li>Optimización de tiempos en la gestión y recabación de firmas.</li>
                                            <li>Ahorro en insumos administrativos.</li>
                                        </dl>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p6">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r6" aria-expanded="false" aria-controls="r6">
                                            9.- ¿Cómo puedo ser usuario de Covenant Firma?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r6" aria-expanded="false" aria-controls="r6"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r6" class="collapse" aria-labelledby="headingThree" data-parent="#accordion2">
                                    <div class="card-body">
                                        1. Cuando se solicita la firma de un documento, de forma automática se crea el usuario para las personas firmantes involucradas (esto se hace a través del llenado de información cuando se va a enviar el documento a firma desde Covenant Documentos). El usuario y contraseña (de un único uso) se hace llegar a través de un correo electrónico proveniente de las cuentas oficiales de Legalex GS: <a class="Linkss" href="mailto: contacto.psc@legalexgs.com">contacto.psc@legalexgs.com</a> o <a class="Linkss" href="soporte.covenant@legalexgs.com">soporte.covenant@legalexgs.com</a>
                                        <br><br>
                                        2. Al contratar los servicios de Covenant con Legalex GS, el Cliente contratante de nuestros servicios decide quienes son los involucrados que dará de alta para poder firmar dentro de Covenant Firma.

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p7">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r7" aria-expanded="false" aria-controls="r7">
                                            10.- ¿Qué hacer si no recibo mi correo de activación o confirmación de cuenta?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r7" aria-expanded="false" aria-controls="r7"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r7" class="collapse" aria-labelledby="headingThree" data-parent="#accordion2">
                                    <div class="card-body">
                                        Reenvie manualmente su correo de activación a otra cuenta de correo electrónico: <br><br>
                                        1) Ingrese a <a class="Linkss" href=" https://www.legalexgs.com/covenant_firma/" target="_blank"> https://www.legalexgs.com/covenant_firma/</a> y de clic en la opción "Reenviar correo de activación". <br>
                                        2) Aparecerá una ventana y deberá ingresar el usuario o RFC y correo electrónico con el cual lo dieron de alta (estos datos son los que usted brindo a la empresa que le envía los documentos a firma), de igual manera, deberá ingresar el correo electrónico al que quiere que le sea reenviado el correo de activación. <br>
                                        3) Por ultimo deberá resolver el reCAPTCHA y dar clic al botón "Enviar".
                                        <br><br>
                                        En caso de que la información proporcionada sea correcta, se enviara de forma automática el correo de activación de cuenta a la dirección de correo electrónico proporcionada como "alternativa".





                                        <br><br>
                                        Recomendaciones: <br><br>
                                        1) Revisar la bandeja de SPAM o correo no deseado. <br>
                                        2) Verificar que el correo electrónico que se proporcionó esté correctamente escrito directamente con el emisor del documento.<br>
                                        3) Verificar que su correo no tenga bloqueado el dominio de Legalex GS para la recepción de correos.<br>
                                        4) Verificar que la bandeja de entrada del correo no este llena o sin espacio.

                                        <br><br>

                                        En caso de que ninguna de las opciones anteriores no resuelva el problema, contactar a Soporte Técnico a través del Chat que se encuentra en la siguiente dirección: <a class="Linkss" href="https://www.legalexgs.com/covenant_firma/" target="_blank">https://www.legalexgs.com/covenant_firma/</a>


                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p8">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r8" aria-expanded="false" aria-controls="r8">
                                            11.- ¿Cómo ingresar a Covenant Firma por primera vez?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r8" aria-expanded="false" aria-controls="r8"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r8" class="collapse" aria-labelledby="headingThree" data-parent="#accordion2">
                                    <div class="card-body">
                                        Primero se debe de activar la cuenta siguiendo los pasos indicados en el correo proveniente de alguna de las cuentas oficiales de Legalex GS: contacto.psc@legalexgs.com o soporte.covenant@legalexgs.com<br><br>
                                        1) Dentro del correo electrónico vendrá el usuario (RFC), el cual será utilizado para ingresar a Covenant Firma, adicional contendra una contraseña de un único uso, la cual se utilizá para acceder por única ocasión en el proceso de confirmación de cuenta.<br>
                                        2) Dentro del mismo correo contiene un botón que te llevara a Covenant Firma <a class="Linkss" href="https://www.legalexgs.com/covenant_firma/" target="_blank">(https://www.legalexgs.com/covenant_firma/</a>). <br>
                                        3) Dentro de la página de inicio de Covenant Firma, deberás copiar y pegar el usuario y la contraseña que te fue proporcionada en el correo electrónico y dar clic en el botón "Entrar". <br>
                                        4) Si los datos son correctos, al ingresar Covenant Firma te pedirá que cambies la contraseña por una diferente a la que llegó en el correo electrónico.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p9">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r9" aria-expanded="false" aria-controls="r9">
                                            12.- ¿Cómo obtener mis archivos de e.firma o FIEL para poder realizar la Firma Electrónica dentro de Covenant Firma?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r9" aria-expanded="false" aria-controls="r9"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r9" class="collapse" aria-labelledby="headingThree" data-parent="#accordion2">
                                    <div class="card-body">
                                        La solicitud de e.firma o FIEL se realiza ante el Sistema de Administración Tributaria (SAT), este proceso es propio del SAT y Legalex GS no interviene en el proceso, ni en las contraseñas de firma que se generan durante este proceso. Para más información sobre la emisión de la Firma Electrónica Avanzada visita el portal oficial del SAT o da clic en el siguiente enlace: <a class="Linkss" href="https://www.sat.gob.mx/tramites/16703/obten-tu-certificado-de-e.firma-(antes-firma-electronica)" target="_blank">https://www.sat.gob.mx/tramites/16703/obten-tu-certificado-de-e.firma-(antes-firma-electronica)</a>




                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p10">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r10" aria-expanded="false" aria-controls="r10">
                                            13.- ¿Cuáles son los elementos que necesito para realizar una firma electrónica?

                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r10" aria-expanded="false" aria-controls="r10"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r10" class="collapse" aria-labelledby="headingThree" data-parent="#accordion2">
                                    <div class="card-body">
                                        Firma Autógrafa Digital (Trazo): <br><br>
                                        1) Únicamente es necesario acceder al documento que se requiere firmar, dar clic en el botón "Firmar" y Covenant Firma le mostrará el espacio para realizar la firma (web o en un dispositivo móvil).
                                        <br><br>
                                        Firma Electrónica Avanzada (e.firma o Fiel que provee el SAT): <br>
                                        1) Deberá tener a la mano sus archivos de firma, los cuales tienen las siguientes extensiones *.cer, *.key y su clave o contraseña privada (esta es la contraseña que designó para su certificado ante el SAT). <br><br>
                                        2) Contar con la e.firma o FIEL vigente y actualizada.
                                        <br><br>
                                        *Nota: para realizar la firma en su equipo de cómputo, deberá cargar estos archivos cada vez que realice una firma. En caso de tener instalada la aplicación móvil de firma, los archivos se cargan una única vez, por lo que deberá considerar tenerlos compartidos en una "nube" o que tenerlos guardados dentro de su dispositivo móvil.

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p11">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r11" aria-expanded="false" aria-controls="r11">
                                            14.- ¿Cómo cancelo o declino mi firma en un documento?

                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r11" aria-expanded="false" aria-controls="r11"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r11" class="collapse" aria-labelledby="headingThree" data-parent="#accordion2">
                                    <div class="card-body">
                                        Al ingresar a Covenant Firma, nos dirigimos a la sección "Documentos Pendientes" y después de seleccionar el o los archivos que se quieren cancelar, se deberá presionar el botón de "Firma" o "Firmar documento(s)" para que la plataforma nos carge la visualización de los documentos que se han seleccionado. Después, se deberá presionar el botón de color rojo con la leyenda "Cancelar Documento" que se encuentra en la parte superior e ingresar el motivo de cancelación, y a continuación se deberá de dar clic el botón "Continuar".

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p12">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r12" aria-expanded="false" aria-controls="r12">
                                            15.- ¿Cuáles son los medios para realizar mi firma electrónica?

                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r12" aria-expanded="false" aria-controls="r12"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r12" class="collapse" aria-labelledby="headingThree" data-parent="#accordion2">
                                    <div class="card-body">
                                        La firma electrónica en Covenant se puede realizar por dos medios:<br><br>
                                        1. Vía web a través del siguiente enlace: <a class="Linkss" href="https://www.legalexgs.com/covenant_firma/" target="_blank">https://www.legalexgs.com/covenant_firma/</a> <br>
                                        2. A través de la aplicación móvil Covenant Firma, disponible para iOS y Android.

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p13">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r13" aria-expanded="false" aria-controls="r13">
                                            16.- ¿Cómo puedo recuperar mi contraseña de acceso?


                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r13" aria-expanded="false" aria-controls="r13r7"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r13" class="collapse" aria-labelledby="headingThree" data-parent="#accordion2">
                                    <div class="card-body">
                                        Si no recuerda su contraseña para acceder, puede recuperarla ingresando a <a class="Linkss" href="https://www.legalexgs.com/covenant_firma/" target="_blank">https://www.legalexgs.com/covenant_firma/</a> y deberá de seleccionar la opción "¿Olvidaste tu contraseña?", se mostrará una ventana donde ingresará el correo electrónico con el cuál fue registrado, posteriormente deberá responder el reCAPTCHA (No soy un robot) y, por último, dar clic sobre el botón "Confirmar". Si los datos son correctos, recibirá un correo electrónico con los datos y pasos para cambiar su contraseña.


                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p14">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r14" aria-expanded="false" aria-controls="r14">
                                            17.- ¿Cómo desbloqueo mi cuenta? ¿Por qué pasa esto?

                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r14" aria-expanded="false" aria-controls="r14"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r14" class="collapse" aria-labelledby="headingThree" data-parent="#accordion2">
                                    <div class="card-body">
                                        Las cuentas se bloquean por tener más de 5 intentos fallidos en la contraseña al intentar ingresar. Si esto llegará a ocurrir, deberás cambiar tu contraseña y una vez que se cambie la cuenta se desbloqueará autómaticamente.

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p15">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r15" aria-expanded="false" aria-controls="r15">
                                            18.- ¿Cómo puedo cambiar mi contraseña?

                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r15" aria-expanded="false" aria-controls="r15"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r15" class="collapse" aria-labelledby="headingThree" data-parent="#accordion2">
                                    <div class="card-body">
                                        Para cambiar tu contraseña deberás ingresar a <a class="Linkss" href="https://www.legalexgs.com/covenant_firma/" target="_blank">https://www.legalexgs.com/covenant_firma/</a> y dar clic en la opción "¿Olvidaste tu contraseña?", aparecerá una ventana dónde se ingresará el correo electrónico con el que fue registrado, posteriormente deberá responder el reCAPTCHA (No soy un robot) y, por último, dar clic sobre el botón "Confirmar", si los datos fueron correctos, recibirá un correo electrónico con los datos y pasos para cambiar su contraseña.

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p16">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r16" aria-expanded="false" aria-controls="r16">
                                            19.- ¿Puedo cambiar mi correo electrónico? ¿Cómo puedo hacerlo?

                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r16" aria-expanded="false" aria-controls="r16"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r16" class="collapse" aria-labelledby="headingThree" data-parent="#accordion2">
                                    <div class="card-body">
                                        Sí, es posible cambiar el correo electrónico después de activar la cuenta, sin embargo, el nuevo correo (correo que se cambia) solo será utilizado para poder recuperar la contraseña. Para realizar el cambio, deberás seguir los siguientes pasos: <br><br>
                                        1) Ingresar con su usuario y contraseña a <a class="Linkss" href="https://www.legalexgs.com/covenant_firma/" target="_blank">https://www.legalexgs.com/covenant_firma/</a><br>
                                        2) Una vez dentro, deberá dar clic en el icono en forma de engranes que se encuentra en la parte superior derecha y seleccionar la opción "Mi perfil".<br>
                                        3) Se cargará la información de la pestaña "Información General" (seleccionada por defecto), y verá listada su información como: usuario, nombre y correo, deberá dar clic en el botón azúl "Editar".<br>
                                        4) Al dar clic aparecerá una ventana donde podrá cambiar el correo actual por el nuevo y finalmente dar clic sobre el botón "Guardar".

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p17">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r17" aria-expanded="false" aria-controls="r17">
                                            20.- ¿Qué navegadores puedo utilizar para visualizar correctamente Covenant Firma?


                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r17" aria-expanded="false" aria-controls="r17"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r17" class="collapse" aria-labelledby="headingThree" data-parent="#accordion2">
                                    <div class="card-body">
                                        Nuestras plataformas mantienen una compatibilidad con los siguientes exploradores o navegadores web:<br><br>
                                        1) Windows: Chrome, Firefox, Opera, Microsoft Edge (ultima actualización).<br>
                                        2) MacOS: Chrome, Firefox, Opera y Safari.<br>
                                        3) Linux: Chrome, Firefox, Opera.<br><br>
                                        Por lo tanto, se excluye por completo el uso de nuestras plataformas mediante el navegador web de Internet Explorer.<br>


                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p18">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r18" aria-expanded="false" aria-controls="r18">
                                            21.- ¿Puedo clasificar mis documentos firmados en mi cuenta?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r18" aria-expanded="false" aria-controls="r18"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r18" class="collapse" aria-labelledby="headingThree" data-parent="#accordion2">
                                    <div class="card-body">
                                        Sí, sólo aplica para documentos firmados. Dentro de Covenant Firma (web) usted podrá crear carpetas para acomodar sus documentos firmados. Para hacerlo deberá seguir los siguientes pasos:<br><br>
                                        1) Después de ingresar y encontrarse en su listado de documentos firmados, podrá visualizar el botón "Crear Carpeta" en la parte media superior derecha. Al dar clic podrá escribir el nombre de la carpeta nueva que quiera crear. Al terminar deberá dar clic en el botón "Crear".<br>
                                        2) Para agregar archivos a sus carpetas, deberá seleccionar los archivos con el "check" que aparece del lado izquierdo (dentro del listado de sus documentos) y posteriormente dar clic al botón que se encuentra ubicado en la parte media superior "Mover a otra carpeta", después deberá seleccionar la carpeta donde desea que se muevan los archivos seleccionados y por ultimo dar clic en el botón "Mover".<br>
                                        3) En caso de querer eliminar o renombrar la carpeta (el eliminar la carpeta no elimina los archivos contenidos en ella, simplemente los regresa al listado principal), deberá buscarla dentro del listado y dar clic en la opción "Eliminar" o "Renombrar" segun se requiera, estas opciones se encuentran al final de su listado en la parte derecha.<br>

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p19">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r19" aria-expanded="false" aria-controls="r19">
                                            22.- ¿Cómo descargo mi representación gráfica (archivo PDF) y el valor de la firma (archivo XML)?

                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r19" aria-expanded="false" aria-controls="r19"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r19" class="collapse" aria-labelledby="headingThree" data-parent="#accordion2">
                                    <div class="card-body">
                                        Para descargar la representación gráfica (XML y PDF) de un documento firmado es necesario dirigirse a la sección de documentos Firmados, donde por cada registro (documento) encontrará un botón llamado "XML" y otro llamado "PDF" en el que podrá dar clic para descargar el archivo deseado.
                                        <br><br>
                                        Para llegar a este menú, deberá ingresar a <a class="Linkss" href="https://www.legalexgs.com/covenant_firma/" target="_blank">https://www.legalexgs.com/covenant_firma/</a> con su usuario y contraseña y dentro del panel de bienvenida dar clic a la opción "Firmados".

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p20">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r20" aria-expanded="false" aria-controls="r20">
                                            23.- ¿Cómo ingreso a firmar por segunda ocasión?

                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r20" aria-expanded="false" aria-controls="r20"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r20" class="collapse" aria-labelledby="headingThree" data-parent="#accordion2">
                                    <div class="card-body">
                                        Para ingresar a Covenant Firma deberá ingresar a <a class="Linkss" href="https://www.legalexgs.com/covenant_firma/" target="_blank">https://www.legalexgs.com/covenant_firma/</a> con su usuario (RFC) y la contraseña establecida en su primer ingreso.

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p21">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r21" aria-expanded="false" aria-controls="r21">
                                            24.- ¿Se pueden seleccionar varios documentos de Firma Electrónica Avanzada y Firma de Trazo para firmar al mismo tiempo?

                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r21" aria-expanded="false" aria-controls="r21"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r21" class="collapse" aria-labelledby="headingThree" data-parent="#accordion2">
                                    <div class="card-body">
                                        No, para poder firmar más de un documento al mismo tiempo debén ser del mismo tipo de firma, es decir, todos los documentos seleccionados para ser firmados con Firma Electrónica Avanzada o con Firma Autógrafa Digital (trazo).

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p22">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r22" aria-expanded="false" aria-controls="r22">
                                            25.- ¿Qué es la figura del no repudio?

                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r22" aria-expanded="false" aria-controls="r22"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r22" class="collapse" aria-labelledby="headingThree" data-parent="#accordion2">
                                    <div class="card-body">
                                        Consiste en que la Firma Electrónica Avanzada contenida en el o los documentos electrónicos garantiza la autoría e integridad del documento y que dicha firma corresponde exclusivamente al firmante. Es decir, el no repudio evita que el emisor o el receptor niegue la firma y recepción del documento firmado. Así, cuando se envía un documento a firma, el receptor puede comprobar que el emisor envió el documento. Por otra parte, cuando se recibe un documento para ser firmado, el emisor puede verificar que el receptor recibió el documento.

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p23">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r23" aria-expanded="false" aria-controls="r23">
                                            26.- ¿Qué es la firma autógrafa?

                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r23" aria-expanded="false" aria-controls="r23"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r23" class="collapse" aria-labelledby="headingThree" data-parent="#accordion2">
                                    <div class="card-body">
                                        La Firma Autógrafa Digital (FAD), es conocida como una firma electrónica simple, con la cual se puede dar un sustento legal mínimo necesario pero se considera repudiable. Esta se realiza dibujando el trazo de la firma autógrafa que comunmente se realiza en papel, es decir, se realiza la misma firma autógrafa pero en lugar de utilizar bolígrafo y papel se realiza de forma digital.

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p24">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r24" aria-expanded="false" aria-controls="rr247">
                                            27.- ¿Qué es la firma electrónica avanzada?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r24" aria-expanded="false" aria-controls="r24"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r24" class="collapse" aria-labelledby="headingThree" data-parent="#accordion2">
                                    <div class="card-body">
                                        Es el conjunto de datos y caracteres que permite la identificación del firmante, está ha sido creada por medios electrónicos bajo el exclusivo control del firmante, de manera que está vinculada únicamente al mismo y a los datos a los que se refiere, lo que permite que sea detectable cualquier modificación ulterior de éstos, y a su vez, esta firma produce los mismos efectos jurídicos que la firma autógrafa.

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p25">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r25" aria-expanded="false" aria-controls="r25">
                                            28.- ¿Qué hacer si no puedo ingresar?

                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r25" aria-expanded="false" aria-controls="r25"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r25" class="collapse" aria-labelledby="headingThree" data-parent="#accordion2">
                                    <div class="card-body">
                                        Deberás verificar que se está ingresando la contraseña correctamente teniendo en cuenta espacios, el uso de mayúsculas y minúsculas. En caso de no recordar la contraseña, puede seleccionar el botón "Recuperar Contraseña", ingresar su correo electrónico con el que fue registrada su cuenta y dar clic sobre el botón "Recuperar", después recibirá un correo electrónico con los pasos a seguir para finalizar este proceso.
                                        <br><br>
                                        Nota: Si no recibe el correo electrónico, deberá revisar su bandeja de SPAM o correo no deseado.
                                        <br><br>
                                        Si no recuerda la cuenta de correo con la que fue registrada su cuenta o no tiene acceso a la misma, deberá contactar a Soporte Técnico a través del Chat que se encuentra integrado al ingresar a Covenant Firma: <a class="Linkss" href="https://www.legalexgs.com/covenant_firma/">https://www.legalexgs.com/covenant_firma/</a>

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p26">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r26" aria-expanded="false" aria-controls="r26">
                                            29.- ¿No reconoce la contraseña de mi certificado?

                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r26" aria-expanded="false" aria-controls="r26"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r26" class="collapse" aria-labelledby="headingThree" data-parent="#accordion2">
                                    <div class="card-body">
                                        En caso de que al intentar firmar un documento con la Firma Electrónica Avanza (e.firma o FIEL) que provee el Servicio de Administración Tributaria (SAT) le regrese el aviso "La contraseña de la clave privada no es correcta" deberá verificar que la clave privada que esta ingresando corresponde a los archivos de firma (archivos con extensión *.cer y *.key). Esta contraseña se designa junto con la emisión o renovación de la e.firma o FIEL que emite el SAT, ya sea desde su portal web (para renovaciones) o en sus instalaciones (emisión por primera vez), una forma de comprobar su contraseña es intentando ingresar al portal del SAT ( <a class="Linkss" href="https://cfdiau.sat.gob.mx/nidp/wsfed/ep?id=SATUPCFDiCon&sid=0&option=credential&sid=0" target="_blank">https://cfdiau.sat.gob.mx/nidp/wsfed/ep?id=SATUPCFDiCon&sid=0&option=credential&sid=0</a> ) mediante la opción de e.firma y no mediante el acceso por contraseña (usuario (RFC) y contraseña).

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p27">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r27" aria-expanded="false" aria-controls="r27">
                                            30.- ¿Puedo firmar sin subir certificados?

                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r27" aria-expanded="false" aria-controls="r27"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r27" class="collapse" aria-labelledby="headingThree" data-parent="#accordion2">
                                    <div class="card-body">
                                        Para realizar la firma desde Covenant Firma en un ambiente web, no. Cada que se realice una transacción de firma deberá cargar sus archivos de la e.firma o FIEL que emite el SAT. En el caso de Legalex GS y sus productos como Covenant, los archivos que se utilizan para la firma electrónica no son guardados o resguardados.
                                        <br><br>
                                        Para las firmas realizadas desde la aplicación móvil de Covenant Firma (disponible para dispositivos con iOS y Android), la aplicación está diseñada para que el usuario le indique en donde se encuentran los archivos de firma (e.firma o FIEL) y que no sea necesario cargar todos los archivos en cada firma, sin embargo, esto no quiere decir que se resguarden en la aplicación, lo unico que se guarda es la ubicación donde se consultará la información.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p28">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r28" aria-expanded="false" aria-controls="r28">
                                            31.- ¿Mis datos de firma son guardados dentro de Covenant o por Legalex GS?

                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r28" aria-expanded="false" aria-controls="r28"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r28" class="collapse" aria-labelledby="headingThree" data-parent="#accordion2">
                                    <div class="card-body">
                                        No, los archivos que se utilizan para la firma electrónica no son guardados por Legalex GS ni a través de sus productos. La firma en el ambiente web, es realizada bajo un proceso sobre la caché del equipo donde se encuentra la persona firmando, una vez que el proceso de firma termina, esta información también es eliminada para evitar el robo de información.

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p29">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r29" aria-expanded="false" aria-controls="r29">
                                            32.- ¿El Servicio de Administración Tributaria (SAT) sabe qué estoy firmando?

                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r29" aria-expanded="false" aria-controls="r29"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r29" class="collapse" aria-labelledby="headingThree" data-parent="#accordion2">
                                    <div class="card-body">
                                        No, el Servicio de Administración Tributaria (SAT) no tiene control ni conocimiento sobre la documentación digital que se esta firmando. Los servicios proveídos por Legalex GS para la firma electrónica solo realizán la consulta en tiempo real ante el SAT para verificar que los archivos de firma con los cuales se intenta realizar el proceso de firma son veridicos, actuales y no se encuentran revocados.

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p30">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r30" aria-expanded="false" aria-controls="r30">
                                            33.- ¿Cómo activo mi cuenta?

                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r30" aria-expanded="false" aria-controls="r30"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r30" class="collapse" aria-labelledby="headingThree" data-parent="#accordion2">
                                    <div class="card-body">
                                        El proceso de "Activación de cuenta" es muy sencillo, solo se realiza una ocasión; recibe mediante un correo electrónico un usuario y contraseña temporal, accede a Covenant Firma, siguiendo las indicaciones del proceso y en cuestión de minutos estarás firmando electrónicamente.

                                    </div>
                                </div>
                            </div>



                        </div>
                    </div>




<br>
<!--------------------------------------------------------------------------------- A  P   I ------------------------------------------------------------->

<div id="accordion3" class="row mob-faq justify-content-center mb-5">
                        <div id="" class="accordion3 col-md-11 ">
                            <div class="ts-title  mt-4">
                                 <h3 class="bold" style="font-size:2rem; color:#c56122"><b>Covenant App</b></h3>
                            </div>

                            <div class="card">
                                <div class="card-header faq" id="headingOne">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link  pregunta_search" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            1.- ‍¿Qué es la aplicación móvil de Covenant Firma?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>

                                <div id="collapseOne" class="collapse " aria-labelledby="headingOne" data-parent="#accordion3">
                                    <div class="card-body">
                                        Covenant Firma es la versión móvil que se encuentra en la Play Store y App Store y facilita la firma de documentos sin importar el tipo de firma que sea involucrada (firma electrónica avanzada o firma autógrafa digital).
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="headingTwo">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            2.- ¿Qué necesidades resuelve la aplicación móvil de Covenant Firma?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion3">
                                    <div class="card-body">
                                    <dl class="vinetas">
                                            <li>Firmar desde cualquier lugar con un dispositivo móvil. Unicamente se requiere descargar la aplicación Covenant Firma y tener acceso a internet.</li>
                                            <li>Recepción de notificaciones cuando se requiere la firma.</li>
                                            <li>Configurar su aplicación móvil para poder utilizar los biométricos del dispositivo móvil y tener un acceso fácil a su aplicación y su firma.</li>
                                            <li>Firma múltiple de documentos en cuestion de segundos.</li>
                                            <li>Tener acceso en todo momento a los documentos firmados.</li>
                                            <li>No es necesario cargar el certificado de la e-firma en cada ocasión que se requiera firmar, ya que solo se carga una vez, quedando guardado unicamente en el dispositivo móvil.</li>
                                        </dl>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="headingThree">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            3.- ¿Cómo puedo ser usuario de la aplicación móvil de Covenant Firma?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion3">
                                    <div class="card-body">
                                        Todos los firmantes que han recibido un documento a firma desde Covenant y que ya confirmaron su cuenta desde un ambiente web pueden descargar, instalar e iniciar sesión en la aplicación móvil para disfrutar de sus beneficios.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p4">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r4" aria-expanded="false" aria-controls="r4">
                                            4.- ‍¿Cómo puedo obtener la aplicación de Covenant Firma?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r4" aria-expanded="false" aria-controls="r4"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r4" class="collapse" aria-labelledby="headingThree" data-parent="#accordion3">
                                    <div class="card-body">
                                        Dirigiéndose a la Play Store (dispositivos Android) o App Store (dispositivos iOS), después buscar la aplicación llamada Covenant Firma, la cual se puede descargar de forma gratuita seleccionando la opción Instalar.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p5">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r5" aria-expanded="false" aria-controls="r5">
                                            5.- ¿Cómo actualizo mis archivos de firma dentro de la app móvil de Covenant Firma?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r5" aria-expanded="false" aria-controls="r4"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r5" class="collapse" aria-labelledby="headingThree" data-parent="#accordion3">
                                    <div class="card-body">
                                        Ingresando a la aplicación móvil en la opción "Actualizar Certificado" del menú lateral izquierdo, dar clic en "Seleccionar archivo .cer", aparecerá un buscador de archivos donde deberá buscar y seleccionar el archivo que corresponde, posteriormente al elegirlo deberá repetir los pasos, pero ahora seleccionando el botón "Seleccionar archivo .key" para terminar de cargar los nuevos archivos de firma (e.firma o FIEL), por ultimo se deberá ingresar la contraseña o clave privada del certificado y presionar el botón Actualizar Certificado.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p6">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r6" aria-expanded="false" aria-controls="r6">
                                            6.- ¿Cómo activo el uso de biométricos para firmar desde la app móvil de Covenant Firma?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r6" aria-expanded="false" aria-controls="r6"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r6" class="collapse" aria-labelledby="headingThree" data-parent="#accordion3">
                                    <div class="card-body">
                                        Existen dos opciones para activar el uso de biométricos en su dispositivo móvil:
                                        <ol>
                                            <li>Al cargar los archivos de firma por primera vez deberá habilitar la opción del "Sensor de huella" antes de dar clic en el botón "Cargar certificado", el cual activará el uso de biométricos con que cuente su dispositivo móvil.</li>
                                            <li>Dirigiéndonos a la opción "Ajustes" dentro del menú lateral izquierdo y habilitando la opción del "Sensor de huella", el cual activará el uso de biométricos con que cuente su dispositivo móvil.</li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p7">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r7" aria-expanded="false" aria-controls="r7">
                                            7.- ¿Cómo desinstalar correctamente la app móvil de Covenant Firma?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r7" aria-expanded="false" aria-controls="r7"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r7" class="collapse" aria-labelledby="headingThree" data-parent="#accordion3">
                                    <div class="card-body">
                                    <dl class="vinetas">
                                            <li>Para iOS: <br>
                                                Identificar el icono de Covenant Firma y presionarlo 2 segundos para que se habilite el listado de opciones, donde se deberá seleccionar Eliminar app y después confirmar la eliminación.</li>
                                            <li>Para Android: <br>
                                                Deberá dirigirse al menú o icono de ajustes dentro de su dispositivo, buscar la opción Aplicaciones y posteriormente buscar la aplicación de Covenant Firma en el listado de aplicaciones, al dar clic en la aplicación aparecerá la opción "Desinstalar" y deberemos confirmar la desinstalación.</li>
                                        </dl>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p8">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r8" aria-expanded="false" aria-controls="r8">
                                            8.- ¿Cómo activar mi cuenta en Covenant Firma antes de ingresar por primera vez?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r8" aria-expanded="false" aria-controls="r8"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r8" class="collapse" aria-labelledby="headingThree" data-parent="#accordion3">
                                    <div class="card-body">
                                        Primero se debe de activar la cuenta siguiendo los pasos indicados en el correo proveniente de alguna de las cuentas oficiales de Legalex GS: <a class="Linkss" href="mailto: contacto.psc@legalexgs.com">contacto.psc@legalexgs.com</a> o <a class="Linkss" href="mailto: soporte.covenant@legalexgs.com">soporte.covenant@legalexgs.com</a>
                                        <ol>
                                            <li>Dentro del correo electrónico vendrá el usuario (RFC), el cual será utilizado para ingresar a Covenant Firma, adicional contendrá una contraseña de un único uso, la cual se utilizá para acceder por única ocasión en el proceso de confirmación de cuenta.</li>
                                            <li>Dentro del mismo correo contiene un botón que te llevará a Covenant Firma <a class="Linkss" href="https://www.legalexgs.com/covenant_firma/" target="_blank">(https://www.legalexgs.com/covenant_firma/)</a>.</li>
                                            <li>Dentro de la página de inicio de Covenant Firma, deberás copiar y pegar el usuario y la contraseña que te fue proporcionada en el correo electrónico y dar clic en el botón "Entrar".</li>
                                            <li>Si los datos son correctos, al ingresar Covenant Firma te pedirá que cambies la contraseña por una diferente a la que llegó en el correo electrónico.</li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p9">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r9" aria-expanded="false" aria-controls="r9">
                                            9.- ¿Cómo ingreso a firmar por segunda ocasión?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r9" aria-expanded="false" aria-controls="r9"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r9" class="collapse" aria-labelledby="headingThree" data-parent="#accordion3">
                                    <div class="card-body">
                                        Al abrir la aplicación móvil tu usuario quedará registrado y únicamente se solicitará ingresar tu contraseña, o en caso de tener activado el uso de biométricos, podrá ingresar con el uso de la huella dactilar o el reconocimiento facial.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p10">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r10" aria-expanded="false" aria-controls="r10">
                                            10.- ¿Se pueden seleccionar varios documentos de Firma Electrónica Avanzada y Firma de Trazo para firmar al mismo tiempo?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r10" aria-expanded="false" aria-controls="r10"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r10" class="collapse" aria-labelledby="headingThree" data-parent="#accordion3">
                                    <div class="card-body">
                                        No, para poder firmar más de un documento al mismo tiempo debén ser del mismo tipo de firma, es decir, todos los documentos seleccionados para ser firmados con Firma Electrónica Avanzada o con Firma Autógrafa Digital (trazo).
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p11">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r11" aria-expanded="false" aria-controls="r11">
                                            11.- ¿Cuál es el máximo de documentos que puedo firmar al mismo tiempo?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r11" aria-expanded="false" aria-controls="r11"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r11" class="collapse" aria-labelledby="headingThree" data-parent="#accordion3">
                                    <div class="card-body">
                                        Por medio de la App móvil puede firmar hasta 6 documentos al mismo tiempo, y al hacerlo vía web puede firmar un máximo de 10 documentos de forma simultánea.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p12">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r12" aria-expanded="false" aria-controls="r12">
                                            12.- ¿Qué es la figura del no repudio?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r12" aria-expanded="false" aria-controls="r12"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r12" class="collapse" aria-labelledby="headingThree" data-parent="#accordion3">
                                    <div class="card-body">
                                        Consiste en que la firma electrónica avanzada contenida en el o los documentos electrónicos garantiza la autoría e integridad del documento y que dicha firma corresponde exclusivamente al firmante. Es decir, el no repudio evita que el emisor o el receptor nieguen la firma y recepción del documento firmado. Así, cuando se envía un documento a firma, el receptor puede comprobar que, el emisor envió el documento. Por otra parte, cuando se recibe un documento para ser firmado, el emisor puede verificar que, el receptor recibió el documento.
                                    </div>

                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p13">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r13" aria-expanded="false" aria-controls="r13">
                                            13.- ¿Qué es la firma autógrafa?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r13" aria-expanded="false" aria-controls="r13"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r13" class="collapse" aria-labelledby="headingThree" data-parent="#accordion3">
                                    <div class="card-body">
                                        La Firma Autógrafa Digital (FAD), es conocida como una firma electrónica simple, con la cual se puede dar un sustento legal mínimo necesario pero se considera repudiable. Está se realiza dibujando el trazo de la firma autógrafa que comunmente se realiza en papel, es decir, se realiza la misma firma autógrafa pero en lugar de utilizar bolígrafo y papel se realiza de forma digital.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p14">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r14" aria-expanded="false" aria-controls="r14">
                                            14.- ¿Qué es la firma electrónica avanzada?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r14" aria-expanded="false" aria-controls="r14"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r14" class="collapse" aria-labelledby="headingThree" data-parent="#accordion3">
                                    <div class="card-body">
                                        Es el conjunto de datos y caracteres que permite la identificación del firmante, está ha sido creada por medios electrónicos bajo el exclusivo control del firmante, de manera que está vinculada únicamente al mismo y a los datos a los que se refiere, lo que permite que sea detectable cualquier modificación ulterior de éstos, y a su vez, esta firma produce los mismos efectos jurídicos que la firma autógrafa.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p15">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r15" aria-expanded="false" aria-controls="r15">
                                            15.- ¿Qué hacer si no puedo ingresar?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r15" aria-expanded="false" aria-controls="r15"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r15" class="collapse" aria-labelledby="headingThree" data-parent="#accordion3">
                                    <div class="card-body">
                                        Deberás de verificar que se está ingresando la contraseña correctamente teniendo en cuenta espacios, el uso de mayúsculas y minúsculas. En caso de no recordar la contraseña, puede seleccionar el botón Recuperar Contraseña, ingresar su correo electrónico con el que fue registrada su cuenta y dar clic sobre el botón "Recuperar", después recibirá un correo electrónico con los pasos a seguir para finalizar este proceso.
                                        <br>
                                        Nota: Si no recibe el correo electrónico, deberá revisar su bandeja de SPAM o correo no deseado.
                                        <br>
                                        Si no recuerda la cuenta de correo con la que fue registrada su cuenta o no tiene acceso a la misma, deberá contactar a Soporte Técnico a través del Chat que se encuentra integrado al ingresar a Covenant Firma: <a class="Linkss" href="https://www.legalexgs.com/covenant_firma/" target="_blank">https://www.legalexgs.com/covenant_firma/</a> o enviando un correo electrónico con sus datos a <a class="Linkss" href="mailto:soporte@legalexgs.com">soporte@legalexgs.com</a>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p16">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r16" aria-expanded="false" aria-controls="r16">
                                            16.- ¿Qué significa el mensaje: “Cuenta vinculada”?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r16" aria-expanded="false" aria-controls="r16"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r16" class="collapse" aria-labelledby="headingThree" data-parent="#accordion3">
                                    <div class="card-body">
                                        Exiten dos razones por la que aparece este mensaje:
                                        <ol>
                                            <li>La cuenta fue utilizada para ingresar desde otro dispositivo móvil.</li>
                                            <li>La aplicación fue desinstalada e instalada nuevamente en el mismo dispositivo móvil.</li>
                                        </ol>
                                        Para poder ingresar, deberá dar clic sobre el botón "Solicitar un código de desvinculación", lo cual le enviará un correo electrónico con dicho código, el cuál deberá ingresar después de dar click sobre el botón "Ingresar código de desvinculación".
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p17">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r17" aria-expanded="false" aria-controls="r17">
                                            17.- ¿Puedo firmar sin subir certificados?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r17" aria-expanded="false" aria-controls="r17"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r17" class="collapse" aria-labelledby="headingThree" data-parent="#accordion3">
                                    <div class="card-body">
                                        Si, siempre y cuando el tipo de firma solicitada sea Firma autógrafa digital (trazo), de lo contrario deberá subir sus archivos de firma.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p18">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r18" aria-expanded="false" aria-controls="r18">
                                            18.- ¿Cómo obtener mis archivos de e.firma o FIEL para poder realizar la Firma Electrónica dentro de Covenant Firma?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r18" aria-expanded="false" aria-controls="r18"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r18" class="collapse" aria-labelledby="headingThree" data-parent="#accordion3">
                                    <div class="card-body">
                                        La solicitud de e.firma o FIEL se realiza ante el Sistema de Administración Tributaria (SAT), este proceso es propio del SAT y Legalex GS no interviene en el, ni en las contraseñas de firma que se generan durante este proceso. Para más información sobre la emisión de la Firma Electrónica Avanzada visita el portal oficial del SAT o da clic en el siguiente enlace: <a class="Linkss" href="https://www.sat.gob.mx/tramites/16703/obten-tu-certificado-de-e.firma-(antes-firma-electronica)" target="_blank">https://www.sat.gob.mx/tramites/16703/obten-tu-certificado-de-e.firma-(antes-firma-electronica)</a>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p19">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r19" aria-expanded="false" aria-controls="r19">
                                            19.- ¿Cómo cancelo o declino mi firma en un documento?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r19" aria-expanded="false" aria-controls="r19"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r19" class="collapse" aria-labelledby="headingThree" data-parent="#accordion3">
                                    <div class="card-body">
                                        Al ingresar a Covenant Firma, nos dirigimos a la sección Documentos Pendientes y después de seleccionar el o los archivos que se quieren cancelar, se deberá presionar el botón de "Firma" o "Firmar documento(s)" para que la plataforma nos carge la visualización de los documentos que se seleccionaron. Después deberemos presionar el botón de color rojo "Cancelar Documento" en la parte superior e ingresar el motivo de cancelación, al terminar deberá dar clic el botón "Continuar".
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p20">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r20" aria-expanded="false" aria-controls="r20">
                                            20.- ¿Cómo puedo recuperar mi contraseña de acceso?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r20" aria-expanded="false" aria-controls="r20"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r20" class="collapse" aria-labelledby="headingThree" data-parent="#accordion3">
                                    <div class="card-body">
                                        Si no recuerda su contraseña para acceder, puede recuperarla ingresando a su aplicación móvil y deberá de seleccionar la opción "Recuperar Contraseña", aparecerá una ventana donde ingresará el correo electrónico con el que fue registrado y por ultimo dar clic sobre el botón "Recuperar", si los datos fueron correctos, recibirá un correo electrónico con los datos y pasos para cambiar su contraseña.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


<br>
<!---------------------------------------------------------------------------------S E L L O  D I G I T A L ---------------------------------------------->

<div id="accordion4" class="row mob-faq justify-content-center mb-5">
                        <div id="" class="accordion4 col-md-11 ">
                             <div class="ts-title  mt-4">
                                 <h3 class="bold" style="font-size:2rem; color:#c56122"><b>Sello Digital de Tiempo</b></h3>
                            </div>

                        
                            <div class="card">
                                <div class="card-header faq" id="headingOne">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link  pregunta_search" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            1.- ‍¿Qué es?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>

                                <div id="collapseOne" class="collapse " aria-labelledby="headingOne" data-parent="#accordion4">
                                    <div class="card-body">
                                        Es un recurso tecnológico, que permite demostrar que una serie de datos han existido en un determinado momento y no han sido alterados a través del tiempo.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="headingTwo">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            2.- ¿Quién puede emitir un Sello Digital de Tiempo?

                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion4">
                                    <div class="card-body">
                                        ‍‍Solo un Prestador de Servicios de Certificación acreditado por la Secretaría de Economía.

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="headingThree">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            3.- ¿Diferencia entre el Sello de tiempo y las Constancias de Conservación de Mensajes de Datos (CCMD) conforme la NOM-151-SCFI-2016?

                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion4">
                                    <div class="card-body">
                                        Un sello digital de tiempo brinda la fecha y hora exacta en la que ocurrio una transacción electrónica, mientras que una constancia de conservación de mensajes de datos conforme a la NOM-151-SCFI-2016 otorga presunción de integridad y la obtención de válidez a través del tiempo, ante una autoridad dentro del territorio mexicano.

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p4">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r4" aria-expanded="false" aria-controls="r4">
                                            4.- ‍¿Cuáles son los documentos a los que se les puede aplicar un Sello de Tiempo?


                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r4" aria-expanded="false" aria-controls="r4"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r4" class="collapse" aria-labelledby="headingThree" data-parent="#accordion4">
                                    <div class="card-body">
                                        Cualquier tipo de documento que requiera de una certeza de fecha y hora especifica en la que este fue emitido o firmado, adicionando que el sello digital de tiempo por si solo puede ser incluido en casi cualquier tipo de archivo por ejemplo de imagen o video hasta un documento editable como es un word, excel, PDF, etc.


                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p5">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r5" aria-expanded="false" aria-controls="r5">
                                            5.- ¿Tiene vigencia, cuánto tiempo?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r5" aria-expanded="false" aria-controls="r4"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r5" class="collapse" aria-labelledby="headingThree" data-parent="#accordion4">
                                    <div class="card-body">
                                        No tiene vigencia, el Sello Digital de Tiempo brinda la existencia de un contenido en una fecha y hora exacta.

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p6">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r6" aria-expanded="false" aria-controls="r6">
                                            6.- ¿Cuánto cuesta el servicio de Sellos Digitales de Tiempo?

                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r6" aria-expanded="false" aria-controls="r6"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r6" class="collapse" aria-labelledby="headingThree" data-parent="#accordion4">
                                    <div class="card-body">
                                        Para más información sobre nuestros servicios, paquetes o planes de venta puede contactarnos en contacto@legalexgs.com o a los teléfonos: <a class="Linkss" href="tel: +52 443 690 6851 ">(+52) 443 690 6851 </a> 

                                    </div>
                                </div>
                            </div>





                        </div>
                    </div>
<br>
<!---------------------------------------------------------------------------------C O N S T A N C I A N O M --------------------------------------------->

<div id="accordion5" class="row mob-faq justify-content-center mb-5">
                        <div id="" class="accordion5 col-md-11 ">
                             <div class="ts-title  mt-4">
                                <h3 class="bold" style="font-size:2rem; color:#c56122"><b>Constancia NOM-151</b></h3>
                            </div>
                       
                            <div class="card">
                                <div class="card-header faq" id="headingOne">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link  pregunta_search" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            1.- ‍¿Qué es?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>

                                <div id="collapseOne" class="collapse " aria-labelledby="headingOne" data-parent="#accordion5">
                                    <div class="card-body">
                                        Una constancia de conservación de mensajes de datos conforme la NOM-151-SCFI-2016, propiamente es un documento o archivo electrónico que permite garantizar que la información que es recibida de forma electrónica (bajo una solicitud) al igual que un sello digital de tiempo, existió en un tiempo y con características específicas.
                                        <br><br>
                                        Esto permite resguardar una evidencia informática de que un archivo no ha sido modificado y se conserva de forma íntegra por un tercero de confianza. El principal beneficio de conservar un mensaje de datos conforme la NOM-151 es la validez a través del tiempo ante una autoridad dentro del territorio mexicano, otorgando certeza jurídica y valor probatorio en juicio; ya que permite demostrar su integridad, en caso de que el documento hubiera sido alterado posteriormente.





                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="headingTwo">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            2.- ¿Quién puede emitir una Constancia de Conservación de Mensajes de Datos?


                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion5">
                                    <div class="card-body">
                                        ‍‍Solo un Prestador de Servicios de Certificación acreditado por la Secretaría de Economía.

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="headingThree">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            3.- ¿Para qué me sirve?


                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion5">
                                    <div class="card-body">
                                        Para brindar a una autoridad dentro del territorio mexicano la certeza que el documento fue creado en el momento que se realizó la operación y no fue creado en el momento que se realiza la revisión por parte de las autoridades. Así como la obtención de integridad y conservación del documento a través del tiempo.


                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p4">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r4" aria-expanded="false" aria-controls="r4">
                                            4.- ¿A cuáles documentos puedo aplicar una CCMD-NOM-151?




                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r4" aria-expanded="false" aria-controls="r4"><i class=" expanded more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r4" class="collapse" aria-labelledby="headingThree" data-parent="#accordion5">
                                    <div class="card-body">
                                        Solo a archivos que tengan extensión PDF, pudiendo ser: contratos, acuerdos de confidencialidad, códigos de ética, pagares, fideicomisos, cartas instrucción, avalúos, etc.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p5">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r5" aria-expanded="false" aria-controls="r5">
                                            5.- ¿Tiene vigencia? ¿Cuánto tiempo?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r5" aria-expanded="false" aria-controls="r4"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r5" class="collapse" aria-labelledby="headingThree" data-parent="#accordion5">
                                    <div class="card-body">
                                        Al aplicar la Constancia de Conservación de Mensajes de Datos, garantiza que el documento se conserva íntegro desde la fecha en que se le aplicó la Constancia de Conservación de Mensajes de Datos y hasta por un periodo de 10 años.


                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p6">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r6" aria-expanded="false" aria-controls="r6">
                                            6.- ¿Cuánto cuesta el servicio de emisión de Constancia de Conservación de Mensajes de Datos?


                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r6" aria-expanded="false" aria-controls="r6"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r6" class="collapse" aria-labelledby="headingThree" data-parent="#accordion5">
                                    <div class="card-body">
                                        Para más información sobre nuestros servicios, paquetes o planes de venta puede contactarnos en contacto@legalexgs.com o a los teléfonos: <a class="Linkss" href="tel: +52 443 690 6851 ">(+52) 443 690 6851 </a> 


                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p7">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r7" aria-expanded="false" aria-controls="r7">
                                            7.- ¿Qué es “fecha cierta”?



                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r7" aria-expanded="false" aria-controls="r7"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r7" class="collapse" aria-labelledby="headingThree" data-parent="#accordion5">
                                    <div class="card-body">
                                        Es un es un requisito exigible respecto de los documentos privados que se presentan a la autoridad fiscal como consecuencia del ejercicio de sus facultades de comprobación, que los contribuyentes tienen el deber de conservar para demostrar la adquisición de un bien o la realización de un contrato u operación que incida en sus actividades fiscales.
                                    </div>
                                </div>
                            </div>




                        </div>
                    </div>
<br>
<!---------------------------------------------------------------------------------P L A N E S C O V E N A T --------------------------------------------->

<div id="accordion6" class="row mob-faq justify-content-center mb-5">
                        <div id="" class="accordion6 col-md-11 ">
                            <div class="ts-title  mt-4">
                                <h3 class="bold" style="font-size:2rem; color:#c56122"><b>Planes de Covenant</b></h3>
                                 
                            </div>
                        
                            <div class="card">
                                <div class="card-header faq" id="headingOne">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link  pregunta_search" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            1.- ‍¿Cómo puedo obtener mi Firma Electrónica?

                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>

                                <div id="collapseOne" class="collapse " aria-labelledby="headingOne" data-parent="#accordion6">
                                    <div class="card-body">
                                        Para más información acerca e cómo obtener tu firma electrónica avanzada visita el siguiente link <a class="Linkss" href="https://www.gob.mx/tramites/ficha/obtencion-de-e-firma/SAT137" target="_blank">https://www.gob.mx/tramites/ficha/obtencion-de-e-firma/SAT137</a>
                                        Encontrarás cómo obtener tu Firma Electrónica Avanzada emitida por el SAT.


                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="headingTwo">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            2.- ¿Qué hacer si deseo incluir más documentos a mi plan contratado?


                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion6">
                                    <div class="card-body">
                                        Puedes cambiar tu suscripción al siguiente plan para obtener más beneficios.

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="headingThree">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            3.- ¿Conexión vía API?



                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion6">
                                    <div class="card-body">
                                        Podemos hacer la integración Vía API al contratar nuestros servicios con el Plan Corporativo.
                                        SERVICIOS API:
                                        Conjunto de métodos y funciones que permite la conexión con los servicios de Covenant a través de la plataforma del cliente (servicio opcional). Como requerimiento principal, la empresa debe desarrollar un cliente WebService de tipo Rest, para enviar y recibir la información, esto de acuerdo con el lenguaje compatible con su propio desarrollo.

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p4">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r4" aria-expanded="false" aria-controls="r4">
                                            4.- ¿Puedo agregar más usuarios a mi plan contratado?





                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r4" aria-expanded="false" aria-controls="r4"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r4" class="collapse" aria-labelledby="headingThree" data-parent="#accordion6">
                                    <div class="card-body">
                                        No, cada Plan contiene un número de usuarios limitado, si desea agregar más usuarios, le sugerimos suscribirse a un Plan mayor que satisfaga sus necesidades.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p5">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r5" aria-expanded="false" aria-controls="r5">
                                            5.- ¿Cuál es el método de pago?


                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r5" aria-expanded="false" aria-controls="r4"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r5" class="collapse" aria-labelledby="headingThree" data-parent="#accordion6">
                                    <div class="card-body">
                                        Puede hacer el pago por suscripción mensual o bien, hacerlo de forma anual de acuerdo con su preferencia.


                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p6">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r6" aria-expanded="false" aria-controls="r6">
                                            6.- ¿Qué pasa con los documentos que no utilice durante el mes?



                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r6" aria-expanded="false" aria-controls="r6"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r6" class="collapse" aria-labelledby="headingThree" data-parent="#accordion6">
                                    <div class="card-body">
                                        Al finalizar el mes, los documentos se cancelan y no podrá hacer uso de los restantes.
                                        Al pagar la suscripción mensual renueva sus beneficios.


                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p8">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r8" aria-expanded="false" aria-controls="r8">
                                            7.- ¿Puedo adquirir más firmas de las que incluye mi plan contratado?




                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r8" aria-expanded="false" aria-controls="r8"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r8" class="collapse" aria-labelledby="headingThree" data-parent="#accordion6">
                                    <div class="card-body">
                                        Legalex puede ofrecerle las firmas que usted necesite, puede contratar paquetes de firmas, de acuerdo con sus necesidades.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p9">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r9" aria-expanded="false" aria-controls="r9">
                                            8.- ¿Qué pasa si excedo o no termino las firmas adquiridas?

                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r9" aria-expanded="false" aria-controls="r9"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r9" class="collapse" aria-labelledby="headingThree" data-parent="#accordion6">
                                    <div class="card-body">
                                        Las firmas que Legalex ofrece se reinician mensualmente, y en caso de excederse tendrá un costo adicional el cual será facturado mensualmente.

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p10">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r10" aria-expanded="false" aria-controls="r10">
                                            9.- ¿El servicio de Constancias de Conservación de Mensajes de Datos NOM-151 está disponible para todos los planes?


                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r10" aria-expanded="false" aria-controls="r10"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r10" class="collapse" aria-labelledby="headingThree" data-parent="#accordion6">
                                    <div class="card-body">
                                        El servicio de las CCMD únicamente esta disponible para el Plan Corporativo, los planes personal, negocios y empresarial únicamente incluyen el servicio de Firma Electrónica.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p11">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r11" aria-expanded="false" aria-controls="r11">
                                            10.- ¿Puedo contratar únicamente el Servicio de Constancias de Conservación de Mensajes de Datos NOM-151 y Sello Digital de Tiempo?


                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r11" aria-expanded="false" aria-controls="r11"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r11" class="collapse" aria-labelledby="headingThree" data-parent="#accordion6">
                                    <div class="card-body">
                                        Si, Legalex puede ofrecer el Servicio que usted requiere de forma individual, tendrá que contactar a ventas vía llamada telefónica o dejandonos un correo electrónico para escoger el servicio deseado y uno de nuestros asesores de ventas con gusto lo atenderá.

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p12">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r12" aria-expanded="false" aria-controls="r12">
                                            11.- ¿Tienen costo los usuarios adicionales?


                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r12" aria-expanded="false" aria-controls="r12"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r12" class="collapse" aria-labelledby="headingThree" data-parent="#accordion6">
                                    <div class="card-body">
                                        Cada plan tiene un número de usuarios determinado, si desea más usuarios deberá cambiar de plan. Para el caso del Plan Corporativo el número de usuarios es ilimitado.


                                    </div>
                                </div>
                            </div>



                        </div>
                    </div>

<br>
<!---------------------------------------------------------------------------------D E F I N I C I O N E S ---------------------------------------------->

<div id="accordion7" class="row mob-faq justify-content-center mb-5">
                        <div id="" class="accordion7 col-md-11 " >
                            <div class="ts-title  mt-4">       
                                <h3 class="bold" style="font-size:2rem; color:#c56122"><b>Definiciones</b></h3>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="headingOne">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link  pregunta_search" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            Secretaría de Economía Federal
                                        </button>
                                    </h5>
                                    <span id="pj1" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>

                                <div id="collapseOne" class="collapse " aria-labelledby="headingOne" data-parent="#accordion7">
                                    <div class="card-body">
                                        La Secretaría de Economía es una de las secretarías de estado que integran el denominado gabinete legal del Presidente de México. Es el despacho del poder ejecutivo federal encargado de la administración, regulación y fomento de la industria, el comercio y la prestación de servicios.
                                        <br><br>
                                        Es la encargada de diseñar, planear, ejecutar y coordinar las políticas públicas en materia de desarrollo económico. Lo anterior incluye establecer, vigilar y regular los precios en productos y servicios del sector privado; inspeccionar el cumplimiento de leyes y normatividades en la comercialización de productos y servicios;
                                        colaborar con la secretaría de Hacienda en la fijación de impuestos y aranceles a las actividades comerciales e industriales; operar la Procuraduría Federal del Consumidor; establecer y normar las bases de datos de la propiedad industrial y mercantil; administrar el buen uso de la Norma Oficial Mexicana y el Sistema Internacional de Unidades; y en coordinación con la Secretaría de Bienestar, fomentar el desarrollo económico a partir de formas de organización colectiva en comunidades rurales o de alta marginación urbana.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="headingTwo">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            ¿Qué es la infraestructura de clave pública o PKI cuál es su relación con la firma electrónica?

                                        </button>
                                    </h5>
                                    <span id="pj2" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion7">
                                    <div class="card-body">
                                        Una infraestructura de claves públicas (PKI) es un sistema de recursos, políticas y servicios que da soporte al uso del cifrado de claves públicas para autenticar a las partes que participan en una transacción.
                                        <br><br>
                                        No hay ningún estándar individual que defina los componentes de una Infraestructura de clave pública, pero normalmente un PKI consta de entidades emisoras de certificados (CA) y entidades emisoras de registro (RA). Las CA proporcionan los servicios siguientes:
                                        <br><br>
                                        Emisión de certificados digitales <br>
                                        Validación de certificados digitales <br>
                                        Revocación de certificados digitales <br>
                                        Distribución de claves públicas <br>
                                        El estándar X.509 proporcionan la base para la industria estándar de la infraestructura Public Key Infrastructure. <br><br>
                                        Consulte Certificados digitales para obtener más información sobre los certificados digitales y las entidades emisoras de certificados (CA). Las RA verifican la información proporcionada cuando se solicitan certificados digitales. Si la RA verifica esta información, la CA puede emitir un certificado digital para el solicitante.
                                        <br><br>
                                        Una PKI también puede proporcionar las herramientas para gestionar los certificados digitales y las claves públicas. Una PKI se describe a veces como una jerarquía fiable para la gestión de certificados digitales, aunque la mayor parte de las definiciones incluyen servicios adicionales. Algunas definiciones incluyen servicios de cifrado y firma digital, pero estos servicios no son esenciales para el funcionamiento de una PKI.

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="headingThree">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            ¿Qué es el Paper Less?

                                        </button>
                                    </h5>
                                    <span id="pj3" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion7">
                                    <div class="card-body">
                                        Paperless es un concepto innovador que invita a trabajar sin papel en las oficinas, creado por la compañía Paperless en Chile en 2002, con la finalidad de digitalizar los documentos de las empresas, trabajando en el proceso de clean technology para reducir la huella de carbono en el planeta.
                                        <br><br>
                                        En la actualidad el sector empresarial invierte en la transformación de su negocio a la era digital, no obstante, continúa invirtiendo en el uso y almacenamiento de papeles, por lo tanto es importante que los empresarios comprendan que la reducción del uso de documentos físicos implica una mejora en el tiempo que se requiere para organizar, clasificar, almacenar y transportar archivos.
                                        <br><br>
                                        Sin embargo, pese a la resistencia a implementar este concepto, cabe resaltar que una de las áreas más beneficiadas en el uso de paperless es de facturación, al ser uno de los departamentos que más papel genera, la facturación electrónica ha sido acogida rápidamente en el sector comercial.
                                        <br><br>
                                        La implementación de paperless en tu empresa trae consigo grandes beneficios:
                                        <br><br>
                                        – Al digitalizar los documentos será mucho más fácil encontrar, almacenar y compartir cualquier archivo, desde cualquier lugar.
                                        <br><br>
                                        – Agiliza la automatización de los procesos, permitiendo mejorar el flujo de trabajo gracias a la facilidad de acceder a los documentos de manera inmediata y lograr identificar qué tareas hay inconclusas.
                                        <br><br>
                                        – Aumento en la productividad del personal de la oficina, pues ya no tienen que invertir tiempo en imprimir, fotocopiar y almacenar documentos, sin mencionar el tiempo que requiere la pérdida de archivos que se pueden traspapelar.
                                        <br><br>
                                        – La implementación de paperless en la empresa contribuye con la reducción de costos en compra de papeles y en mobiliario de almacenamiento, mientras promueve el concepto de empresa eco amigable.
                                        <br><br>
                                        – En el caso de la pequeña y mediana empresa tener una oficina sin papeles la puede llevar a no necesitar espacios grandes y disminuir costos de alquiler.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r4" aria-expanded="false" aria-controls="r4">
                                            ¿Qué es el no repudio?
                                        </button>
                                    </h5>
                                    <span id="pj4" data-toggle="collapse" data-target="#r4" aria-expanded="false" aria-controls="r4"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r4" class="collapse" aria-labelledby="headingThree" data-parent="#accordion7">
                                    <div class="card-body">
                                        No repudio se refiere a un estado de negocios donde el supuesto autor de una declaración no es capaz de desafiar con éxito la validez de declaración o contrato. El término es a menudo visto en un entorno legal donde la autenticidad de una firma está siendo desafiada. En tal la autenticidad se está "repudiando".
                                        <br>
                                        <br>
                                        <h5>No repudio en seguridad digital</h5>
                                        Con respecto a la seguridad digital, el significado criptológico y aplicación de los cambios de no repudio significa:
                                            <dl class="vinetas">
                                            <li>Un servicio que proporciona pruebas de la integridad y origen de los datos.</li>
                                            <li>Una autentificación que con un alto aseguramiento pueda ser reafirmado como genuino.</li>
                                            <li>Pruebas de la integridad de los datos es típicamente el más fácil de estos requerimientos para ser cumplido. Un resumen criptográfico de datos, tal como el SHA2, es usualmente suficiente para establecer que la probabilidad de que cambios en los datos no se detecten sea extremadamente bajo. Incluso con esta seguridad, es todavía posible alterar los datos en tránsito, a través de un “hombre en el medio” o fraude electrónico. Debido a este flujo, la integridad de los datos es mejor reafirmada cuando el recipiente ya posee la información necesaria de verificación.</li>
                                        </dl>

                                        El método más común de reafirmar el origen digital de los datos es a través de certificados digitales, una forma de clave de infraestructura publica, la cual firma digitalmente la pertenencia. Ello puede usar también la encriptación. El origen digital únicamente significa que el certificado o firma de los datos puede ser, con razonable certeza, confiable en ser de alguien quien posee la clave privada correspondiente al certificado firmado. Si la clave no es apropiadamente protegida por el propietario original, la falsificación digital puede llegar a ser una preocupación mayor.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r5" aria-expanded="false" aria-controls="r5">
                                            ¿Qué es una (Prestador de Servicios de Certificación (PSC)?
                                        </button>
                                    </h5>
                                    <span id="pj5" data-toggle="collapse" data-target="#r5" aria-expanded="false" aria-controls="r5"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r5" class="collapse" aria-labelledby="headingThree" data-parent="#accordion7">
                                    <div class="card-body">
                                        Un PSC es una organización pública o privada que está facultada por una autoridad para, entre otras cosas, expedir certificados de firma electrónica y emitir constancias de conservación de datos ( <a class="link-azul" class="Linkss" href="nom-151">NOM 151</a> ).
                                        La Secretaría de Economía es la autoridad principal que se encarga de otorgar los permisos a los PSCs (aquí una lista).
                                        Sin embargo, el Banco de México también está facultado para otorgar estos permisos. El SAT es un PSC autorizado por el Banco de México.
                                        Es común que la gente asocie los certificados que emite el SAT (e.firma/FIEL), en su carácter de PSC, con trámites y documentos fiscales.
                                        No obstante, los certificados que emite el SAT legalmente son equivalentes a los certificados que emiten los PSCs de la Secretaría de Economía y por lo tanto, pueden también ser usados en contextos no fiscales.
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>


<br>
<!---------------------------------------------------------------------------------P I E D E P A G I N A ------------------------------------------------>
                    <?php
                    include 'inc/pie.php';
                    ?>
<!---------------------------------------------------------------------------------S E L L O  D I G I T A L-------------------------------------------->



                </div>



                
            </div>
        </div>
    </section>
</main>

<script type="text/javascript" src="assets/js/faqs.js"></script>
<script>
    switch (window.location.search) {
        case '?faq=secretaria-de-economia-federal':
            $('#pj1').click();
            break;
        case '?faq=firma-electronica-pki':
            $('#pj2').click();
            break;
        case '?faq=paper-less':
            $('#pj3').click();
            break;
        case '?faq=no-repudio':
            $('#pj4').click();
            break;
        case '?faq=que-es-una-psc':
            $('#pj5').click();
            break;
        default:
            break;
    }
</script>