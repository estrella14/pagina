<?php

$pageLink = "surety";
include 'inc/cab.php';

?>

    <!--Content
    ====================================================================================================================
    -->
    <div class="ts-content">
        <!--HERO HEADER
        ================================================================================================================
        ================================================================================================================
        -->
        <header id="ts-hero" class="ts-height--100vh text-white">

<div class="owl-carousel ts-slider" data-owl-dots="1" data-owl-loop="1" data-owl-items="1">

    <div class="ts-slide ts-has-overlay">
        <div class="container">
        <h1>
                <a>SURETY</a>
            </h1>
            <h4>Sistema de Garantías Dinámicas y Financiamientos Estructurados.</h4>
            <a href="Soluciones/Surety#blockquote" class="btn btn-outline-light">Conoce más</a>
        </div>
        <div class="ts-background">
            <div class="ts-img-into-bg ts-background-image ts-parallax-element"  >
              <img src="assets/img/slide1.jpg" alt=""  >
            </div>
        </div>
    </div>
    

    

</div>

</header>

        <!--MAIN CONTENT
        ================================================================================================================
        ================================================================================================================
        -->
        <main id="ts-content">



       
            <section id="blockquote" class="ts-block" data-animate="ts-fadeInLeft" data-ts-delay=".1s">
                <div  class="container">
                       <center> <img src="assets/img/surety.png" alt="" style="width:20%" ></center>
                 
                    <blockquote class="blockquote mb-5 mt-5 text-center">
                    SURETY, Sistema de  Garantías Dinámicas y Financiamientos Estructurados. Es una solución informática, orientada al control eficiente de las funciones administrativas, derivadas del Financiamiento respaldado con garantías dinámicas.
                    </blockquote>

                   <hr>

                   

                </div>
            </section>

                
            <section class="ts-block text-white text-center pb-5" data-bg-color="#191a21">
                <div class="container">

                    <div class="ts-title text-center text-white">
                        <h2 class="text-white">Beneficios de Surety</h2>
                    </div>

                    <div class="row">
                    <div class="col-md-6">
                           <div class="row">
                                     <div class="col-md-2">
                                        <img  src="assets/img/icon/5.png" alt="" style="width:60px">
                                    </div>
                                    <p class="col-md-10" data-animate="ts-fadeInLeft" data-ts-delay=".1s"> Diseña, crea y gestiona todos los documentos e información para tus garantías en minutos.</p>
                                    <div class="col-md-2">
                                        <img  src="assets/img/icon/1.png" alt="" style="width:60px">
                                    </div>
                                    <p class="col-md-10" data-animate="ts-fadeInLeft" data-ts-delay=".1s"> Estructura robusta - combina el poder tecnológico con el respaldo de un fideicomiso de acuerdo a tus garantías.</p>
                                    <div class="col-md-2">
                                        <img  src="assets/img/icon/9.png" alt="" style="width:60px">
                                    </div>
                                    <p class="col-md-10" data-animate="ts-fadeInLeft" data-ts-delay=".1s"> Notificaciones y alertas vía SMS - Recibe mensajes de texto y correo electrónico.</p>
                                   
                           </div>
                        </div>
                      

                        <div class="col-md-6">
                           <div class="row">
                           <div class="col-md-2">
                                        <img  src="assets/img/icon/7.png" alt="" style="width:60px">
                                    </div>
                                    <p class="col-md-10" data-animate="ts-fadeInLeft" data-ts-delay=".1s"> Reporting 24/7 - Genera y descarga reportes en tiempo real.</p><div class="col-md-2">
                                        <img  src="assets/img/icon/2.png" alt="" style="width:60px">
                                    </div>
                                    <p class="col-md-10" data-animate="ts-fadeInLeft" data-ts-delay=".1s"> Certeza jurídica y validez legal - Siéntete totalmente seguro, tranquilo y respaldado en todas tus operaciones electrónicas ya que somos un PSC.</p><div class="col-md-2">
                                        <img  src="assets/img/icon/10.png" alt="" style="width:60px">
                                    </div>
                                    <p class="col-md-10" data-animate="ts-fadeInLeft" data-ts-delay=".1s"> Acceso fácil y rápido - Desde cualquier dispositivo móvil con conexión a internet.</p>
                           </div>
                        </div>

                    </div>
                </div>
            </section>
       
       
        

       
<?php
include 'inc/pie.php';
?>
