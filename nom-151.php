<?php
$pageLink = "nom";
include 'inc/cab.php';
?>

<style>



body {
  height: 400px;
  background-image: url("assets/img/bg__pattern--topo.png");

}
</style>
<div class="ts-content p-4">
    <header id="ts-hero" class="ts-block  justify-content-center ">

        <section id="about" class="ts-block py-2">
            <div class="container m-lg-2 m-md-2 m-xl-2 m-sm-2" data-animate="ts-fadeInDown" data-ts-delay=".30s">
                <!-- ts-block ts-height--100vh  -->
                <div class="row">
                    <h2 class="font-weight-bold"> Constancia de Conservación de Mensajes de Datos de conformidad con la NOM-151</h2>
                    <p class="text-justify">
                        Una constancia de conservación de mensajes de datos (CCMD) conforme la NOM-151-SCFI-2016, es un documento o archivo electrónico que permite garantizar que la información recibida de forma electrónica (bajo una solicitud) al igual que un sello digital de tiempo, existió en un tiempo y con características específicas.<br><br>
                        Esto permite resguardar una evidencia informática de que un archivo no ha sido modificado y se conserva de forma íntegra por un tercero de confianza, éste debe estar previamente acreditado y fungir como una autoridad siendo un Prestador de Servicios de Certificación (PSC).<br><br>
                        El principal beneficio de conservar un mensaje de datos conforme la NOM-151, es la validez a través del tiempo ante una autoridad dentro del territorio mexicano.
                    </p>
                </div>
            </div>
            <div class="row justify-content-center" data-animate="ts-fadeInDown">
                <div class="col-12 col-md-4 col-sm-4 p-2">
                    <a href="Servicios/nom/validarNOM.jsp" class="btn btn-naranja btn-sm btn-block rounded-sm" data-animate="ts-fadeInUp" data-ts-delay=".40s">Validador</a>
                </div>
                <div class="col-12 col-md-4 col-sm-4 p-2">
                    <a href="Servicios/nom/documentos" class="btn btn-naranja btn-sm btn-block rounded-sm" data-animate="ts-fadeInUp" data-ts-delay=".40s">Documentación</a>
                </div>
                <div class="col-12 col-md-4 col-sm-4 p-2">
                    <a href="Servicios/nom/certificados" class="btn btn-naranja btn-sm btn-block rounded-sm" data-animate="ts-fadeInUp" data-ts-delay=".45s">Información del Certificado</a>
                </div>
            </div>
            <center data-animate="ts-fadeInUp" data-ts-delay=".20s">
                <br>
                <div>
                    <div>
                        <h2>¿Tienes dudas?</h2>
                    </div>
                    <div>
                        <p>Consulta nuestras <a href="preguntas" style="color: #c56122;"><u><b>preguntas frecuentes.</b></u></a></p>
                    </div>

                </div>
            </center>
        </section>
        
    </header>
</div>

<?php
include 'inc/pie.php';
?>