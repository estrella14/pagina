<?php

$pageLink = "Válidar SDT";

include 'inc/cab.php';

?>

   
    <div class="ts-content" style="background-color:#f7f8f9">   
        <main id="ts-content" >
            <section class="pt-5 mt-4 mb-5  container">
                <div class="mb-5 text-center">
                    <h2>VALIDAR SELLO DIGITAL DE TIEMPO (SDT)</h2>
                    <p>Sube el archivo original y los archivos .tsq y .tsr. Nos encargaremos de verificar su validez.</p>
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-7 carta-req pt-3" >
                        <form action="https://www.legalexgs.com/legalex_tsa/Validador/validarSDT" id="form_validar_sello" method="post">
                            <div class="cuerpo-titulo">
                                <h3 class="mb-3">Validar Sello Digital de Tiempo</h3>
                                <p>Ahora sube tus archivos para poder validar la información.</p>
                            </div>
                            <div class="carta-cuerpo">
                                <div class="text-inp">
                                    <p class="mb-0 bold">Subir archivo .TSQ</p>
                                    <small id="resFile1">archivo.pdf</small>
                                </div>
                                <div class="inp-inp">
                                    <label class="btn-submit-file" for="1" id="ltsq"><i class="fas fa-upload"></i> <span> Subir archivo </span>.TSQ </label>
                                    <input type="file" id="1" accept=".tsq" name="tsq">
                                </div>
                            </div>
                    
                            <div class="carta-cuerpo">
                                <div class="text-inp">
                                    <p class="mb-0 bold">Subir archivo .TSR</p>
                                    <small  id="resFile2">archivo.pdf</small>
                                </div>
                                <div class="inp-inp">
                                    <label class="btn-submit-file" for="2" id="ltsr"><i class="fas fa-upload"></i> <span> Subir archivo </span> .TSR</label>
                                    <input type="file" id="2" name="tsr" >
                                </div>
                            </div>
                            <div class="carta-cuerpo">
                                <div  class="text-inp">
                                    <p class="mb-0 bold">Subir el documento</p>
                                    <small  id="resFile3">archivo.pdf</small>
                                </div>
                                <div class="inp-inp">
                                    <label class="btn-submit-file" for="3" id="ldoc-file"><i class="fas fa-upload"></i> <span> Seleccionar</span> </label>
                                    <input type="file" id="3" name="archivo">
                                </div>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-outline-naranja mt-4">Validar </button>
                            </div>
                            <div class="cargando hide" >
                                <img src="assets/img/logolg.png" alt="">
                                <small>Cargando...</small>
                                
                            </div>
                            <div class="pt-5">
                                <p id="id_res_error" class="pb-3 text-center hide">Ocurrio un error por favor ingrese correctamente los archivos</p>
                            </div>
                        </form>
                    </div>      
                </div>
               
                    
                
               
                
            </section>
            <section>
            <div id="resultados" class="hide">
                    <h2>Estos son los resultados de la validación</h2><br>
                    <div class="info-validacion mb-3">
                        <h5 id="mensaje-accept">Esta puede ser una nota para la validación: Lorem ipsum dolor sit amet consectetur adipisicing elit. Vel iure officia deserunt ipsum provident qui quidem atque.</h5>
                    </div>
                    <div class="tabla-validar-row">
                        <div class="validar-col">
                            <h5>Número de serie:</h5>
                            <p id="res_numserie">sdf4-ht4f-h456-456h-45f5</p>
                        </div>
                        <div class="validar-col">
                            <h5>Exactitud:</h5>
                            <p id="res_exactitud">sdf4-ht4f-h456-456h-45f5</p>
                        </div>
                        <div class="validar-col">
                            <h5>Tiempo:</h5>
                            <p id="res_tiempo">sdf4-ht4f-h456-456h-45f5</p>
                        </div>
                    </div>
                    <div class="tabla-validar-row">
                        <div class="validar-col">
                            <h5>Algoritmo:</h5>
                            <p id="res_algoritmo">sdf4-ht4f-h456-456h-45f5</p>
                        </div>
                        <div class="validar-col">
                            <h5>Versión:</h5>
                            <p id="res_version">sdf4-ht4f-h456-456h-45f5</p>
                        </div>
                        <div class="validar-col">
                            <h5>Policy:</h5>
                            <p id="res_policy">njskfdnlsdjnk nasfndjk fnms njk jk njk</p>
                        </div>
                    </div>
                    <div class="tabla-validar-row">
                        <div class="validar-col">
                            <h5>Hash:</h5>
                            <p id="res_hash">sdf4-ht4f-h456-456h-45f5</p>
                        </div>
                    </div>
                    
                </div>
            </section>
        </main>
    </div>
<?php
include 'inc/pie.php';
?>
<script>
    $('#form_validar_sello').submit(function (ev) {
        
        ev.preventDefault();
        $(".cargando").removeClass("hide");
        
    $.ajax({
        type: $('#form_validar_sello').attr('method'), 
        url:  'respuesta.php',
        data: $('#form_validar_sello').serialize(),
        success: function (data) { 
            data = JSON.parse(data);
            if(data.error == 0){
                $("#res_numserie").html(data.numero_serie);
                $("#res_exactitud").html(data.exactitud);
                $("#res_tiempo").html(data.tiempo);
                $("#res_algoritmo").html(data.algoritmo);
                $("#res_hash").html(data.hash);
                $("#res_policy").html(data.policy);
                $("#res_version").html(data.version);
                $(".cargando").addClass("hide");
                $("#resultados").removeClass("hide");
                $("#mensaje-accept").html(data.message.description);
                document.getElementById("resultados").scrollIntoView({
                    behavior:"smooth"
                });     
            }else{
                $(".cargando").addClass("hide");
                $(".carta-req").addClass("error_val");
                $("#res_mensaje").html(data.message.description);
                $("#id_res_error").show();
            }
            setTimeout(() => {
           
            }, 2000);
        },
        error: function (jqXHR, exception) {
            var msg = '';
            if (jqXHR.status === 0) {
                msg = 'Not connect.\n Verify Network.';
            } else if (jqXHR.status == 404) {
                msg = 'Requested page not found. [404]';
            } else if (jqXHR.status == 500) {
                msg = 'Internal Server Error [500].';
            } else if (exception === 'parsererror') {
                msg = 'Requested JSON parse failed.';
            } else if (exception === 'timeout') {
                msg = 'Time out error.';
            } else if (exception === 'abort') {
                msg = 'Ajax request aborted.';
            } else {
                msg = 'Uncaught Error.\n' + jqXHR.responseText;
            }
            $(".cargando").addClass("hide");    
            alert(msg);

        }, 
    });
    
});
    function validar(){
        
       
    }
        $('input[type=file]').change(function(){
        
        var filename = $(this).val().split('\\').pop();
        var idname = $(this).attr('id');
        var lname = "l"+idname;
                //Archivo Válido
            $("#"+lname+"").addClass('file-cargado');
            $("#resFile"+idname+"").html('<i class="fas fa-check"></i> <span>'+filename+' </span>');

            //Archivo invalido
            /*
            $("#"+lname+"").addClass('file-cargado-mal');
            $("#"+lname+"").html('<i class="fas fa-times"></i> <span>Archivo Invalido </span>');
            */
        });
</script>
