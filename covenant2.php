<?php
$pageLink = "cloud";
include 'inc/cab.php';

?>
<head><link rel="stylesheet" href="assets/css/cov.css"></head>
<head><link rel="stylesheet" href="assets/css/s.css"></head>
<main id="ts-content">
<div class="ts-content">
    <header id="ts-hero" class="text-black" >
        <div class="container covenant">
            <section id="blockquote" class="ts-block" data-animate="ts-fadeInLeft" data-ts-delay=".4s">
                <div class="container" aling="center">
                    <div aling="center"><img src="assets/img/covenat.png" alt="" style="width:50%" ></div>
                        <blockquote class="blockquote mb-5 mt-5 text-justify" >
                            Covenant es la solución informática dedicada a la gestión de documentos digitales, partiendo de su creación, integrando flujos de aprobación, notificaciones, almacenamiento seguro y consulta de una forma dinámica en resumen ofrece una serie de herramientas que permiten la total administración del flujo de un documento, escalable conforme el tamaño de la empresa, haciendo más fácil la incorporación de la firma electrónica a cualquier documento digital sin olvidar la certeza jurídica de uno o más actos que se lleven a cabo.
                        </blockquote><hr>
                </div>
            </section>
        </div>   
           
</div>

</header>
<section class="mb-8"  data-animate="ts-fadeInLeft" data-ts-delay=".6s">
    <div class="container ">
        <div class="row">
            <div class="col-md-6 ">
                <div class="row container" >
                    <div class="col-md-12">
                        <div class="ts-title">
                            <h2 >Covenant Firma</h2>
                        </div>
                    </div>
                    <div class="col-md-12 text-justify">
                        <p class=" text-black" >Es un intrumento de seguridad, diseñado para llevar a cabo la firma electronica de un docuemnto. 
                            <ul>
                                <li>Evita la Presencia del firmante.</li>
                                <li>Valida la información del firmante.</li>
                                <li>Realiza el proceso vía web o aplicación móvil desde un celular.</li>
                            </ul>
                        </p>
                    </div>
                            
                            
                </div>
            </div>
            <div class="col-md-6 ">
                <div class="col-md-6" data-animate="ts-fadeInRight" data-ts-delay=".7s">
                    <img src="assets/img/covenant/mobile5.png" style="width:150%" alt="">
                </div>
            </div>
           
        </div>
</section>
<br>
<br>
<br>
<section class="mb-8"  data-animate="ts-fadeInLeft" data-ts-delay=".8s">
    <div class="container ">
        <div class="row">
            <div class="col-md-6 ">
                <div class="col-md-6" data-animate="ts-fadeInRight" data-ts-delay=".9s">
                    <img src="assets/img/covenant/mobile5.png" style="width:150%" alt="">
                </div>
            </div>
            <div class="col-md-6 ">
                <div class="row container" >
                    <div class="col-md-12">
                        <div class="ts-title">
                            <h2 >Covenant Docuementos</h2>
                        </div>
                    </div>
                    <div class="col-md-12 text-justify">
                    <p class=" text-black">Permite la creación, gestión  y resguardo de los documentos digitales. Mediante el uso de la Firma Electrónica Avanzada, Sello Digital de Tiempo y Nom-151, se da garantía leagl de: 
                        <ul>
                            <li>Quién firma es quin dice ser Tiempo exacto de la acción de firma.</li>
                            <li>Garantizar la integridad de la información.</li>
                        </ul>
                            Se puede prestar servicio a través de una API o utilizando nuestra Plataforma Covenant.
                            <br>
                            <b>Servicios api:</b>  Conjunto de métodos y funciones que permite la conexión con los servición de Covenant a través de la plataforma del cliente (servicio opcional).
                    </p>
                </div>              
            </div>
        </div>     
    </div>
</section>
    
<br>
      
<section id="features" class="pt-5 pb-5 text-black text-center text-sm-left" data-bg-color="#132035" >  <!--CDE3F7  B7D1E8 C1D6E9 D3E6F7-->
    <div class="container">
        <div class="ts-title text-center mt-5" data-animate="ts-fadeInUp" data-ts-delay=".4s" >
            <h2 style="color:#c56122">Problematicas que resuelve Covenant</h2>
                    
        </div>  

        <div class="row">
            <div class="col-md-6 mt-2  mb-3 text-justify text-white">
                <div class="d-flex" aling="justify"><div class="precio-tabla-check"></div><p class="ml-3"> Evita la presencia física de los firmantes.</p></div>
                <div class="d-flex" aling="justify"><div class="precio-tabla-check"></div><p class="ml-3" > Reducción de costos y tiempo en procesos administrativos.</p></div>
                <div class="d-flex" aling="justify"><div class="precio-tabla-check"></div><p class="ml-3"> Garantiza la integridad del contenido del documento, evitando el riesgo físico de traslados.</p></div>
                                  
            </div>

            <div class="col-md-6  mt-2  mb-3 text-justify text-white">
                <div class="d-flex" ><div class="precio-tabla-check"></div><p class="ml-3"> Certeza de que el firmante es quien dice ser. </p></div>
                <div class="d-flex"><div class="precio-tabla-check"></div><p class="ml-3"> Presunción jurídica y no repudio ante un juez (respaldo legal y tecnológico).</p></div>
                <div class="d-flex"><div class="precio-tabla-check"></div><p class="ml-3"> Confidencialidad de actos jurídicos o comerciales con firmas seguras mediante un ambiente web y móvil.</p></div>
            </div>
        </div>

    </div>
                    
</section>      

<section id="features" class="pt-5 pb-5 text-black text-center text-sm-left" data-bg-color="#132035">  <!--CDE3F7  B7D1E8 C1D6E9 D3E6F7-->
    <div class="container">
        
            <div class="ts-title text-center mt-5" data-animate="ts-fadeInUp" data-ts-delay=".6s">
                <h2 class="" style="color:#c56122"> Beneficios de utilizar nuestro servicio</h2>
            </div>

            <div class="row text-center iconos-cloud justify-content-center text-justify text-white">
                <div class="col-md-3 col-6">
                    <img src="assets/img/covenant/ib4.png" alt="">
                        <h5>Reduce riesgos de falsificación</h5>
                </div>
                <div class="col-md-3 col-6">
                    <img src="assets/img/covenant/ib1.png" alt="">
                        <h5>Firma desde cualquier lugar y dispositivo</h5>
                </div>
                <div class="col-md-3 col-6">
                    <img src="assets/img/covenant/ib2.png" alt="">
                     <h5>Reduce costos, tiempos y asociados</h5>
                </div>
                <div class="col-md-3 col-6">
                    <img src="assets/img/covenant/ib3.png" alt="">
                        <h5>Certeza jurídica y validez legal</h5>
                </div>
                </div>
                <div class="row justify-content-center text-justify text-white">
                    <div class="col-md-6 mt-5">
                        <div class="d-flex"><div class="precio-tabla-check"></div><p class="ml-3"> Seguro y transparente, cada transacción se registra permitiendo dar integridad al documento sin que intervenga el factor humano.</p></div>
                        <div class="d-flex"><div class="precio-tabla-check"></div><p class="ml-3" > Creación de expedientes digitales y almacenamiento de información en centros de datos certificados.</p></div>
                                    
                    </div>
                        
                    <div class="col-md-6 mt-5">
                        <div class="d-flex"><div class="precio-tabla-check"></div><p class="ml-3"> Firma electrónicamente desde cualquier lugar, obteniendo los mismos efectos jurídicos que con la firma autógrafa.</p></div>
                        <div class="d-flex"><div class="precio-tabla-check"></div><p class="ml-3"> Reducción del riesgo de falsificación en la documentación digital.</p></div>
                        <div class="d-flex"><div class="precio-tabla-check"></div><p class="ml-3"> Regulados por la Secretaría de Economía.</p></div>
                    </div>
                </div>
                   
            </div>
                                        

    </div>

</section>
<br> AQUI VA LOS PASOS

<section class="mb-8"  data-animate="ts-fadeInLeft" data-ts-delay=".3s">
    <div class="container ">
        <div class="row">
            <div class="c">
                <div class="row container" >
                    <div class="col-md-12" data-animate="ts-fadeInRight" data-ts-delay=".4s" >
                        <div class="ts-title">
                            <h2 >Utiliza todo el potencial de covenant</h2>
                        </div>
                    </div>
                    <div class="col-md-12" data-animate="ts-fadeInRight" data-ts-delay=".6s" >
                        <div class="ts-title">
                            <h4 >Creacion y asignación de plantillas</h4>
                        </div>
                    </div>
                               
            </div>
        </div>     
    </div>
</section>

<section id="features" class="pt-5 pb-5 text-black text-center text-sm-left" data-bg-color="#132035" >  <!--CDE3F7  B7D1E8 C1D6E9 D3E6F7-->
    <div class="container">
        <div class="ts-title text-center mt-5" data-animate="ts-fadeInUp" data-ts-delay=".4s" >
            <h2 style="color:#c56122">Algunos beneficios de crear plantillas</h2>
                    
        </div>  

        <div class="row">
            <div class="col-md-6 mt-2  mb-3 text-justify text-white" >
                <div class="d-flex" aling="justify"><div class="precio-tabla-check"></div><p class="ml-3"> Agrega el diseño que neesites.</p></div>
                <div class="d-flex" aling="justify"><div class="precio-tabla-check"></div><p class="ml-3"> Utiliando la Firma Autógrafa Digital s puede elegir el lugar en el docuemnto donde se icorpore el trazo de la firma.</p></div>
                <div class="d-flex" aling="justify"><div class="precio-tabla-check"></div><p class="ml-3" > Asigna la plantilla al área  o áreas que requieran utilizarla</p></div>
                <div class="d-flex" aling="justify"><div class="precio-tabla-check"></div><p class="ml-3"> El usuario "Checker"(Vlidador), Podra dar el visto bueno a las plantillas elaborads para qe posteiormente seas asignadas al áre que hará uso de ellas.</p></div>
                                  
            </div>

            <div class="col-md-6  mt-2  mb-3 text-justify text-white">
                <div class="d-flex" ><div class="precio-tabla-check"></div><p class="ml-3"> Permite deshabilitar temporalmente el uso de las plantillas para modificar el contenido del documento fuente.</p></div>
                <div class="d-flex"><div class="precio-tabla-check"></div><p class="ml-3"> Los documentos capturados  se pueden configurar para pasar por un proceso de validación antes de enviar a firma.</p></div>
                <div class="d-flex"><div class="precio-tabla-check"></div><p class="ml-3"> Mantiene la integridad de tu documento fuente, capturando la información variable (la cual se muestra con el simbolo [*] por medio de un formalrio).</p></div>
                <div class="d-flex" aling="justify"><div class="precio-tabla-check"></div><p class="ml-3"> Previsuliza el documento en cualquier momento el proceso de creación.</p></div>
            </div>
        </div>

    </div>
                    
</section>

<!--section id="partners" class="py-3" data-bg-color="#FFFFFF">
<div class="wrapper">
    
    </div>
</section--> 
             
<section class=" ts-background-repeat back-parallax" data-bg-color="#F0F0F0">
    <h2 class="pt-5 text-center">Áreas de oportunidad</h2>
        <div class="container ts-promo-numbers text-center">
            <div class="ts-title text-center" data-animate="ts-fadeInLeft" data-ts-delay=".1s"></div>
                <div class="container ">

                    <div class="valores">
                        <ul  class="row">    
                            <li class="sector col-md-4">
                                <div class="row ts-feature text-center">
                                    <!-- <h2 class="bold  col-md-12" ><i class="fa fa-money-bill-alt fa-2x"></i></h2> -->
                                    <h2 class="bold  col-md-12" ><i class="fa fa-university fa-2x"></i></h2>
                                    <h4 class="bold col-md-12" >Sector Financiero</h4>
                                    <hr  class="areas_opt">
                                    <p style="text-align: justify;" class="col-md-12">Agilizamos los procesos de documentación, trámites y contratos con la información personalizada y confidencial de tus clientes en tiempo real usando nuestra firma electrónica aceptada por la autoridad.</p>
                                </div>
                            </li>
                            <li class="sector col-md-4">
                                <div class="row ts-feature text-center">
                                    <h2 class="bold  col-md-12" ><i class="fa fa-gavel fa-2x"></i></h2>
                                    <h4 class="bold col-md-12">Despachos Jurídicos</h4>
                                    <hr  class="areas_opt">
                                    <p style="text-align: justify;" class="col-md-12">Con nuestra solución tecnológica, puedes diseñar, crear y gestionar todo tipo de documento de manera inteligente con toda certeza jurídica y validez legal que tanto tus clientes como tú necesitan.</p>
                                </div>
                            </li>
                            <li class="sector col-md-4">
                                <div class="row ts-feature text-center">
                                    <h2 class="bold  col-md-12" ><i class="fa fa-home fa-2x"></i></h2>
                                    <h4 class="bold  col-md-12">Sector Inmobiliario</h4>
                                    <hr  class="areas_opt">
                                    <p style="text-align: justify;" class="col-md-12">Logra ser más eficiente y eficaz con tus procesos internos, reduce costos y asociados firmando electrónicamente todos tus documentos desde la comodidad de tu dispositivo móvil.</p>
                                </div>
                            </li>
                            <li class="sector col-md-4">
                                <div class="row ts-feature text-center">
                                    <h2 class="bold  col-md-12" ><i class="fa fa-shield-alt fa-2x"></i></h2>
                                    <h4 class="bold  col-md-12">Aseguradoras</h4>
                                    <hr  class="areas_opt">
                                    <p style="text-align:justify;" class="col-md-12">Agilizamos los trámites y servicios que realizas con tus clientes, de una manera única, digital y transparente en toda la operación desde su contrato inicial hasta firmas posteriores.</p>
                                </div>
                            </li>
                            <li class="sector col-md-4">
                                <div class="row ts-feature text-center">
                                    <h2 class="bold col-md-12" ><i class="fa fa-rocket fa-2x"></i></h2>
                                    <h4 class=" bold col-md-12">Start-Ups</h4>
                                    <hr  class="areas_opt">
                                    <p style="text-align: justify;" class="col-md-12">Innova, gestiona y eficientiza desde tu gran comienzo. Automatiza todos tus procesos en segundos para que te dediques a lo importante: hacer crecer tu negocio. </p>
                                </div>
                            </li>
                            <li class="sector col-md-4">
                                <div class="row ts-feature text-center">
                                    <h2 class="bold col-md-12" ><i class="fa fa-plus-circle fa-2x"></i></h2>
                                    <h4 class=" bold  col-md-12">Otros</h4>
                                    <hr  class="areas_opt">
                                    <p style="text-align: justify;" class="col-md-12">Sé parte del futuro hoy e incrementa tu capacidad como organización líder con nuestra solución tecnológica que brinda a ti y a tus clientes todo el respaldo que necesitan.</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                
            </div>
        </div>
</section>

 


            <!--section id="partners" class="py-3" data-bg-color="#fafafa">
            <div class="container py-5">
            <h2 class="text-center g1">Características</h2>
                <div class="row">
                    <div class="col-md-6 d-flex align-items-center" data-animate="ts-fadeInLeft" data-ts-delay=".4s">
                        <div class="row container" >
                            <div class="col-md-12">
                                <div class="ts-title">
                                    <h2 >Seguro y Transparente</h2>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <p>Todas las operaciones de firma electrónica avanzada que ocurren en COVENANT son realizadas de forma “local” garantizando que no se exponen datos ni contraseñas de nuestros clientes.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6" data-animate="ts-fadeInRight" data-ts-delay=".4s">
                        <img src="assets/img/covenant/mobile5.png" style="width:100%" alt="">
                    </div>
                    <div class="col-md-6 mt-5" data-animate="ts-fadeInLeft" data-ts-delay=".5s">
                        <img src="assets/img/covenant/mobile6.png" style="width:100%" alt="">
                    </div>
                    <div class="col-md-6 d-flex align-items-center" data-animate="ts-fadeInRight" data-ts-delay=".5s">
                        <div class="row container">
                            <div class="col-md-12">
                                <div class="ts-title">
                                    <h2 >Auténtico e Identificable</h2>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <p>Cada transacción es registrada de forma exacta y permite dar integridad a cada uno de los documentos, gracias a los Sellos Digitales de Tiempo (SDT) y a la huella digital que se incluye en cada uno de ellos.</p>
                            </div>
                        </div>
                    </div>
                </div>
           
            </div>
        </section>
   
        <section id="features" class="pt-5 pb-5 text-black text-center text-sm-left" data-bg-color="#ececf5 ">
                <div class="container">
                <div class="ts-title text-center" >
                    <h2 style="color:#051c33">Descarga la App ahora</h2>
                   
                </div>  

                <div class="flex-btn-social social-btns">
                    <a class="app-btn blu flex-btn-social vert" href="https://apps.apple.com/cl/app/covenant-firma/id1499385817">
                        <i class="fab fa-apple"></i>
                        <p>Disponible en <br/> <span class="big-txt">App Store</span></p>
                    </a>

                    <a class="app-btn blu flex-btn-social vert" href="https://play.google.com/store/apps/details?id=com.legalexgs.covenant_firma">
                        <i class="fab fa-google-play"></i>
                        <p>Disponible en <br/> <span class="big-txt">Google Play</span></p>
                    </a>
                        
                
                </div>

                <img src="assets/img/mobile-1.png" alt="" class="mt-5 img-fluid" >
                </div>
            </section-->

  
<section class="mt-5 mb-5">
    <div class="container">
        <div class="row justify-content-center text-center">
             <div class="col-md-7">
                <div class="ts-title">
                    <h2 >Documentos Demo</h2>
                </div>
            </div>
            <div class="col-md-7">
                <p>Videos de utilidad sobre el uso de Covenant Firma desde nuestro sitio web</p>
            </div>
                            
        </div>
        <div class="row justify-content-center text-center">
            <div class="col-md-4">
                <button class="btn btn-outline-naranja my-2 video-btn ts-fadeInUp animated" data-toggle="modal" data-target="#ModalPDF">FEA Hoja Membretada</button>
            </div>
            <div class="col-md-4">
                <button class="btn btn-outline-naranja my-2 video-btn ts-fadeInUp animated" data-toggle="modal" data-target="#ModalPDF">Autógrafa Hoja Membretada</button>
            </div>
        </div>
    </div>
</section>




        <section class="mt-5 mb-5 container">
                        <div class="row  justify-content-center text-center">
                            <div class="col-md-7">
                                <div class="ts-title">
                                    <h2 >Tutoriales Web</h2>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <p>Videos de utilidad sobre el uso de Covenant Firma desde nuestro sitio web</p>
                            </div>
                        </div>
            <div id="videos-promo" class="owl-carousel owl-theme">
                <div class="modal-video item">
                    <div class="video-tutorial">
                        <img src="assets/img/covenant/lap-tablet-cel.png" alt="Avatar" class="image" style="width:100%">

                        <div class="titulo">
                            <h5 class="video-btn" data-toggle="modal" data-src="https://www.youtube.com/embed/xJACkwvfApU" data-target="#videoModalbtn"><i class="fa fa-play"></i></h5>
                        </div>
                    </div>
                    <p class="text-center">Firmar Covenant</p>
                </div>
                <div class="modal-video item">
                    <div class="video-tutorial">
                        <img src="assets/img/covenant/lap-tablet-cel.png" alt="Avatar" class="image" style="width:100%">

                        <div class="titulo">
                            <h5 class="video-btn" data-toggle="modal" data-src="https://www.youtube.com/embed/Rl4WWPh5r0I" data-target="#videoModalbtn"><i class="fa fa-play"></i></h5>
                        </div>
                    </div>
                    <p class="text-center">Firmar más documentos</p>
                </div>
                <div class="modal-video item">
                    <div class="video-tutorial">
                        <img src="assets/img/covenant/lap-tablet-cel.png" alt="Avatar" class="image" style="width:100%">

                        <div class="titulo">
                            <h5 class="video-btn" data-toggle="modal" data-src="https://www.youtube.com/embed/eWTUZOsYGnU" data-target="#videoModalbtn"><i class="fa fa-play"></i></h5>
                        </div>
                    </div>
                    <p class="text-center">Ingresar por primera vez</p>
                </div>
                <div class="modal-video item">
                    <div class="video-tutorial">
                        <img src="assets/img/covenant/lap-tablet-cel.png" alt="Avatar" class="image" style="width:100%">

                        <div class="titulo">
                            <h5 class="video-btn" data-toggle="modal" data-src="https://www.youtube.com/embed/LsUnVUl3B7k" data-target="#videoModalbtn"><i class="fa fa-play"></i></h5>
                        </div>
                    </div>
                    <p class="text-center">Recuperar Contraseña</p>
                </div>
                <div class="modal-video item">
                    <div class="video-tutorial">
                        <img src="assets/img/covenant/lap-tablet-cel.png" alt="Avatar" class="image" style="width:100%">

                        <div class="titulo">
                            <h5 class="video-btn" data-toggle="modal" data-src="https://www.youtube.com/embed/9SByM34ceGI" data-target="#videoModalbtn"><i class="fa fa-play"></i></h5>
                        </div>
                    </div>
                    <p class="text-center">Ordenar Documentos en una Carpeta</p>
                </div>
                <div class="modal-video item">
                    <div class="video-tutorial">
                        <img src="assets/img/covenant/lap-tablet-cel.png" alt="Avatar" class="image" style="width:100%">

                        <div class="titulo">
                            <h5 class="video-btn" data-toggle="modal" data-src="https://www.youtube.com/embed/yb_CvXdYGlg" data-target="#videoModalbtn"><i class="fa fa-play"></i></h5>
                        </div>
                    </div>
                    <p class="text-center">Documento cancelado por el firmante</p>
                </div>

            </div>
            <div class="container mb-3 mt-3">
                <div class="row  justify-content-center">
                    <div class="col-md-1 col-2">
                         <i class="fa fa-arrow-left customNextBtn" aria-hidden="true"></i>
                    </div>
                    <div class="col-md-1 col-2">
                         <i class="fa fa-arrow-right customPrevBtn" aria-hidden="true"></i>
                    </div>
                </div>                
            </div>
            <div class="row container justify-content-center text-center mt-5">
                            <div class="col-md-7">
                                <div class="ts-title">
                                    <h2 >Tutoriales App</h2>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <p>Videos de utilidad sobre el uso de Covenant Firma con nuestra aplicacón</p>
                            </div>
             </div>
            
             <div id="videos-promo2" class="owl-carousel owl-theme">
             <div class="modal-video item">
                    <div class="video-tutorial">
                        <img src="assets/img/covenant/lap-tablet-cel.png" alt="Avatar" class="image" style="width:100%">

                        <div class="titulo">
                            <h5 class="video-btn" data-toggle="modal" data-src="https://www.youtube.com/embed/KNQzOfDQUk0" data-target="#videoModalbtn"><i class="fa fa-play"></i></h5>
                        </div>
                    </div>
                    <p class="text-center">Firmar Covenant</p>
                </div>
                <div class="modal-video item">
                <div class="video-tutorial " >
                    <img src="assets/img/covenant/lap-tablet-cel.png" alt="Avatar" class="image" style="width:100%">
                    <div class="titulo">
                        
                        <h5 class="video-btn" data-toggle="modal" data-src="https://www.youtube.com/embed/06eC4gQ1vJI" data-target="#videoModalbtn"><i class="fa fa-play"></i></h5>
                    </div>
                    
                    </div>
                    <p class="text-center">Instalar Covenant App</p>
                </div>
                <div class="modal-video item">
                <div class="video-tutorial " >
                <img src="assets/img/covenant/lap-tablet-cel.png" alt="Avatar" class="image" style="width:100%">

                    <div class="titulo">
                        
                        <h5 class="video-btn" data-toggle="modal" data-src="https://www.youtube.com/embed/wxSIO2WELHg" data-target="#videoModalbtn"><i class="fa fa-play"></i></h5>
                    </div>
                    
                    
                    </div>
                    <p class="text-center">Firma con trazo </p>
                </div>
                <div class="modal-video item">
                    <div class="video-tutorial " >
                        <img src="assets/img/covenant/lap-tablet-cel.png" alt="Avatar" class="image" style="width:100%">

                        <div class="titulo">
                            
                            <h5 class="video-btn" data-toggle="modal" data-src="https://www.youtube.com/embed/iVMNmrFEQdg" data-target="#videoModalbtn"><i class="fa fa-play"></i></h5>
                        </div>
                    
                    
                    </div>
                    <p class="text-center">Carga de Certificados y Firma FEA</p>
                </div>
                <div class="modal-video item">
                    <div class="video-tutorial " >
                        <img src="assets/img/covenant/lap-tablet-cel.png" alt="Avatar" class="image" style="width:100%">

                        <div class="titulo">
                            
                            <h5 class="video-btn" data-toggle="modal" data-src="https://www.youtube.com/embed/LsUnVUl3B7k" data-target="#videoModalbtn"><i class="fa fa-play"></i></h5>
                        </div>
                    
                    
                    </div>
                    <p class="text-center">Recuperar Contraseña</p>
                </div>
                <div class="modal-video item">
                <div class="video-tutorial ">
                    <img src="assets/img/covenant/lap-tablet-cel.png" alt="Avatar" class="image" style="width:100%">

                    <div class="titulo">
                    
                    <h5 class="video-btn" data-toggle="modal" data-src="https://www.youtube.com/embed/1UR_l9vUkeY" data-target="#videoModalbtn"><i class="fa fa-play"></i></h5>
                    </div>
                    </div>
                    
                <p  class="text-center">Desinstalar Covenant App</p>
                </div>
                <div class="modal-video item">
                <div class="video-tutorial ">
                <img src="assets/img/covenant/lap-tablet-cel.png" alt="Avatar" class="image" style="width:100%">

                    <div class="titulo">
                    <h5 class="video-btn" data-toggle="modal" data-src="https://www.youtube.com/embed/wpxJXaprx14" data-target="#videoModalbtn"><i class="fa fa-play"></i></h5>
                    </div>
                    
                    </div>
                    <p class="text-center">Activar datos biométricos</p>
                </div>
            </div>
            <div class="container mt-3">
                <div class="row  justify-content-center">
                    <div class="col-md-1  col-2">
                         <i class="icon ion-ios-arrow-back customNextBtn2" aria-hidden="true"></i>
                    </div>
                    <div class="col-md-1 col-2">
                         <i class="icon ion-ios-arrow-forward customPrevBtn2" aria-hidden="true"></i>
                    </div>
                </div>                
            </div>
           
        </section>
            <!--Features
            ============================================================================================================
            -->
</main>


        

       
<?php
include 'inc/pie.php';
?>
<script src="s.js"></script>
<script>



var $videoSrc;  
$('.video-btn').click(function() {
    $videoSrc = $(this).data( "src" );
    console.log($videoSrc);
});
console.log($videoSrc);

$('#videoModalbtn').on('shown.bs.modal', function (e) {
    $("#videoModalSrc").attr('src',$videoSrc + "?autoplay=1&amp;modestbranding=1&amp;showinfo=0" ); 
})

$('#videoModalbtn').on('hide.bs.modal', function (e) {
    $("#videoModalSrc").attr('src',$videoSrc); 
}) 

var owlp = $('#videos-promo');
owlp.owlCarousel({
    loop:true,
    autoplay:true,
    autoplayTimeout:4000,
    margin:15,
    center: true,
    autoplaySpeed: 3000,
    smartSpeed: 800,
    autoplayHoverPause:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:3
        }
    }
});
$('.customPrevBtn').click(function() {
    owlp.trigger('next.owl.carousel');
})
// Go to the previous item
$('.customNextBtn').click(function() {
    owlp.trigger('prev.owl.carousel', [300]);
})

var owlp2 = $('#videos-promo2');
owlp2.owlCarousel({
    loop:true,
    autoplay:true,
    margin:15,
    center: true,
    autoplaySpeed: 3000,
    autoplayHoverPause:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:3
        }
    }
});
$('.customPrevBtn2').click(function() {
    owlp2.trigger('next.owl.carousel');
})
// Go to the previous item
$('.customNextBtn2').click(function() {
    owlp2.trigger('prev.owl.carousel');
})

$('#videos-promo2').on('click', function () {
    owlp2.trigger('stop.owl.autoplay');
});
$('#videos-promo').on('click', function () {
    owlp.trigger('stop.owl.autoplay');
});
$(".btn-close-videos-tut").on('click',function(){
    owlp.trigger('play.owl.autoplay');
    owlp2.trigger('play.owl.autoplay');
    owlp.trigger('to.owl.carousel', $(this).data( 'position' ) ); 
    owlp2.trigger('to.owl.carousel', $(this).data( 'position' ) ); 
});

</script>