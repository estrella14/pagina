<?php 
$pageLink = "inicio";
include 'inc/cab.php';
?>

<head>

    <meta http-equiv='cache-control' content='no-cache'>
    <meta http-equiv='expires' content='0'>
    <meta http-equiv='pragma' content='no-cache'>
</head>
    <!--Content
    =============================Prueba3=======================================================================================
    -->
    <div class="ts-content">
        <!-- HEADER
        ================================================================================================================
        ================================================================================================================
        -->
        <header id="ts-hero" class="ts-height--100vh ts-has-overlay text-white">

            <div class="container-fluid text-center">
                
                <div class="row justify-content-center">
                    
                    <div class="col-md-5">
                        <div class="mt-3">
                            <img src="assets/img/logo_cuadro.png" alt="" style="width:80%">
                            <h3 class="mt-3">El futuro es ahora, construyámoslo juntos</h3>
                        </div>
                    </div>
                </div>

            </div>

            <div class="ts-background" data-bg-parallax="scroll" data-bg-parallax-speed="3" data-bg-color="#000">
                <div class="ts-video-bg ts-parallax-element" data-vide-bg="assets/video/legalex" data-vide-options="loop: true, muted: true, position: 50% 50%"></div>
            </div>

        </header>

        
        <main id="ts-content">

            <!--section class="pt-5 pb-5" style="background-color:#e5ebf1">
                <div class="container">
                <h1 class=" mt-5 mb-2">Nuestros Servdcadfsdfficios PSC</h1>
                <p class="mb-5">Cumplen con los estrictos estándares de seguridad a nivel nacional</p>
                <div class="col-md-12">
                        <div>
                            <div class="car-index  owl-carousel owl-theme">
                                <div class="carta">
                                    <div class="textos-carta"  data-animate="ts-fadeInDown" data-ts-delay=".15s"> 
                                        <p>Constancia NOM-151</p>
                                        <h3>Garantiza la información electrónica recibida con un tiempo y con características específicas.</h3>
                                    </div>
                                    
                                    <div data-animate="ts-fadeInUp" data-ts-delay=".15s">
                                        <a href="#conocenos" class="btn btn-outline-generico btn-sm">Más información</a>
                                    </div>
                                </div>
                                <div class="carta">
                                    <div class="textos-carta"  data-animate="ts-fadeInDown" data-ts-delay=".15s">
                                    <p>Sello digital de tiempo</p>
                                    <h3>Autentica la existencia de los datos electrónicos en una fecha y hora concretos.</h3>
                                    </div>
                                    
                                    <div data-animate="ts-fadeInUp" data-ts-delay=".15s">
                                        <a href="#conocenos" class="btn btn-outline-generico btn-sm">Más información</a>
                                    </div>
                                </div>
                            </div>
                            <div class="row  justify-content-center mt-3">
                                <div class="col-md-1 col-2">
                                    <i class="icon ion-ios-arrow-back customNextBtn customNextBtnind" aria-hidden="true"></i>
                                </div>
                                <div class="col-md-1 col-2">
                                    <i class="icon ion-ios-arrow-forward customPrevBtn customNextBtnind" aria-hidden="true"></i>
                                </div>
                            </div> 
                        </div> 
                    </div>
                </div>
            </section-->

            <section id="conocenos" class="ts-block ts-bg-black text-white">
                <div class="container ">
                    <div class="row">
                        <div class="col-md-3  align-items-center ">
                          <center ><img class="segurolef" data-animate="ts-fadeInRight"  data-ts-delay=".1s" src="assets/img/logolg.png" alt=""></center>
                          
                        </div>
                        <div class="col-md-9" data-animate="ts-fadeInUp" data-ts-delay=".1s">
                        <blockquote class="blockquote "> 
                            <h3 class="text-justify">Somos un <a  class="link-azul" href="preguntas?faq=que-es-una-psc#pj5">Prestador de Servicios de Certificación (PSC)</a> a nivel nacional acreditado y regulado por la <a class="link-azul " href="preguntas?faq=ecretaria-de-economia-federal#pj1">Secretaría de Economía</a>. Apoyamos a empresas y clientes en su transformación digital e innovación de procesos.</h3>
                            <footer class="blockquote-footer">El futuro es ahora, construyámoslo juntos</footer>
                        </blockquote>
                        </div>
                    </div>
                  

                </div>
            </section>
            
            
               <section id="features" class="ts-block text-white text-center text-sm-left">
                <div class="container ">
                    <div class="row">

                        <div class="col-md-4 " data-animate="ts-fadeInUp" data-ts-delay=".1s">
                            <div class="ts-feature text-center">
                                <img src="assets/img/idea.png" alt="" class="mb-5">
                                <h4>CREAMOS</h4>
                                <p style="text-align: justify;">Creamos soluciones informáticas integrales haciendo uso de la mejor tecnología, garantizando a nuestros clientes absoluta seguridad, rapidez y eficiencia.</p>
                            </div>
                        </div>
                        <!--end Feature-->

                        <div class="col-md-4" data-animate="ts-fadeInUp" data-ts-delay=".1s">
                            <div class="ts-feature text-center">
                                <img src="assets/img/cuete.png" alt="" class="mb-5">
                                <h4>INNOVAMOS</h4>
                                <p style="text-align: justify;">Buscamos permanentemente nuevas estrategias para crear soluciones digitales a situaciones reales.</p>
                            </div>
                        </div>
                        <!--end Feature-->

                        <div class="col-md-4" data-animate="ts-fadeInUp" data-ts-delay=".1s">
                            <div class="ts-feature text-center">
                                <img src="assets/img/agil.png" alt="" class="mb-5">
                                <h4>AGILIZAMOS</h4>
                                <p style="text-align: justify;"> Optimizamos procesos en diferentes ámbitos, garantizando la certeza jurídica y validez legal que nuestros clientes necesitan.</p>
                            </div>
                        </div>
                        <!--end Feature-->

                    </div>
                </div>

                <div class="ts-background ts-bg-black">
                    <div class="ts-background-image ts-background-repeat ts-opacity__10 ts-img-into-bg back-parallax">
                        <img src="assets/img/bg__pattern--topo-white.png" alt="">
                    </div>
                </div>
            </section>

            <section class="video-in">
                <div class="delimitador">
                    <div class="contenedor">
                        <iframe width="100%" height="450" src="https://www.youtube.com/embed/RuD2LWFhMKc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
            </section>
                        
         
            <!--section id="prices" class="prices maskedtretret section" >
<div class="container rel-1">
    <div class="row">
        <div class="col-md-6 col-sm-6 col-lg-3">
            <div class="price-box">
                <div class="price-body">
                    <div class="price-inner">
                        <header class="price-header">
                        <h4 class="price-title">Personal</h4>
                        <div class="price">
                            <span class="price-currency">$</span>
                            <strong class="price-amount">999</strong><span class="price-delimiter">/</span><span class="price-period">mes</span>
                        </div>
                        </header>
                        <div class="price-features">
                            <ul>
                                <li>Firma electrónica Avanzada con Sello Digital</li>
                                <li>10 Documentos por mes</li>
                                <li>1 Usuarios</li>
                                <li>Covenant Móvil App</li>
                                <li>Plataforma</li>
                            </ul>
                        </div>
                        <div class="price-footer">
                        <a href="precio#comparacion" class="btn btn-outline-generico btn-sm">Conoce más</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class=" col-md-6 col-sm-6 col-lg-3 wow flipInY" style="visibility: visible; animation-name: flipInY;">
            <div class="price-box">
                <div class="price-body">
                    <div class="price-inner">
                        <header class="price-header">
                        <h4 class="price-title">Negocios</h4>
                        <div class="price">
                            <span class="price-currency">$</span>
                            <strong class="price-amount">1,799</strong><span class="price-delimiter">/</span><span class="price-period">mes</span>
                        </div>
                        </header>
                        <div class="price-features">
                            <ul>
                                <li>Firma electrónica Avanzada con Sello Digital</li>
                                <li>20 Documentos por mes</li>
                                <li>3 Usuarios</li>
                                <li>Covenant Móvil App</li>
                                <li>Plataforma</li>
                            </ul>
                        </div>
                        <div class="price-footer">
                        <a href="precio#comparacion" class="btn btn-outline-generico btn-sm">Conoce más</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="  col-md-6 col-sm-6 col-lg-3 " >
            <div class="price-box">
                <div class="price-body">
                    <div class="price-inner">
                        <header class="price-header">
                        <h4 class="price-title">Empresarial</h4>
                            <div class="price">
                                <span class="price-currency">$</span>
                                <strong class="price-amount">3,399</strong><span class="price-delimiter">/</span><span class="price-period">mes</span>
                            </div>
                        </header>
                        <div class="price-features">
                            <ul>
                                <li>Firma electrónica Avanzada con Sello Digital</li>
                                <li>30 Documentos por mes</li>
                                <li>5 Usuarios</li>
                                <li>Covenant Móvil App</li>
                                <li>Plataforma</li>
                            </ul>
                        </div>
                        <div class="price-footer">
                            <a href="precio#comparacion" class="btn btn-outline-generico btn-sm">Conoce más</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-lg-3 leading" data-animate="ts-fadeInUp" data-ts-delay=".15s">
            <div class="price-box">
                <div class="price-body">
                    <div class="price-inner">
                        <header class="price-header">
                        <h4 class="price-title">Corporativo</h4>
                            <div class="price">
                                
                                <strong class="price-amount " style="font-size:x-large;">Precio <br>Personalizado</strong>
                            </div>
                        </header>
                        <div class="price-features">
                            <ul>
                                <li>Firma electrónica Avanzada con Sello Digital</li>
                                <li>Por firma
                                    <div class="precio-info"> <span class="tooltips">Cantidad de firmas para los documentos</span></div>
                                </li>
                                <li>Usuarios Ilimitados</li>
                                <li>Covenant Móvil App</li>
                                <li>Plataforma/Web Service  <div class="precio-info"> <span class="tooltips">Servicio API para interconectar nuestras aplicaciónes</span></div></li>
                            </ul>
                        </div>
                        <div class="price-footer">
                        <a href="nosotros/contacto" class="btn btn-outline-generico btn-sm btn-block">Contactar</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="ts-background ts-bg-black">
                    <div class="ts-background-image   ts-img-into-bg back-parallax">
                        <img src="assets/img/precios.jpg" alt="">
                    </div>
                </div>
</section>
                
            </section-->
            <style>
                .izq .precio-tabla-check{
                    margin-left: 0;
                    margin-right: 17px;
                }
            </style>
            <section class="beneficios ">
                <div class="container-fluid">

                    <h1 class="text-center mt-5 mb-2">¿Por qué Legalex?</h1>
                    <div class="row divisor">
                    <div class="col-md-6 col-6 bloque-div" data-animate="ts-fadeInLeft" data-ts-delay=".1s">
                        <div class="beneficio-card izq">
                            
                            <ul class="left">
                                <li><div class="precio-tabla-check"></div>Nuestros procesos se basan en estándares de seguridad 
internacionales</li>
                                <li><div class="precio-tabla-check"></div>Estamos Regulados por el Gobierno Federal a través de SE</li>
                                <li><div class="precio-tabla-check"></div>Garantizamos la simplificación de los procesos administrativos, 
con resultados en la disminución de tiempo y costos</li>
                                <li><div class="precio-tabla-check"></div>Blindamos tu información y  ofrecemos disponibilidad 24/7                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6 col-6 bloque-div ventLgx" data-animate="ts-fadeInRight" data-ts-delay=".1s">
                        <div class="beneficio-card izq">
                           
                            <ul>
                                <li><div class="precio-tabla-check"></div> <div> Con el uso de la e.firma se obtiene la figura del <a class="link-azul" href="preguntas?faq=no-repudio#pj4">no repudio</a></div> </li>
                                <li><div class="precio-tabla-check"></div> Infraestructura propia en los mejores centros de datos de América Latina</li>
                                <li><div class="precio-tabla-check"></div> Algunas de las más influyentes empresas de México son nuestros clientes </li>
                                <li><div class="precio-tabla-check"></div>Promovemos la transformación digital hacia una cultura de 
Paperless.</li>
                            </ul>
                        </div>
                    </div>
                    </div>
                    
                </div>
            </section>
         <section class="certificaciones-inicio pt-5 pb-5" style="background: #4045490f;">  
            <div class="container">
                <h1 class="text-center mt-5 mb-2" data-animate="ts-fadeInDown" data-ts-delay=".1s">Seguridad en el manejo de tu información</h1>
                <p class="text-center mb-5 ">Siéntete totalmente seguro, tranquilo y respaldado ya que toda tu información se administra en infraestructura de almacenamiento propiedad de Legalex, los cuales están alojados en los mejores centros de datos de Latinoamérica y estos son algunas de sus certificaciones</p>
                <div class="row">
                    <div class="col-md-4" data-animate="ts-fadeInLeft" data-ts-delay=".1s">
                        <ul>
                            <li>ICREA nivel V</li>
                            <li>NFPA 75</li>
                            <li>ISO/IEC 20000-1</li>
                            <li>ISO 9001</li>
                            <li>ISO 14001</li>
                            <li>ISO 22301 e ISO/IEC 38500</li>
                        </ul>
                    </div>
                    <div class="col-md-4" data-animate="ts-fadeInDown" data-ts-delay=".1s">
                        <ul>
                            
                            <li>ISO/IEC 27001</li>
                            <li>ISO/IEC 27017</li>
                            <li>ISO/IEC 27018</li>
                            <li>SOC 1 Tipo II - ISAE3403/SSAE18</li>
                            <li>PCI DSS</li>
                            <li>CLOUD & MANAGED SERVICES MASTER</li>
                            
                        </ul>
                    </div>
                    <div class="col-md-4" data-animate="ts-fadeInRight" data-ts-delay=".1s">
                        <ul>
                            
                            <li>SAP Hosting Services</li>
                            <li>SAP Certified in Cloud and Infreastructure Operations</li>
                            <li>LFPDPPP (SGDP)</li>
                            <li>CSA STAR Nivel I</li>
                        </ul>
                    </div>
                </div>
            
            </div>
         </section>
         <section style="background:#ffffff" class="pt-5 pb-3 firma-electronica-sect" >
             <div class="container-fluid">
                <div class="row">
                    <div class="col-md-5 " data-animate="ts-fadeInLeft" data-ts-delay=".1s">
                        <img src="assets/img/firma-ele.png" alt="">
                    </div>
                    <div class="col-md-6">
                        <div class="texto-firma" data-animate="ts-fadeInRight" data-ts-delay=".3s">
                            <h3 ><b><a class="link-azul" href="preguntas?faq=firma-electronica-pki#pj2">Firma Electrónica</a> Avanzada (e.firma)</b> </h3>
                            <p class="text-justify">La Firma Electrónica Avanzada (<a href="https://www.gob.mx/efirma" target="_blank" class="link-azul">e.firma</a>) es un conjunto de datos que se adjuntan a un mensaje electrónico, cuyo propósito es identificar al emisor del mensaje como autor legítimo de éste; como si se tratara de una firma autógrafa.​​​</p>
                            <p class="text-justify">Por sus características, la e.firma brinda seguridad a las transacciones electrónicas de los contribuyentes, con su uso se puede identificar al autor del mensaje y verificar que no haya sido modificado.
                            Su diseño se basa en estándares internacionales de infraestructura de claves públicas (o PKI por sus siglas en inglés:Public Key Infrastructure) en donde se utilizan dos claves o llaves para el envío de mensajes</p>
                            <p style="display: flex; justify-content: space-around;">  
                                <a href="firma-electronica" class="btn btn-outline-naranja btn-sm botonFirmeElec" style="width: 35%;">
                                    Saber más
                                </a>
                                
                                <a href="Soluciones/Covenant/index.php" class="btn btn-outline-naranja btn-sm botonFirmeElec"  style="width: 50%;">
                                    Nuestra firma electrónica   
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
             </div>
         </section> 
         <style>
             .covenant ul{
                 list-style:none;
                 margin-bottom:20px;
             }
             .covenant ul li{
                 font-size:1.3rem;
                 margin-bottom:10px;
             }
             .covenant ul li i{
                 margin-right:10px;
                 color:#c56122;
             }
             .owl-carousel .owl-stage, .owl-carousel .owl-stage-outer, .owl-carousel .owl-item {
    display: inline-flex;
    align-items: center;
    justify-content: center;
    height: 100%;
    justify-content: flex-start;
}
.no-js .owl-carousel, .owl-carousel.owl-loaded {
    display: inline-grid;
}
@media  (max-width: 450px){
	
.owl-carousel .owl-stage, .owl-carousel .owl-stage-outer, .owl-carousel .owl-item {
    display: inline-flex;
    align-items: center;
    justify-content: center;
    justify-self: baseline;
    height: 100%;
    /* justify-content: flex-start; */
}	


}



         </style>
         <section class="servicios-le  mb-5 covenant"  style="background: #4045490f;" >
            <div class="container">
                    
                <div class="row">
                    <div class="col col-md-6">
                        <h1 data-animate="ts-reveal">
                            <span class="wrapper" style="margin-top: 40px;">
                                <span>¡Olvídate de los papeles! </span>
                            </span>
                        </h1>
                        <div class="mt-4" data-animate="ts-fadeInUp" data-ts-delay=".1s">
                            <p class="text-justify">
                                Logra una cultura <a class="link-azul" href="preguntas?faq=paper-less#pj3">Paper Less</a> a través de <a href="covenant" class="link-naranja">COVENANT</a>.
                                <br> <br>
                                <a href="covenant" class="link-naranja">COVENANT</a> te ayuda al diseño, creación y gestión de todos tus documentos de manera inteligente, transparente, digital y con toda la validez legal que necesitas.              
                            </p>
                        </div>

                        <div class="ts-group  d-md-block text-center">
                            <a href="Soluciones/Covenant/index.php" class="btn btn-outline-naranja my-2" >Comenzar Ahora</a>
                            <div class="flex-btn-social social-btns inicio ">
                                <a class="app-btn blu flex-btn-social btn-sm" href="https://apps.apple.com/cl/app/covenant-firma/id1499385817" target="_blank">
                                    <div class="row">  <i class="fab fa-apple"></i>
                                  
                                    <p>Disponible en <br/> <span class="big-txt">App Store</span></p></div>
                                </a>

                                <a class="app-btn blu flex-btn-social  btn-sm" href="https://play.google.com/store/apps/details?id=com.legalexgs.covenant_firma" target="_blank">
                                <div class="row">   
                                <i class="fab fa-google-play"></i>
                                    <p>Disponible en <br/> <span class="big-txt">Google Play</span></p></div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 text-center"  data-animate="ts-fadeInRight" data-ts-delay=".1s">
                        <img src="assets/img/covenant/mobile5.png" alt="" style="width:80%">
                    </div>
                </div>
            </div>
        </section>
            <!--section class="servicios-le mt-5 mb-5">
                <div class="container">
                    <h1 class="text-center mt-5 mb-2" data-animate="ts-fadeInDown" data-ts-delay=".2s">Covenant APP</h1>
                    <p class="text-center mb-5">Covenant es una solución informática para el diseño, creación y gestión de documentos digitales usando la firma electrónica para dar certeza jurídica y validez legal en todo acto jurídico.

</p>
                    <div class="row">
                        <div class="col-md-4" data-animate="ts-fadeInUp" data-ts-delay=".3s">
                            <div class="ts-feature text-center">
                                <div class="sevim">
                                    <img src="assets/img/covenat.png" alt="Covenant APP" class="mb-2"  style="width:50%">   
                                </div>
                                <p>Covenant es una solución informática para el diseño, creación y gestión de documentos digitales usando la firma electrónica para dar certeza jurídica y validez legal en todo acto jurídico.</p>
                                <a href="covenant" class=" btn  btn-outline-naranja">Ver más</a>
                            </div>
                        </div>
                        <div class="col-md-4" data-animate="ts-fadeInUp" data-ts-delay=".4s">
                            <div class="ts-feature text-center">
                                <div class="sevim">
                                    <img src="assets/img/notificamex.png" alt="Covenant APP" class="mb-2"  >  
                                </div>
                                <p>Notificaciones electrónicas con eficiencia y seguridad de que el notificado reciba la información correspondiente en tiempo y forma.</p>
                                <a href="notifimex" class=" btn  btn-outline-naranja">Ver más</a>
                            </div>
                        </div>
                        <div class="col-md-4" data-animate="ts-fadeInUp" data-ts-delay=".6s">
                            <div class="ts-feature text-center">
                                <div class="sevim">
                                    <img src="assets/img/surety.png" alt="Covenant APP" class="mb-2"  style="width:50%">   
                                </div>
                                
                                <p>SURETY, Sistema de Garantías Dinámicas y Financiamientos Estructurados. Es una solución informática, orientada al control eficiente de las funciones administrativas, derivadas del Financiamiento respaldado con garantías dinámicas.</p>
                                <a href="surety" class=" btn  btn-outline-naranja">Ver más</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section-->
            

            <section id="" class="py-5"  data-animate="ts-fadeInUp" data-ts-delay=".1s" style="background-color: ffffff;">
            
                <div class="container py-5">
                    <h2 class="text-center pb-5">Ellos han innovado con nosotros</h2>
                        <div class="d-block text-center ts-partners ">
                            <div id="carrusel-patners" class="owl-carousel owl-theme" >
                            
                                <a  class="item" >
                                    <img src="assets/img/pat/clientes/anteris.png" alt="Anteris" style="width: 110px;">
                                </a>
                            
                                <a class="item ">
                                    <img src="assets/img/pat/clientes/cor.png" alt="60 COR" style="width: 130px;">
                                </a>
                                <a  class="item ">
                                    <img src="assets/img/pat/clientes/ow.png" alt="OÑATE, WILLY Y CIA"  style="width: 130px;">
                                </a>
                                <a class="item  ">
                                    <img src="assets/img/pat/clientes/bcoppel.png" alt="BanCoppel" style="width: 130px;">
                                </a>
                                <a  class="item ">
                                    <img src="assets/img/pat/clientes/citibanamex.png" alt="Citibanamex" style="width: 130px;">
                                </a>
                                <a  class="item ">
                                    <img src="assets/img/pat/clientes/coppel.png" alt="Coppel" style="width: 130px;">
                                </a>
                                <a  class="item ">
                                    <img src="assets/img/pat/clientes/credix.png" alt="Credix" style="width: 130px;">
                                </a>
                                <a  class="item ">
                                    <img src="assets/img/pat/clientes/g500.png" alt="G500" style="width: 130px;">
                                </a>
                                <a  class="item ">
                                    <img src="assets/img/pat/clientes/iar.png" alt="Iar" style="width: 100px;">
                                </a>
                                <a class="item ">
                                    <img src="assets/img/pat/clientes/serta.png" alt="Serta Capital" style="width: 130px;">
                                </a>
                                <a  class="item ">
                                    <img src="assets/img/pat/clientes/kavak.png" alt="Kavak" style="width: 130px;">
                                </a>
                                <a  class="item ">
                                    <img src="assets/img/pat/clientes/monex.png" alt="Monex" style="width: 130px;">
                                </a>
                                <a  class="item ">
                                    <img src="assets/img/pat/clientes/ofx.png" alt="OFX" style="width: 90px;">
                                </a>
                                <a  class="item ">
                                    <img src="assets/img/pat/clientes/montiel.jpg" alt="Montiel" style="width: 100px;">
                                </a>
                                <a  class="item ">
                                    <img src="assets/img/pat/clientes/altum.jpg" alt="Altum Capital" style="width: 130px;">
                                </a>
                                <a  class="item ">
                                    <img src="assets/img/pat/clientes/central.png" alt="Central ADN" style="width: 130px;">
                                </a>
                                
                            </div>
                        </div>
                    <div class="row  justify-content-center">
                        <div class="col-md-1 col-2">
                            <i class="icon ion-ios-arrow-back customNextBtn" aria-hidden="true"></i>
                        </div>
                        <div class="col-md-1 col-2">
                            <i class="icon ion-ios-arrow-forward customPrevBtn" aria-hidden="true"></i>
                        </div>
                    </div>

                </div>
                
            </section>

           
  

    </main>
       
 <?php
    include 'inc/pie.php'
 ?>

 <script>

    var owlp = $('#carrusel-patners');
    owlp.owlCarousel({
        loop:true,
        autoplay:10,
        autoplayTimeout:4000,
        margin:15,
        center: true,
        autoplaySpeed: 3000,
        smartSpeed: 800,
        autoplayHoverPause:true,
        responsive:{
            0:{
                items:1,
            },
            600:{
                items:3,
            },
            1000:{
                items:4,
            }
        }
    });
    $('.customPrevBtn').click(function() {
        owlp.trigger('next.owl.carousel');
    })

    // Go to the previous item
    $('.customNextBtn').click(function() {
        owlp.trigger('prev.owl.carousel', [300]);
    })
 </script>
 
<script>
    $(function () {
        $(document).scroll(function () {
            var scrollTop = $(this).scrollTop();
            var $nav = $("#logo-nav-b");
            $nav.toggleClass('scrolled', $(this).scrollTop() > 15);

        $('.jumbo-inicio').css({
            opacity: function() {
                var elementHeight = $(this).height(),
                    opacity = ((elementHeight - scrollTop) / elementHeight);
                return opacity;
            },
            transform:function(){
                var elementHeight = $(this).height(),
                    opacity = ((elementHeight - scrollTop) / elementHeight);
                return opacity;
            }
        });
        
        });
    });
</script>
<!--Cache-->
<script type="text/javascript" src="assets/css/styles.css?v=<?php echo rand(); ?>"></script>
<script type="text/javascript" src="assets/js/faqs.js?v=<?php echo rand(); ?>"></script>
<script type="text/javascript" src="assets/js/custom.js?v=<?php echo rand(); ?>"></script>
  <!------------------------------------------------------------------>