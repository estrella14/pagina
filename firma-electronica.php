<?php 
$pageLink = "Blog";
    include 'inc/cab.php';
?>

    <section class="banner-recursos container">
        <div class="header-recursos ver-blog">
            <h1>Firma electrónica de documentos</h1>
            <a href="nosotros/contacto" class="btn btn-outline-generico">Contactar a Ventas</a>
        </div>
        
        <hr>
        <div class="row">
            <div class="col-md-8">
                <div class="contenido-blog-art">
                    <img class="mb-4" src="assets/img/blog/firma-autógrafa.jpeg" alt="Archivos en la nube" style="width:100%">
                    <h3>¿Qué es la e.firma?</h3>
                   <p>Conforme al artículo 97 del CC, una firma electrónica se considera avanzada o fiable si cumple con los siguientes requisitos: “I. Los Datos de Creación de la Firma, en el contexto en que son utilizados, corresponden exclusivamente al Firmante; II. Los Datos de Creación de la Firma estaban, en el momento de la firma, bajo el control exclusivo del Firmante; III. Es posible detectar cualquier alteración de la Firma Electrónica hecha después del momento de la firma, y  IV. Respecto a la integridad de la información de un Mensaje de Datos, es posible detectar cualquier alteración de ésta hecha después del momento de la firma”.</p>
                    <p>La legislación mercantil permite el uso de cualquier método de firma electrónica que demuestre técnicamente el cumplimiento de dichos requisitos, según el principio de neutralidad tecnológica prevista en los artículos 89, segundo párrafo, 96 y 97, último párrafo, todos del CC.</p>
                    <p>El artículo 17-F del Código Fiscal de la Federación permite la posibilidad de los particulares utilicen como medio de autenticación o firmado de documentos electrónicos el certificado de firma electrónica avanzada generada en el ámbito fiscal, y conocido como e.firma. Para tal fin, esea misma disposición también establece la posibilidad de que el Servicio de Administración Tributaria autorice a determinadas personas que cumplan con estrictos requerimientos técnicos para que accedan a un servicio público de verificación y autenticación de los certificados de firmas electrónicas avanzadas.</p>
                    <p>De manera técnica general, el proceso de firma electrónica avanzada de documentos por medio de los referidos certificados públicos se efectúa mediante la asociación lógica del mensaje de datos y los datos de creación de firma electrónica del emisor, mediante un proceso de encriptación con infraestructura de clave pública. El resultado es un archivo electrónico que admite su ulterior consulta y un proceso informático de validación de la autoría y de su integridad, técnicamente infalible, por su base en principios matemáticos. Este proceso de validación se robustece con la consulta al servicio público verificación y autenticación de los certificados de firmas electrónicas avanzadas ofrecido por el Servicio de Administración Tributaria a través de las personas autorizadas para tal fin.</p>
                    <p>VI.- Seguridad jurídica aportada.  Los artículos 89, 89 bis y 97 del CC confieren absoluta fuerza jurídica a los documentos firmados electrónicamente en forma avanzada, conforme a los procedimientos que Legalex GS tiene autorizados y acreditados y que han quedado descritos en este documento.</p>
                    <p>Adicionalmente, el sello digital de tiempo es un mecanismo con reconocimiento legal (artículo 89 del CC) para dejar constancia de la fecha cierta de los documentos privados mercantiles, cumpliéndose así el requerimiento fiscal expuesto en la tesis de jurisprudencia 2a./J. 161/2019 de la Segunda Sala de la Suprema Corte de Justicia de la Nación, bajo el rubro “DOCUMENTOS PRIVADOS. DEBEN CUMPLIR CON EL REQUISITO DE "FECHA CIERTA" TRATÁNDOSE DEL EJERCICIO DE LAS FACULTADES DE COMPROBACIÓN, PARA VERIFICAR EL CUMPLIMIENTO DE OBLIGACIONES FISCALES DEL CONTRIBUYENTE”. </p>

                </div>
            </div>
            <div class="col-md-4">
                <div class="blog-aside">
                    <h1 class="h4">Síguenos</h1>
                    <p>
                       <a href="https://www.facebook.com/Legalex-GS-104654391141572/"  target="_blank"><i class="fab fa-facebook text-white"></i> </a> &nbsp; 
                       <a href="https://www.youtube.com/channel/UC32g56nRFbuzZRsr-1yjQcw"  target="_blank"><i class="fab fa-youtube  text-white"></i></a>  &nbsp; 
                       <a href="https://www.linkedin.com/company/legalex-gs/?originalSubdomain=mx"  target="_blank"><i class="fab fa-linkedin  text-white"></i></a> 
                    </p>
                </div>
                <div class="blog-aside">
                    <h1 class="h4">Crea contratos digitales</h1>
                    <p>¿Sabías que en Legalex GS puedes crear contratos digitales?</p>
                    
                    <a href="Soluciones/Covenant/index.php" class="btn btn-outline-generico btn-sm mt-3">Saber más</a>
                </div>
            </div>
            <div class="col-md-12  ">
                <div class=" final-banner-blog ">
                    <div class="texto">
                        <div>
                        <h1 class="h3">Contacta a ventas</h1>
                        <p>¿Necesitas crear contratos para tus clientes o empleados?</p>
                        </div>
                        
                    </div>
                    <div class="boton-footer-blog">
                        <a href="nosotros/contacto" class="btn btn-outline-generico">Contactar a Ventas</a>
                    </div>
                </div>
            </div>
        </div>
       
        
    </section>
<?php
    include 'inc/pie.php';
?>