<?php 
$pageLink = "Centro de Recursos";
    include 'inc/cab.php';
?>
<head>

<style>

.precio-info span.rubri {
    background: #fff;
    bottom: 100%;
    box-shadow: 1px 1px 15px rgb(104 115 125);
    color: #03363d;
    display: none;
    font-size: .7rem;
    left: -1rem;
    line-height: 1.25em;
    padding: 0.6rem;
    position: absolute;
    width: 6rem;
    z-index: 5;
    border-radius: 5px;
    max-width: 160px;
    left: -1rem;
    left: auto;
    bottom: 22px;
    width: 500px;
    text-align: justify;
}



   
   


</style>
</head>
   <section class="banner-recursos precios-ban " style="background-color:#e5ebf1" data-animate="ts-fadeInLeft" data-ts-delay=".10s">
        <div class="header-recursos container">
          <br>
          <span style="color:#c56122;" class="wrapper" >
              <h1>Planes y precios</h1>                           
          </span> 
          
          <!-- <p style=" font-size:1.3rem; color:black">Seleccione una solución</p> -->
          <p style=" font-size:1.3rem; color:black">Legalex GS, tiene las siguientes opciones para ti:</p>
        </div>
        <div class="container rel-1">
        <div class="row">
        <div class="col-md-6 col-sm-6 col-lg-3" data-animate="ts-fadeInLeft" data-ts-delay=".20s"> 
            <div class="price-box">
                <div class="price-body">
                    <div class="price-inner">
                        <header class="price-header">
                        <h4 class="price-title text-center">Personal</h4>
                        <div class="price">
                            <span class="price-currency">$</span>
                            <strong class="price-amount">999</strong><span class="price-delimiter">/</span><span class="price-period">mes *</span>
                        </div>
                        </header>
                        <div class="price-features">
                            <ul>
                                <li>Firma Electrónica Avanzada con Sello Digital</li>
                                <li>Firma Autógrafa</li>
                                <li>10 Documentos por mes</li>
                                <li>1 Usuario</li>
                                <li>Firma con App móvil o vía web</li>
                            </ul>
                        </div>
                        <div class="price-footer">
                        <a href="checkout" class="btn btn-outline-generico btn-sm">Seleccionar Plan</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class=" col-md-6 col-sm-6 col-lg-3 wow flipInY" data-animate="ts-fadeInLeft" data-ts-delay=".30s">
            <div class="price-box">
                <div class="price-body">
                    <div class="price-inner">
                        <header class="price-header">
                        <h4 class="price-title text-center">Negocios</h4>
                        <div class="price">
                            <span class="price-currency">$</span>
                            <strong class="price-amount">1,799</strong><span class="price-delimiter">/</span><span class="price-period">mes *</span>
                        </div>
                        </header>
                        <div class="price-features">
                            <ul>
                                <li>Firma Electrónica Avanzada con Sello Digital</li>
                                <li>Firma Autógrafa</li>
                                <li>20 Documentos por mes</li>
                                <li>1 Usuario</li>
                                <li>Firma con App móvil o vía web</li>
                            </ul>
                        </div>
                        <div class="price-footer">
                        <a href="checkout" class="btn btn-outline-generico btn-sm">Seleccionar Plan</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="  col-md-6 col-sm-6 col-lg-3 " data-animate="ts-fadeInLeft" data-ts-delay=".40s" >
            <div class="price-box">
                <div class="price-body">
                    <div class="price-inner">
                        <header class="price-header">
                        <h4 class="price-title text-center">Empresarial</h4>
                            <div class="price">
                                <span class="price-currency">$</span>
                                <strong class="price-amount">3,399</strong><span class="price-delimiter">/</span><span class="price-period">mes *</span>
                            </div>
                        </header>
                        <div class="price-features">
                            <ul>
                                <li>Firma Electrónica Avanzada con Sello Digital</li>
                                <li>Firma Autógrafa</li>
                                <li>30 Documentos por mes</li>
                                <li>3 Usuarios</li>
                                <li>Firma con App móvil o vía web</li>
                                
                            </ul>
                        </div>
                        <div class="price-footer">
                            <a href="checkout" class="btn btn-outline-generico btn-sm">Seleccionar Plan</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="leading col-md-6 col-sm-6 col-lg-3" data-animate="ts-fadeInLeft" data-ts-delay=".50s">
            <div class="price-box">
                <div class="price-body">
                    <div class="price-inner">
                        <header class="price-header">
                        <h4 class="price-title text-center">Corporativo</h4>
                            <div class="price">
                                
                                <strong class="price-amount " style="font-size:x-large;">Precio <br>Personalizado</strong>
                            </div>
                        </header>
                        <div class="price-features">
                            <ul>
                                <li>Firma Electrónica Avanzada con Sello Digital</li>
                                <li>Firma Autógrafa</li>
                                <li>Por firma
                                    <div class="precio-info"> <span class="tooltips">Cantidad de firmas para los documentos.</span></div>
                                </li>
                                <li>Usuarios Ilimitados</li>
                                <li>Firma con App móvil o vía web</li>
                                <li>Permite comunicación Vía API</li>
                                <li>Servicio de CCMD NOM-151</li>
                            </ul>
                        </div>
                        <div class="price-footer">
                        <a href="Soluciones/Covenant/contacto" class="btn btn-outline-generico btn-sm">Contactar Asesor </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="leading col-md-12 col-sm-12">
          <br>
          <p style=" font-size: .9rem;" >* Todos nuestros precios son en pesos mexicanos e incluyen I.V.A. (Impuesto al Valor Agregado) y están sujetos a cambios sin previo aviso.</p>
        </div>
    </div>
    </div>

    </section>

    <section id="comparacion" class="mb-5 mt-5" >
      <div class="container " >
       
        <div class="text-center mb-5 " data-animate="ts-fadeInUp" data-ts-delay=".10s">
          <h2>Compara nuestros planes de Covenant</h2>
          <p>Compare los diferentes planes de Covenant a fin de encontrar la mejor opción para su empresa.</p>
       <a href="assets/centro-de-recursos/paquetes/PDF-informativo.pdf" target="_blank" class="btn btn-outline-generico btn-sm " >Descargar PDF comparativo</a>
         
        </div>
       
        
    <div id="tabla-pdf">
      <div class="precio-tabla-row titulo container"  id="tabla-tit-nav" >
        <div class="precio-tabla-col precio-tabla-col-descripcion" id="top-titulo">
          </div>

          <div class="precio-tabla-col">
            <div class="precio-col-header">
              <p>PERSONAL</p>
              <a href="checkout" class="contratar-text btn btn-outline-generico btn-sm">Contratar</a>
              <a href="checkout" class="contratar-icon btn btn-outline-generico btn-sm"><i class="fas fa-2x fa-shopping-cart"></i></a>
            </div>
            
          </div>
          <div class="precio-tabla-col">
            
            <div class="precio-col-header">
              <p>NEGOCIOS</p>
              <a href="checkout" class="contratar-text  btn btn-outline-generico btn-sm">Contratar</a>
              <a href="checkout" class="contratar-icon btn btn-outline-generico btn-sm"><i class="fas fa-2x fa-shopping-cart"></i></a>

            </div>
          </div>
          <div class="precio-tabla-col">
            
            <div class="precio-col-header">
              <p>EMPRESARIAL</p>
              <a href="checkout" class="contratar-text btn btn-outline-generico btn-sm">Contratar</a>
              <a href="checkout" class="contratar-icon btn btn-outline-generico btn-sm"><i class="fas fa-2x fa-shopping-cart"></i></a>

            </div>
          </div>
          <div class="precio-tabla-col">
            
            <div class="precio-col-header">
              <p>CORPORATIVO </p>
              <a href="Soluciones/Covenant/contacto" class="contratar-text  btn btn-outline-generico btn-sm">Contactar</a>
              <a href="checkout" class="contratar-icon btn btn-outline-generico btn-sm"><i class="fas fa-2x fa-shopping-cart"></i></a>

            </div>
          </div>
        </div>
        <div class="precio-tabla-caracteristicas precio-tabla-row " id="prueba">
          <div class="precio-tabla-col precio-tabla-col-descripcion ">
            <p>Documentos por mes<div class="precio-info"> <span class="tooltips">Cantidad de documentos disponibles por mes.</span></div></p>
          </div>
          <div class="precio-tabla-col">
            <p>10</p>
          </div>
          <div class="precio-tabla-col">
            <p>20</p>
          </div>
          <div class="precio-tabla-col">
            <p>30</p>
          </div>
          <div class="precio-tabla-col">
            <p>Por firma</p>
          </div>
        </div>
        <div class="precio-tabla-caracteristicas precio-tabla-row">
          <div class="precio-tabla-col precio-tabla-col-descripcion ">
            <p>Número de usuarios <div class="precio-info"> <span class="tooltips">Cantidad de usuarios para entrar y administrar la aplicación.</span></div></p>
          </div>
          <div class="precio-tabla-col">
            <p>1</p>
          </div>
          <div class="precio-tabla-col">
            <p>1</p>
          </div>
          <div class="precio-tabla-col">
            <p>3</p>
          </div>
          <div class="precio-tabla-col">
            <p>Ilimitado</p>
          </div>
        </div>

        <div class="precio-tabla-caracteristicas precio-tabla-row">
          <div class="precio-tabla-col precio-tabla-col-descripcion ">
            <p>Firma Electrónica Avanzada con Certificación Digital <div class="precio-info"> <span class="tooltips">Acceso a la Firma Electrónica Avanzanda con Certificado Digital.</span></div></p>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
        </div>

        <div class="precio-tabla-caracteristicas precio-tabla-row">
          <div class="precio-tabla-col precio-tabla-col-descripcion ">
            <p>Firma Autógrafa Digital <div class="precio-info"> <span class="tooltips">La Firma Autógrafa Digital es aquella que es trazada en un documento digital por una persona con su puño y letra.</span></div></p>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
        </div>

        <div class="precio-tabla-caracteristicas precio-tabla-row">
          <div class="precio-tabla-col precio-tabla-col-descripcion ">
            <p>Sello Digital de Tiempo (SDT) <div class="precio-info"> <span class="tooltips">Es la generación de un Sello Digital de Tiempo en la creación de cada firma.</span></div></p>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
        </div>

        <div class="precio-tabla-caracteristicas precio-tabla-row">
          <div class="precio-tabla-col precio-tabla-col-descripcion ">
            <p>Firma de documentos <div class="precio-info"> <span class="tooltips">Los firmantes pueden firmar los documentos mediante Covenant Firma en web o desde su celular mediante la aplicación movil.</span></div></p>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
        </div>

        <div class="precio-tabla-caracteristicas precio-tabla-row">
          <div class="precio-tabla-col precio-tabla-col-descripcion ">
            <p>Covenant Firma (aplicación móvil) <div class="precio-info"> <span class="tooltips">Nuestra aplicación permite automatizar el proceso de firma solo con el uso de los biométricos del celular.</span></div></p>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
        </div>

      

        <div class="precio-tabla-caracteristicas precio-tabla-row">
          <div class="precio-tabla-col precio-tabla-col-descripcion ">
            <p>Orden de firmantes <div class="precio-info"> <span class="tooltips">Gestión de firmas, aplicar un orden secuencial en el proceso.</span></div></p>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
        </div>

        <div class="precio-tabla-caracteristicas precio-tabla-row">
          <div class="precio-tabla-col precio-tabla-col-descripcion ">
            <p>Rechazar documentos <div class="precio-info"> <span class="tooltips">El firmante puede rechazar documentos y registrar el motivo.</span></div></p>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
        </div>

        <div class="precio-tabla-caracteristicas precio-tabla-row">
          <div class="precio-tabla-col precio-tabla-col-descripcion ">
            <p>Cancelar documentos <div class="precio-info"> <span class="tooltips">El emisor del documento puede cancelar los documentos enviados a firmar y agregar motivos de cancelación.</span></div></p>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
        </div>

        <div class="precio-tabla-caracteristicas precio-tabla-row">
          <div class="precio-tabla-col precio-tabla-col-descripcion ">
            <p>Notificaciones de firma <div class="precio-info"> <span class="tooltips"> Notificaciones en tiempo real de firma a través de la aplicación y correo electrónico.</span></div></p>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
        </div>

        <div class="precio-tabla-caracteristicas precio-tabla-row">
          <div class="precio-tabla-col precio-tabla-col-descripcion ">
            <p>Notificaciones a terceros <div class="precio-info"> <span class="tooltips">Si así lo decide, podrá elegir que el documento sea enviado de manera automática a las direcciones de correo que decida, una vez firmado el documento por todos los participantes.</span></div></p>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
        </div>

        <div class="precio-tabla-caracteristicas precio-tabla-row">
          <div class="precio-tabla-col precio-tabla-col-descripcion ">
            <p>Tiempo de respuesta soporte técnico <div class="precio-info"> <span class="tooltips">Tiempo de respuesta de soporte técnico por chat y correo electrónico.</span></div></p>
          </div>
          <div class="precio-tabla-col">
            <p>24 Hrs.</p>
          </div>
          <div class="precio-tabla-col">
            <p>12 Hrs.</p>
          </div>
          <div class="precio-tabla-col">
            <p>6 Hrs.</p>
          </div>
          <div class="precio-tabla-col">
            <p>2 Hrs.</p>
          </div>
        </div>

        <div class="precio-tabla-caracteristicas precio-tabla-row">
          <div class="precio-tabla-col precio-tabla-col-descripcion ">
            <p class="textLg">Almacenamiento <div class="precio-info"> <span class="tooltips">Almacenamiento y disponibilidad 24/7 de los documentos generados.</span></div></p>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
          <div class="precio-tabla-col">
           <div class="precio-tabla-check"></div>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
        </div>

        <div class="precio-tabla-caracteristicas precio-tabla-row">
          <div class="precio-tabla-col precio-tabla-col-descripcion ">
            <p>Eliminar documentos <div class="precio-info"> <span class="tooltips">Eliminar documentos del listado cuando se encuentran capturados, cancelados y firmados.</span></div></p>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
          <div class="precio-tabla-col">
           <div class="precio-tabla-check"></div>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
        </div>

        <div class="precio-tabla-caracteristicas precio-tabla-row">
          <div class="precio-tabla-col precio-tabla-col-descripcion ">
            <p>Estatus de firma <div class="precio-info"> <span class="tooltips">Conocer en tiempo real en el que personas han firmado el documento.</span></div></p>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
          <div class="precio-tabla-col">
           <div class="precio-tabla-check"></div>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
        </div>

        <div class="precio-tabla-caracteristicas precio-tabla-row">
          <div class="precio-tabla-col precio-tabla-col-descripcion ">
            <p>Plazo para cancelar documentos <div class="precio-info"> <span class="tooltips">Definir un plazo de tiempo para la cancelación automática del documento que fue enviado a firmar al no concluir el proceso de firma.</span></div></p>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
          <div class="precio-tabla-col">
           <div class="precio-tabla-check"></div>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
        </div>

        <div class="precio-tabla-caracteristicas precio-tabla-row">
          <div class="precio-tabla-col precio-tabla-col-descripcion ">
            <p>Notificación de seguimiento de documentos <div class="precio-info"> <span class="tooltips">Se puede definir una fecha futura en la que el sistema notifique automáticamente el vencimiento de un documento o recordatorio de otra acción.</span></div></p>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
          <div class="precio-tabla-col">
           <div class="precio-tabla-check"></div>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
        </div>

        <div class="precio-tabla-caracteristicas precio-tabla-row">
          <div class="precio-tabla-col precio-tabla-col-descripcion ">
            <p>Creación de plantillas <div class="precio-info"> <span class="tooltips">Editor de texto para la creación de documentos modelo (plantillas), que permite utilizarlas las veces que se requiera, solo capturando los datos variable que intervienen en el documento (formulario).</span></div></p>
          </div>
          <div class="precio-tabla-col">
          </div>
          <div class="precio-tabla-col">
          </div>
          <div class="precio-tabla-col">
           <div class="precio-tabla-check"></div>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
        </div>
      
        <div class="precio-tabla-caracteristicas precio-tabla-row">
          <div class="precio-tabla-col precio-tabla-col-descripcion ">
            <p>Diseño de plantillas <div class="precio-info"> <span class="tooltips">Permite crear un alto diseño gráfico en los documentos y plantillas, agregar marcas de agua, imágenes, logos y numeración sin límite de creación.</span></div></p>
          </div>
          <div class="precio-tabla-col">
          </div>
          <div class="precio-tabla-col">
          </div>
          <div class="precio-tabla-col">
           <div class="precio-tabla-check"></div>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
        </div>

        <div class="precio-tabla-caracteristicas precio-tabla-row">
          <div class="precio-tabla-col precio-tabla-col-descripcion ">
            <p>Crear estructura organizacional <div class="precio-info"> <span class="tooltips">Dividir la operación en una estructura organizacional.</span></div></p>
          </div>
          <div class="precio-tabla-col">
          </div>
          <div class="precio-tabla-col">
          </div>
          <div class="precio-tabla-col">
           <div class="precio-tabla-check"></div>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
        </div>

        <div class="precio-tabla-caracteristicas precio-tabla-row">
          <div class="precio-tabla-col precio-tabla-col-descripcion ">
            <p>Control de cambios <div class="precio-info"> <span class="tooltips">Control de cambios de la información final que fue capturada en el documento con la intervención del cliente (dentro del mismo sistema) previo a su envío de firma.</span></div></p>
          </div>
          <div class="precio-tabla-col">
          </div>
          <div class="precio-tabla-col">
          </div>
          <div class="precio-tabla-col">
           <div class="precio-tabla-check"></div>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
        </div>

        <div class="precio-tabla-caracteristicas precio-tabla-row">
          <div class="precio-tabla-col precio-tabla-col-descripcion ">
            <p>Crea un expediente digital <div class="precio-info"> <span class="tooltips">Integración de un expediente digital respecto al documento firmado (documento de soporte para la integración del documento, por ejemplo: identificación oficial, comprobante de domicilio, etc.).</span></div></p>
          </div>
          <div class="precio-tabla-col">
          </div>
          <div class="precio-tabla-col">
          </div>
          <div class="precio-tabla-col">
           <div class="precio-tabla-check"></div>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
        </div>

        <div class="precio-tabla-caracteristicas precio-tabla-row">
          <div class="precio-tabla-col precio-tabla-col-descripcion ">
            <p>Incorpora anexos a tus expedientes <div class="precio-info"> <span class="tooltips">Integración y administración de anexos limitados al documento o contrato principal.</span></div></p>
          </div>
          <div class="precio-tabla-col">
          </div>
          <div class="precio-tabla-col">
          </div>
          <div class="precio-tabla-col">
           <div class="precio-tabla-check"></div>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
        </div>

        <div class="precio-tabla-caracteristicas precio-tabla-row">
          <div class="precio-tabla-col precio-tabla-col-descripcion ">
            <p>Validación de diseño y contenido de plantillas <div class="precio-info"> <span class="tooltips">Validación de diseño y contenido de plantillas.</span></div></p>
          </div>
          <div class="precio-tabla-col">
          </div>
          <div class="precio-tabla-col">
          </div>
          <div class="precio-tabla-col">
           <div class="precio-tabla-check"></div>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
        </div>

        <div class="precio-tabla-caracteristicas precio-tabla-row">
          <div class="precio-tabla-col precio-tabla-col-descripcion ">
            <p>Asignación de plantillas <div class="precio-info"> <span class="tooltips">Asignación organizacional de documentos y plantillas con base a departamentos y áreas.</span></div></p>
          </div>
          <div class="precio-tabla-col">
          </div>
          <div class="precio-tabla-col">
          </div>
          <div class="precio-tabla-col">
           <div class="precio-tabla-check"></div>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
        </div>

        <div class="precio-tabla-caracteristicas precio-tabla-row">
          <div class="precio-tabla-col precio-tabla-col-descripcion ">
            <p>Asignación de flujo para validación de documentos <div class="precio-info"> <span class="tooltips">Asignación del flujo para validación de documentos.</span></div></p>
          </div>
          <div class="precio-tabla-col">
          </div>
          <div class="precio-tabla-col">
          </div>
          <div class="precio-tabla-col">
           <div class="precio-tabla-check"></div>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
        </div>

        <div class="precio-tabla-caracteristicas precio-tabla-row">
          <div class="precio-tabla-col precio-tabla-col-descripcion ">
            <p>Edición de documentos basados en plantillas <div class="precio-info"> <span class="tooltips">Edición de información de documentos (formularios) antes de enviarse a firma.</span></div></p>
          </div>
          <div class="precio-tabla-col">
          </div>
          <div class="precio-tabla-col">
          </div>
          <div class="precio-tabla-col">
           <div class="precio-tabla-check"></div>
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
        </div>

        <div class="precio-tabla-caracteristicas precio-tabla-row">
          <div class="precio-tabla-col precio-tabla-col-descripcion ">
            <p>Servicio vía API <div class="precio-info"> <span class="tooltips">Servicio vía API para conectar entre plataformas.</span></div></p>
          </div>
          <div class="precio-tabla-col">
          </div>
          <div class="precio-tabla-col">
          </div>
          <div class="precio-tabla-col">
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
        </div>

        <div class="precio-tabla-caracteristicas precio-tabla-row">
          <div class="precio-tabla-col precio-tabla-col-descripcion ">
            <p>Generación de NOM-151 en cada documento <div class="precio-info"> <span class="tooltips">Generación de NOM-151 en cada documento que se crea.</span></div></p>
          </div>
          <div class="precio-tabla-col">
          </div>
          <div class="precio-tabla-col">
          </div>
          <div class="precio-tabla-col">
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
        </div>

        <div class="precio-tabla-caracteristicas precio-tabla-row">
          <div class="precio-tabla-col precio-tabla-col-descripcion ">
            <p>Administración de usuarios <div class="precio-info"> <span class="tooltips">Administra de forma libre a los usuarios.</span></div></p>
          </div>
          <div class="precio-tabla-col">
          </div>
          <div class="precio-tabla-col">
          </div>
          <div class="precio-tabla-col">
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
        </div>

        <div class="precio-tabla-caracteristicas precio-tabla-row">
          <div class="precio-tabla-col precio-tabla-col-descripcion ">
            <p>Administración de la estructura organizacional<div class="precio-info"> <span class="tooltips">Administración de la estructura organizacional.</span></div></p>
          </div>
          <div class="precio-tabla-col">
          </div>
          <div class="precio-tabla-col">
          </div>
          <div class="precio-tabla-col">
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
        </div>

        <div class="precio-tabla-caracteristicas precio-tabla-row">
          <div class="precio-tabla-col precio-tabla-col-descripcion ">
            <p>Administración de la plataforma<div class="precio-info"> <span class="tooltips">Consulta centralizada de la plataforma (plantillas, documentos, documentos PDF, documentos cancelados o eliminados).</span></div></p>
          </div>
          <div class="precio-tabla-col">
          </div>
          <div class="precio-tabla-col">
          </div>
          <div class="precio-tabla-col">
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
        </div>

        <div class="precio-tabla-caracteristicas precio-tabla-row">
          <div class="precio-tabla-col precio-tabla-col-descripcion ">
            <p>Servicios y soporte especializado para la conectividad vía API<div class="precio-info"> <span class="tooltips">Servicios y soporte especializado para conectar las plataformas vía API.</span></div></p>
          </div>
          <div class="precio-tabla-col">
          </div>
          <div class="precio-tabla-col">
          </div>
          <div class="precio-tabla-col">
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
        </div>     
        
        <div class="precio-tabla-caracteristicas precio-tabla-row">
          <div class="precio-tabla-col precio-tabla-col-descripcion ">
            <p>Correo de notificación perzonalizado<div class="precio-info"> <span class="tooltips">Creación de la notifiación del correo.</span></div></p>
          </div>
          <div class="precio-tabla-col">
          </div>
          <div class="precio-tabla-col">
          </div>
          <div class="precio-tabla-col">
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
        </div>

        <div class="precio-tabla-caracteristicas precio-tabla-row">
          <div class="precio-tabla-col precio-tabla-col-descripcion ">
            <p>Rubrica de documentos<div class="precio-info"> <span class="tooltips rubri" >Se imprimen las Firmas Electrónicas (resumida) de los involucrados en cada hoja del documento digital el cual fue enviado a firma. Está firma,
               se repite en el costado derecho del documento digital y admite solo la impresión de 5 firmas.</span></div></p>
          </div>
          <div class="precio-tabla-col">
          </div>
          <div class="precio-tabla-col">
          </div>
          <div class="precio-tabla-col">
          </div>
          <div class="precio-tabla-col">
            <div class="precio-tabla-check"></div>
          </div>
        </div>
        
      </div>
    </div>
    </section>
   

 

<?php 
include 'inc/pie.php';
?>
<script>
  var ancho = window.innerWidth;
  //console.log("Ancho "+  ancho);
  if(ancho >= 577){
    barraGr();
  }else{
    barraSm();
  }
  $(window).on("resize", function(){
    ancho = window.innerWidth;
    //console.log("Ancho "+  ancho);
    if(ancho >= 577){
      barraGr();
    }else{
      barraSm();
    }
  });


  function barraSm(){
    // Funciona para pantallas grandes
    window.onscroll = function() {myFunction2()};
      var nav = document.getElementById("tabla-tit-nav");
      var contenedor = document.getElementById('comparacion');
      var sticky2 = contenedor.offsetTop;
      var suma2 = sticky2 + contenedor.offsetHeight;
      // console.log(sticky2);
      function myFunction2() { 
          if (window.pageYOffset >= sticky2) {
            if(window.pageYOffset <= suma2){
              nav.classList.add("is-fixed");
            }else{
              nav.classList.remove("is-fixed");
            }  

          }else{
            nav.classList.remove("is-fixed");
          }
      }
      // Fin funcion pantallas grandes
  }

  function barraGr(){
    // Funcion en pantallas pequeñas
    window.onscroll = function() {myFunction()};
          var navpre = document.getElementById("tabla-tit-nav");
      var cont = document.getElementById('comparacion');
      var barra= document.getElementById("top-titulo");
      var sticky = cont.offsetTop;
      var suma = sticky + cont.offsetHeight;
      // console.log(sticky);
      function myFunction() { 
        if(window.pageYOffset >= sticky){
          if(window.pageYOffset >= barra.offsetHeight){
          navpre.classList.add("is-fixed");
          }else{
            navpre.classList.remove("is-fixed");
          }
        }else{
          navpre.classList.remove("is-fixed");
        }
      }
      // Fin de funcion en pantallas pequeñas
  }
////En ancho 577 cambia la configuración

  function descargaPDF(){

    document.getElementById("holi").style.display = "block";
    //const holaaaa = "<h2> hola lulu </h2>"
    const $elementoParaConvertir = document.getElementById("tabla-pdf"); // <-- Aquí puedes elegir cualquier elemento del DOM
    
    console.log($elementoParaConvertir)
html2pdf()
    .set({
        margin: 1,
        filename: 'documento.pdf',
        image: {
            type: 'jpeg',
            quality: 0.98
        },
        html2canvas: {
            scale: 3, // A mayor escala, mejores gráficos, pero más peso
            letterRendering: true,
        },
        jsPDF: {
            unit: "in",
            format: "a3",
            orientation: 'portrait' // landscape o portrait
        }
    })
    .from($elementoParaConvertir)
    .save()
    .catch(err => console.log(err));
    setInterval(() => {
      document.getElementById("holi").style.display = "none";
    }, 40);
   
  }
  

</script>     