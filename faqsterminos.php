<?php
$pageLink = "Preguntas frecuentes";
include 'inc/cab.php';
?>
<style>
    .buscador-faqs {
        display: flex;
        justify-content: center;
        margin-bottom: 3rem;
    }

    .inputbuscar {
        display: flex;
        box-shadow: 0px 0px 10px 1px #00000052;
        align-items: center;
        background-color: #fff;
        border-radius: 15px;
        width: 50%;
    }

    .inputbuscar input {
        border: none;
        padding: 1.3rem 0.5rem;
        border-radius: 15px;
        width: 100%;
    }

    .inputbuscar input:focus {
        outline: none;
    }

    .inputbuscar .iconobuscar {
        margin: 0 0.2rem 0 1rem;
    }

    .lateral_faq {
        height: 100vh;
        box-shadow: 4px 0 4px -2px #00000054;
        padding: 0 15px;
        position: fixed;
        left: 0;
        width: 17.7%;
        background-color: #fff;
        font-size: .9375rem;
    }

    .item_nav_faq {
        display: flex;
        padding: 1rem 1.2rem;
        border-radius: 15px;
        margin-bottom: 10px;
    }

    .item_nav_faq.active a div span {
        color: #fff;
    }

    .item_nav_faq.active {
        background-color: #051c33;
        color: #fff;
    }

    .item_nav_faq.active:hover {
        background-color: #051c33;
        cursor: auto;
    }

    .item_nav_faq .icono_nav {
        padding: 0 1.2rem 0 0rem;
    }

    .item_nav_faq:hover {
        background-color: #9ac5f0;
        cursor: pointer;
    }

    .select_mob_faqs {
        display: none;
    }

    @media (max-width:767px) {
        .lateral_faq {
            display: none;
        }

        .select_mob_faqs {
            display: flex;
            justify-content: center;
        }

        .inputbuscar {
            width: 90%;
        }

        .faqs .container-fluid {
            padding-right: 0px;
            padding-left: 0px;
        }

        .faqs .mob-faq {
            padding-right: 15px;
            padding-left: 15px;
        }
    }
</style>
<main style="background-color:#f5f5f5">
    <section class="faqs">
        <div class="container-fluid">
            <div class="row">

                <?php
                include 'menuFaqs.php';
                ?>
                <div class="col-md-10 mt-5 contenedor-mob" style="text-align:justify">

                    <div class="ts-title text-center mt-4">
                        <h2>Terminos utilizados en Legalex GS</h2>
                    </div>
                    <div>
                        <p class="text-center">A continuación se muestran definiciones de terminos utilizados en este sitio para su mayor compresión.</p>
                    </div>

                    <center>
                        <p id="textoselect" hidden>Seleccione alguna sección de la cual dese buscar</p>
                    </center>
                    <div class="select_mob_faqs mb-5 form-group container">
                        <select name="" id="whatfaqs" class="form-control" onchange="whatsfaqs()">
                            <option value="Covenant Documentos">Covenant Documentos</option>
                            <option value="Covenant Firmas">Covenant Firmas</option>
                            <option value="Covenant App">Covenant App</option>
                            <option value="Sello Digital de Tiempo">Sello Digital de Tiempo</option>
                            <option value="Constancia NOM-151">Constancia NOM-151</option>
                            <option value="Planes de Covenant">Planes de Covenant</option>
                            <option value="Definiciones" selected>Definiciones</option>
                        </select>
                    </div>
                    <div class="buscador-faqs">
                        <div class="inputbuscar">
                            <div class="iconobuscar">
                                <i class="fa fa-search"></i>
                            </div>
                            <input type="input" placeholder="Escriba algo a buscar..." id="buscador">
                        </div>
                    </div>
                    <div id="accordion" class="row mob-faq justify-content-center mb-5">
                        <div id="" class="accordion col-md-11 ">
                            <div class="card">
                                <div class="card-header faq" id="headingOne">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link  pregunta_search" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            Secretaría de Economía Federal
                                        </button>
                                    </h5>
                                    <span id="p1" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>

                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="card-body">
                                        La Secretaría de Economía es una de las secretarías de estado que integran el denominado gabinete legal del Presidente de México. Es el despacho del poder ejecutivo federal encargado de la administración, regulación y fomento de la industria, el comercio y la prestación de servicios.
                                        <br><br>
                                        Es la encargada de diseñar, planear, ejecutar y coordinar las políticas públicas en materia de desarrollo económico. Lo anterior incluye establecer, vigilar y regular los precios en productos y servicios del sector privado; inspeccionar el cumplimiento de leyes y normatividades en la comercialización de productos y servicios;
                                        colaborar con la secretaría de Hacienda en la fijación de impuestos y aranceles a las actividades comerciales e industriales; operar la Procuraduría Federal del Consumidor; establecer y normar las bases de datos de la propiedad industrial y mercantil; administrar el buen uso de la Norma Oficial Mexicana y el Sistema Internacional de Unidades; y en coordinación con la Secretaría de Bienestar, fomentar el desarrollo económico a partir de formas de organización colectiva en comunidades rurales o de alta marginación urbana.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="headingTwo">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            ¿Qué es la infraestructura de clave pública o PKI cuál es su relación con la firma electrónica?

                                        </button>
                                    </h5>
                                    <span id="pj2" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                    <div class="card-body">
                                        Una infraestructura de claves públicas (PKI) es un sistema de recursos, políticas y servicios que da soporte al uso del cifrado de claves públicas para autenticar a las partes que participan en una transacción.
                                        <br><br>
                                        No hay ningún estándar individual que defina los componentes de una Infraestructura de clave pública, pero normalmente un PKI consta de entidades emisoras de certificados (CA) y entidades emisoras de registro (RA). Las CA proporcionan los servicios siguientes:
                                        <br><br>
                                        Emisión de certificados digitales <br>
                                        Validación de certificados digitales <br>
                                        Revocación de certificados digitales <br>
                                        Distribución de claves públicas <br>
                                        El estándar X.509 proporcionan la base para la industria estándar de la infraestructura Public Key Infrastructure. <br><br>
                                        Consulte Certificados digitales para obtener más información sobre los certificados digitales y las entidades emisoras de certificados (CA). Las RA verifican la información proporcionada cuando se solicitan certificados digitales. Si la RA verifica esta información, la CA puede emitir un certificado digital para el solicitante.
                                        <br><br>
                                        Una PKI también puede proporcionar las herramientas para gestionar los certificados digitales y las claves públicas. Una PKI se describe a veces como una jerarquía fiable para la gestión de certificados digitales, aunque la mayor parte de las definiciones incluyen servicios adicionales. Algunas definiciones incluyen servicios de cifrado y firma digital, pero estos servicios no son esenciales para el funcionamiento de una PKI.

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="headingThree">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            ¿Qué es el Paper Less?

                                        </button>
                                    </h5>
                                    <span id="p3" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        Paperless es un concepto innovador que invita a trabajar sin papel en las oficinas, creado por la compañía Paperless en Chile en 2002, con la finalidad de digitalizar los documentos de las empresas, trabajando en el proceso de clean technology para reducir la huella de carbono en el planeta.
                                        <br><br>
                                        En la actualidad el sector empresarial invierte en la transformación de su negocio a la era digital, no obstante, continúa invirtiendo en el uso y almacenamiento de papeles, por lo tanto es importante que los empresarios comprendan que la reducción del uso de documentos físicos implica una mejora en el tiempo que se requiere para organizar, clasificar, almacenar y transportar archivos.
                                        <br><br>
                                        Sin embargo, pese a la resistencia a implementar este concepto, cabe resaltar que una de las áreas más beneficiadas en el uso de paperless es de facturación, al ser uno de los departamentos que más papel genera, la facturación electrónica ha sido acogida rápidamente en el sector comercial.
                                        <br><br>
                                        La implementación de paperless en tu empresa trae consigo grandes beneficios:
                                        <br><br>
                                        – Al digitalizar los documentos será mucho más fácil encontrar, almacenar y compartir cualquier archivo, desde cualquier lugar.
                                        <br><br>
                                        – Agiliza la automatización de los procesos, permitiendo mejorar el flujo de trabajo gracias a la facilidad de acceder a los documentos de manera inmediata y lograr identificar qué tareas hay inconclusas.
                                        <br><br>
                                        – Aumento en la productividad del personal de la oficina, pues ya no tienen que invertir tiempo en imprimir, fotocopiar y almacenar documentos, sin mencionar el tiempo que requiere la pérdida de archivos que se pueden traspapelar.
                                        <br><br>
                                        – La implementación de paperless en la empresa contribuye con la reducción de costos en compra de papeles y en mobiliario de almacenamiento, mientras promueve el concepto de empresa eco amigable.
                                        <br><br>
                                        – En el caso de la pequeña y mediana empresa tener una oficina sin papeles la puede llevar a no necesitar espacios grandes y disminuir costos de alquiler.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r4" aria-expanded="false" aria-controls="r4">
                                            ¿Qué es el no repudio?
                                        </button>
                                    </h5>
                                    <span id="p4" data-toggle="collapse" data-target="#r4" aria-expanded="false" aria-controls="r4"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r4" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        No repudio se refiere a un estado de negocios donde el supuesto autor de una declaración no es capaz de desafiar con éxito la validez de declaración o contrato. El término es a menudo visto en un entorno legal donde la autenticidad de una firma está siendo desafiada. En tal la autenticidad se está "repudiando".
                                        <br>
                                        <br>
                                        <h5>No repudio en seguridad digital</h5>
                                        Con respecto a la seguridad digital, el significado criptológico y aplicación de los cambios de no repudio significa:
                                        <ul>
                                            <li>Un servicio que proporciona pruebas de la integridad y origen de los datos.</li>
                                            <li>Una autentificación que con un alto aseguramiento pueda ser reafirmado como genuino.</li>
                                            <li>Pruebas de la integridad de los datos es típicamente el más fácil de estos requerimientos para ser cumplido. Un resumen criptográfico de datos, tal como el SHA2, es usualmente suficiente para establecer que la probabilidad de que cambios en los datos no se detecten sea extremadamente bajo. Incluso con esta seguridad, es todavía posible alterar los datos en tránsito, a través de un “hombre en el medio” o fraude electrónico. Debido a este flujo, la integridad de los datos es mejor reafirmada cuando el recipiente ya posee la información necesaria de verificación.</li>
                                        </ul>

                                        El método más común de reafirmar el origen digital de los datos es a través de certificados digitales, una forma de clave de infraestructura publica, la cual firma digitalmente la pertenencia. Ello puede usar también la encriptación. El origen digital únicamente significa que el certificado o firma de los datos puede ser, con razonable certeza, confiable en ser de alguien quien posee la clave privada correspondiente al certificado firmado. Si la clave no es apropiadamente protegida por el propietario original, la falsificación digital puede llegar a ser una preocupación mayor.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r5" aria-expanded="false" aria-controls="r5">
                                            ¿Qué es una (Prestador de Servicios de Certificación (PSC)?
                                        </button>
                                    </h5>
                                    <span id="p5" data-toggle="collapse" data-target="#r5" aria-expanded="false" aria-controls="r5"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r5" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        Un PSC es una organización pública o privada que está facultada por una autoridad para, entre otras cosas, expedir certificados de firma electrónica y emitir constancias de conservación de datos ( <a class="link-azul" class="Linkss" href="nom-151">NOM 151</a> ).
                                        La Secretaría de Economía es la autoridad principal que se encarga de otorgar los permisos a los PSCs (aquí una lista).
                                        Sin embargo, el Banco de México también está facultado para otorgar estos permisos. El SAT es un PSC autorizado por el Banco de México.
                                        Es común que la gente asocie los certificados que emite el SAT (e.firma/FIEL), en su carácter de PSC, con trámites y documentos fiscales.
                                        No obstante, los certificados que emite el SAT legalmente son equivalentes a los certificados que emiten los PSCs de la Secretaría de Economía y por lo tanto, pueden también ser usados en contextos no fiscales.
                                    </div>
                                </div>
                            </div>






                        </div>
                    </div>
                    <?php
                    include 'inc/pie.php';
                    ?>
                </div>
            </div>
        </div>
    </section>
</main>
<script type="text/javascript" src="assets/js/faqs.js"></script>
<script>
    switch (window.location.search) {
        case '?faq=secretaria-de-economia-federal':
            $('#p1').click();
            break;
        case '?faq=firma-electronica-pki':
            $('#pj2').click();
            break;
        case '?faq=paper-less':
            $('#p3').click();
            break;
        case '?faq=no-repudio':
            $('#p4').click();
            break;
        case '?faq=que-es-una-psc':
            $('#p5').click();
            break;
        default:
            break;
    }
</script>