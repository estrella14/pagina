<?php
include 'inc/cab.php';

?>

<!--Content
    ====================================================================================================================
    -->
<div class="ts-content">
    <!--HERO HEADER
        ================================================================================================================
        ================================================================================================================
        -->
    <header id="ts-hero" class="text-white">

        <div class="owl-carousel ts-slider" data-owl-dots="1" data-owl-loop="1" data-owl-items="1">

            <div class="ts-slide ts-has-overlay">
                <div class="container">
                    <h1>
                        <a href="#ts-content" class="ts-underline">Fideicomiso de Garantía de Crédito</a>
                    </h1>
                    <h4>Compartimos contigo el poder de la eficiencia, ofreciéndote seguridad y certeza jurídica</h4>
                    <a href="#ts-content" class="btn btn-outline-light">Conoce más</a>
                </div>
                <div class="ts-background">
                    <div class="ts-img-into-bg ts-background-image ts-parallax-element">
                        <img src="assets/img/slide1.jpg" alt="">
                    </div>
                </div>
            </div>
            <!--end Slide-->

            <div class="ts-slide ts-has-overlay">
                <div class="container">
                    <h1>
                        <a href="#" class="ts-underline">Fideicomiso de Garantía de Crédito</a>
                    </h1>
                    <h4>Compartimos contigo el poder de la eficiencia, ofreciéndote seguridad y certeza jurídica</h4>
                    <a href="#ts-content" class="btn btn-outline-light">Conoce más</a>
                </div>
                <div class="ts-background">
                    <div class="ts-img-into-bg ts-background-image ts-parallax-element">
                        <img src="assets/img/slide2.jpg" alt="">
                    </div>
                </div>
            </div>
            <!--end Slide-->

            <div class="ts-slide ts-has-overlay">
                <div class="container">
                    <h1>
                        <a href="#ts-content" class="ts-underline">Fideicomiso de Garantía de Crédito</a>
                    </h1>
                    <h4>Compartimos contigo el poder de la eficiencia, ofreciéndote seguridad y certeza jurídica</h4>
                    <a href="#ts-content" class="btn btn-outline-light">Conoce más</a>
                </div>
                <div class="ts-background">
                    <div class="ts-img-into-bg ts-background-image ts-parallax-element">
                        <img src="assets/img/slide3.jpg" alt="">
                    </div>
                </div>
            </div>
            <!--end Slide-->

        </div>

    </header>

    <!--MAIN CONTENT
        ================================================================================================================
        ================================================================================================================
        -->
    <main id="ts-content">



        <!--Blockquote
            ============================================================================================================
            -->
        <section id="blockquote" class="ts-block" data-animate="ts-fadeInLeft" data-ts-delay=".4s">
            <div class="container">

                <div class="ts-title">
                    <h2>¿Que es un Fideicomiso de Garantía de Crédito?</h2>
                </div>
                <blockquote class="blockquote mb-5">
                    Es un contrato a través del cual una persona (Fideicomitente) transmite sus bienes a un Fideicomiso para que el Fiduciario (CREDIX GS) conserve la titularidad de los bienes con el fin de asegurar el cumplimiento de las obligaciones existentes entre el Fideicomitente y un acreedor (Fideicomisario).
                </blockquote>

                <hr>

                <h3>¿Cómo funciona?</h3>

                <blockquote>
                    En caso de incumplimiento de las obligaciones pactadas entre el Fideicomitente y su acreedor, se establece un procedimiento convencional de venta de los bienes aportados al fideicomiso, para que el fiduciario realice el pago a favor del acreedor con el producto de dicha venta o bien, se lleve a cabo la adjudicación del inmueble a favor del acreedor.
                </blockquote>

            </div>
        </section>

        <!--Our Experience
            ============================================================================================================
            -->
        <section class="ts-block text-white pb-0" data-bg-color="#191a21">
            <div class="container">

                <div class="ts-title text-center">
                    <h2>Beneficios del Fideicomiso con Garantía Inmobiliaria</h2>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <ul class="list list-text list-dashed">

                            <li data-animate="ts-fadeInRight">
                                <h4> El inmueble queda registrado ante el Registro Público de la Propiedad a favor del Fiduciario. </h4>

                            </li>
                            <!--end Experience block-->

                            <li data-animate="ts-fadeInRight" data-ts-delay=".1s">
                                <h4>El patrimonio de un fideicomiso se considera inembargable.</h4>

                            </li>
                            <!--end Experience block-->

                            <li data-animate="ts-fadeInRight" data-ts-delay=".2s">
                                <h4>Celeridad en la recuperación en caso de incumplimiento.</h4>

                            </li>
                            <!--end Experience block-->
                            <li data-animate="ts-fadeInRight" data-ts-delay=".2s">
                                <h4>Protección al acreedor en caso de muerte del acreditado.</h4>

                            </li>
                            <!--end Experience block-->
                            <li data-animate="ts-fadeInRight" data-ts-delay=".2s">
                                <h4>El acreedor puede tener liquidez al ceder sus derechos Fideicomisarios a un tercero.</h4>

                            </li>
                            <!--end Experience block-->



                        </ul>
                    </div>


                    <div class="col-md-6">
                        <ul class="list list-text list-dashed">

                            <li data-animate="ts-fadeInRight">
                                <h4>Seguridad Jurídica. </h4>
                            </li>
                            <!--end Experience block-->

                            <li data-animate="ts-fadeInRight" data-ts-delay=".1s">
                                <h4>Confidencialidad (secreto fiduciario)</h4>
                            </li>
                            <!--end Experience block-->

                            <li data-animate="ts-fadeInRight" data-ts-delay=".2s">
                                <h4>Protección de activos.</h4>
                            </li>
                            <!--end Experience block-->

                            <li data-animate="ts-fadeInRight" data-ts-delay=".2s">
                                <h4>Una vez constituida la garantía puede inclusive servir para garantizar más operaciones de crédito de forma revolvente.</h4>
                            </li>
                            <!--end Experience block-->

                            <li data-animate="ts-fadeInRight" data-ts-delay=".2s">
                                <h4>El Fideicomiso permite reducir los riesgos y agilizar el pago del adeudo.</h4>
                            </li>
                            <!--end Experience block-->

                        </ul>
                    </div>

                </div>
            </div>
        </section>
        <!--Soluciones
            ============================================================================================================
            -->
        <section id="partners" class="py-5" data-bg-color="#fafafa">


            <div class="container py-5">
                <div class="ts-title">
                    <h2>Soluciones Financieras y
                        Legislación del Fideicomiso Fiduciarias
                        de Garantía en México</h2>
                    <p>Estos son TODOS los artículos dentro de la legislación mexicana que regulan al Fideicomiso de garantía</p>
                </div>
                <div class="d-block d-md-flex justify-content-around align-items-center text-center ts-partners">

                    <p data-animate="ts-fadeInRight" data-ts-delay=".2s">LEY GENERAIL DE TÍTULOS Y OPERACIONES DE CRÉDITO (Artículos 381 al 407) DEL FIDEICOMISO</p>

                    <p data-animate="ts-fadeInRight" data-ts-delay=".4s">LEY DE INSTITUCIONES DE CRÉDITO </p>

                    <p data-animate="ts-fadeInRight" data-ts-delay=".5s">III LEY GENERAL DE ORGANIZACIONES AUXILIARES DEL CRÉDITO (ArtículO 87-Ñ) PROHIBICIONES PARA LAS SOFOM</p>

                    <p data-animate="ts-fadeInRight" data-ts-delay=".6s">CÓDIGO FISCAL DE LA FEDERACIÓN DE LA ENAJENACIÓN DE BIENES</p>

                </div>
            </div>
        </section>
        <!--Features
            ============================================================================================================
            -->
        <section id="features" class="ts-block text-white text-center text-sm-left" data-bg-color="#cc9933">
            <div class="container">
                <div class="ts-title" style="color:#fff">
                    <h2>Checklist de documentación para fideicomisos de garantía de Crédito</h2>
                    <p>A continuacion se deberá presentar la documentación según corresponda</p>
                </div>

                <div class="row" data-animate="ts-fadeInUp" data-ts-delay=".6s">
                    <div class="col-md-3">
                        <!-- Tabs nav -->
                        <div class="nav flex-column nav-pills nav-pills-custom" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                            <a href="#"class="nav-link mb-3 p-3 shadow active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">
                                <i class="fa fa-check mr-2"></i>
                                <span class="font-weight-bold small text-uppercase">1.- Acreditante</span></a>

                            <a href="#"class="nav-link mb-3 p-3 shadow" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">
                                <i class="fa fa-check mr-2"></i>
                                <span class="font-weight-bold small text-uppercase">2.- Acreditado</span></a>

                            <a href="#"class="nav-link mb-3 p-3 shadow" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">
                                <i class="fa fa-check mr-2"></i>
                                <span class="font-weight-bold small text-uppercase">3.- Fideicomitente</span></a>

                            <a href="#"class="nav-link mb-3 p-3 shadow" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false">
                                <i class="fa fa-check mr-2"></i>
                                <span class="font-weight-bold small text-uppercase">4.- Inmueble</span></a>
                        </div>
                    </div>


                    <div class="col-md-9">
                        <!-- Tabs content -->
                        <div class="tab-content" id="v-pills-tabContent">
                            <div class="tab-pane fade shadow rounded bg-white show active p-5" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                                <h4 class="font-italic mb-4">Persona Moral</h4>
                                <p class="font-italic text-muted mb-2">
                                <ul>
                                    <li>FORMATO DE IDENTIFICACIÓN DE CLIENTES</li>
                                    <li>ACTA CONSTITUTIVA con datos de inscripción</li>
                                    <li>PODERE(S) con datos de inscripción</li>
                                    <li>ID VIGENTE del APODERADO</li>
                                    <li>CÉDULA RFC</li>
                                    <li>COMPROBANTE DOMICILIO RECIENTE</li>
                                </ul>
                                </p>
                            </div>

                            <div class="tab-pane fade shadow rounded bg-white p-5" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                                <h4 class="font-italic mb-4">Persona Moral</h4>
                                <ul>
                                    <li>FORMATO DE IDENTIFICACIÓN DE CLIENTES</li>
                                    <li>ACTA CONSTITUTIVA con datos de inscripción</li>
                                    <li>PODERE(S) con datos de inscripción</li>
                                    <li>CEDULA RFC</li>
                                    <li>ID VIGENTE del APODERADO</li>
                                    <li>COMPROBANTE DOMICILIO RECIENTE</li>
                                    <li>AUTORIZACIÓN PARA CONSULTA A BURÓ DE CRÉDITO</li>
                                </ul>
                                <h4 class="font-italic mb-4">Persona Fisica</h4>
                                <ul>
                                    <li>FORMATO DE IDENTIFICACIÓN DE CLIENTES</li>
                                    <li>IDENTIFICACIÓN VIGENTE</li>
                                    <li>COMPROBANTE DOMICILIO RECIENTE</li>
                                    <li>CÉDULA RFC</li>
                                    <li>CURP</li>
                                    <li>ACTA DE MATRIMONIO / DIVORCIO</li>
                                </ul>
                            </div>

                            <div class="tab-pane fade shadow rounded bg-white p-5" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">

                                <p class="font-italic text-muted mb-2">(PROPIETARIO(S) DEL INMUEBLE CON EL QUE SE GARANTIZARÁ EL CRÉDITO)</p>
                                <h4 class="font-italic mb-4">Persona Moral</h4>
                                <ul>
                                    <li>FORMATO DE IDENTIFICACIÓN DE CLIENTES</li>
                                    <li>ACTA CONSTITUTIVA con datos de inscripción</li>
                                    <li>PODERE(S) con datos de inscripción</li>
                                    <li>CEDULA RFC</li>
                                    <li>ID VIGENTE del APODERADO</li>
                                    <li>COMPROBANTE DOMICILIO RECIENTE</li>
                                    <li>AUTORIZACIÓN PARA CONSULTA A BURÓ DE CRÉDITO</li>
                                </ul>
                                <h4 class="font-italic mb-4">Persona Fisica</h4>
                                <ul>
                                    <li>FORMATO DE IDENTIFICACIÓN DE CLIENTES</li>
                                    <li>IDENTIFICACIÓN VIGENTE</li>
                                    <li>COMPROBANTE DOMICILIO RECIENTE</li>
                                    <li>CÉDULA RFC</li>
                                    <li>CURP</li>
                                    <li>ACTA DE MATRIMONIO / DIVORCIO</li>
                                    <li>AUTORIZACIÓN PARA CONSULTA A BURÓ DE CRÉDITO</li>
                                </ul>
                            </div>

                            <div class="tab-pane fade shadow rounded bg-white p-5" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">
                                <h4 class="font-italic mb-4">Documentación de inmueble</h4>
                                <ul>
                                    <li>COPIA ESCRITURA DE PROPIEDAD EN GARANTÍA</li>
                                    <li>DATOS DE INSCRIPCIÓN DE PROPIEDAD EN GARANTÍA</li>
                                    <li>AVALÚO PERICIAL CATASTRAL DE LA PROPIEDAD</li>
                                    <li>RECIBO ÚLTIMO PAGO DE PREDIAL</li>
                                    <li>RECIBO ÚLTIMO PAGO DE AGUA</li>
                                    <li>CERTIFICADO DE LIBERTAD DE GRAVÁMENES (LO TRAMITA LA NOTARÍA)</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <!--Achieved  ============================================================================================================             -->

        <section class="ts-block ts-background-repeat" data-bg-image="assets/img/bg__pattern--topo.png">
            <div class="container ts-promo-numbers">
                <div class="row">

                    <div class="col-sm-6 col-md-4">
                        <div class="ts-promo-number text-center">
                            <figure class="odometer" data-odometer-final="20">0</figure>
                            <h5>Proyectos inmobiliarios</h5>
                        </div>
                    </div>
                    <!--end Promo number-->

                    <div class="col-sm-6 col-md-4">
                        <div class="ts-promo-number text-center">
                            <figure class="odometer" data-odometer-final="20000">0</figure>
                            <h5>Unidades vendibles a través de la misma plataforma</h5>
                        </div>
                    </div>
                    <!--end Promo number-->

                    <div class="col-sm-6 col-md-4">
                        <div class="ts-promo-number text-center">
                            <figure class="odometer" data-odometer-final="2000">0</figure>
                            <h5>Hectarias confiadas en el patrimonio de nuestros fideicomisos</h5>
                        </div>
                    </div>
                    <!--end Promo number-->


                </div>
            </div>
        </section>
    </main>
</div>




        <?php
        include 'inc/pie.php';
        ?>