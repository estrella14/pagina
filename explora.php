<?php 
$pageLink = "Centro de Recursos";
    include 'inc/cab.php';
?>
<head><link rel="stylesheet" href="assets/css/fond.css">


<section class="banner-recursos container context  ">
    <div class="header-recursos" data-animate="ts-fadeInRight" data-ts-delay=".3s">
        <h1 data-animate="ts-reveal">
            <span style="color:#c56122;" >
                <span>Descargas </span>
            </span>
        </h1>
        <h5>Centro de recursos</h5>
           
    </div>
            <hr class="divisorColor">
    <div class="row " data-animate="ts-fadeInLeft" data-ts-delay=".3s">

        <div class="col-lg-3 mb-5">
            <ul class="nav-tabs nav explora"  id="myTab" role="tablist">
                <li class="link-recurso">
                    <a data-toggle="tab" class="active" href="#legales">Legales</a>
                </li>
                <li class="link-recurso">
                    <a  data-toggle="tab" href="#manuales">Manuales</a>
                </li>
                <!--li class="link-recurso">
                    <a  data-toggle="tab" href="#certificaciones">Certificaciones</a>
                </li-->
                <li class="link-recurso">
                    <a  data-toggle="tab" href="#presentaciones">Presentaciones Comerciales</a>
                </li>
                <li class="link-recurso">
                    <a  data-toggle="tab" href="#videos">Videos</a>
                </li>
            </ul>
        </div>

        <div class="col-md-9">
            <div class="tab-content">
                <div id="legales" class="resultado tab-pane fade show active text-center">
                    <h4 style="color:#c56122;"><b>Legales</b></h4>
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                
                            </li>
                    
                        </ul>
                        <div class="row leaderboard__profiles">
                            <div class="col-md-6">
                            
                                <a class="leaderboard__profile" href="assets/centro-de-recursos/legal/avisoprivacidad.pdf" target="_blank">
                                    <img src="assets/img/expediente.png" alt="" class="leaderboard__picture">
                                    <span class="leaderboard__name" >Aviso de privacidad</span>
                                </a>
                                
                            </div>

                            <div class="col-md-6">
                        
                            <a class="leaderboard__profile" href="assets/centro-de-recursos/legal/terminos-y-condiciones.pdf" target="_blank">
                                <img src="assets/img/expediente.png" alt="" class="leaderboard__picture">
                                <span class="leaderboard__name">Términos y condiciones</span>
                            </a>
                            
                            </div>
                                
                            <div class="col-md-6">
                        
                            <a class="leaderboard__profile" href="assets/centro-de-recursos/legal/9-Políticas de CCMD 1_2 VP.pdf" target="_blank">
                                <img src="assets/img/expediente.png" alt="" class="leaderboard__picture">
                                <span class="leaderboard__name">Políticas de Constancias NOM-151 - Versión Pública</span>
                            </a>
                            
                            </div>
                                
                            <div class="col-md-6">
                        
                            <a class="leaderboard__profile" href="assets/centro-de-recursos/legal/Politicas-de-SDT-VPublica.pdf" target="_blank">
                                <img src="assets/img/expediente.png" alt="" class="leaderboard__picture">
                                <span class="leaderboard__name">Políticas de Sellos Digitales de Tiempo - Versión Pública</span>
                            </a>
                            
                            </div>
                                
                            <div class="col-md-6">
                        
                            <a class="leaderboard__profile" href="assets/centro-de-recursos/legal/10-Declaración de Prácticas de CCMD_V1_2 VP.pdf" target="_blank">
                                <img src="assets/img/expediente.png" alt="" class="leaderboard__picture">
                                <span class="leaderboard__name">Declaración de prácticas de Constancias NOM-151 - Versión Pública</span>
                            </a>
                            
                            </div>
                                
                            <div class="col-md-6">
                        
                            <a class="leaderboard__profile" href="assets/centro-de-recursos/legal/declaracion_practicas_sellos.pdf" target="_blank">
                                <img src="assets/img/expediente.png" alt="" class="leaderboard__picture">
                                <span class="leaderboard__name">Declaración de prácticas Sellos Digitales de Tiempo - Versión Pública</span>
                            </a>
                            
                            </div>
                                
                                
                
                    </div>
            </div>
                
                
                <div id="manuales" class="resultado tab-pane text-center">
                    <h4 style="color:#c56122;"><b>Manuales</b></h4>
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <!--a class="nav-link active" id="covenant-tab" data-toggle="tab" href="#covenant" role="tab" aria-controls="home" aria-selected="true">Covenant</a-->
                            </li>
                            <!--li class="nav-item">
                                <a class="nav-link" id="noti-tab" data-toggle="tab" href="#noti" role="tab" aria-controls="profile" aria-selected="false">Notificamex</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="sure-tab" data-toggle="tab" href="#sure" role="tab" aria-controls="contact" aria-selected="false">Surety</a>
                            </li-->
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="covenant" role="tabpanel" aria-labelledby="covenant-tab">
                                <div class="row leaderboard__profiles">
                                    <div class="col-md-6">
                                    
                                        <a class="leaderboard__profile" href="assets/centro-de-recursos/manuales/Manual de Usuario - Sellos Digitales de Tiempo.pdf" target="_blank">
                                            <img src="assets/img/expediente.png" alt="" class="leaderboard__picture">
                                            <span class="leaderboard__name">Manual de uso para Sellos Digitales de Tiempo</span>
                                        </a>
                                            
                                    </div>
                                    <!--div class="col-md-6">
                                    
                                        <a class="leaderboard__profile" href="assets/centro-de-recursos/manuales/Documentación-para-dar-de-alta-empresas-y-usuarios-COVENANT.docx">
                                            <img src="assets/img/expediente.png" alt="" class="leaderboard__picture">
                                            <span class="leaderboard__name">Alta de empresas y usuarios Covenant</span>
                                        </a>
                                            
                                    </div-->
                                    <div class="col-md-6">
                                    
                                        <a class="leaderboard__profile" href="assets/centro-de-recursos/manuales/Envio-de-documento-PDF-a-firma-GENERICO.pdf" target="_blank">
                                            <img src="assets/img/expediente.png" alt="" class="leaderboard__picture">
                                            <span class="leaderboard__name">Envio de documento PDF a firma genérico</span>
                                        </a>
                                            
                                    </div>
                                    <div class="col-md-6">
                                    
                                        <a class="leaderboard__profile" href="assets/centro-de-recursos/manuales/Manual-corto-Administrador-GRAL.pdf" target="_blank">
                                            <img src="assets/img/expediente.png" alt="" class="leaderboard__picture">
                                            <span class="leaderboard__name">Manual corto administrador general</span>
                                        </a>
                                        
                                    </div>
                                    <div class="col-md-6">
                                    
                                        <a class="leaderboard__profile" href="assets/centro-de-recursos/manuales/Manual del usuario Firmante - Covenant Firma Julio21.pdf" target="_blank">
                                            <img src="assets/img/expediente.png" alt="" class="leaderboard__picture">
                                            <span class="leaderboard__name">Covenant Firma - Manual para el Firmante</span>
                                        </a>
                                        
                                    </div>
                                    <div class="col-md-6">
                                    
                                        <a class="leaderboard__profile" href="assets/centro-de-recursos/manuales/Manual del Capturista.pdf" target="_blank">
                                            <img src="assets/img/expediente.png" alt="" class="leaderboard__picture">
                                            <span class="leaderboard__name">Covenant Documentos - Manual para el Capturista</span>
                                        </a>
                                        
                                    </div>





                                
                                    
                                </div>
                            </div>

                            <!--div class="tab-pane fade" id="noti" role="tabpanel" aria-labelledby="noti-tab">
                                <div class="row mt-4">
                                    <div class="col-md-6">
                                        
                                        <a class="leaderboard__profile" href="#" target="_blank">
                                            <img src="assets/img/expediente.png" alt="" class="leaderboard__picture">
                                            <span class="leaderboard__name">La guía de legalidad de la firma electrónica en México</span>
                                        </a>
                                        
                                    </div>
                                    <div class="col-md-6">
                                        
                                        <a class="leaderboard__profile" href="#" target="_blank">
                                            <img src="assets/img/expediente.png" alt="" class="leaderboard__picture">
                                            <span class="leaderboard__name">Todo lo que necesitas saber acerca de las firmas electrónicas</span>
                                        </a>
                                        
                                    </div>
                                    
                                
                                </div>
                            </div>

                            <div class="tab-pane fade" id="sure" role="tabpanel" aria-labelledby="sure-tab">
                                <div class="row mt-4">
                                    <div class="col-md-6">
                                        
                                        <a class="leaderboard__profile" href="#" target="_blank">
                                            <img src="assets/img/expediente.png" alt="" class="leaderboard__picture">
                                            <span class="leaderboard__name">La guía de legalidad de la firma electrónica en México</span>
                                        </a>
                                        
                                    </div>
                                    <div class="col-md-6">
                                        
                                        <a class="leaderboard__profile" href="#" target="_blank">
                                            <img src="assets/img/expediente.png" alt="" class="leaderboard__picture">
                                            <span class="leaderboard__name">Todo lo que necesitas saber acerca de las firmas electrónicas</span>
                                        </a>
                                        
                                    </div>
                                    <div class="col-md-6">
                                        
                                        <a class="leaderboard__profile" href="#" target="_blank">
                                            <img src="assets/img/expediente.png" alt="" class="leaderboard__picture">
                                            <span class="leaderboard__name">Introducción a las firmas electrónicas</span>
                                        </a>
                                        
                                    </div>
                                    
                                  
                                
                                </div>
                            </div-->
                        </div>
                        
                    </div>













                    <!--div id="certificaciones" class="resultado tab-pane text-center">
                        <h3>Certificaciones</h3>
                        <div class="row">
                            <div class="col-md-6">
                                        
                                <a class="leaderboard__profile" href="#" target="_blank">
                                    <img src="assets/img/expediente.png" alt="" class="leaderboard__picture">
                                    <span class="leaderboard__name">La guía de legalidad de la firma electrónica en México</span>
                                </a>
                                        
                            </div>
                            <div class="col-md-6">
                                        
                                <a class="leaderboard__profile" href="#" target="_blank">
                                    <img src="assets/img/expediente.png" alt="" class="leaderboard__picture">
                                    <span class="leaderboard__name">Todo lo que necesitas saber acerca de las firmas electrónicas</span>
                                </a>
                                        
                            </div>
                            <div class="col-md-6">
                                        
                                <a class="leaderboard__profile" href="#" target="_blank">
                                    <img src="assets/img/expediente.png" alt="" class="leaderboard__picture">
                                    <span class="leaderboard__name">Introducción a las firmas electrónicas</span>
                                </a>
                                        
                            </div>

                        
                    
                        </div>
                    </div-->
                  
                    <div id="presentaciones" class="resultado tab-pane text-center ">
                        <h4 style="color:#c56122;"><b>Presentaciones Comerciales</b></h4>
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <!--a class="nav-link active" id="home-tab" data-toggle="tab" href="#pcovenant" role="tab" aria-controls="home" aria-selected="true">Covenant</a-->
                            </li>
                            <!--li class="nav-item">
                                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#pnotificamex" role="tab" aria-controls="profile" aria-selected="false">Notificamex</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="contact-tab" data-toggle="tab" href="#psurety" role="tab" aria-controls="contact" aria-selected="false">Surety</a>
                            </li-->
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="pcovenant" role="tabpanel" aria-labelledby="home-tab">
                                <div class="row leaderboard__profiles">
                                    <div class="col-md-6">
                                        
                                        <a class="leaderboard__profile"  href="assets/centro-de-recursos/presentaciones-comerciales/covenant-2020-presentacion.pdf" target="_blank">
                                            <img src="assets/img/expediente.png" alt="" class="leaderboard__picture">
                                            <span class="leaderboard__name">Covenant 2021</span>
                                        </a>
                                                
                                    </div>
                                    <div class="col-md-6">
                                        
                                        <a class="leaderboard__profile" href="assets/centro-de-recursos/presentaciones-comerciales/PRESENTACION-COVENANT.pdf" target="_blank">
                                            <img src="assets/img/expediente.png" alt="" class="leaderboard__picture">
                                            <span class="leaderboard__name">Presentación General</span>
                                        </a>
                                                
                                    </div>
                                    
                                </div>
                            </div>

                            <div class="tab-pane fade" id="pnotificamex" role="tabpanel" aria-labelledby="profile-tab">
                                <div class="row mt-4">
                                    <div class="col-md-6">
                                        
                                        <a class="leaderboard__profile" href="#" target="_blank">
                                            <img src="assets/img/expediente.png" alt="" class="leaderboard__picture">
                                            <span class="leaderboard__name">La guía de legalidad de la firma electrónica en México</span>
                                        </a>
                                                
                                    </div>
                                    <div class="col-md-6">
                                        
                                        <a class="leaderboard__profile" href="#" target="_blank">
                                            <img src="assets/img/expediente.png" alt="" class="leaderboard__picture">
                                            <span class="leaderboard__name">Todo lo que necesitas saber acerca de las firmas electrónicas</span>
                                        </a>
                                                
                                    </div>
                                    <div class="col-md-6">
                                        
                                        <a class="leaderboard__profile" href="#" target="_blank">
                                            <img src="assets/img/expediente.png" alt="" class="leaderboard__picture">
                                            <span class="leaderboard__name">Introducción a las firmas electrónicas</span>
                                        </a>
                                                
                                    </div>

                                    
                                </div>
                            </div>
                            <div class="tab-pane fade" id="psurety" role="tabpanel" aria-labelledby="contact-tab">
                                <div class="row mt-4">
                                    <div class="col-md-6">
                                        
                                        <a class="leaderboard__profile" href="#" target="_blank">
                                            <img src="assets/img/expediente.png" alt="" class="leaderboard__picture">
                                            <span class="leaderboard__name">La guía de legalidad de la firma electrónica en México</span>
                                        </a>
                                                
                                    </div>
                                    <div class="col-md-6">
                                        
                                        <a class="leaderboard__profile" href="#" target="_blank">
                                            <img src="assets/img/expediente.png" alt="" class="leaderboard__picture">
                                            <span class="leaderboard__name">Todo lo que necesitas saber acerca de las firmas electrónicas</span>
                                        </a>
                                                
                                    </div>
                                    <div class="col-md-6">
                                        
                                        <a class="leaderboard__profile"  href="#" target="_blank">
                                            <img src="assets/img/expediente.png" alt="" class="leaderboard__picture">
                                            <span class="leaderboard__name">Introducción a las firmas electrónicas</span>
                                        </a>
                                                
                                    </div>
                                
                                </div>
                            </div>

                        </div>
                    </div>
                  
                    <div id="videos" class="resultado tab-pane text-center">
                        <h4 style="color:#c56122;"><b>Videos Coorporativos</b></h4>

                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <!--a class="nav-link active" id="home-tab" data-toggle="tab" href="#vcovenant" role="tab" aria-controls="home" aria-selected="true">Covenant</a-->
                            </li>
                            <!--li class="nav-item">
                                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#vnoti" role="tab" aria-controls="profile" aria-selected="false">Notificamex</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="contact-tab" data-toggle="tab" href="#vsurety" role="tab" aria-controls="contact" aria-selected="false">Surety</a>
                            </li-->
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="vcovenant" role="tabpanel" aria-labelledby="home-tab">
                                <div class="row leaderboard__profiles">
                                    <div class="col-md-6 ">
                                        
                                        <a class="leaderboard__profile"  href="https://www.youtube.com/watch?v=JnHa9-neTX4" target="_blank">
                                            <img src="assets/img/youtube.png" alt="" class="leaderboard__picture">
                                            <span class="leaderboard__name">Recuperar Contraseña (web)</span>
                                        </a>
                                                
                                    </div>
                                    <div class="col-md-6 ">
                                        
                                        <a class="leaderboard__profile"  href="https://www.youtube.com/watch?v=POP57C8lYYo" target="_blank">
                                            <img src="assets/img/youtube.png" alt="" class="leaderboard__picture">
                                            <span class="leaderboard__name">Ordenar Documentos en Carpetas (web)</span>
                                        </a>
                                                
                                    </div>
                                    <div class="col-md-6 ">
                                        
                                        <a class="leaderboard__profile"  href="https://www.youtube.com/watch?v=T7hiyuo9OkE" target="_blank">
                                            <img src="assets/img/youtube.png" alt="" class="leaderboard__picture">
                                            <span class="leaderboard__name">Documento Canceldo por el Firmante (web)</span>
                                        </a>
                                                
                                    </div>
                                    <div class="col-md-6">
                                        
                                        <a class="leaderboard__profile" href="https://www.youtube.com/watch?v=deSlarOc5UA" target="_blank">
                                            <img src="assets/img/youtube.png" alt="" class="leaderboard__picture">
                                            <span class="leaderboard__name">Firma Electróncia Avanzada (FEA) vía web (web)</span>
                                        </a>
                                                
                                    </div>
                                    <div class="col-md-6">
                                        
                                        <a class="leaderboard__profile" href="https://www.youtube.com/watch?v=7pYOWBCGC94" target="_blank">
                                            <img src="assets/img/youtube.png" alt="https://www.youtube.com/watch?v=7pYOWBCGC94" class="leaderboard__picture">
                                            <span class="leaderboard__name">Cambio de Contraseña (web)</span>
                                        </a>
                                                
                                    </div>
                                    <div class="col-md-6 ">
                                        
                                        <a class="leaderboard__profile" href="https://www.youtube.com/watch?v=maUjsYD3z4s" target="_blank">
                                            <img src="assets/img/youtube.png" alt="" class="leaderboard__picture">
                                            <span class="leaderboard__name">Como Ingresar por Primera vez (Activación de Cuenta)</span>
                                        </a>
                                                
                                    </div>
                                    <div class="col-md-6 ">
                                        
                                        <a class="leaderboard__profile" href="https://www.youtube.com/watch?v=OT7HxZjHzb4" target="_blank">
                                            <img src="assets/img/youtube.png" alt="" class="leaderboard__picture">
                                            <span class="leaderboard__name">Firma Múltiple de Documentos (web)</span>
                                        </a>
                                                
                                    </div>

                                    <div class="col-md-6 ">
                                        
                                        <a class="leaderboard__profile" href="https://www.youtube.com/watch?v=csnztM2rJnY" target="_blank">
                                            <img src="assets/img/youtube.png" alt="" class="leaderboard__picture">
                                            <span class="leaderboard__name">Inicio de Covenant Firma (web)</span>
                                        </a>
                                                
                                    </div>

                                    <div class="col-md-6 ">
                                        
                                        <a class="leaderboard__profile" href="https://www.youtube.com/watch?v=xf2QwC6Cu5c" target="_blank">
                                            <img src="assets/img/youtube.png" alt="" class="leaderboard__picture">
                                            <span class="leaderboard__name">Recuperar Contraseña (App)</span>
                                        </a>
                                                
                                    </div>
                                    <div class="col-md-6 ">
                                        
                                        <a class="leaderboard__profile" href="https://www.youtube.com/watch?v=hraPNy24KAY" target="_blank">
                                            <img src="assets/img/youtube.png" alt="" class="leaderboard__picture">
                                            <span class="leaderboard__name">Activación de Datos Biométricos (App)</span>
                                        </a>
                                                
                                    </div>
                                    <div class="col-md-6 ">
                                        
                                        <a class="leaderboard__profile" href="https://www.youtube.com/watch?v=hfLmudWHyEQ" target="_blank">
                                            <img src="assets/img/youtube.png" alt="" class="leaderboard__picture">
                                            <span class="leaderboard__name">Firma de Trazo Digital(App)</span>
                                        </a>
                                                
                                    </div>
                                    <div class="col-md-6 ">
                                        
                                        <a class="leaderboard__profile" href="https://www.youtube.com/watch?v=QDCCJh7xzuk" target="_blank">
                                            <img src="assets/img/youtube.png" alt="" class="leaderboard__picture">
                                            <span class="leaderboard__name">Carga de Certificados y Firma FEA (App)</span>
                                        </a>
                                                
                                    </div>
                                    <div class="col-md-6 ">
                                        
                                        <a class="leaderboard__profile" href="https://www.youtube.com/watch?v=1UR_l9vUkeY" target="_blank">
                                            <img src="assets/img/youtube.png" alt="" class="leaderboard__picture">
                                            <span class="leaderboard__name">¿Cómo Desinstalo e Instalo la App Móvil de Covenant Firma? (App) </span>
                                        </a>
                                                
                                    </div>
                                    <div class="col-md-6 ">
                                        
                                        <a class="leaderboard__profile" href="https://www.youtube.com/watch?v=06eC4gQ1vJI" target="_blank">
                                            <img src="assets/img/youtube.png" alt="" class="leaderboard__picture">
                                            <span class="leaderboard__name">¿Cómo Instalo la App Móvil de covenant Firma?(App)</span>
                                        </a>
                                                
                                    </div>
    
                                </div>
                            </div>
                        
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        


</section>

                <br>
                 <br>
                 <br>
                 <br> 


<!--section class="banner-recursos container context ">
        <div class="header-recursos" data-animate="ts-fadeInRight" data-ts-delay=".3s">
            <h1 data-animate="ts-reveal">
                <span style="color:#c56122;" >
                    <span>Descargas </span>
                </span>
            </h1>
           <p>Centro de recursos</p>
           
        </div>
      <hr class="divisorColor">
        <div class="row " data-animate="ts-fadeInLeft" data-ts-delay=".3s">
           
            <div class="col-lg-3 mb-5">
                <ul class="nav-tabs nav explora"  id="myTab" role="tablist">
                <li class="link-recurso">
                    <a data-toggle="tab" class="active" href="#legales">Legales</a>
                </li>
                <li class="link-recurso">
                    <a  data-toggle="tab" href="#manuales">Manuales</a>
                </li>
                <li class="link-recurso">
                    <a  data-toggle="tab" href="#certificaciones">Certificaciones</a>
                </li>
                <li class="link-recurso">
                    <a  data-toggle="tab" href="#presentaciones">Presentaciones Comerciales</a>
                </li>
                <li class="link-recurso">
                    <a  data-toggle="tab" href="#videos">Videos</a>
                </li>
                </ul>
            </div>
            <div class="col-md-9">
                <div class="tab-content">
                    <div id="legales" class="resultado tab-pane fade show active text-center">
                    <h3 >Legales</h3>
                        <div class="row leaderboard__profiles">
                            <div class="col-md-6">
                            
                                <article class="leaderboard__profile" onclick="window.location='assets/centro-de-recursos/legal/avisoprivacidad.pdf'">
                                    <img src="assets/img/expediente.png" alt="" class="leaderboard__picture">
                                    <span class="leaderboard__name">Aviso de privacidad</span>
                                </article>
                                
                             </div>

                             <div class="col-md-6">
                            
                                <article class="leaderboard__profile" onclick="window.location='assets/centro-de-recursos/legal/avisoprivacidad.pdf'">
                                    <img src="assets/img/expediente.png" alt="" class="leaderboard__picture">
                                    <span class="leaderboard__name">Aviso de privacidad</span>
                                </article>
                                
                             </div>
                                   
                             <div class="col-md-6">
                            
                                <article class="leaderboard__profile" onclick="window.location='assets/centro-de-recursos/legal/avisoprivacidad.pdf'">
                                    <img src="assets/img/expediente.png" alt="" class="leaderboard__picture">
                                    <span class="leaderboard__name">Aviso de privacidad</span>
                                </article>
                                
                             </div>
                                   
                             <div class="col-md-6">
                            
                                <article class="leaderboard__profile" onclick="window.location='assets/centro-de-recursos/legal/avisoprivacidad.pdf'">
                                    <img src="assets/img/expediente.png" alt="" class="leaderboard__picture">
                                    <span class="leaderboard__name">Aviso de privacidad</span>
                                </article>
                                
                             </div>
                                   
                             <div class="col-md-6">
                            
                                <article class="leaderboard__profile" onclick="window.location='assets/centro-de-recursos/legal/avisoprivacidad.pdf'">
                                    <img src="assets/img/expediente.png" alt="" class="leaderboard__picture">
                                    <span class="leaderboard__name">Aviso de privacidad</span>
                                </article>
                                
                             </div>
                                   
                             <div class="col-md-6">
                            
                                <article class="leaderboard__profile" onclick="window.location='assets/centro-de-recursos/legal/avisoprivacidad.pdf'">
                                    <img src="assets/img/expediente.png" alt="" class="leaderboard__picture">
                                    <span class="leaderboard__name">Aviso de privacidad</span>
                                </article>
                                
                             </div>
                                   
                                   
                    
                        </div>
                        <h3 >Legales</h3>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="item-cr ">
                                    <p><i class="fa fa-file-pdf"></i></p>
                                    <h6 >Legal</h6>
                                    <a href="assets/centro-de-recursos/legal/avisoprivacidad.pdf">Aviso de privacidad</a>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="item-cr">
                                    <p><i class="fa fa-file-pdf"></i></p>
                                    <h6>Legal</h6>
                                    <a href="assets/centro-de-recursos/legal/terminos-y-condiciones.pdf">Términos y condiciones</a>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="item-cr">
                                    <p><i class="fa fa-file-pdf"></i></p>
                                    <h6>Legal</h6>
                                    <a href="assets/centro-de-recursos/legal/politicas-ccmd-nom-151.pdf">Políticas CCMD NOM-151</a>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="item-cr">
                                    <p><i class="fa fa-file-pdf"></i></p>
                                    <h6>Legal</h6>
                                    <a href="assets/centro-de-recursos/legal/Politicas-de-SDT-VPublica.pdf">Políticas de SDT-VPública</a>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="item-cr">
                                    <p><i class="fa fa-file-pdf"></i></p>
                                    <h6>Legal</h6>
                                    <a href="assets/centro-de-recursos/legal/declaracion_practicas-ccmd-nom-151.pdf">Declaración de prácticas NOM-151</a>
                                </div>
                            </div>  
                            <div class="col-md-3">
                                <div class="item-cr">
                                    <p><i class="fa fa-file-pdf"></i></p>
                                    <h6>Legal</h6>
                                    <a href="assets/centro-de-recursos/legal/declaracion_practicas_sellos.pdf">Declaración de prácticas Sellos</a>
                                </div>
                            </div>      
                                               
                        </div>
                    </div>
                    <div id="manuales" class="resultado tab-pane text-center">
                        <h3>Manuales</h3>
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="covenant-tab" data-toggle="tab" href="#covenant" role="tab" aria-controls="home" aria-selected="true">Covenant</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="noti-tab" data-toggle="tab" href="#noti" role="tab" aria-controls="profile" aria-selected="false">Notificamex</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="sure-tab" data-toggle="tab" href="#sure" role="tab" aria-controls="contact" aria-selected="false">Surety</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="covenant" role="tabpanel" aria-labelledby="covenant-tab">
                                <div class="row mt-4">
                                    <div class="col-md-3">
                                        <div class="item-cr">
                                            <p><i class="fa fa-file-pdf"></i></p>
                                            <h6>Manual</h6>
                                            <a download href="assets/centro-de-recursos/manuales/Manual-de-usuario-SDT.pdf">Manual de usuario SDT</a>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="item-cr">
                                            <p><i class="fa fa-file-pdf"></i></p>
                                            <h6>Manual</h6>
                                            <a download href="assets/centro-de-recursos/manuales/Documentación-para-dar-de-alta-empresas-y-usuarios-COVENANT.docx">Alta de empresas y usuarios Covenant</a>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="item-cr">
                                            <p><i class="fa fa-file-pdf"></i></p>
                                            <h6>Manual</h6>
                                            <a download href="assets/centro-de-recursos/manuales/Envio-de-documento-PDF-a-firma-GENERICO.pdf">Envio de documento PDF a firma genérico</a>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="item-cr">
                                            <p><i class="fa fa-file-pdf"></i></p>
                                            <h6>Manual</h6>
                                            <a download  href="assets/centro-de-recursos/manuales/Manual-corto-Administrador-GRAL.pdf">Manual corto administrador general</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="noti" role="tabpanel" aria-labelledby="noti-tab">
                                <div class="row mt-4">
                                    <div class="col-md-3">
                                        <div class="item-cr">
                                            <p><i class="fa fa-file-pdf"></i></p>
                                            <h6>Manual</h6>
                                            <a href="#">La guía de legalidad de la firma electrónica en México</a>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="item-cr">
                                            <p><i class="fa fa-file-pdf"></i></p>
                                            <h6>Manual</h6>
                                            <a href="#">Todo lo que necesitas saber acerca de las firmas electrónicas</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="sure" role="tabpanel" aria-labelledby="sure-tab">
                                <div class="row mt-4">
                                    <div class="col-md-3">
                                        <div class="item-cr">
                                            <p><i class="fa fa-file-pdf"></i></p>
                                            <h6>Manual</h6>
                                            <a href="#">La guía de legalidad de la firma electrónica en México</a>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="item-cr">
                                            <p><i class="fa fa-file-pdf"></i></p>
                                            <h6>Manual</h6>
                                            <a href="#">Todo lo que necesitas saber acerca de las firmas electrónicas</a>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="item-cr">
                                            <p><i class="fa fa-file-pdf"></i></p>
                                            <h6>Manual</h6>
                                            <a href="#">Introducción a las firmas electrónicas</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <div id="certificaciones" class="resultado tab-pane text-center">
                        <h3>Certificaciones</h3>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="item-cr">
                                    <p><i class="fa fa-certificate"></i></p>
                                    <h6>Certificación</h6>
                                    <a href="#">La guía de legalidad de la firma electrónica en México</a>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="item-cr">
                                    <p><i class="fa fa-certificate"></i></p>
                                    <h6>Certificación</h6>
                                    <a href="#">Todo lo que necesitas saber acerca de las firmas electrónicas</a>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="item-cr">
                                    <p><i class="fa fa-certificate"></i></p>
                                    <h6>Certificación</h6>
                                    <a href="#">Introducción a las firmas electrónicas</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="presentaciones" class="resultado tab-pane text-center ">
                        <h3>Presentaciones Comerciales</h3>
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#pcovenant" role="tab" aria-controls="home" aria-selected="true">Covenant</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#pnotificamex" role="tab" aria-controls="profile" aria-selected="false">Notificamex</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="contact-tab" data-toggle="tab" href="#psurety" role="tab" aria-controls="contact" aria-selected="false">Surety</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="pcovenant" role="tabpanel" aria-labelledby="home-tab">
                            <div class="row mt-4">
                                <div class="col-md-3">
                                    <div class="item-cr">
                                        <p><i class="fa fa-file-pdf"></i></p>
                                        <h6>Presentación Comercial</h6>
                                        <a href="assets/centro-de-recursos/presentaciones-comerciales/covenant-2020-presentacion.pdf">Covenant 2021</a>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="item-cr">
                                        <p><i class="fa fa-file-pdf"></i></p>
                                        <h6>Presentación Comercial</h6>
                                        <a href="assets/centro-de-recursos/presentaciones-comerciales/PRESENTACION-COVENANT.pdf ">Presentación Genaral</a>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <div class="tab-pane fade" id="pnotificamex" role="tabpanel" aria-labelledby="profile-tab">
                            <div class="row mt-4">
                                <div class="col-md-3">
                                    <div class="item-cr">
                                        <p><i class="fa fa-file-pdf"></i></p>
                                        <h6>Presentación Comercial</h6>
                                        <a href="#">La guía de legalidad de la firma electrónica en México</a>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="item-cr">
                                        <p><i class="fa fa-file-pdf"></i></p>
                                        <h6>Presentación Comercial</h6>
                                        <a href="#">Todo lo que necesitas saber acerca de las firmas electrónicas</a>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="item-cr">
                                        <p><i class="fa fa-file-pdf"></i></p>
                                        <h6>Presentación Comercial</h6>
                                        <a href="#">Introducción a las firmas electrónicas</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="psurety" role="tabpanel" aria-labelledby="contact-tab">
                            <div class="row mt-4">
                                <div class="col-md-3">
                                    <div class="item-cr">
                                        <p><i class="fa fa-file-pdf"></i></p>
                                        <h6>Presentación Comercial</h6>
                                        <a href="#">La guía de legalidad de la firma electrónica en México</a>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="item-cr">
                                        <p><i class="fa fa-file-pdf"></i></p>
                                        <h6>Presentación Comercial</h6>
                                        <a href="#">Todo lo que necesitas saber acerca de las firmas electrónicas</a>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="item-cr">
                                        <p><i class="fa fa-file-pdf"></i></p>
                                        <h6>Presentación Comercial</h6>
                                        <a href="#">Introducción a las firmas electrónicas</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                        
                    </div>
                    <div id="videos" class="resultado tab-pane text-center">
                        <h3>Videos Cooporativos</h3>
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#vcovenant" role="tab" aria-controls="home" aria-selected="true">Covenant</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#vnoti" role="tab" aria-controls="profile" aria-selected="false">Notificamex</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="contact-tab" data-toggle="tab" href="#vsurety" role="tab" aria-controls="contact" aria-selected="false">Surety</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="vcovenant" role="tabpanel" aria-labelledby="home-tab">
                                <div class="row mt-4">
                                    <div class="col-md-3">
                                        <div class="item-cr">
                                            <p><i class="fab fa-youtube-square"></i></p>
                                            <h6>Video</h6>
                                            <a href="https://www.youtube.com/watch?v=_Ah3q3KIZnI">¿Cómo firmar?</a>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="item-cr">
                                            <p><i class="fab fa-youtube-square"></i></p>
                                            <h6>Video</h6>
                                            <a href="https://www.youtube.com/watch?v=wToNUNp4T2I">Firma Múltiple de Covenant Documentos</a>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="item-cr">
                                            <p><i class="fab fa-youtube-square"></i></p>
                                            <h6>Video</h6>
                                            <a href="https://www.youtube.com/watch?v=YnhsN0_sty8">Firma electrónica avanzada </a>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="item-cr">
                                            <p><i class="fab fa-youtube-square"></i></p>
                                            <h6>Video</h6>
                                            <a href="https://www.youtube.com/watch?v=JZ50dGn641A">Documento cancelado por el firmante</a>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="item-cr">
                                            <p><i class="fab fa-youtube-square"></i></p>
                                            <h6>Video</h6>
                                            <a href="https://www.youtube.com/watch?v=X-GGi-9EPxE">Cambiar la contraseña </a>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="item-cr">
                                            <p><i class="fab fa-youtube-square"></i></p>
                                            <h6>Video</h6>
                                            <a href="https://www.youtube.com/watch?v=pFS1WN3x0hc">¿Cómo recuperar mi contraseña?</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="vnoti" role="tabpanel" aria-labelledby="profile-tab">
                                <div class="row mt-4">
                                    <div class="col-md-3">
                                        <div class="item-cr">
                                            <p><i class="fab fa-youtube-square"></i></p>
                                            <h6>Video</h6>
                                            <a href="#">Recibir Notificaciones</a>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="item-cr">
                                            <p><i class="fab fa-youtube-square"></i></p>
                                            <h6>Video</h6>
                                            <a href="#">Configurar Alertas</a>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="item-cr">
                                            <p><i class="fab fa-youtube-square"></i></p>
                                            <h6>Video</h6>
                                            <a href="#">Introducción a las firmas electrónicas</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="vsurety" role="tabpanel" aria-labelledby="contact-tab">
                                <div class="row mt-4">
                                    <div class="col-md-3">
                                        <div class="item-cr">
                                            <p><i class="fab fa-youtube-square"></i></p>
                                            <h6>Video</h6>
                                            <a href="#">La guía de legalidad de la firma electrónica en México</a>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="item-cr">
                                            <p><i class="fab fa-youtube-square"></i></p>
                                            <h6>Video</h6>
                                            <a href="#">Todo lo que necesitas saber acerca de las firmas electrónicas</a>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="item-cr">
                                            <p><i class="fab fa-youtube-square"></i></p>
                                            <h6>Video</h6>
                                            <a href="#">Introducción a las firmas electrónicas</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        
</section-->


<section class="banner-recursos container  mt-5"> 

</section>
   

<?php
    include 'inc/pie.php';
?>