<?php

$pageLink = "Válidar SDT";

include 'inc/cab.php';

?>
<style>
  body {
    
    background-color:#f7f8f9;

    }

    .divhash{
        word-wrap: break-word;
    }
    .tablaaa {
        table-layout:fixed;
        width:100%;
    }
 

@media (max-width: 1000px){
    .con {
        min-width: 820px;
    }
        }

        @media (max-width: 900px){
    .con {
        min-width: 770px;
    }
        }

        @media (max-width: 870px){
    .con {
        min-width: 770px;
    }
        }
        @media (max-width: 800px){
    .con {
        min-width: 770px;
    }
        }

        @media (max-width: 780px){
    .con {
        min-width: 740px;
        margin-left: 1rem !important;
    }
        }
        @media (max-width: 760px){
    .con {
        min-width: 550px;
        margin-left: 1rem !important;
    }
        }



        @media (max-width: 750px){
.con {
    min-width: 508px;
    margin-left: 1rem !important;
}
}   


@media (max-width: 550px){
.con {
    min-width: 370px;
    margin-left: 0rem !important;
}
}  

@media (max-width: 350px){
.con {
    min-width: 365px;
    margin-left: -1.9rem !important;
}
}  


</style>

<div class="ts-content" >
<div class="notify"><span id="notifyType" class=""></span></div>
    <main id="ts-content ">
        <section class="pt-5 mt-4 mb-5  container">
            <div class="mb-5 ">
                <h2 class="font-weight-bold">VALIDAR SELLO DIGITAL DE TIEMPO (SDT)</h2>
                <p style="text-align: justify;">La validación del Sello Digital de Tiempo sirve para verificar que los archivos obtenidos del proceso continúen íntegros y con esto asegurar que no hay ninguna modificación desde que se emitió el Sello o Timestamping. </p>
                <p style="text-align: justify;">A continuación, carga el archivo original, la petición del sello (archivo con extensión *.TSQ), el resultado del sello (archivo con extensión *.TSR) y nosotros nos encargaremos de verificar su integridad: </p>
            </div>
            <div class="container con">
                <div class="row justify-content-center hola">
                    <div class="col-md-7 col-sm-12 carta-req pollo">
                        <form id="form_validar_sello">
                            <div class="cuerpo-titulo">
                                <!--<h3 class="mb-3">Validar Sello Digital de Tiempo</h3>-->
                                
                            </div>
                            <div class="carta-cuerpo col-sm-12">
                                <div class="col-sm-5 col-md-5 text-inp" style="overflow:hidden">
                                    <p class="mb-0 bold">Subir archivo .TSQ</p>
                                    <small id="resFile1"></small>
                                </div>
                                <div class="col-sm-7 col-md-7 inp-inp">
                                    <label class="btn-submit-file" for="1" id="tsq"><i class="fas fa-upload"></i> <span> Subir archivo </span>.TSQ </label>
                                    <input type="file" id="1" accept=".tsq" name="tsq" accept=".tsq" onchange="infDocs(1,'tsq', 'tsq')">
                                </div>
                            </div>

                            <div class="carta-cuerpo col-sm-12">
                                <div class="col-sm-5 col-md-5 text-inp" style="overflow:hidden">
                                    <p class="mb-0 bold">Subir archivo .TSR</p>
                                    <small id="resFile2"></small>
                                </div>
                                <div class="col-sm-7 col-md-7 inp-inp" >
                                    <label class="btn-submit-file" for="2" id="tsr"><i class="fas fa-upload"></i> <span> Subir archivo </span> .TSR</label>
                                    <input type="file" id="2" name="tsr" accept=".tsr" onchange="infDocs(2,'tsr','tsr')">
                                </div>
                            </div>
                            <div class="carta-cuerpo col-sm-12">
                                <div class="col-sm-5 col-md-5 text-inp" style="overflow:hidden">
                                    <p class="mb-0 bold">Subir el documento</p>
                                    <small id="resFile3"></small>
                                </div>
                                <div class="col-sm-7 col-md-7 inp-inp" >
                                    <label class="btn-submit-file sel" for="3" id="ldoc-file"><i class="fas fa-upload"></i> <span> Seleccionar</span> </label>
                                    <input type="file" id="3" name="archivo" accept="*" onchange="infDocs(3,'archivo','archivo')">
                                </div>
                            </div>
                            <div class="text-center">
                                <button type="button" class="btn btn-outline-naranja mt-4" onclick="Validar(1)" value="click">Validar </button>
                            </div>
                            <div class="cargando hide">
                                <img src="assets/img/logolg.png" alt="">
                                <small>Cargando...</small>

                            </div>
                            <div class="pt-5">
                                <p id="id_res_error" class="pb-3 text-center hide">Ocurrio un error por favor ingrese correctamente los archivos</p>
                            </div>
                        </form>
                    </div>
                    <div class=" carta-req hide arroz col-sm-12">
                        <div id="resultados mt-3">
                                <!-- <h2  class="text-center bold" style="background:#dad7d7; padding: 1em; border-bottom: 5px solid;">Resultados de la validación</h2> -->
                                <h3  class="text-center text-white bold" style="background:#051c33; padding: 1em; border-bottom: 5px solid #c56122;">Resultados de la validación</h3>
                                <div class="info-validacion mb-3">
                                    <h5 id="mensaje-accept"></h5>
                                </div>
                                
                                <div class="info-validacion" style="padding: 1em;"> <!-- habia un mt-2 en la class -->
                                    
                                    <table class="tablaaa tabla tabla-sm table-hover ">
                                        <tbody>
                                            <tr>
                                                <td id="numserie_label" class="text-right bold pb-7s" style="width:33%; padding-right:10px;">Número de Serie: </td>
                                                <td  id="res_numserie" class="pb-7s"></td>
                                            </tr>
                                            <tr>
                                                <td id="exactitud_label" class="text-right bold pb-7s" style="width:33%; padding-right:10px;">Exactitud: </td>
                                                <td id="res_exactitud" class="pb-7s"></td>
                                            </tr>
                                            <tr>
                                                <td id="tiempo_label" class="text-right bold pb-7s" style="width:33%; padding-right:10px;">Tiempo: </td>
                                                <td id="res_tiempo" class="pb-7s"></td>
                                            </tr>
                                            <tr>
                                                <td id="algoritmo_label" class="text-right bold pb-7s" style="width:33%; padding-right:10px;">Algoritmo: </td>
                                                <td id="res_algoritmo" class="pb-7s"></td>
                                            </tr>
                                            <tr>
                                                <td id="version_label" class="text-right bold pb-7s" style="width:33%; padding-right:10px;">Versión: </td>
                                                <td id="res_version" class="pb-7s"></td>
                                            </tr>
                                            <tr>
                                                <td id="policy_label" class="text-right bold pb-7s" style="width:33%; padding-right:10px;">Policy: </td>
                                                <td id="res_policy" class="pb-7s"></td>
                                            </tr>
                                            <tr>
                                                <td id="hash_label" class="text-right bold pb-7s" style="width:33%; padding-right:10px;">Hash: </td>
                                                <td class="divhash" id="res_hash" class="pb-7s"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                <div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
</div>
<?php
include 'inc/pie.php';
?>
<script type="test/javascript" src="assets/js/JQuery.js"></script>
<script src="assets/js/notify-js.js"></script>
<script type="text/javascript" src="api/js/Validacion.js"></script>
<script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.0.17/dist/sweetalert2.all.min.js" ></script>