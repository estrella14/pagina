<?php header('Access-Control-Allow-Origin: *'); ?>
<?php 
$pageLink = "contacto";
include 'inc/cab.php';
?>

<head>
    <link rel="stylesheet" href="styles__ltr.css">
</head>
<style>
.btn-naranja {
    background-color: #c56122;
    border-width: 0.125rem;
    box-shadow: none;
    font-weight: 600;
    font-size: 0.875rem;
    padding: 1.2rem 2.5rem;
    position: relative;
    outline: none !important;
    text-decoration: none;
    text-transform: uppercase;
    color: #fff;
    margin: 20px;
}
</style>
<main id="ts-content">
    <div class="ts-content">
        <!-- <header id="ts-hero" class=" text-black" > #F5F5F8 #ececf  #F2F2F5 -->
        <header id="ts-hero" class="pt-4 pb-4  bannerPrincipal  text-black" data-bg-color="#F2F2F5" >
            <div class="container-fluid covenant">
                 <div class="col-md-12 text-center">
                    <h1 data-animate="ts-reveal">
                        <span style="color:#c56122;" data-animate="ts-fadeInRight" data-ts-delay=".3s">
                            <span>Ponte en contacto </span>
                        </span>
                    </h1>
                       
                </div>
                <section class="mb-5"  data-animate="ts-fadeInLeft" data-ts-delay=".3s">
                    <div class="divPrincipalCovenant container-fluid ">
                        <div class="row">
                            <div class="col-md-6 logoCovenant">
                                <section class="map h-100 w-100" id="map-contact" style="border-top: 5px solid #13203570;
    border-left: 5px solid #13203570;
    border-right: 5px solid #13203570;
    border-bottom: 5px solid #13203570; "></section>
                            </div>
                            <div class="col-md-6 " data-animate="ts-fadeInLeft" data-ts-delay=".3s">
                                <div class="row container" >
                                    <div class="col-md-12 text-left">
                                    <div class="ts-title mb-5">
                                            <h2>Oficina Central</h2>
                                        </div>
                                        <address>
                                        <i class="fas fa-map-marker-alt mr-2 "></i>
                                            Periférico Paseo de la República No. 2650, Piso 3 Int. C: Edificio Arquimo, Col. Prados del Campestre, C.P. 58297, Morelia, Mich.
                                            <br>
                                            
                                            <i class="fas fa-phone mr-2 "></i><strong>Tel:</strong> <a href="tel:443 690 68 51">443 690 68 51</a>
                                            <br>
                                            <i class="fas fa-envelope mr-2 "></i><strong>Email:</strong> <a href="mailto:contacto@credixthrust.com">contacto@legalexgs.com</a>
                                            
                                        </address>
                                       
                                    </div>
                                    <div class="col-md-12 text-center">
                                    </div>
                                    <div class="col-md-12 tex-left">
                                        <div class="ts-title mb-5">
                                            <h6 style="color:#c56122;" class="wrapper">Redes Sociales</h6>
                                            
                                      
                                        
                                        <ul class="list-unstyled">
                                            <li>
                                                <a href="https://www.linkedin.com/company/legalex-gs/" target="_blank">
                                                    <i class="fab fa-linkedin-in mr-2"></i>
                                                    Linkedin
                                                </a>
                                            </li>
                                            <li>
                                                <a href="https://www.youtube.com/channel/UC32g56nRFbuzZRsr-1yjQcw" target="_blank">
                                                    <i class="fab fa-youtube-square mr-2"></i>
                                                    Youtube
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
  </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </section>         
    </header>

    <section id="features" class="pt-5 pb-5 text-black text-center text-sm-left" data-bg-color="#132035">  <!--CDE3F7  B7D1E8 C1D6E9 D3E6F7-->
    <div class="container">
                <div class="row justify-content-center text-left text-white">
                    <div class="col-md-5 mt-7">
                        <div class="ts-title  mt-5" data-animate="ts-fadeInUp" data-ts-delay=".4s">
                            <h3 class="" style="color:#c56122"> Oficinas Secundarias</h3>
                        </div>
                        <h4 class="font-weight-bold">CDMX</h4>
                             <address>
                                <i class="fas fa-map-marker-alt mr-2 "></i>
                                    Miguel de Cervantes Saavedra No. 157 Piso 6, Col. Ampliación Granda, Miguel Hidalgo, CDMX. C.P. 11520
                            </address>
                        
                    </div>
                    <div class="col-md-1">
                    </div>
                        
                    <div class="col-md-6 mt-7">
                        <div class="ts-title  mt-5" data-animate="ts-fadeInUp" data-ts-delay=".4s" >
                                <h3 class="" style="color:#c56122"> Oficinas de Representación</h3>
                        </div>
                        <div class="row ">
                                        <div class="col-sm-6 col-lg-6 "  data-animate="ts-fadeInLeft" data-ts-delay=".2s">
                                        
                                            <h4 class="font-weight-bold">Guadalajara</h4>
                                            <address>
                                            <i class="fas fa-map-marker-alt mr-2 "></i>
                                            Paseo de Los Virreyes No. 45, Piso 4, Interior 116, Puerta de Hierro, C.P. 45116, Zapopan, Jal
                                            </address>
                                        </div>
                                        <div class="col-sm-6 col-lg-6"  data-animate="ts-fadeInLeft" data-ts-delay=".4s">
                                            <h4 class="font-weight-bold">Monterrey</h4>
                                            <address>
                                                <i class="fas fa-map-marker-alt mr-2 "></i>
                                                Deal Center, Av. Insurgentes No. 4474, Piso 3, Colinas de San Jerónimo, C.P. 64630, Monterrey, N.L.
                                            </address>
                                        </div>
                            
                        </div>
                  </div>
                   
     </div>
                                        
</section>               
</main>
        


    <section id="formulario-seccion" data-animate="ts-fadeInDown" data-ts-delay="0s" >
            <div class="container mt-5" >
            
           
                                <div class="ts-title mb-3 text-center">
                                    <h2><b>Escríbenos y nosotros te contactamos</b></h2>
                                </div>
                                <form  id="form-contact" class="clearfix ">
                                <div class="form-group">
                                        <label for="form-contact-subject">Soluciones *</label>
                                        <select name="solucion" id="servicio" class="form-control">
                                            <option style="color: #c56122;" disabled selected>Seleccione un Servicio PSC o Solución</option>
                                            <option value="SDT">&nbsp;&nbsp;&nbsp;&nbsp;Sello Digital de Tiempo</option>
                                            <option value="NOM-151">&nbsp;&nbsp;&nbsp;&nbsp;Constancia NOM-151</option>
                                            <option style="color: #c56122" disabled>Soluciones</option>
                                            <option value="Covenant">&nbsp;&nbsp;&nbsp;&nbsp;Covenant</option>
                                            <option value="Surety">&nbsp;&nbsp;&nbsp;&nbsp;Surety</option>
                                            <option value="Notificamex">&nbsp;&nbsp;&nbsp;&nbsp;Notificamex</option>
                                        </select>
                                    </div>

                                   
                                    <div class="row" >

                                    
                                       
                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <label for="form-contact-name">Nombre *</label>
                                                <input type="text" class="form-control" id="inputname" name="nombre" placeholder="Nombre" >
                                            </div>
                                        </div>

                                        <div class="col-md-3  col-sm-6">
                                            <div class="form-group">
                                                <label for="form-contact-tel">Apellido paterno *</label>
                                                <input  class="form-control" id="inputapp" name="paterno" placeholder="Apellido Paterno" >
                                            </div>
                                        </div>
                                        <div class="col-md-3  col-sm-6">
                                            <div class="form-group">
                                                <label for="form-contact-tel">Apellido materno </label>
                                                <input  class="form-control" id="inputapm" name="materno" placeholder="Apellido Materno">
                                            </div>
                                        </div>

                                        
                                        <div class="col-md-6 col-sm-4">
                                            <div class="form-group">
                                                <label for="form-contact-email">Correo *</label>
                                                <input type="email" class="form-control" id="inputemail" name="correo" placeholder="Correo" >
                                            </div>
                                        </div>

                                        <div class="col-md-6  col-sm-4">
                                            <div class="form-group">
                                                <label for="form-contact-tel">Teléfono *</label>
                                                <input  class="form-control" id="inputtel" name="telefono" placeholder="Teléfono" >
                                            </div>
                                        </div>
                                        <div class="col-md-6  col-sm-4">
                                            <div class="form-group">
                                                <label for="form-contact-tel">Empresa </label>
                                                <input  class="form-control" id="inputempresa" name="empresa" placeholder="Empresa" >
                                            </div>
                                        </div>
                                        <div class="col-md-6  col-sm-4">
                                            <div class="form-group">
                                                <label for="form-contact-tel">Puesto </label>
                                                <input  class="form-control" id="inputPuesto" name="puesto" placeholder="Puesto" >
                                            </div>
                                        </div>                                       

                                    </div>
                                   
                                    <div class="form-group">
                                        <label for="form-contact-message">Cuéntanos sobre tus necesidades *</label>
                                        <textarea class="form-control" id="inputmessage" rows="5" name="mensaje" placeholder="Cuéntanos sobre tus necesidades" ></textarea>
                                    </div>
                                    
                                    <div class="info-form">
                                <small style="font-size:11px">
                                Al enviar este formulario, aceptas que podemos utilizar los datos que nos proporciones para contactar contigo sobre información relacionada con tu solicitud y sobre el producto de Covenant en cuestión. Para más información, consulta el aviso de privacidad de Legalex GS.
                                </small>
                            </div>
                                    <div class="form-group clearfix">
                                     <input type="checkbox" name="avprivacidad" id="avp">    
                                     <label for="check">He leído y aceptado el<a href="aviso"> <span style="color:#006699">aviso de privacidad</span>  </a></label>
                                    </div>
                                    <div class="col-md-6  col-sm-4 row">
                                            <div class="g-recaptcha " data-sitekey="6Lf8mT8cAAAAAE-RDsd0T7nErDHYTRi2W87RvLez"></div>
                                            
                                <p id="captcha_mensaje" class="text-danger" style="font-size:11px" hidden>
                                Complete el captcha para poder continuar.
                                </p>
                            

                                        </div>    
                                    
                                    
                                    <div class="form-group clearfix">
                                        
                                        <button  type="submit" class="btn btn-naranja float-right" id="form-contact-submit">Enviar Mensaje</button>
                                        
                                    </div>
                                    
                                </form>
                                </div>
                            </section>

        <!--end #content-->

  <?php 

  $mapaPie = "Si";
  include 'inc/pie.php';
  ?>
<script type="test/javascript" src="assets/js/JQuery.js"></script>
<script src='api/js/ContactoPsp.js'></script>
<script src="assets/js/notify-js.js"></script>
<script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.0.17/dist/sweetalert2.all.min.js"></script>

<script language="javascript">
$(document).ready(function() {
    $(".rc-anchor-pt").hide();
    
});
</script>
 