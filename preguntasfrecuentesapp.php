<?php
$pageLink = "Preguntas frecuentes";
include 'inc/cab.php';
?>
<style>
    .buscador-faqs {
        display: flex;
        justify-content: center;
        margin-bottom: 3rem;
    }

    .inputbuscar {
        display: flex;
        box-shadow: 0px 0px 10px 1px #00000052;
        align-items: center;
        background-color: #fff;
        border-radius: 15px;
        width: 50%;
    }

    .inputbuscar input {
        border: none;
        padding: 1.3rem 0.5rem;
        border-radius: 15px;
        width: 100%;
    }

    .inputbuscar input:focus {
        outline: none;
    }

    .inputbuscar .iconobuscar {
        margin: 0 0.2rem 0 1rem;
    }

    .lateral_faq {
        height: 100vh;
        box-shadow: 4px 0 4px -2px #00000054;
        padding: 0 15px;
        position: fixed;
        left: 0;
        width: 17.7%;
        background-color: #fff;
        font-size: .9375rem;
    }

    .item_nav_faq {
        display: flex;
        padding: 1rem 1.2rem;
        border-radius: 15px;
        margin-bottom: 10px;
    }

    .item_nav_faq.active {
        background-color: #051c33;
        color: #fff;
    }

    .item_nav_faq.active:hover {
        background-color: #051c33;
        cursor: auto;
    }

    .item_nav_faq .icono_nav {
        padding: 0 1.2rem 0 0rem;
    }

    .item_nav_faq.active a div span {
        color: #fff;
    }

    .item_nav_faq:hover {
        background-color: #9ac5f0;
        cursor: pointer;
    }

    .select_mob_faqs {
        display: none;
    }

    @media (max-width:767px) {
        .lateral_faq {
            display: none;
        }

        .select_mob_faqs {
            display: flex;
            justify-content: center;
        }

        .inputbuscar {
            width: 90%;
        }

        .faqs .container-fluid {
            padding-right: 0px;
            padding-left: 0px;
        }

        .faqs .mob-faq {
            padding-right: 15px;
            padding-left: 15px;
        }
    }
</style>
<main style="background-color:#f5f5f5">
    <section class="faqs">
        <div class="container-fluid">
            <div class="row">

                <?php
                include 'menuFaqs.php';
                ?>
                <div class="col-md-10 mt-5 contenedor-mob " style="text-align:justify">

                    <div class="ts-title text-center mt-4">
                        <h2>Preguntas Frecuentes - Covenant Firmas App</h2>
                    </div>
                    <center>
                        <p id="textoselect" hidden>Seleccione alguna sección de la cual desee buscar</p>
                    </center>
                    <div class="select_mob_faqs mb-5 form-group container">
                        <select name="" id="whatfaqs" class="form-control" onchange="whatsfaqs()">
                            <option value="Covenant Documentos">Covenant Documentos</option>
                            <option value="Covenant Firmas">Covenant Firmas</option>
                            <option value="Covenant App" selected>Covenant App</option>
                            <option value="Sello Digital de Tiempo">Sello Digital de Tiempo</option>
                            <option value="Constancia NOM-151">Constancia NOM-151</option>
                            <option value="Planes de Covenant">Planes de Covenant</option>
                            <option value="Definiciones">Definiciones</option>
                        </select>
                    </div>
                    <div class="buscador-faqs">
                        <div class="inputbuscar">
                            <div class="iconobuscar">
                                <i class="fa fa-search"></i>
                            </div>
                            <input type="input" placeholder="Escriba algo a buscar..." id="buscador">
                        </div>
                    </div>
                    <div id="accordion" class="row mob-faq justify-content-center mb-5">
                        <div id="" class="accordion col-md-11 ">
                            <div class="card">
                                <div class="card-header faq" id="headingOne">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link  pregunta_search" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            1.- ‍¿Qué es la aplicación móvil de Covenant Firma?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>

                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="card-body">
                                        Covenant Firma es la versión móvil que se encuentra en la Play Store y App Store y facilita la firma de documentos sin importar el tipo de firma que sea involucrada (firma electrónica avanzada o firma autógrafa digital).
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="headingTwo">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            2.- ¿Qué necesidades resuelve la aplicación móvil de Covenant Firma?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                    <div class="card-body">
                                        <ul>
                                            <li>Firmar desde cualquier lugar con un dispositivo móvil. Unicamente se requiere descargar la aplicación Covenant Firma y tener acceso a internet.</li>
                                            <li>Recepción de notificaciones cuando se requiere la firma.</li>
                                            <li>Configurar su aplicación móvil para poder utilizar los biométricos del dispositivo móvil y tener un acceso fácil a su aplicación y su firma.</li>
                                            <li>Firma múltiple de documentos en cuestion de segundos.</li>
                                            <li>Tener acceso en todo momento a los documentos firmados.</li>
                                            <li>No es necesario cargar el certificado de la e-firma en cada ocasión que se requiera firmar, ya que solo se carga una vez, quedando guardado unicamente en el dispositivo móvil.</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="headingThree">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            3.- ¿Cómo puedo ser usuario de la aplicación móvil de Covenant Firma?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        Todos los firmantes que han recibido un documento a firma desde Covenant y que ya confirmaron su cuenta desde un ambiente web pueden descargar, instalar e iniciar sesión en la aplicación móvil para disfrutar de sus beneficios.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p4">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r4" aria-expanded="false" aria-controls="r4">
                                            4.- ‍¿Cómo puedo obtener la aplicación de Covenant Firma?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r4" aria-expanded="false" aria-controls="r4"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r4" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        Dirigiéndose a la Play Store (dispositivos Android) o App Store (dispositivos iOS), después buscar la aplicación llamada Covenant Firma, la cual se puede descargar de forma gratuita seleccionando la opción Instalar.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p5">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r5" aria-expanded="false" aria-controls="r5">
                                            5.- ¿Cómo actualizo mis archivos de firma dentro de la app móvil de Covenant Firma?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r5" aria-expanded="false" aria-controls="r4"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r5" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        Ingresando a la aplicación móvil en la opción "Actualizar Certificado" del menú lateral izquierdo, dar clic en "Seleccionar archivo .cer", aparecerá un buscador de archivos donde deberá buscar y seleccionar el archivo que corresponde, posteriormente al elegirlo deberá repetir los pasos, pero ahora seleccionando el botón "Seleccionar archivo .key" para terminar de cargar los nuevos archivos de firma (e.firma o FIEL), por ultimo se deberá ingresar la contraseña o clave privada del certificado y presionar el botón Actualizar Certificado.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p6">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r6" aria-expanded="false" aria-controls="r6">
                                            6.- ¿Cómo activo el uso de biométricos para firmar desde la app móvil de Covenant Firma?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r6" aria-expanded="false" aria-controls="r6"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r6" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        Existen dos opciones para activar el uso de biométricos en su dispositivo móvil:
                                        <ol>
                                            <li>Al cargar los archivos de firma por primera vez deberá habilitar la opción del "Sensor de huella" antes de dar clic en el botón "Cargar certificado", el cual activará el uso de biométricos con que cuente su dispositivo móvil.</li>
                                            <li>Dirigiéndonos a la opción "Ajustes" dentro del menú lateral izquierdo y habilitando la opción del "Sensor de huella", el cual activará el uso de biométricos con que cuente su dispositivo móvil.</li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p7">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r7" aria-expanded="false" aria-controls="r7">
                                            7.- ¿Cómo desinstalar correctamente la app móvil de Covenant Firma?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r7" aria-expanded="false" aria-controls="r7"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r7" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        <ul>
                                            <li>Para iOS: <br>
                                                Identificar el icono de Covenant Firma y presionarlo 2 segundos para que se habilite el listado de opciones, donde se deberá seleccionar Eliminar app y después confirmar la eliminación.</li>
                                            <li>Para Android: <br>
                                                Deberá dirigirse al menú o icono de ajustes dentro de su dispositivo, buscar la opción Aplicaciones y posteriormente buscar la aplicación de Covenant Firma en el listado de aplicaciones, al dar clic en la aplicación aparecerá la opción "Desinstalar" y deberemos confirmar la desinstalación.</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p8">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r8" aria-expanded="false" aria-controls="r8">
                                            8.- ¿Cómo activar mi cuenta en Covenant Firma antes de ingresar por primera vez?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r8" aria-expanded="false" aria-controls="r8"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r8" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        Primero se debe de activar la cuenta siguiendo los pasos indicados en el correo proveniente de alguna de las cuentas oficiales de Legalex GS: <a class="Linkss" href="mailto: contacto.psc@legalexgs.com">contacto.psc@legalexgs.com</a> o <a class="Linkss" href="mailto: soporte.covenant@legalexgs.com">soporte.covenant@legalexgs.com</a>
                                        <ol>
                                            <li>Dentro del correo electrónico vendrá el usuario (RFC), el cual será utilizado para ingresar a Covenant Firma, adicional contendrá una contraseña de un único uso, la cual se utilizá para acceder por única ocasión en el proceso de confirmación de cuenta.</li>
                                            <li>Dentro del mismo correo contiene un botón que te llevará a Covenant Firma <a class="Linkss" href="https://www.legalexgs.com/covenant_firma/">(https://www.legalexgs.com/covenant_firma/)</a>.</li>
                                            <li>Dentro de la página de inicio de Covenant Firma, deberás copiar y pegar el usuario y la contraseña que te fue proporcionada en el correo electrónico y dar clic en el botón "Entrar".</li>
                                            <li>Si los datos son correctos, al ingresar Covenant Firma te pedirá que cambies la contraseña por una diferente a la que llegó en el correo electrónico.</li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p9">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r9" aria-expanded="false" aria-controls="r9">
                                            9.- ¿Cómo ingreso a firmar por segunda ocasión?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r9" aria-expanded="false" aria-controls="r9"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r9" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        Al abrir la aplicación móvil tu usuario quedará registrado y únicamente se solicitará ingresar tu contraseña, o en caso de tener activado el uso de biométricos, podrá ingresar con el uso de la huella dactilar o el reconocimiento facial.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p10">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r10" aria-expanded="false" aria-controls="r10">
                                            10.- ¿Se pueden seleccionar varios documentos de Firma Electrónica Avanzada y Firma de Trazo para firmar al mismo tiempo?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r10" aria-expanded="false" aria-controls="r10"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r10" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        No, para poder firmar más de un documento al mismo tiempo debén ser del mismo tipo de firma, es decir, todos los documentos seleccionados para ser firmados con Firma Electrónica Avanzada o con Firma Autógrafa Digital (trazo).
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p11">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r11" aria-expanded="false" aria-controls="r11">
                                            11.- ¿Cuál es el máximo de documentos que puedo firmar al mismo tiempo?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r11" aria-expanded="false" aria-controls="r11"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r11" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        Por medio de la App móvil puede firmar hasta 6 documentos al mismo tiempo, y al hacerlo vía web puede firmar un máximo de 10 documentos de forma simultánea.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p12">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r12" aria-expanded="false" aria-controls="r12">
                                            12.- ¿Qué es la figura del no repudio?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r12" aria-expanded="false" aria-controls="r12"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r12" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        Consiste en que la firma electrónica avanzada contenida en el o los documentos electrónicos garantiza la autoría e integridad del documento y que dicha firma corresponde exclusivamente al firmante. Es decir, el no repudio evita que el emisor o el receptor nieguen la firma y recepción del documento firmado. Así, cuando se envía un documento a firma, el receptor puede comprobar que, el emisor envió el documento. Por otra parte, cuando se recibe un documento para ser firmado, el emisor puede verificar que, el receptor recibió el documento.
                                    </div>

                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p13">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r13" aria-expanded="false" aria-controls="r13">
                                            13.- ¿Qué es la firma autógrafa?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r13" aria-expanded="false" aria-controls="r13"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r13" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        La Firma Autógrafa Digital (FAD), es conocida como una firma electrónica simple, con la cual se puede dar un sustento legal mínimo necesario pero se considera repudiable. Está se realiza dibujando el trazo de la firma autógrafa que comunmente se realiza en papel, es decir, se realiza la misma firma autógrafa pero en lugar de utilizar bolígrafo y papel se realiza de forma digital.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p14">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r14" aria-expanded="false" aria-controls="r14">
                                            14.- ¿Qué es la firma electrónica avanzada?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r14" aria-expanded="false" aria-controls="r14"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r14" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        Es el conjunto de datos y caracteres que permite la identificación del firmante, está ha sido creada por medios electrónicos bajo el exclusivo control del firmante, de manera que está vinculada únicamente al mismo y a los datos a los que se refiere, lo que permite que sea detectable cualquier modificación ulterior de éstos, y a su vez, esta firma produce los mismos efectos jurídicos que la firma autógrafa.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p15">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r15" aria-expanded="false" aria-controls="r15">
                                            15.- ¿Qué hacer si no puedo ingresar?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r15" aria-expanded="false" aria-controls="r15"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r15" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        Deberás de verificar que se está ingresando la contraseña correctamente teniendo en cuenta espacios, el uso de mayúsculas y minúsculas. En caso de no recordar la contraseña, puede seleccionar el botón Recuperar Contraseña, ingresar su correo electrónico con el que fue registrada su cuenta y dar clic sobre el botón "Recuperar", después recibirá un correo electrónico con los pasos a seguir para finalizar este proceso.
                                        <br>
                                        Nota: Si no recibe el correo electrónico, deberá revisar su bandeja de SPAM o correo no deseado.
                                        <br>
                                        Si no recuerda la cuenta de correo con la que fue registrada su cuenta o no tiene acceso a la misma, deberá contactar a Soporte Técnico a través del Chat que se encuentra integrado al ingresar a Covenant Firma: <a class="Linkss" href="https://www.legalexgs.com/covenant_firma/">https://www.legalexgs.com/covenant_firma/</a> o enviando un correo electrónico con sus datos a <a class="Linkss" href="mailto:soporte@legalexgs.com">soporte@legalexgs.com</a>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p16">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r16" aria-expanded="false" aria-controls="r16">
                                            16.- ¿Qué significa el mensaje: “Cuenta vinculada”?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r16" aria-expanded="false" aria-controls="r16"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r16" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        Exiten dos razones por la que aparece este mensaje:
                                        <ol>
                                            <li>La cuenta fue utilizada para ingresar desde otro dispositivo móvil.</li>
                                            <li>La aplicación fue desinstalada e instalada nuevamente en el mismo dispositivo móvil.</li>
                                        </ol>
                                        Para poder ingresar, deberá dar clic sobre el botón "Solicitar un código de desvinculación", lo cual le enviará un correo electrónico con dicho código, el cuál deberá ingresar después de dar click sobre el botón "Ingresar código de desvinculación".
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p17">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r17" aria-expanded="false" aria-controls="r17">
                                            17.- ¿Puedo firmar sin subir certificados?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r17" aria-expanded="false" aria-controls="r17"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r17" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        Si, siempre y cuando el tipo de firma solicitada sea Firma autógrafa digital (trazo), de lo contrario deberá subir sus archivos de firma.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p18">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r18" aria-expanded="false" aria-controls="r18">
                                            18.- ¿Cómo obtener mis archivos de e.firma o FIEL para poder realizar la Firma Electrónica dentro de Covenant Firma?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r18" aria-expanded="false" aria-controls="r18"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r18" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        La solicitud de e.firma o FIEL se realiza ante el Sistema de Administración Tributaria (SAT), este proceso es propio del SAT y Legalex GS no interviene en el, ni en las contraseñas de firma que se generan durante este proceso. Para más información sobre la emisión de la Firma Electrónica Avanzada visita el portal oficial del SAT o da clic en el siguiente enlace: <a class="Linkss" href="https://www.sat.gob.mx/tramites/16703/obten-tu-certificado-de-e.firma-(antes-firma-electronica)">https://www.sat.gob.mx/tramites/16703/obten-tu-certificado-de-e.firma-(antes-firma-electronica)</a>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p19">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r19" aria-expanded="false" aria-controls="r19">
                                            19.- ¿Cómo cancelo o declino mi firma en un documento?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r19" aria-expanded="false" aria-controls="r19"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r19" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        Al ingresar a Covenant Firma, nos dirigimos a la sección Documentos Pendientes y después de seleccionar el o los archivos que se quieren cancelar, se deberá presionar el botón de "Firma" o "Firmar documento(s)" para que la plataforma nos carge la visualización de los documentos que se seleccionaron. Después deberemos presionar el botón de color rojo "Cancelar Documento" en la parte superior e ingresar el motivo de cancelación, al terminar deberá dar clic el botón "Continuar".
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p20">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r20" aria-expanded="false" aria-controls="r20">
                                            20.- ¿Cómo puedo recuperar mi contraseña de acceso?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r20" aria-expanded="false" aria-controls="r20"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r20" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        Si no recuerda su contraseña para acceder, puede recuperarla ingresando a su aplicación móvil y deberá de seleccionar la opción "Recuperar Contraseña", aparecerá una ventana donde ingresará el correo electrónico con el que fue registrado y por ultimo dar clic sobre el botón "Recuperar", si los datos fueron correctos, recibirá un correo electrónico con los datos y pasos para cambiar su contraseña.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    include 'inc/pie.php';
                    ?>
                </div>
            </div>
        </div>
    </section>
</main>

<script type="text/javascript" src="assets/js/faqs.js"></script>