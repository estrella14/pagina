<?php 
$pageLink = "notificamex";
include 'inc/cab.php';
?>

    
    <div class="ts-content">

        <header id="ts-hero" class="ts-block ts-height--100vh">

            <div class="container">

                <h1 data-animate="ts-reveal">
                    <span class="wrapper">
                   Notificamex
                    </span>
                </h1>
                <h4 class="font-weight-bold" data-animate="ts-fadeInUp" data-ts-delay=".1s">Notificaciones electrónicas con eficiencia y seguridad de
                  que el notificado reciba la información correspondiente en
                  tiempo y forma</h4>
                

                <a href="https://mail.notificamex.com" class="btn btn-naranja" data-animate="ts-fadeInUp" data-ts-delay=".15s">Ir a tu cuenta</a>

            </div>

            <div class="ts-background">
                <div class="ts-background-image ts-background-repeat ts-img-into-bg ts-opacity__70">
                    <img src="assets/img/bg__pattern--dot--gray.png" alt="">
                </div>
            </div>

        </header>

       
        <main id="ts-content">

        
            <section id="about" class="ts-block py-5">
                <div class="container pt-5">
                    <div class="row">
                        <div class="col-md-12 mb-5">
                            <center><img src="assets/img/notificamex.png" alt=""></center>
                        </div>
                        <div class="col-md-4">
                            <div class="ts-title">
                                <h2> Notificamex </h2>
                            </div>
                        </div>

                        <div class="col-md-8">
                            <p style="color:#000;opacity: 1;">
                            Tú Buzón de Notificamex es confidencial, seguro y personalizado <br><br>
El Buzón de Notificamex funciona como un mecanismo de comunicación electrónico entre Cliente y la entidad competente. Esta herramienta sirve para intercambiar e interactuar documentos digitales de forma ágil y segura.
<br><br>
Algunas acciones que se pueden realizar son:
<ul>
        <li>Hacer una notificación,</li>
        <li>Enviar información para el cumplimiento de las obligaciones contraídas,</li>
        <li>Solicitar información o</li>
        <li>Requerir respuesta a una petición la depositará en este buzón.</li>
        <li>A su vez, el cliente puede dar respuesta a los requerimientos solicitados.</li>
</ul>


                            </p>
                        </div>

                    </div>
                </div>

            </section>

            
            <section id="features" class="pt-5 text-white text-center text-sm-left pb-5 " >
                <div class="container">
                <div class="ts-title text-center" >
                    <h2 style="color:#fff">Preguntas Frecuentes</h2>
                </div>  
        <div id="accordion" class="row mob-faq justify-content-center">
          <div id="" class="accordion col-md-7 ">
            <div class="card">
              <div class="card-header faq" id="headingOne">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                  1.- ‍¿Qué es un mensaje de datos?
                  </button>
                </h5>
                <span  data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><i class="expand more-less fas fa-plus-circle"></i></span>
              </div>
              
              <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="card-body">
                Es toda aquella información generada, enviada, recibida o archivada por medios electrónicos, ópticos o cualquier otra tecnología. (Articulo 89 del Código de Comercio).
                </div>
              </div>
            </div>
            <div class="card">
              <div class="card-header faq" id="headingTwo">
                <h5 class="mb-0">
                  <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                  2.-  ¿Cuál es el valor probatorio de un mensaje de datos?
                  </button>
                </h5>
                <span  data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"><i class=" more-less fas fa-plus-circle"></i></span>
              </div>
              <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                <div class="card-body">
                ‍El Código de Comercio en su artículo 89 bis establece que no se negarán efectos jurídicos, validez o fuerza obligatoria a cualquier tipo de información por la sola razón de que esté contenida en un Mensaje de Datos. Por tanto, dichos mensajes podrán ser utilizados como medio probatorio en cualquier diligencia ante autoridad legalmente reconocida, y surtirán los mismos efectos jurídicos que la documentación impresa, siempre y cuando los mensajes de datos se ajusten a las disposiciones de este Código y a los lineamientos normativos correspondientes.
                </div>
              </div>
            </div>
            <div class="card">
              <div class="card-header faq" id="headingThree">
                <h5 class="mb-0">
                  <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                  3.- ¿Qué certeza se tiene de que el mensaje de datos fue enviado por el emisor?
                  </button>
                </h5>
                <span  data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree"><i class=" more-less fas fa-plus-circle"></i></span>
              </div>
              <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                <div class="card-body">
                De conformidad al artículo 90 bis del Código de Comercio, se presume que un Mensaje de Datos ha sido enviado por el Emisor y, por lo tanto, el Destinatario o la Parte que Confía, en su caso, podrá actuar en consecuencia, cuando:
I. Haya aplicado en forma adecuada el procedimiento acordado previamente con el Emisor, con el fin de establecer que el Mensaje de Datos provenía efectivamente de éste, o
II. El Mensaje de Datos que reciba el Destinatario o la Parte que Confía, resulte de los actos de un Intermediario que le haya dado acceso a algún método utilizado por el Emisor para identificar un Mensaje de Datos como propio.
Por lo tanto los usuarios de NOTIFICAMEX deberán acordar en los actos jurídicos que celebren, la utilización de dicho sistema para hacerse llegar comunicaciones entre ellos.
                </div>
              </div>
            </div>
            
            <div class="card">
              <div class="card-header faq" id="heading4">
                <h5 class="mb-0">
                  <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse4" aria-expanded="false" aria-controls="collapse4">
                  4.- ¿En que momento se tiene por expedido un mensaje de datos?
                  </button>
                </h5>
                <span  data-toggle="collapse" data-target="#collapse4" aria-expanded="false" aria-controls="collapse4"><i class=" more-less fas fa-plus-circle"></i></span>
              </div>
              <div id="collapse4" class="collapse" aria-labelledby="heading4" data-parent="#accordion">
                <div class="card-body">
                ‍Salvo pacto en contrario entre el Emisor y el Destinatario, el Mensaje de Datos se tendrá por expedido cuando ingrese en un Sistema de Información que no esté bajo el control del Emisor o del Intermediario. (Artículo 91 bis del Código de Comercio)
Es decir, en el momento en que el mensaje de datos se ingrese en el sistema NOTIFICAMEX.
                </div>
              </div>
            </div>
            <div class="card">
              <div class="card-header faq" id="heading5">
                <h5 class="mb-0">
                  <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse5" aria-expanded="false" aria-controls="collapse5">
                  5.- ¿En qué momento se puede considerar que el destinatario ha recibido el mensaje?
                  </button>
                </h5>
                <span  data-toggle="collapse" data-target="#collapse5" aria-expanded="false" aria-controls="collapse5"><i class=" more-less fas fa-plus-circle"></i></span>
              </div>
              <div id="collapse5" class="collapse" aria-labelledby="heading5" data-parent="#accordion">
                <div class="card-body">
                ‍Salvo pacto en contrario entre el Emisor y el Destinatario, el Mensaje de Datos se tendrá por
                        expedido en el lugar donde
                        el Emisor tenga su establecimiento y por recibido en el lugar donde el Destinatario tenga el
                        suyo:
                        <p>
                        <ul class="align-left" style="text-align: left !important; list-style-type: none;">
                          <li>
                            I. Si el Emisor o el Destinatario tienen más de un establecimiento, su establecimiento será
                            el que guarde una relación
                            más estrecha con la operación subyacente o, de no haber una operación subyacente, su
                            establecimiento principal, y
                          </li>
                          <li>
                            II. Si el Emisor o el Destinatario no tienen establecimiento, se tendrá en cuenta su lugar
                            de residencia habitual.
                          </li>
                        </ul>
                        </p>
                        <p>(Articulo 94 del Código de Comercio)</p>
                </div>
              </div>
            </div>
            <div class="card">
              <div class="card-header faq" id="heading6">
                <h5 class="mb-0">
                  <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse6" aria-expanded="false" aria-controls="collapse6">
                  6.- ¿En que lugar se tiene por expedido y por recibido el mensaje de datos?
                  </button>
                </h5>
                <span  data-toggle="collapse" data-target="#collapse6" aria-expanded="false" aria-controls="collapse6"><i class=" more-less fas fa-plus-circle"></i></span>
              </div>
              <div id="collapse6" class="collapse" aria-labelledby="heading6" data-parent="#accordion">
                <div class="card-body">
                Salvo pacto en contrario entre el Emisor y el Destinatario, el Mensaje de Datos se tendrá por
                        expedido en el lugar donde
                        el Emisor tenga su establecimiento y por recibido en el lugar donde el Destinatario tenga el
                        suyo:
                        <p>
                        <ul class="align-left" style="text-align: left !important; list-style-type: none;">
                          <li>
                            I. Si el Emisor o el Destinatario tienen más de un establecimiento, su establecimiento será
                            el que guarde una relación
                            más estrecha con la operación subyacente o, de no haber una operación subyacente, su
                            establecimiento principal, y
                          </li>
                          <li>
                            II. Si el Emisor o el Destinatario no tienen establecimiento, se tendrá en cuenta su lugar
                            de residencia habitual.
                          </li>
                        </ul>
                        </p>
                        <p>(Articulo 94 del Código de Comercio)</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      
</div>
                
                <div class="ts-background ts-bg-blue">
                    <div class="ts-background-image ts-background-repeat ts-opacity__10 ts-img-into-bg back-parallax" style="background-image: url(&quot;assets/img/bg__pattern--topo-white.png&quot;);">
                        <img src="assets/img/bg__pattern--topo-white.png" alt="">
                    </div>
                </div>

            </section>

 

            
        

           
            
        </main>
        <!--end #content-->
<?php
include 'inc/pie.php';
?>
