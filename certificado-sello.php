<?php 
$pageLink = "sello";
include 'inc/cab.php';
?>

<head>
    <style>
        body {
    height: 400px;
    background-image: url("assets/img/bg__pattern--topo.png");
    }
#mensaje  {
		visibility: hidden;
		display: none;	
}	
@media  (max-width: 300px){
	
#certificado  {
		visibility: hidden;
		display: none;	
}	
	


#mensaje  {
	display: inherit;
	visibility: inherit;
	width: 100%;

font-size: 12px;


}
.mensaje div{
    margin: 200px 20px 19px 4px;
}

}

    </style>
</head>

    
    <div class="ts-content">


       
        <main id="ts-content">

        <!--div class="ts-background-image ts-background-repeat ts-img-into-bg ts-opacity__70 back-parallax">
                <img src="assets/img/bg__pattern--topo.png" alt="">
            </div-->
            

            
            <section id="certificado" class="ts-block text-center ts-background-repeat ts-height--100vh" >
                <div class="container ">

                    <blockquote class="blockquote">
                        <h2 align="left" class="font-weight-bold" data-animate="ts-fadeInDown" data-ts-delay=".15s">CERTIFICADO DE SELLO DIGITAL DE TIEMPO</h2>
                        <footer class="" data-animate="ts-fadeInDown" data-ts-delay=".25s">Para más información sobre nuestra acreditación, descarga nuestro certificado o consulta nuestro aviso de acreditación ante la Secretaría de Economía. </footer>
                    </blockquote>

                    <h3 data-animate="ts-fadeInDown" data-ts-delay=".35s">DESCARGA DEL CERTIFICADO</h3>
                    <img data-animate="ts-fadeInDown" data-ts-delay=".45s" src="assets/img/cert.svg" alt="" style="width:10%">
                    <p data-animate="ts-fadeInDown" data-ts-delay=".55s">Valor Hash SHA-256 del Certificado: <br>
                    <strong data-animate="ts-fadeInDown" data-ts-delay=".55s">
0c987b2347a4769ad9bbc38492536cff77b2606b4c71ec387671d69d3fa414bf</strong></p>
                    <div class="d-block d-md-flex justify-content-around align-items-center text-center ts-partners">
                    
                        <div  data-animate="ts-fadeInRight" data-ts-delay=".2s">
                            <a class=" mb-5"  href="https://www.legalexgs.com/legalexm.cer" ><p class="mt-5"><i class="fas fa-download"></i> Descargar Certificado</p></a>
                        </div>
                        <div data-animate="ts-fadeInRight" data-ts-delay=".2s">
                            <a class=" mb-5" href="http://www.firmadigital.gob.mx/docs_l/ACRE_Legalex_TSA.pdf" target="_blank"><p class="mt-5"><i class="fas fa-download"></i> Aviso de Acreditación</p></a>
                        </div>
                    </div>
                </div>
            </section>


            
<section id="mensaje" class="ts-height--100vh  py-5 ts-background-repeat back-parallax" >

<div class="  mensaje ">
<div class="col-md-12" data-animate="ts-fadeInUp" data-ts-delay=".3s">
<div class="ts-title text-center">
            <h2  style="color:#c56122;"><b>"Para poder ver mejor nuestro contenido"</b></h2>
            <br>
            <h5  style="color:#333231;"><b>Vizualizar en una pantalla más grande</b></h5>
</div>
</div>
</section>


        </main>
        
<?php
include 'inc/pie.php';
?>
