<?php
$pageLink = "cloud";
include 'inc/cab.php';

?>

<!--Content
    ====================================================================================================================
    -->
<div class="ts-content">
    <!--HERO HEADER
        ================================================================================================================
        ================================================================================================================
        -->
    <header id="ts-hero" class=" text-black">

        <div class="container">
            <div class="row ts-align__vertical">

                <div class="col-sm-6 col-md-6">
                    <h1 data-animate="ts-reveal">
                        <span class="wrapper">
                            <span>Acceso sencillo y seguro a todo el contenido</span>
                        </span>
                    </h1>

                    <div data-animate="ts-fadeInUp" data-ts-delay=".1s">
                        <p>
                            Almacena archivos y carpetas, compártelos y colabora en ellos desde cualquier dispositivo móvil, tablet o computadora.
                        </p>
                    </div>

                    <div class="ts-group d-none d-md-block">

                        <a href="nosotros/contacto" class="btn btn-outline-primary my-2" data-animate="ts-fadeInUp" data-ts-delay=".2s">Probar Cloud en tu dispositivo</a>
                    </div>
                </div>
                <div class="col-md-6" data-animate="ts-fadeInRight" data-ts-delay=".5s">
                    <img src="assets/img/cloud-devices.png" alt="" style="width:100%">
                </div>


            </div>
        </div>

        <div class="ts-background ts-bg-white">
            <div class="ts-img-into-bg ts-background-image ts-parallax-element" style="opacity: 0.5;">

            </div>
        </div>

    </header>

    <!--MAIN CONTENT
        ================================================================================================================
        ================================================================================================================
        -->
    <main id="ts-content">



        <!--Blockquote
            ============================================================================================================
            -->
        <section data-animate="ts-fadeInLeft" data-ts-delay=".4s">
            <div class="container ">
                <div class="row">

                    <div class="col-md-6">
                        <img style="width:100%" src="assets/img/mockup.jpg" alt="">
                    </div>
                    <div class="col-md-6 d-flex align-items-center">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="ts-title text-center">
                                    <h2 class="tit-azul">Comparte y colabora</h2>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <p class="text-center text-black">Comparte archivos, carpetas y documentos con tus colaboradores y equipo de trabajo. Se acabaron los datos adjuntos enormes en el correo o las memorias USB: simplemente envía un vínculo por correo o mensaje de texto.</p>
                            </div>
                        </div>


                    </div>
                </div>

            </div>
        </section>

        <!--Our Experience
            ============================================================================================================
            -->
        <section class="ts-block text-black pb-0" data-bg-color="#f5f5f5">
            <div class="container">

                <div class="ts-title text-center ">
                    <h2 class="tit-azul">Cualquier archivo, en cualquier lugar, siempre protegido</h2>
                </div>

                <div class="row text-center iconos-cloud">
                    <div class="col-md-4">
                        <img src="assets/img/icon/alldevices.png" alt="">
                        <b>
                            <h6>Acceso desde cualquier lugar</h6>
                        </b>
                        <p>Disfruta de la libertad de acceder, editar y compartir los archivos en todos tus dispositivos, dondequiera que estés.</p>
                    </div>

                    <div class="col-md-4">
                        <img src="assets/img/icon/archivo.png" alt="">
                        <b>
                            <h6>Realiza la copia de seguridad y protege</h6>
                        </b>
                        <p>Si pierdes tu dispositivo, no perderás tus archivos y documentos cuando los hayas guardado en Cloud de Legalex GS.</p>
                    </div>

                    <div class="col-md-4">
                        <img src="assets/img/icon/share_doc.png" alt="">
                        <b>
                            <h6>Comparte y colabora</h6>
                        </b>
                        <p>Sigue conectado, comparte tus documentos y archivos con tu equipo de trabajo y colabora en tiempo real con las aplicaciones de Legalex GS.</p>
                    </div>

                </div>
            </div>
        </section>
        <!--Soluciones
            ============================================================================================================
            -->
        <section id="partners" class="py-5" data-bg-color="#fafafa">


            <div class="container py-5">
                <div class="row">
                    <div class="col-md-6 d-flex align-items-center">
                        <div class="row container">
                            <div class="col-md-12">
                                <div class="ts-title">
                                    <h2 class="tit-azul">Digitalización de documentos</h2>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <p>Usa tu dispositivo móvil para digitalizar y almacenar documentos, recibos y mucho más en Cloud de Legalex GS.</p>
                            </div>
                        </div>


                    </div>
                    <div class="col-md-6">
                        <img src="assets/img/tablet.png" style="width:100%" alt="">
                    </div>
                </div>

            </div>
        </section>
        <!--Features
            ============================================================================================================
            -->
        <section id="features" class="ts-block text-black text-center text-sm-left" data-bg-color="#f5f5f5">
            <div class="container">
                <div class="ts-title text-center" style="color:#000">
                    <h2 class="tit-azul">Usa Cloud en cualquier dispositivo</h2>
                    <p>Cloud funciona en todas las plataformas más importantes, lo que te permite trabajar sin problemas en un navegador, dispositivo móvil, tablet o computadora.</p>
                </div>

                <div class="flex-btn-social social-btns">
                    <a class="app-btn blu flex-btn-social vert" href="http:apple.com">
                        <i class="fab fa-apple"></i>
                        <p>Disponible en <br /> <span class="big-txt">App Store</span></p>
                    </a>

                    <a class="app-btn blu flex-btn-social vert" href="http:google.com">
                        <i class="fab fa-google-play"></i>
                        <p>Disponible en <br /> <span class="big-txt">Google Play</span></p>
                    </a>


                </div>


            </div>
        </section>
</div>






        <?php
        include 'inc/pie.php';
        ?>