<?php 
$pageLink = "Centro de Recursos";
    include 'inc/cab.php';
?>
    <section class="banner-recursos container">
        <div class="header-recursos">
            <h1>Centro de recursos</h1>
        </div>
        <hr>
        <div class="row">
            <div class="col-lg-3">
                <div class="link-recurso">
                    <a href="#">Legal</a>
                </div>
                <div class="link-recurso">
                    <a href="#">Videos</a>
                </div>
                <div class="link-recurso">
                    <a href="#">Descargas</a>
                </div>
                <div class="link-recurso">
                    <a href="#">Preguntas Frecuentes</a>
                </div>
            </div>
            <div class="col-md-9">
                <div class="resultado">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="item-cr">
                                <p><i class="fa fa-file"></i></p>
                                <h6>Tipo de recurso</h6>
                                <a href="#">La guía de legalidad de la firma electrónica en México</a>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="item-cr">
                                <p><i class="fa fa-file"></i></p>
                                <h6>Tipo de recurso</h6>
                                <a href="#">Todo lo que necesitas saber acerca de las firmas electrónicas</a>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="item-cr">
                                <p><i class="fa fa-file"></i></p>
                                <h6>Tipo de recurso</h6>
                                <a href="#">Introducción a las firmas electrónicas</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php
    include 'inc/pie.php';
?>