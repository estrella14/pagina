<?php
$pageLink = "sello";
include 'inc/cab.php';
?>

<style>



body {
  height: 400px;
  background-image: url("assets/img/bg__pattern--topo.png");

}
</style>
<div class="ts-content p-4">
    <header id="ts-hero" class="ts-block  justify-content-center ">
        
        <section id="about" class="ts-block py-2">
            <div class="container m-lg-2 m-md-2 m-xl-2 m-sm-2" data-animate="ts-fadeInDown" data-ts-delay=".30s">
                 <div class="row">
                    <h2 class="font-weight-bold"> SELLOS DIGITALES DE TIEMPO</h2>
                    <p class="text-justify">
                        El Sello Digital de Tiempo (SDT) o timestamping, es un mecanismo en línea que permite demostrar que una serie de datos han existido y no han sido alterados desde un instante específico en el tiempo. Una autoridad de sellado de tiempo (ASDT) actúa como una tercera parte de confianza, autentica la existencia de los datos electrónicos en una fecha y hora concretos.
                        <br>
                        <br>
                        Mediante el uso de los Sellos Digitales de Tiempo, se incorpora a un documento el registro de la fecha y hora exacta, sincronizada con el CENAM (Centro Nacional de Metrología) que funge como el laboratorio nacional de referencia en materia de mediciones, brindando la hora oficial en México.
                    </p>
                </div>
            </div>

            <div class="row justify-content-center" data-animate="ts-fadeInDown">
                <div class="col-12 col-md-4 col-sm-4 p-2">
                    <a href="Servicios/sellos/validar.jsp" class="btn btn-naranja btn-sm btn-block rounded-sm" data-animate="ts-fadeInUp" data-ts-delay=".40s">Validador</a>
                </div>
                <div class="col-12 col-md-4 col-sm-4 p-2">
                    <a href="Servicios/sellos/documentos" class="btn btn-naranja btn-sm btn-block rounded-sm" data-animate="ts-fadeInUp" data-ts-delay=".40s">Documentación</a>
                </div>
                <div class="col-12 col-md-4 col-sm-4 p-2">
                    <a href="Servicios/sellos/certificados" class="btn btn-naranja btn-sm btn-block rounded-sm" data-animate="ts-fadeInUp" data-ts-delay=".45s">Información del Certificado</a>
                </div>
            </div>

            <center data-animate="ts-fadeInUp" data-ts-delay=".20s">
                <br>
                <div>
                    <div>
                        <h2>¿Tienes dudas?</h2>
                    </div>
                    <div>
                        <p>Consulta nuestras <a href="preguntas" style="color:#c56122;"><u><b>preguntas frecuentes.</b></u></a></p>
                    </div>

                </div>
            </center>
        </section>

    </header>  
</div>

<?php
include 'inc/pie.php';
?>



