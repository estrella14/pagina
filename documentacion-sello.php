
<?php
$pageLink = "sello";
include 'inc/cab.php';
?>

<style>
    body {
    height: 400px;
    background-image: url("assets/img/bg__pattern--topo.png");
    }
    #mensaje  {
        visibility: hidden;
        display: none;	
    }	

    .margen{
        margin: 50px auto auto auto;
    }


@media  (max-width: 300px){
	#documentacion  {
		visibility: hidden;
		display: none;	
    }	
	#mensaje  {
	display: inherit;
	visibility: inherit;
	width: 100%;
    font-size: 12px;}

    .mensaje div{
    margin: 200px 20px 19px 4px;
    }

}

</style>


<div class="ts-content">
    <main id="ts-content">
        <section id="documentacion" class="ts-height--100vh  py-5 ts-background-repeat back-parallax" >
            <div class="container py-5 margen">
                <div class="ts-title">
                    
                    <h2 class="font-weight-bold" data-animate="ts-fadeInDown" data-ts-delay=".15s">Documentación de Sellos Digitales de Tiempo</h2>
                </div>
                <div class="d-block d-md-flex justify-content-around align-items-center text-center ts-partners">

                    <div data-animate="ts-fadeInRight " data-ts-delay=".2s" style="margin: 20px"  data-toggle="modal" data-target="#ModalPDF">
                            <p class="mt-3"><i class="fa fa-file-pdf"></i> Politicas de Sello Digital de Tiempo <small>versión pública</small></p>
                    </div>
            
                    <div data-animate="ts-fadeInRight " data-ts-delay=".2s" style="margin: 20px"  data-toggle="modal" data-target="#ModalPDFC">
                   
                        <p class="mt-3"><i class="fa fa-file-pdf"></i> Declaración de Prácticas de Sello Digital de Tiempo <small>versión pública</small></p>
                    </div>

                    <div data-animate="ts-fadeInRight " data-ts-delay=".2s" style="margin: 20px"  data-toggle="modal" data-target="#ModalPDFM">
                   
                            <p class="mt-3"><i class="fa fa-file-pdf"></i> Manual de usuario de Sello Digital de Tiempo</p>
                    </div>
                </div>
            </div>
        </section>

        <section id="mensaje" class="ts-height--100vh  py-5 ts-background-repeat back-parallax" >
                <div class="  mensaje ">
                    <div class="col-md-12" data-animate="ts-fadeInUp" data-ts-delay=".3s">
                        <div class="ts-title text-center">
                        <h2  style="color:#c56122;"><b>"Para poder ver mejor nuestro contenido"</b></h2>
                            <br>
                            <h5  style="color:#333231;"><b>Vizualizar en una pantalla más grande</b></h5>
                        </div>
                    </div>
                </div>
        </section>
        
    </main>
</div>

<?php
include 'inc/pie.php';
?>