<?php
$pageLink = "Preguntas frecuentes covenant documentos";
include 'inc/cab.php';
?>
<style>
    .buscador-faqs {
        display: flex;
        justify-content: center;
        margin-bottom: 3rem;
    }

    .inputbuscar {
        display: flex;
        box-shadow: 0px 0px 10px 1px #00000052;
        align-items: center;
        background-color: #fff;
        border-radius: 15px;
        width: 50%;
    }

    .inputbuscar input {
        border: none;
        padding: 1.3rem 0.5rem;
        border-radius: 15px;
        width: 100%;
    }

    .inputbuscar input:focus {
        outline: none;
    }

    .inputbuscar .iconobuscar {
        margin: 0 0.2rem 0 1rem;
    }

    .lateral_faq {
        height: 100vh;
        box-shadow: 4px 0 4px -2px #00000054;
        padding: 0 15px;
        position: fixed;
        left: 0;
        width: 17.7%;
        background-color: #fff;
        font-size: .9375rem;
    }

    .item_nav_faq {
        display: flex;
        padding: 1rem 1.2rem;
        border-radius: 15px;
        margin-bottom: 10px;
    }

    .item_nav_faq.active {
        background-color: #051c33;
        color: #fff;
    }

    .item_nav_faq.active:hover {
        background-color: #051c33;
        cursor: auto;
    }

    .item_nav_faq .icono_nav {
        padding: 0 1.2rem 0 0rem;
    }

    .item_nav_faq:hover {
        background-color: #9ac5f0;
        cursor: pointer;
    }

    .select_mob_faqs {
        display: none;
    }

    .item_nav_faq.active a div span {
        color: #fff;
    }

    @media (max-width:767px) {
        .lateral_faq {
            display: none;
        }

        .select_mob_faqs {
            display: flex;
            justify-content: center;
        }

        .inputbuscar {
            width: 90%;
        }

        .faqs .container-fluid {
            padding-right: 0px;
            padding-left: 0px;
        }

        .faqs .mob-faq {
            padding-right: 15px;
            padding-left: 15px;
        }
    }
</style>
<main style="background-color:#f5f5f5">
    <section class="faqs">
        <div class="container-fluid">
            <div class="row">

                <?php
                include 'menuFaqs.php';
                ?>
                <div class="col-md-10 mt-5 contenedor-mob" style="text-align:justify">

                    <div class="ts-title text-center mt-4">
                        <h2>Preguntas Frecuentes - Covenant Documentos</h2>
                    </div>

                    <center>
                        <p id="textoselect" hidden>Seleccione alguna sección de la cual desee buscar</p>
                    </center>
                    <div class="select_mob_faqs mb-5 form-group container">
                        <select name="" id="whatfaqs" class="form-control" onchange="whatsfaqs()">
                            <option value="Covenant Documentos" selected>Covenant Documentos</option>
                            <option value="Covenant Firmas">Covenant Firmas</option>
                            <option value="Covenant App">Covenant App</option>
                            <option value="Sello Digital de Tiempo">Sello Digital de Tiempo</option>
                            <option value="Constancia NOM-151">Constancia NOM-151</option>
                            <option value="Planes de Covenant">Planes de Covenant</option>
                            <option value="Definiciones">Definiciones</option>
                        </select>
                    </div>
                    <div class="buscador-faqs">
                        <div class="inputbuscar">
                            <div class="iconobuscar">
                                <i class="fa fa-search"></i>
                            </div>
                            <input type="input" placeholder="Escriba algo a buscar..." id="buscador">
                        </div>
                    </div>
                    <div id="accordion" class="row mob-faq justify-content-center mb-5">
                        <div id="" class="accordion col-md-11 ">
                            <div class="card">
                                <div class="card-header faq" id="headingOne">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link  pregunta_search" data-toggle="collapse" data-target="#collapseO" aria-expanded="true" aria-controls="collapseO">
                                            1.- ¿Qué es Covenant Documentos?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#collapseO" aria-expanded="true" aria-controls="collapseO"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>

                                <div id="collapseO" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="card-body">
                                        <h4>COVENANT DOCUMENTOS es una solución informática que permite la creación, gestión y resguardo de los documentos digitales.</h4>
                                        <ul>
                                            <li>Mediante el uso de la Firma Electrónica Avanzada, Sello Digital de Tiempo y NOM-151, se da garantía legal de:</li>
                                            <li>Quién ﬁrma es quien dice ser.</li>
                                            <li>Tiempo exacto de la acción de ﬁrma.</li>
                                            <li>Garantizar la Integridad de la información.</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="headingOne">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link  pregunta_search" data-toggle="collapse" data-target="#collapse2" aria-expanded="true" aria-controls="collapse2">
                                            2.- Ventajas de utilizar Covenant Documentos
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#collapse2" aria-expanded="true" aria-controls="collapse2"><i class="  more-less fas fa-plus-circle"></i></span>
                                </div>

                                <div id="collapse2" class="collapse " aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="card-body">
                                        <p>COVENANT DOCUMENTOS te permitirá innovar de manera administrativa y tecnológica en el diseño, creación y gestión de todos los documentos digitales desde su origen de manera organizada y en tiempo real para su posterior envío a firma con las partes involucradas. Entre otras de las ventajas que obtiene al utilizar Covenant, se encuentran:</p>
                                        1) Reducción del riesgo de falsificación, ya que todos los documentos electrónicos contienen bloques de seguridad tecnológica para garantizar su integridad.
                                        <br><br>
                                        2) Los firmantes que intervienen pueden realizar la firma desde casi cualquier dispositivo móvil o de escritorio y desde cualquier lugar.
                                        <br><br>
                                        3) Certeza jurídica y validez legal de todos los documentos que pasan por el proceso de firma electrónica avanzada.
                                        <br><br>
                                        4) Reducción de costos, tiempos y asociados con la intervención desde la elaboración de un documento o contrato hasta la recolecta de la firma de las partes.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="headingOne">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link  pregunta_search" data-toggle="collapse" data-target="#collapse3" aria-expanded="true" aria-controls="collapse3">
                                            3.- ¿Qué son los servicios API de Covenant?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#collapse3" aria-expanded="true" aria-controls="collapse3"><i class="  more-less fas fa-plus-circle"></i></span>
                                </div>

                                <div id="collapse3" class="collapse " aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="card-body">
                                        Es el conjunto de métodos y funciones que permiten la comunicación entre plataformas (Covenant y los servicios de un cliente) a nivel servidor por medio de webservices (WS) para lograr un proceso transparente y un flujo continuo entre los usuarios finales. Este servicio se puede incluir o no, conforme las necesidades de nuestros clientes.

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="headingOne">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link  pregunta_search" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            4.- ¿Qué necesidades resuelve Covenant Documentos?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><i class="  more-less fas fa-plus-circle"></i></span>
                                </div>

                                <div id="collapseOne" class="collapse " aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="card-body">
                                        <ul>
                                            <li>Disminución en costos de oficina.</li>
                                            <li>Ahorro en espacio físico de almacenamiento.</li>
                                            <li>Integridad en la elaboración de documentos.</li>
                                            <li>Respaldo seguro de su información.</li>
                                            <li>Reducción de costos y tiempos en recolección de firmas.</li>
                                            <li>Reducción de suplantación en la identidad de los firmantes.</li>
                                            <li>Reducción de la eficacia probatoria y actos fraudulentos en juicios legales.</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="headingTwo">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            5.- ¿Cómo puedo ser usuario de Covenant Documentos?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                    <div class="card-body">
                                    Contratando nuestros servicios puede adquirir el acceso para usted, su empresa y colaboradores, así como, todos los firmantes que requiera dentro de su documentación de una forma muy sencilla. Para obtener más información sobre nuestros servicios, paquetes o planes de venta puede contactarnos en <a class="Linkss" href="mailto: contacto@legalexgs.com">contacto@legalexgs.com</a> o a los teléfonos: <a class="Linkss" href="tel: +52 443 690 6851 p101"> +52 443 690 6851 Ext.101</a> y <a class="Linkss" href="tel:+52 443 690 6855 "> +52 443 690 6855 </a>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="headingThree">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            6.- ¿Qué hacer si no recibo mi correo de bienvenida o confirmación de cuenta?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        Recupere su contraseña manualmente:<br>
                                        1) Ingrese a su Covenant Documentos y de clic en la opción ¿Olvidó su contraseña?.<br>
                                        2) Aparecerá una ventana y deberá ingresar el correo electrónico con el cual dieron de alta su cuenta.<br>
                                        3) Por ultimo deberá resolver el reCAPTCHA y dar clic al botón "Enviar".
                                        <br><br>
                                        Recomendaciones:<br>
                                        1) Revisar la bandeja de SPAM o correo no deseado.<br>
                                        2) Verificar que el correo electrónico que se proporcionó este correctamente escrito directamente con el administrador de la plataforma (en caso de no tener uno, acuda directamente a soporte técnico de Legalex GS).<br>
                                        3) Verificar que su correo no tenga bloqueado el dominio de Legalex GS para la recepción de correos.<br>
                                        4) Verificar que la bandeja de entrada del correo no este llena o sin espacio.
                                        <br><br>
                                        En caso de que ninguna de las opciones anteriores resuelva el problema, contactar a Soporte Técnico a través de los siguientes medios:<br>
                                        Correo electrónico para soporte Covenant Documentos: <a class="Linkss" href="mailto: soporte@legalexgs.com"> soporte@legalexgs.com</a>, <a class="Linkss" href="mailto: lilia.tovar@legalexgs.com">lilia.tovar@legalexgs.com</a> o <a class="Linkss" href="mailto: rene.baeza@legalexgs.com">rene.baeza@legalexgs.com</a>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p4">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r4" aria-expanded="false" aria-controls="r4">
                                            7.- ‍¿Qué pasa si no terminé de elaborar mi documento y la sesión se cerró?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r4" aria-expanded="false" aria-controls="r4"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r4" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        En el caso de los documentos que son realizados en base a plantillas, cerró la sesión cuando se estaba capturando el documento sin haber dado guardar anteriormente, éste tendrá que ser rellenado nuevamente. Ya que cuando se captura información sin ser guardada el avance se pierde.
                                        <br><br>
                                        Para los documentos que son cargados en la plataforma mediante el uso de archivos con extensión PDF, existe el guardado parcial. Si después de cargar el documento PDF dio clic en "Guardar archivo", podrá regresar al documento y continuar con el proceso de agregar a los firmantes y de igual manera si ya había agregado algún firmante y dio clic en el botón "guardar firmantes" podrá regresar al ultimo estatus de guardado donde lo dejó. Si el documento PDF nunca fue guardado desde la primer captura de información, este tendrá que subirse nuevamente.
                                        <br> <br>

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p5">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r5" aria-expanded="false" aria-controls="r5">
                                            8.- ¿Cómo se agrega a un nuevo firmante?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r5" aria-expanded="false" aria-controls="r4"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r5" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        Los firmantes se agregan de forma automática al ingresar la información de cada uno de los participantes que estarán involucrados en la firma del documento, Covenant "recuerda" a aquellos que ya se les solicito anteriormente la firma para poder precargar su información, cuando esta no se precargue significa que estará agregando la información de un nuevo firmante.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p6">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r6" aria-expanded="false" aria-controls="r6">
                                            9.- ¿Guardan mi información personal?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r6" aria-expanded="false" aria-controls="r6"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r6" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        La información que es guardada dentro de Covenant es: <br>
                                        -Información miníma necesaria y requerida para realizar el envío del documento a los firmantes involucrados.<br>
                                        -Copia del documento que se envío a firma (esta copia solo es accedible por el emisor del documento y los firmantes).<br>
                                        -Información de alta de usuarios, como correo electrónico, nombre, RFC y en el caso de uso de plantillas la información que se registra para las variables.


                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p7">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r7" aria-expanded="false" aria-controls="r7">
                                            10.- ¿Dónde tienen sus centros de datos?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r7" aria-expanded="false" aria-controls="r7"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r7" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        Nuestros Centros de Datos están coubicados en uno de los centros más importantes de Latinoamerica, TRIARA.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p8">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r8" aria-expanded="false" aria-controls="r8">
                                            11.- ¿Qué es la Firma Autógrafa Digital (FAD) o de Trazo?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r8" aria-expanded="false" aria-controls="r8"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r8" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                    La Firma Autógrafa Digital (FAD), es conocida como una firma electrónica simple, con la cual se puede dar un sustento legal mínimo necesario pero se considera repudiable. Esta se realiza dibujando el trazo de la firma autógrafa que comunmente se realiza en papel, es decir, se realiza la misma firma autógrafa pero en lugar de utilizar bolígrafo y papel se realiza de forma digital.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p9">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r9" aria-expanded="false" aria-controls="r9">
                                            12.- ¿Qué es la firma electrónica avanzada?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r9" aria-expanded="false" aria-controls="r9"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r9" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        Es el conjunto de datos y caracteres que permite la identificación del firmante, está ha sido creada por medios electrónicos bajo el exclusivo control del firmante, de manera que está vinculada únicamente al mismo y a los datos a los que se refiere, lo que permite que sea detectable cualquier modificación ulterior de éstos, y a su vez, esta firma produce los mismos efectos jurídicos que la firma autógrafa.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p10">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r10" aria-expanded="false" aria-controls="r10">
                                            13.- ¿Cuál es el fundamento legal de la Firma Electrónica Avanzada?

                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r10" aria-expanded="false" aria-controls="r10"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r10" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        La Firma electrónica se fundamenta principalmente en la Ley de Firma Electrónica Avanzada publicada en el Diario Oficial de la Federación el 11 de enero de 2012, así como en el Código de Comercio Federal Art. 89, 96 y 97.<br><br>


                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p11">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r11" aria-expanded="false" aria-controls="r11">
                                            14.- ¿Es necesario tener una cuenta para firmar un documento?

                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r11" aria-expanded="false" aria-controls="r11"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r11" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        Sí.

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p12">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r12" aria-expanded="false" aria-controls="r12">
                                            15.- ¿Cuántos firmantes puede tener un documento?

                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r12" aria-expanded="false" aria-controls="r12"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r12" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        Al contratar Covenant con Legalex GS, no existe limitantes en cuanto al número de firmantes incluidos en un mismo documento, pueden incorporar los que sean necesarios.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p13">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r13" aria-expanded="false" aria-controls="r13">
                                            16.- ¿Cuáles son las ventajas de utilizar Covenant Documentos?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r13" aria-expanded="false" aria-controls="r13r7"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r13" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        <ul>
                                            <li>Generar plantillas de acuerdo a tus necesidades.</li>
                                            <li>Crear flujos de revisión o administración en la documentación digital basada en plantillas. </li>
                                            <li>Garantiza que solo la persona autorizada pueda realizar cambios al contenido del documento. </li>
                                            <li>Asignar contenido especifico a las diferentes áreas de su empresa para que puedan utilizarlas sin realizar cambios en el contenido del documento.</li>
                                            <li>Cancelación o declinación de los documentos enviados a firma con un solo clic.</li>
                                            <li>Fácil envio a firma y administración del seguimiento del documento y de los firmantes en tiempo real.</li>

                                        </ul>


                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p14">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r14" aria-expanded="false" aria-controls="r14">
                                            17.- ¿Qué es una plantilla?

                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r14" aria-expanded="false" aria-controls="r14"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r14" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        Es un formato o "machote" donde se define la estructura de un documento, como puede ser el contenido estático, diseño y posteriormente se agregan variables para que estas sean rellenadas sin la necesidad de que alguien intervenga en la edición del contenido estático o principal del documento.

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p15">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r15" aria-expanded="false" aria-controls="r15">
                                            18.- ¿Existen validadores al generar una plantilla?

                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r15" aria-expanded="false" aria-controls="r15"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r15" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                    Sí, el usuario que realiza está acción es llamado "Checker" y su función principal es aprobar las plantillas que son creadas en Covenant así como los documentos capturados que pasen por un proceso de verificación o aprobación.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p16">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r16" aria-expanded="false" aria-controls="r16">
                                            19.- Una vez que el documento haya sido firmado, ¿Se puede modificar?

                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r16" aria-expanded="false" aria-controls="r16"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r16" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        No, una vez que un documento es enviado a firma solo puede ser cancelado, ya que cualquier modificación afectaría la integridad del documento y no lo haría confiable.

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p17">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r17" aria-expanded="false" aria-controls="r17">
                                            20.- ¿Puedo cancelar un documento si ya fue firmado parcial o totalmente?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r17" aria-expanded="false" aria-controls="r17"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r17" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        El emisor del documento puede cancelarlo siempre y cuando el documento no se encuentre firmado en su totalidad.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p18">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r18" aria-expanded="false" aria-controls="r18">
                                            21.- ¿Por qué es importante ingresar el RFC para el alta de usuarios?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r18" aria-expanded="false" aria-controls="r18"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r18" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        El RFC se vuelve un identificador único para los usuarios, con esto garantizamos que no existan usuarios iguales y de igual forma se utiliza para comparar la información relacionada con la firma y verificar que solo el usuario propietario de la cuenta es quién decide hacer la firma dentro de nuestra plataforma.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p19">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r19" aria-expanded="false" aria-controls="r19">
                                            22.- ¿Puedo combinar los tipos de firma en un mismo documento (Firma Autógrafa y Firma Electrónica Avanzada)?

                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r19" aria-expanded="false" aria-controls="r19"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r19" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        No, los documentos estan diseñados para llevar el mismo de tipo de firma para todas las firmas involucradas, esto nos ayuda a mantener un mejor control de la integridad del documento y que al mismo tiempo obtenga unicidad en el proceso de integración tecnológica y posteriormente que el documento completo pueda obtener el no repudio.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p20">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r20" aria-expanded="false" aria-controls="r20">
                                            23.- ¿Qué es la figura del no repudio?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r20" aria-expanded="false" aria-controls="r20"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r20" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        Consiste en que la firma electrónica avanzada contenida en el o los documentos electrónicos garantiza la autoría e integridad del documento y que dicha firma corresponde exclusivamente al firmante. Es decir, el no repudio evita que el emisor o el receptor nieguen la firma y recepción del documento firmado. Así, cuando se envía un documento a firma, el receptor puede comprobar que, el emisor envió el documento. Por otra parte, cuando se recibe un documento para ser firmado, el emisor puede verificar que, el receptor recibió el documento.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p21">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r21" aria-expanded="false" aria-controls="r21">
                                            24.- ¿Qué hacer si no puedo ingresar?

                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r21" aria-expanded="false" aria-controls="r21"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r21" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        Deberás de verificar que se está ingresando la contraseña correctamente teniendo en cuenta espacios, el uso de mayúsculas y minúsculas. En caso de no recordar la contraseña, puede seleccionar el botón Recuperar Contraseña, ingresar su correo electrónico con el que fue registrada su cuenta y dar clic sobre el botón "Recuperar", después recibirá un correo electrónico con los pasos a seguir para finalizar este proceso.
                                        <br><br>
                                        Nota: Si no recibe el correo electrónico, deberá revisar su bandeja de SPAM o correo no deseado.
                                        <br><br>
                                        Si no recuerda la cuenta de correo con la que fue registrada su cuenta o no tiene acceso a la misma, deberá contactar a Soporte Técnico a través del correo electrónico <a class="Linkss" href="mailto: soporte@legalexgs.com">soporte@legalexgs.com</a>

                                    </div>
                                </div>
                            </div>
                            <!-- <div class="card">
                                <div class="card-header faq" id="p22">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r22" aria-expanded="false" aria-controls="r22">
                                            25.- ¿Qué es la figura del no repudio?

                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r22" aria-expanded="false" aria-controls="r22"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r22" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        Consiste en que la Firma Electrónica Avanzada contenida en el o los documentos electrónicos garantiza la autoría e integridad del documento y que dicha firma corresponde exclusivamente al firmante. Es decir, el no repudio evita que el emisor o el receptor niegue la firma y recepción del documento firmado. Así, cuando se envía un documento a firma, el receptor puede comprobar que el emisor envió el documento. Por otra parte, cuando se recibe un documento para ser firmado, el emisor puede verificar que el receptor recibió el documento.

                                    </div>
                                </div>
                            </div> -->
                            <!-- <div class="card">
                                <div class="card-header faq" id="p23">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r23" aria-expanded="false" aria-controls="r23">
                                            26.- ¿Qué es la firma autógrafa?

                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r23" aria-expanded="false" aria-controls="r23"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r23" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        La Firma Autógrafa Digital (FAD), es conocida como una firma electrónica simple, con la cual se puede dar un sustento legal mínimo necesario pero se considera repudiable. Esta se realiza dibujando el trazo de la firma autógrafa que comunmente se realiza en papel, es decir, se realiza la misma firma autógrafa pero en lugar de utilizar bolígrafo y papel se realiza de forma digital.

                                    </div>
                                </div>
                            </div> -->
                            <!-- <div class="card">
                                <div class="card-header faq" id="p24">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r24" aria-expanded="false" aria-controls="rr247">
                                            27.- ¿Qué es la firma electrónica avanzada?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r24" aria-expanded="false" aria-controls="r24"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r24" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        Es el conjunto de datos y caracteres que permite la identificación del firmante, está ha sido creada por medios electrónicos bajo el exclusivo control del firmante, de manera que está vinculada únicamente al mismo y a los datos a los que se refiere, lo que permite que sea detectable cualquier modificación ulterior de éstos, y a su vez, esta firma produce los mismos efectos jurídicos que la firma autógrafa.

                                    </div>
                                </div>
                            </div> -->
                            <!-- <div class="card">
                                <div class="card-header faq" id="p25">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r25" aria-expanded="false" aria-controls="r25">
                                            28.- ¿Qué hacer si no puedo ingresar?

                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r25" aria-expanded="false" aria-controls="r25"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r25" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        Deberás verificar que se está ingresando la contraseña correctamente teniendo en cuenta espacios, el uso de mayúsculas y minúsculas. En caso de no recordar la contraseña, puede seleccionar el botón "Recuperar Contraseña", ingresar su correo electrónico con el que fue registrada su cuenta y dar clic sobre el botón "Recuperar", después recibirá un correo electrónico con los pasos a seguir para finalizar este proceso.
                                        <br><br>
                                        Nota: Si no recibe el correo electróniNota: Si no recibe el correo electrónico, deberá revisar su bandeja de SPAM o correo no deseado.co, deberá revisar su bandeja de SPAM o correo no deseado.
                                        <br><br>
                                        Si no recuerda la cuenta de correo con la que fue registrada su cuenta o no tiene acceso a la misma, deberá contactar a Soporte Técnico a través del Chat que se encuentra integrado al ingresar a Covenant Firma: <a class="Linkss" href="https://www.legalexgs.com/covenant_firma/">https://www.legalexgs.com/covenant_firma/</a>

                                    </div>
                                </div>
                            </div> -->
                            <!-- <div class="card">
                                <div class="card-header faq" id="p26">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r26" aria-expanded="false" aria-controls="r26">
                                            29.- ¿No reconoce la contraseña de mi certificado?

                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r26" aria-expanded="false" aria-controls="r26"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r26" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        En caso de que al intentar firmar un documento con la Firma Electrónica Avanza (e.firma o FIEL) que provee el Servicio de Administración Tributaria (SAT) le regrese el aviso "La contraseña de la clave privada no es correcta" deberá verificar que la clave privada que esta ingresando corresponde a los archivos de firma (archivos con extensión *.cer y *.key). Esta contraseña se designa junto con la emisión o renovación de la e.firma o FIEL que emite el SAT, ya sea desde su portal web (para renovaciones) o en sus instalaciones (emisión por primera vez), una forma de comprobar su contraseña es intentando ingresar al portal del SAT ( <a class="Linkss" href="https://cfdiau.sat.gob.mx/nidp/wsfed/ep?id=SATUPCFDiCon&sid=0&option=credential&sid=0">https://cfdiau.sat.gob.mx/nidp/wsfed/ep?id=SATUPCFDiCon&sid=0&option=credential&sid=0</a> ) mediante la opción de e.firma y no mediante el acceso por contraseña (usuario (RFC) y contraseña).

                                    </div>
                                </div>
                            </div> -->
                            <!-- <div class="card">
                                <div class="card-header faq" id="p27">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r27" aria-expanded="false" aria-controls="r27">
                                            30.- ¿Puedo firmar sin subir certificados?

                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r27" aria-expanded="false" aria-controls="r27"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r27" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        Para realizar la firma desde Covenant Firma en un ambiente web, no. Cada que se realice una transacción de firma deberá cargar sus archivos de la e.firma o FIEL que emite el SAT. En el caso de Legalex GS y sus productos como Covenant, los archivos que se utilizan para la firma electrónica no son guardados o resguardados.
                                        <br><br>
                                        Para las firmas realizadas desde la aplicación móvil de Covenant Firma (disponible para dispositivos con iOS y Android), la aplicación está diseñada para que el usuario le indique en donde se encuentran los archivos de firma (e.firma o FIEL) y que no sea necesario cargar todos los archivos en cada firma, sin embargo, esto no quiere decir que se resguarden en la aplicación, lo unico que se guarda es la ubicación donde se consultará la información.
                                    </div>
                                </div>
                            </div> -->
                            <!-- <div class="card">
                                <div class="card-header faq" id="p28">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r28" aria-expanded="false" aria-controls="r28">
                                            31.- ¿Mis datos de firma son guardados dentro de Covenant o por Legalex GS?

                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r28" aria-expanded="false" aria-controls="r28"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r28" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        No, los archivos que se utilizan para la firma electrónica no son guardados por Legalex GS ni a través de sus productos. La firma en el ambiente web, es realizada bajo un proceso sobre la caché del equipo donde se encuentra la persona firmando, una vez que el proceso de firma termina, esta información también es eliminada para evitar el robo de información.

                                    </div>
                                </div>
                            </div> -->
                            <!-- <div class="card">
                                <div class="card-header faq" id="p29">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r29" aria-expanded="false" aria-controls="r29">
                                            32.- ¿El Servicio de Administración Tributaria (SAT) sabe qué estoy firmando?

                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r29" aria-expanded="false" aria-controls="r29"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r29" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        No, el Servicio de Administración Tributaria (SAT) no tiene control ni conocimiento sobre la documentación digital que se esta firmando. Los servicios proveídos por Legalex GS para la firma electrónica solo realizán la consulta en tiempo real ante el SAT para verificar que los archivos de firma con los cuales se intenta realizar el proceso de firma son veridicos, actuales y no se encuentran revocados.

                                    </div>
                                </div>
                            </div> -->
                            <!-- <div class="card">
                                <div class="card-header faq" id="p30">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r30" aria-expanded="false" aria-controls="r30">
                                            33.- ¿Cómo activo mi cuenta?

                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r30" aria-expanded="false" aria-controls="r30"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r30" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        El proceso de "Activación de cuenta" es muy sencillo, solo se realiza una ocasión; recibe mediante un correo electrónico un usuario y contraseña temporal, accede a Covenant Firma, siguiendo las indicaciones del proceso y en cuestión de minutos estarás firmando electrónicamente.

                                    </div>
                                </div>
                            </div> -->



                        </div>
                    </div>
                    <?php
                    include 'inc/pie.php';
                    ?>
                </div>
            </div>
        </div>
    </section>
</main>

<script type="text/javascript" src="assets/js/faqs.js"></script>