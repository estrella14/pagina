$(document).ready(function () {
    
    $("#inputtel").mask('(000) 000 0000');
    jQuery.validator.addMethod("correo_ok", function (value) {
        //expresion que valida correo
        value = value.trim().toUpperCase();
        var patt = new RegExp(/^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,13}|[0-9]{1,3})(\]?)$/g);
        let condicion = patt.test(value);
        if (condicion) {
            return true;
        } else {
            return false;
        }
    }, "");

    $("#form-contact").validate({
        rules:{
            solucion:{
                required:true
            },
            nombre:{
                 required:true,
                 minlength:3

             },
            paterno:{
                 required:true,
                 minlength:3
             },
                     correo:{
                 required:true,
                 correo_ok:true,
                 email:true
             },
            telefono:{
                 required:true,
                 minlength:10,
                 maxlength:14
             },
             mensaje:{
                 required: true,
                 minlength:15
             },
             avprivacidad:{
                 required:true
             }
        },
        messages:{
            solucion:{
                 required: "Seleccione una solucion para poder ayudarle"
             },
                      nombre:{
                 required:"Campo obligatorio",
                 minlength:"Minimo 3 caracteres"

             },
             paterno:{
                 required:"Campo obligatorio",
                 minlength:"Minimo 3 caracteres"
             },
             correo:{
                 required:"Campo obligatorio",
                 correo_ok:"Formato incorrecto, ingresa un correo válido",
                 email:"Formato incorrecto,ingresa un correo válido"
             },
             telefono:{
                 required:"Campo obligatorio",
                 minlength:"Minimo 10 caracteres",
                 maxlength:"Maximo 14 caracteres"
             },
             mensaje:{
                 required: "Campo obligatorio",
                 minlength:"Minimo 15 caracteres"
             },
             avprivacidad:{
                 required:"Acepte nuestro el aviso de privacidad para poder enviar el mensaje"
             }

        },
         errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        },
        submitHandler: function () {
            var response = grecaptcha.getResponse();
            if(response.length> 0){
                 RegistroPsp();
            }
            else{
                $("#captcha_mensaje").removeAttr("hidden");
        }
    
            
        }
    });


});


function RegistroPsp() {
    let response = grecaptcha.getResponse();
    let soluccion = $('#servicio').val();
    let name = $('#inputname').val();
    let app = $('#inputapp').val();
    let apm = $('#inputapm').val();
    let email = $('#inputemail').val();
    let empresa = $('#inputempresa').val();
    let puesto = $('#inputpuesto').val();
    $("inputtel").unmask();
    let tel = $('#inputtel').val();
    let ms = $('#inputmessage').val();
    
        
        data = {
            token_registro: 'v4rkg5662cp4v47vqv8t9rngj0asgf7a26lspl66ffjkt4c889',
            nombre: name,
            paterno: app,
            materno: apm,
            correo: email,
            telefono: tel,
            solucion: soluccion,
            mensaje: ms,
            empresa:empresa,
            puesto:puesto

        }
        $.ajax({
            url: 'https://desarrollo.legalexgs.com/prospeccion/api/rest/Registro/nuevoProspecto',
            method: 'POST',
            dataType: 'Json',
            data: data,
            success: function (response) {
                const Toast = Swal.mixin({
                toast: true,
                position: 'center',
                showConfirmButton: true,
                confirmButtonColor: '#db590dd3',
                confirmButtonText: 'Aceptar',
                timer: 5000,
                timerProgressBar: false, //ponemos en false esta propiedad(incluso podemos borrarla)

            });
                switch(response.errorCode){
                    case 1: Toast.fire({
                            icon: 'success',
                            title: '<h2 style="color:#db590dd3">Aviso:</h2>',
                            html: '<p style=”text-align: right;”>Sus datos han sido guardados correctamente.<p>'
                            //text: response.message.description
                        }).then((result)=>{
                             $("#form-contact")[0].reset();
                            location.reload();
                        });
                    break;
                    default:Toast.fire({
                    icon: 'error',
                    title: '¡Error!',
                    html: '<p style=”text-align: right;”>Error con el guardado de datos.<p>',
                    text: response.message
                }).then((result)=>{
                    
                });
                break 
                }
            },
            error: function (response) {
                const Toast = Swal.mixin({
                toast: true,
                position: 'center',
                showConfirmButton: true,
                confirmButtonColor: '#db590dd3',
                confirmButtonText: 'Aceptar',
                timer: 5000,
                timerProgressBar: false, //ponemos en false esta propiedad(incluso podemos borrarla)

            });
                Toast.fire({
                    icon: 'error',
                    title: '¡Error!',
                    html: '<p style=”text-align: right;”>Error al conectar con el servidor<p>',
                    text: response.message
                }).then((result)=>{
                    
                });
                
            }
        })

}