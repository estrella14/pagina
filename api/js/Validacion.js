let url;
let form;


//TODO: metodo principal para validar los archivos de sello y nom11
function Validar(tipo) {
    //debugger;
    let textoExito = "";
    let tipodeDocumento = "";
    form = new FormData()
    let tsq = $("[name='tsq']")[0];
    let tsr = $("[name='tsr']")[0];
    let pdf = $("[name='archivo']")[0];;
    switch (tipo) {
        case 1:
            url = "https://www.legalexgs.com/legalex_nom/Validador/validarSDT";
            tipodeDocumento = "Sellado"; textoExito = "El SDT ha sido validado exitosamente";
            break;
        case 2:
            url = "https://www.legalexgs.com/legalex_nom/Validador/validarNOM151";
            tipodeDocumento = "Conservado"; textoExito= "La constancia ha sido validada exitosamente";
            break;
    }

    if (
        (tsq.files && tsq.files[0]) &&
        (tsr.files && tsr.files[0]) &&
        (pdf.files && pdf.files[0])
    ) {
        form.append("tsq", tsq.files[0]);
        form.append("tsr", tsr.files[0]);
        form.append("archivo", pdf.files[0]);
        form.append("formAction", "validarTSA");
        $.ajax({
            url: url,
            method: "POST",
            timeout: 0,
            crossDomain: true,
            processData: false,
            mimeType: "multipart/form-data",
            contentType: false,
            data: form,
            dataType: "json"

        })
            .done(function () {
                document.getElementById("res_numserie").innerHTML = "cargando...";
                document.getElementById("res_exactitud").innerHTML = "cargando...";
                document.getElementById("res_tiempo").innerHTML = "cargando...";
                document.getElementById("res_algoritmo").innerHTML = "cargando...";
                document.getElementById("res_version").innerHTML = "cargando...";
                document.getElementById("res_policy").innerHTML = "cargando...";
                document.getElementById("res_hash").innerHTML = "cargando...";
                //document.getElementById("resultados").classList.remove("hide");

            })
            .fail(function (response) {
                console.log("soy error")
            })
            .always(function (response) {
                typeresponse(response, textoExito);
            });
    } else {
        var mensajeError= "";
        var mensajeError2= "";
        var mensajeError3 = "";
        if(tsq.files[0] == undefined){ // si es undefined significa que no ha cargado este archivo
            tipo == 2 ? mensajeError = "Archivo .ltsq<br>" : mensajeError = "Archivo .tsq<br>"
        }
        if(tsr.files[0] == undefined){
            tipo == 2 ? mensajeError2 = "Archivo .ltsr<br>" : mensajeError2 = "Archivo .tsr<br>"
        }
        if(pdf.files[0] == undefined){
            mensajeError3 = "Documento "+tipodeDocumento
        }
        const Toast = Swal.mixin({
            toast: true,
            position: 'center',
            showConfirmButton: true,
            confirmButtonText: 'Aceptar',
            confirmButtonColor: '#db590dd3',
            timer: 2500,
        })

        Toast.fire({
            icon: 'error',
            title: '<h5 style="color:#db590dd3">Favor de subir los siguientes archivos:</h5>',
            html: '<p>' +mensajeError+mensajeError2+mensajeError3+ '</p>',
            //text:
        })
    }
}

function infDocs(file, tipo, tipo2) {
    document.getElementById('resFile' + file).innerHTML = $("[name=" + tipo + "]")[0].files[0].name;

    switch(tipo2){
        case "ltsq":
            var btnconstancialtsq = document.getElementById("ltsq");//obtenermos el elemento que tiene como id ltsq
            btnconstancialtsq.classList.remove("btn-submit-file"); // removemos la clase que tiene el color gris
            btnconstancialtsq.classList.add("btn-submit-file-success"); // añadimos la claswe que tiene el color naranja
            break;
        case "tsq":
            var btnconstanciatsq = document.getElementById("tsq"); //obtenermos el elemento que tiene como id tsq
            btnconstanciatsq.classList.remove("btn-submit-file"); // removemos la clase que tiene el color gris
            btnconstanciatsq.classList.add("btn-submit-file-success"); // añadimos la claswe que tiene el color naranja
            break;
        case "ltsr":
            var btnconstancialtsr = document.getElementById("ltsr");//obtenermos el elemento que tiene como id ltsr
            btnconstancialtsr.classList.remove("btn-submit-file"); // removemos la clase que tiene el color gris
            btnconstancialtsr.classList.add("btn-submit-file-success"); // añadimos la claswe que tiene el color naranja
            break;
        case "tsr":
            var btnconstanciatsr = document.getElementById("tsr"); //obtenermos el elemento que tiene como id tsr
            btnconstanciatsr.classList.remove("btn-submit-file"); // removemos la clase que tiene el color gris
            btnconstanciatsr.classList.add("btn-submit-file-success"); // añadimos la claswe que tiene el color naranja
            break;
        case "archivo":
            var btnconstanciaarchivo = document.getElementById("ldoc-file");//obtenermos el elemento que tiene como id archivo
            btnconstanciaarchivo.classList.remove("btn-submit-file"); // removemos la clase que tiene el color gris
            btnconstanciaarchivo.classList.add("btn-submit-file-success"); // añadimos la claswe que tiene el color naranja
            break;
    }
}

//TODO: revisa los errores regresados por el back-end para responder al Front-end correctamente
//TODO: aun faltan detallitos
function typeresponse(response, textoExito) {
    //debugger;
    
    switch (response.message.type) {

        case 2:
        case 3:
            //document.getElementById("mensaje-accept").innerHTML = response.message.description;
            const Toast = Swal.mixin({
                toast: true,
                position: 'center',
                showConfirmButton: true,
                confirmButtonColor: '#db590dd3',
                confirmButtonText: 'Aceptar',
                timer: 2500,
                timerProgressBar: false, //ponemos en false esta propiedad(incluso podemos borrarla)

            })
            Toast.fire({
                icon: 'success',
                title: '<h2 style="color:#db590dd3">¡Validado!</h2>',
                html: '<p style=”text-align: right;”>'+textoExito+'<p>'
                //text: response.message.description
            })
            document.getElementById("res_numserie").innerHTML = response.numero_serie;
            document.getElementById("res_exactitud").innerHTML = response.exactitud;
            document.getElementById("res_tiempo").innerHTML = response.tiempo;
            document.getElementById("res_algoritmo").innerHTML =response.algoritmo;
            document.getElementById("res_version").innerHTML = response.version;
            document.getElementById("res_policy").innerHTML =response.policy;
            document.getElementById("res_hash").innerHTML = response.hash;
            var hola= document.querySelector("div.hola");
            var pollo= document.querySelector("div.pollo");
            var arroz= document.querySelector("div.arroz");
            hola.className = "row slideLeft col-md-12 hola"
            pollo.className = "col-md-5 carta-req-success mr-5 col-sm-12 pollo"
            arroz.className = "col-md-6 carta-req-success col-sm-12 arroz"
            break;
        case 510:

            var hola= document.querySelector("div.hola");
            var pollo= document.querySelector("div.pollo");
            var arroz= document.querySelector("div.arroz");
            hola.className = "row justify-content-center hola "
            pollo.className = "col-md-7 col-sm-12 carta-req pollo"
            arroz.className = " carta-req hide arroz col-sm-12"
            const Toast2 = Swal.mixin({
                toast: true,
                position: 'center',
                timer: 2500,
                confirmButtonColor: '#db590dd3',
                showConfirmButton: true,
                confirmButtonText: 'Aceptar',

            })
            Toast2.fire({
                icon: 'error',
                title: '¡Error!',
                text: response.message.description
            })
            break;
        case 501:

            //document.getElementById("resultados").classList.add("hide");
            var hola= document.querySelector("div.hola");
            var pollo= document.querySelector("div.pollo");
            var arroz= document.querySelector("div.arroz");
            hola.className = "row justify-content-center hola " // recorriendo a la izquierda
            pollo.className = "col-md-7 col-sm-12 carta-req pollo"
            arroz.className = " carta-req hide arroz col-sm-12"
            const Toast3 = Swal.mixin({
                toast: true,
                position: 'center',
                timer: 2500,
                confirmButtonColor: '#db590dd3',
                showConfirmButton: true,
                confirmButtonText: 'Aceptar',

            })

            Toast3.fire({
                icon: 'error',
                title: 'Error 501',
                text: response.message.description
            })
            break;
        case 50:
            var hola= document.querySelector("div.hola");
            var pollo= document.querySelector("div.pollo");
            var arroz= document.querySelector("div.arroz");
            hola.className = "row justify-content-center hola " // recorriendo a la izquierda
            pollo.className = "col-md-7 col-sm-12 carta-req pollo"
            arroz.className = " carta-req hide arroz col-sm-12"
            const Toast4 = Swal.mixin({
                toast: true,
                position: 'center',
                timer: 2500,
                confirmButtonColor: '#db590dd3',
                showConfirmButton: true,
                confirmButtonText: 'Aceptar',

            })
            Toast4.fire({
                icon: 'error',
                title: '¡Error!',
                text: "archivos incorrectos"
            })
            break
    }

}