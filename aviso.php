<?php
$pageLink = "Centro de Recursos";
include 'inc/cab.php';
?>
<section class="banner-recursos container">
    <div class="header-recursos">
        <h1>Aviso de Privacidad</h1>
    </div>
    <hr>
    <div class="row justify-content-center">
        <div class="col-md-9">
            <div class="texto-tc">
                <p>
                    En <b><b>Legalex</b></b>, la privacidad es una prioridad. Esta Política de Privacidad establece la forma en que <b><b>Legalex</b></b>, Inc. y su grupo corporativo ("nosotros", "nuestro" o "nos") recaban y tratan los datos personales de los clientes y otras personas (colectivamente "usted") que acceden o utilizan nuestros sitios web, incluyendo <b><b>legalexgs.com</b></b>, nuestras aplicaciones móviles, nuestro cliente web o cliente profesional, y/o cualquiera de nuestros otros sitios, productos o servicios que se vinculan a esta Política de Privacidad (los "Servicios"). Al utilizar nuestros Servicios, usted entiende que recabaremos y trataremos sus datos personales según se describe en esta Política de Privacidad.
                    <br><br>
                    En algunos casos podemos tratar sus datos personales en virtud de un convenio que tengamos con una organización de terceros. En esos casos, los términos de ese acuerdo pueden regir la forma en que tratamos su información personal. Si cree que una organización tercera nos ha pedido que tratemos sus datos personales en su nombre, por favor, consulte con ellos en primer lugar, ya que ellos serán los responsables de cómo tratamos sus datos. Este Aviso de privacidad no se aplica a los sitios web y aplicaciones de terceros que usted pueda utilizar, incluido cualquier otro sitio vinculado a nuestros Servicios. Debe revisar los términos y políticas de los sitios web y aplicaciones de terceros antes de hacer clic en cualquier enlace.
                    <br><br>
                    Los productos y servicios principales de <b><b><b>Legalex</b></b></b> ayudan a los usuarios a crear, completar y demostrar la validez de las transacciones digitales o electrónicas, tal como la firma electrónica de un contrato de servicios de telefonía móvil o la colocación de una firma digital en una solicitud de préstamo. Como parte de nuestros Servicios, los usuarios quieren que recopilemos y registremos información que ayude a las partes a demostrar la validez de las transacciones. Esta información incluye a las personas que participan en las transacciones y los dispositivos que esas personas utilizan.
                </p>
            </div>

        </div>

    </div>
    <div class="row justify-content-center"></div>
    <div class="embed-responsive embed-responsive-16by9">
        <iframe class="embed-responsive-item" src="assets/pdf/avisoprivacidad.pdf#toolbar=0&navpanes=0&scrollbar=0&view=fitH,100" type="application/pdf" width="100%" height="100%" allowTransparency=”true”></iframe>
    </div>
    </div>
</section>
<?php
include 'inc/pie.php';
?>