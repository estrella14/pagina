<?php 
$pageLink = "Centro de Recursos";
    include 'inc/cab.php';
?>
<style>
      @media (max-width: 1200px){
    .resumen div {
	width: 100%;
    font-size: 14px;
}
.cuadro-detalles-compra {
    border: 1px solid #051c33;
    padding: 8px 10px;
    margin: 0 36px;
    background-color: #fff;
    max-width: 300px;
}
}
    @media (max-width: 930px){
    .resumen div {
	width: 140%;
    font-size: 14px;
}
}
@media (max-width:900px){
    .resumen div {
        width: 100%;
    font-size: 14px;
    margin-top: 15px;
    padding-left: 11px;
    margin: 13px auto;
}
}


</style>
   <section class="banner-recursos pago" style="background-color:#e5ebf1">
        <div class="container">
            <div class="row">
                <div class="col-md-8 formulario-compra-back" style="background-color:#fff" >
                    <h1 style="font-size:22px;margin-bottom:25px;">Comprar Plan Empresarial</h1>
                    <div class="formulario-compra">
                        <div class="header-compra">
                            <span class="paso-compra active">1</span> Datos de cuenta <span class="pago-log">¿Ya tienes una? Inicia Sesión</span>
                        </div>
                        <form action="#" id="form-datos" class="formulario-detalles">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="nombrepago">Nombre (s) *</label>
                                        <input type="text" class="form-control" id="nombrepago" name="nombrepago" placeholder="Nombre (s) " required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="apelliedospago">Apellidos *</label>
                                        <input type="text" class="form-control" id="apelliedospago" name="apelliedospago" placeholder="Apellidos" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="correopago">Correo electrónico *</label>
                                        <input type="text" class="form-control" id="correopago" name="correopago" placeholder="Correo electrónico" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="telefonopago">Teléfono *</label>
                                        <input type="text" class="form-control" id="telefonopago" name="telefonopago" placeholder="Teléfono" required>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="empresapago">Empresa *</label>
                                        <input type="text" class="form-control" id="empresapago" name="empresapago" placeholder="Nombre de la empresa o negocio" required>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="giro">Giro de la empresa</label>
                                        <select name="giro" id="giro" class="form-control" title="" required>
                                            <option value="null">Seleccionar tipo de empresa</option>
                                            <option value="Accounting">Contabilidad y impuestos</option>
                                            <option value="Business Services">Servicios de negocios y Consultoria</option>
                                            <option value="Construction">Construcción</option>
                                            <option value="Education">Educación</option>
                                            <option value="Financial Services">Servicios financieros</option>
                                            <option value="Government">Gobierno</option>
                                            <option value="Insurance - Health">Planes de salud y contribuyentes</option>
                                            <option value="Healthcare - Providers">Atención de la salud - Proveedores</option>
                                            <option value="Insurance">Seguros</option><option value="Legal">Despachos de abogados</option>
                                            <option value="Healthcare/Life Sciences">Ciencias de la vida</option>
                                            <option value="Manufacturing">Manufactura</option>
                                            <option value="Real Estate - Mortgage">Hipotecas</option>
                                            <option value="Not For Profit">Sin fines de lucro</option>
                                            <option value="Real Estate - Commercial">Bienes raíces: comercial</option>
                                            <option value="Real Estate - Agent">Bienes raíces residenciales</option>
                                            <option value="Retail">Minorista</option>
                                            <option value="Individual">Estudiante</option>
                                            <option value="Technology">Tecnología</option>
                                            <option value="Other">Otros</option>
                                        </select>
                                    </div>
                                   
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <button id="guardarDPersonal" class="btn btn-outline-generico2 btn-sm btn-block ">Continuar</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="formulario-compra mt-5 hide" id="facturacion">
                        <div class="header-compra">
                            <span class="paso-compra">2</span> Facturación 
                        </div>
                        <form action="#" style="display:block;" class="formulario-detalles">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="tipof" value="" id="facmen">
                                        <label class="form-check-label" for="facmen">
                                            Facturación Mensual
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="tipof" value="" id="facan">
                                        <label class="form-check-label" for="facan">
                                            Facturación Anual
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="rfc">RFC *</label>
                                        <input type="text" class="form-control" id="rfc" name="rfc" placeholder="Introduzca el RFC" required>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="razon">Razón Social *</label>
                                        <input type="text" class="form-control" id="razon" name="razon" placeholder="Introduzca la Razón Social" required>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="rfc">Dirección *</label>
                                        <input type="text" class="form-control" id="direccion" name="direccion" placeholder="Introduzca la dirección" required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="cp">Código Postal *</label>
                                        <input type="text" class="form-control" id="cp" name="cp" placeholder="Código Postal" required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="estado">Estado *</label>
                                        <select name="estado" id="estado" class="form-control">
                                            
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="ciudad">Ciudad *</label>
                                        <select name="ciudad" id="ciudad" class="form-control"> 
                                            <option value="">Seleccione una ciudad</option>
                                        </select>
                                    </div>
                                </div>
                                <script src="assets/js/estadosarray.js"></script>
                                <script>
                                    populateCountries('estado','ciudad');
                                </script>
                                 <div class="col-md-12">
                                    <div class="form-group">
                                        <button id="guardarDFact" class="btn btn-outline-generico2 btn-sm btn-block ">Continuar</button>
                                    </div>
                                </div>
                            </div>  
                        </form>
                    </div>
                    <div class="formulario-compra mt-5 hide" id="pago">
                        <div class="header-compra">
                            <span class="paso-compra">3</span> Pago
                        </div>
                        <form action="#" style="display:block" class="formulario-detalles">
                            <div class="payPaypal mt-4">
                                <h5>Pago con Paypal</h5>
                                <div class="imgpaypal">
                                    <div id="paypal-button"></div>
                                </div>
                            </div>
                            <hr>
                            <h5>Pago con tarjeta</h5>
                            <div class="row mt-3">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="numtargeta">Número de la tarjeta</label>
                                        <input type="text" class="form-control" id="numtargeta" name="numtargeta" placeholder="XXXX XXXX XXXX XXXX " required>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="nombretarjeta">Nombre del tarjeta habiente *</label>
                                        <input type="text" class="form-control" id="nombretarjeta" name="nombretarjeta" placeholder="Nombre completo" required>
                                    </div>
                                </div>
                                <div class="col-md-8 ">
                                    <div class="row">
                                       
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="mesexp">Mes</label>
                                                <select name="mesexp" id="mesexp" class="form-control"></select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="anoexp">Año</label>
                                                <select name="anoexp" id="anoexp" class="form-control"></select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="cvv">CCV *</label>
                                        <input type="text" class="form-control" id="ccv" name="ccv" placeholder="" required>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <button id="datosTarjeta" class="btn btn-outline-generico2 btn-sm btn-block ">Proceder a pagar</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
               
                <div class="col-md-4">
                    <div class= "resumen">
                    <div class="cuadro-detalles-compra">
                        <div class="detalles-concepto">
                            <small>Resumen del plan</small>
                            <hr>
                            <p>Plan: Empresarial</p>
                            <p>Cliente: Jose Javier</p>
                            <p>Correo: javier@test.com</p>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                               <b>Empresarial</b> 
                            </div>
                            <div class="col-md-6">
                               <b>$3400/mes</b> 
                            </div>
                        </div>
                        <hr>
                        <div class="logos-pay text-center">
                            <img src="assets/img/payment-logo.png" alt="" srcset="">
                            <a href="/nosotros/contacto" style="color:#929292;font-size:10px"><i class="icon ion-ios-contacts"></i> Contactar a Soporte</a>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php 
include 'inc/pie.php';
?>
<script src="https://www.paypalobjects.com/api/checkout.js"></script>
<script>
  paypal.Button.render({
    env: 'sandbox', // Or 'production'
    // Set up the payment:
    // 1. Add a payment callback
    payment: function(data, actions) {
      // 2. Make a request to your server
      return actions.request.post('/my-api/create-payment/')
        .then(function(res) {
          // 3. Return res.id from the response
          return res.id;
        });
    },
    // Execute the payment:
    // 1. Add an onAuthorize callback
    onAuthorize: function(data, actions) {
      // 2. Make a request to your server
      return actions.request.post('/my-api/execute-payment/', {
        paymentID: data.paymentID,
        payerID:   data.payerID
      })
        .then(function(res) {
          // 3. Show the buyer a confirmation message.
        });
    }
  }, '#paypal-button');
/** diseño */
    window.onscroll = function() {myFunction()};
    var sticky = $(".cuadro-detalles-compra").offset().top;
    var contenido = $('.formulario-compra-back').outerHeight();
    console.log($(".cuadro-detalles-compra").outerHeight());
    var suma = contenido-$(".cuadro-detalles-compra").outerHeight(); 
    var cuadro = $(".cuadro-detalles-compra").outerHeight();
    console.log(sticky);
    console.log(contenido);
    console.log("Cantidad total del cuadro " + suma);
function cambiarDatos(){
    sticky = $(".cuadro-detalles-compra").offset().top ;
    contenido = $('.formulario-compra-back').outerHeight();
    cuadro = $(".cuadro-detalles-compra").outerHeight();
    suma = (contenido); 
 
}
function myFunction() {
    console.log(window.pageYOffset);
    if (window.pageYOffset >= sticky) {
      if(window.pageYOffset <= suma){
        $(".cuadro-detalles-compra").addClass('fix-pay');
      }else{
        $(".cuadro-detalles-compra").removeClass('fix-pay');
      }  

    }else{
        $(".cuadro-detalles-compra").removeClass('fix-pay');
    }
   
}
    for(let i=1;i<=12;i++){
        var txtFecha = "";
        if(i<10){
            txtFecha= "0"+i;
        }else{
            txtFecha = i;
        }
        $("#mesexp").append('<option value="'+txtFecha+'">'+txtFecha+'</>');
    }
    const anioActual = new Date().getFullYear();
    for(let u = anioActual; u<=anioActual+10;u++){
        $("#anoexp").append('<option value="'+u+'">'+u+'</>');
    }
    $("#telefonopago").mask('(000) 000 0000');
    $("#numtargeta").mask('0000 - 0000 - 0000 - 0000');
    $("#ccv").mask('000');
    $("#cp").mask('00000');
    $("#guardarDPersonal").on("click", function(e){
        
        var $button = $(this);
        var $form = $("#form-datos");
        var pathToPhp = "hola";
        $.validator.addMethod("customemail", 
            function(value, element) {
                return /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(value);
            }
        );
        $.validator.setDefaults({
           errorClass:'help-block',
            highlight: function(element){
                $(element)
                .closest('.form-group')
                .addClass('has-error');
               
            },
            unhighlight:function(element){
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorPlacement: function(error,element){
                if(element.prop('type') === "checkbox"){
                    error.insertAfter(element.parent());
                }else{
                    error.insertAfter(element);
                }
            }
        });
        
        $form.validate({
            rules: {
                nombrepago: {
                    required: true,
                },
                correopago :{
                    required: true,
                    email: true,
                    customemail:true,
                },
                apelliedospago:{
                    required:true
                },
                telefonopago:{
                    required:true
                },
                empresapago:{
                    required:true
                },
                giro:{
                    required:true
                }
              },
              messages:{
                    nombrepago:{
                    required:"Introduzca su nombre."    
                  },
                  apelliedospago:{
                    required:"Introduzca sus apellidos."
                },
                telefonopago:{
                      required:"Introduzca su número de celular a 10 dígitos por favor.",
                      
                      min:"Por favor introduzca al menos 10 dígitos.",
                      max:"Por favor introduzca los 10 dígitos."
                  },
                  correopago:{
                      required:"Introduzca una dirección de correo electrónico válida.",
                        email:"Introduzca una dirección de correo electrónico válida: ejemplo@dominio.com.",
                        customemail:"Introduzca un dominio válido."
                  },
                  empresapago:{
                      required:"Introduzca su empresa."
                  },
                  giro:{
                      required:"Seleccione un giro de la empresa."
                  }
              },
            submitHandler: function() {
                $button.addClass("processing");
                $("#facturacion").show(1000);
                document.getElementById("facturacion").scrollIntoView({
    behavior:"smooth"
});
                cambiarDatos();
                $.post( pathToPhp, $form.serialize(),  function(response) {
                 //  alert();
                });
                return false;
            }
        });
    });
    $("#datosTarjeta").on("click", function(e){
        
        var $button = $(this);
        var $form = $(this).closest("form");
        var pathToPhp = "hola";
        $.validator.addMethod("customemail", 
            function(value, element) {
                return /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(value);
            }
        );
        $.validator.setDefaults({
           errorClass:'help-block',
            highlight: function(element){
                $(element)
                .closest('.form-group')
                .addClass('has-error');
               
            },
            unhighlight:function(element){
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorPlacement: function(error,element){
                if(element.prop('type') === "checkbox"){
                    error.insertAfter(element.parent());
                }else{
                    error.insertAfter(element);
                }
            }
        });
        
        $form.validate({
            rules: {
                numtargeta: {
                    required: true,
                },
                
                nombretarjeta:{
                    required:true
                },
                mesexp:{
                    required:true
                },
                anoexp:{
                    required:true
                },
                ccv:{
                    required:true
                }
              },
              messages:{
                numtargeta:{
                    required:"Introduzca su número de tajeta completo."    
                  },
                  nombretarjeta:{
                    required:"Introduzca su nombre completo."
                },
                
                  
                mesexp:{
                      required:"Introduzca el mes."
                  },
                  anoexp:{
                      required:"Introduzca el año."
                  },
                  
                  ccv:{
                      required:"Introduzca el código de seguridad."
                  }
              },
            submitHandler: function() {
                $button.addClass("processing");
                cambiarDatos();
                $.post( pathToPhp, $form.serialize(),  function(response) {
                   alert();
                });
                return false;
            }
        });
    });
    $("#guardarDFact").on("click", function(e){
        
        var $button = $(this);
        var $form = $(this).closest("form");
        var pathToPhp = "hola";
        $.validator.addMethod("customemail", 
            function(value, element) {
                return /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(value);
            }
        );
        $.validator.setDefaults({
           errorClass:'help-block',
            highlight: function(element){
                $(element)
                .closest('.form-group')
                .addClass('has-error');
               
            },
            unhighlight:function(element){
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorPlacement: function(error,element){
                if(element.prop('type') === "checkbox"){
                    error.insertAfter(element.parent());
                }else{
                    error.insertAfter(element);
                }
            }
        });
        
        $form.validate({
            rules: {
                tipof: {
                    required: true,
                },
                
                rfc:{
                    required:true
                },
                razon:{
                    required:true
                },
                direccion:{
                    required:true
                },
                cp:{
                    required:true
                },
                estado:{
                    required:true
                },
                ciudad:{
                    required:true
                }
              },
              messages:{
                rfc:{
                    required:"Introduzca su RFC completo."    
                  },
                  tipof:{
                    required:"Seleccione su tipo de facturación."
                },
                
                  
                razon:{
                      required:"Introduzca su razón social."
                  },
                  direccion:{
                      required:"Introduzca su dirección."
                  },
                  
                 
                  cp:{
                      required:"Introduzca su código postal."
                  },
                  estado:{
                      required:"Introduzca su estado."
                  },
                  ciudad:{
                      required:"Introduzca su ciudad."
                  },
              },
            submitHandler: function() {
                $button.addClass("processing");
                $("#pago").show(1000);
                document.getElementById("pago").scrollIntoView({
    behavior:"smooth"
});
                cambiarDatos();
                $.post( pathToPhp, $form.serialize(),  function(response) {
                 //  alert();
                });
                return false;
            }
        });
    });
</script>