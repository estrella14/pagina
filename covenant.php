<?php
$pageLink = "cloud";
include 'inc/cab.php';

?>
<head><link rel="stylesheet" href="assets/css/cov.css">
<link rel="stylesheet" href="assets/css/steps-cargaPDF.css">

</head>
<style>
:root {
    --bg: #1a1e24;
    --color: #eee;
    --font: Montserrat, Roboto, Helvetica, Arial, sans-serif;
}
.wrapper {
    padding: 1.5rem 0;
    filter: url("#goo");
    display: none;
}

.clicc {
  display: none;
}
.clic {
  display: none;
}

.imgp img {
    display: flex;
    width: 95%;
    margin: 20px 20px 20px 20px;
    flex-wrap: wrap;
}




.btnn-outline-naranja {
    color: #c56122;
    border-color: #c56122;
    background-color: #f8f9fa;
}
.btnn-outline-naranja:hover {
    color: white;
    background-color: #c56122;
    border-color: #c56122;
}

.cenboton {
    
    align-items: center;
}

.app-btnn {
    width: 25%;
    max-width: 100%;
    color: #051c33;
    margin: 26px 15px;
    text-align: left;
    border-radius: 50px;
    text-decoration: none;
    font-family: "Lucida Grande", sans-serif;
    font-size: 10px;
    text-transform: uppercase;
    box-shadow: 2px 4px 6px 0px #000;
}

#mensaje  {
		visibility: hidden;
		display: none;	
}	

.margen{
    margin: 50px auto auto auto;
}


.faa
 {
  -moz-osx-font-smoothing: grayscale;
  -webkit-font-smoothing: antialiased;
  display: contents;
  font-style: normal;
  font-variant: normal;
  text-rendering: auto;
  line-height: 1; 
}


/* esto nuevo*/
@media (max-width: 1100px) {
    .app-btnn {
    width: 30%;
    max-width: 100%;
    color: #051c33;
    margin: 26px 15px;
    text-align: left;
    border-radius: 50px;
    text-decoration: none;
    font-family: "Lucida Grande", sans-serif;
    font-size: 10px;
    text-transform: uppercase;
    box-shadow: 2px 4px 6px 0px #000;

 }
}

    
@media (min-width: 1000px){

    .imgCovenant {
    width: 70%;
    }
   

    
}

@media (max-width: 980px) {
   

    .imgCovenant {
    width: 90%;
    }


}

@media (max-width: 900px) {
    .app-btnn {
    width: 35%;
    max-width: 100%;
    color: #051c33;
    margin: 26px 15px;
    text-align: left;
    border-radius: 50px;
    text-decoration: none;
    font-family: "Lucida Grande", sans-serif;
    font-size: 10px;
    text-transform: uppercase;
    box-shadow: 2px 4px 6px 0px #000;
 }


    .lapCovenant {
        width: 270% 
    }
    .movilCovenant {
    width: 200%;
    }

    .imgCovenant {
    width: 90%;
    }
  
    

}

/* */


@media only screen and (max-width: 800px){
    .imgp img {
        display:none;
    }
    .clic {
        display: initial;
        text-align: center;
    }
    .clicc {
        display: initial;
        text-align: center;
    }

    


    /*.button {
   margin: auto 50px auto 50px;
     display: list-item;
     display: inline-block;
    text-align: center;
    background: var(--color);
    color: var(--bg);
    font-weight: bold;
    padding: 1.18em 1.32em 1.03em;
    line-height: 1;
    border-radius: 1em;
    position: relative;
    min-width: 17.23em;
    text-decoration: none;
    font-family: var(--font);
    font-size: 1.25rem;
    

      background-color: #008CBA;
      color: white;
    }
    .button {
        padding: 1.18em 1.32em 1.03em;
        min-width: 17.23em;
        background-color: #008CBA;
        color: white;
    }
    */

    .btnnn-outline-naranja {
        /* color: #c56122; */
        /* border-color: #c56122; */
        /* background-color: #f8f9fa; */
        padding: 1.18em 1.32em 1.03em;
        line-height: 1;
        border-radius: 1em;
        min-width: 17.23em;
        background-color: #008CBA;
        color: white;
    }
    .btnnn-outline-naranja:hover {
        background-color: #8FB8C6;
        color: white;
    }
    
  
    
}


/* esto nuevo*/
@media only screen and (max-width: 780px){
    
 .app-btnn {
    width: 35%;
    max-width: 100%;
    color: #051c33;
    margin: 26px 15px;
    text-align: left;
    border-radius: 50px;
    text-decoration: none;
    font-family: "Lucida Grande", sans-serif;
    font-size: 10px;
    text-transform: uppercase;
    box-shadow: 2px 4px 6px 0px #000;
    }
}

@media (max-width: 770px){
    .cardcontanier{
         
    max-width: 800px;
    }
    

    .descripCovenant {
    padding-top: 3rem !important;
    padding-left: 2rem !important;
    padding-right: 0rem !important;
    }
    .imgCovenant {
        width: 60%;
        }
        .container {
        max-width: 700px;
    }
    .lapCovenant {
        width: 270%;
    }
    
    .imgCovenant {
    width: 100%;
    }





}
@media (max-width: 766px){
    .lapCovenant {
    width: 100%;
    }
    .movilCovenant {
    width: 100%;
    }
    .imgCovenant {
    width: 70%;
    }
}

@media (min-width: 768px){

    .imgres{
    
        align-self: center;
    }
   

}

  
@media (max-width: 700px){
    .imgCovenant {
    width: 70%;
    }
   
}
 /* */
 
@media  (max-width: 500px){
	
    .app-btnn {
        width: 40%;
        max-width: 100%;
        color: #051c33;
        margin: 26px 15px;
        text-align: left;
        border-radius: 50px;
        text-decoration: none;
        font-family: "Lucida Grande", sans-serif;
        font-size: 10px;
        text-transform: uppercase;
        box-shadow: 2px 4px 6px 0px #000;
    }

  
}

@media  (max-width: 450px){
	
    .app-btnn {
        width: 45%;
        max-width: 100%;
        color: #051c33;
        margin: 26px 15px;
        text-align: left;
        border-radius: 50px;
        text-decoration: none;
        font-family: "Lucida Grande", sans-serif;
        font-size: 10px;
        text-transform: uppercase;
        box-shadow: 2px 4px 6px 0px #000;
    }
    .app-btnn i {
    width: 25%;
    text-align: center;
    font-size: 35px;
    margin: 10px 5px 10px 5px;
 }
  .app-btnn .big-txt {
    display: none;
    }
    .app-btnn p {
        display: none;
    }

}
@media  (max-width: 400px){

    .app-btnn .big-txt {
    display: none;
    }
    .app-btnn p {
        display: none;
    }
    .left{
    margin: 0px 10px 0px -20px;
   
    }
    .right{
        margin: 0px 10px 0px 0px;
    }

}


@media  (max-width: 300px){
	#documentacion  {
		visibility: hidden;
		display: none;	
 } 	
        
    #mensaje  {
        display: inherit;
        visibility: inherit;
        width: 100%;

    font-size: 12px;

    }

    .mensaje div{
        margin: 200px 20px 19px 4px;
    }
 
    .app-btnn .big-txt {
    display: none;
 }
    .app-btnn p {
        display: none;
    }


   
}

@media  (max-width: 250px){
	
    .app-btnn i {
    width: 35%;
    text-align: center;
    font-size: 35px;
    margin: 15px 5px 15px 4px;
    }
}


</style>
<main id="ts-content">
<div class="ts-content">
    <!-- <header id="ts-hero" class=" text-black" > #F5F5F8 #ececf  #F2F2F5 -->
    <header id="ts-hero" class="pt-4 pb-4 bannerPrincipal  text-black" data-bg-color="#F2F2F5" >
        <div class="container-fluid covenant">
        <section class="mb-5"  data-animate="ts-fadeInLeft" data-ts-delay=".3s">
            <div class="divPrincipalCovenant container-fluid ">
                <div class="row">
                    <div class="col-md-6  imgres logoCovenant">
                        <div class="text-center" data-animate="ts-fadeInRight" data-ts-delay=".3s">
                            <img src="assets/img/covenant/covenant_soloicono.png" class ="imgCovenant" alt="">
                        <!-- <div data-animate="ts-fadeInRight" data-ts-delay=".3s">
                            <img src="assets/img/covenant/mobile5.png" class ="imgCovenant" alt=""> -->
                        </div>
                    </div>
                    <div class="descripCovenant col-md-6">
                        <div class="row container" >
                            <div class="col-md-12 text-center">
                                <h1 data-animate="ts-reveal">
                                    <div style="color:#c56122;" class="wrapper" >
                                        <b>Covenant </b>
                                    </div>
                                           
                                </h1>
                                <hr class="divisorColor">
                            </div>
                            <div class="col-md-12 text-justify">
                                <p class=" text-black " > Covenant, es una solución informática dedicada a la gestión de documentos digitales que parte desde su creación e integración de flujos de aprobación, así como, notificaciones, almacenamiento seguro y consulta de documentos de una forma dinámica. En resumen, ofrece una serie de herramientas que permiten la total administración del flujo de un documento escalable conforme el tamaño de la empresa, haciendo más fácil la incorporación de la firma electrónica a cualquier documento digital sin olvidar la certeza jurídica de uno o más actos que se lleven a cabo.
                                
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
        </section>
    </header>
<section class=" pt-3 pb-3"  data-animate="ts-fadeInLeft" data-ts-delay=".4s">
    <div class="container ">
        <div class="row">
            <div class="descripCovenant col-md-6 ">
                <div class="row container" >
                    <div class="col-md-12 text-center">
                        <div class="ts-title">
                            <h2 >Covenant Documentos</h2>
                        </div>
                    </div>
                    <div class="col-md-12 text-justify">
                        <p class=" text-black">
                        Módulo especializado para la administración y gestión de documentos digitales. Permite la creación, edición, configuración de flujos y
                        resguardo de los documentos digitales, para su posterior envío a firma, realizando el proceso de recolección de firmas electrónicas
                        de forma óptima. Además, con la adición de nuestros servicios mediante el uso de la Firma Electrónica Avanzada o la Firma Autógrafa Digital, Sello Digital de Tiempo y
                        Constancias de Conservación de Mensajes de Datos (CCMD) de conformidad con la NOM-151, se obtiene: 
                            <dl class="vinetas">
                                <ul>
                                <li>Consulta de documentos las 24 horas del día.</li>
                                <li>Estatus del documento y las firmas realizadas en tiempo real.</li>
                                <li>Integridad de la información.</li>
                        </ul>
                    </dl>
                            Nuestros servicios son flexibles, por lo cual se pueden obtener a través del uso directo de Covenant Documentos o mediante una API (webservice). 
                        </p>
                    </div>   
                </div>
            </div>
            <div class="col-md-6  imgres">
                <div class="col-md-6 mt-6" data-animate="ts-fadeInRight" data-ts-delay=".4s">
                    <img class ="lapCovenant" src="assets/img/covenant/lap-tablet-cel.png" alt="">
                </div>
            </div>
        </div>
    </div>
</section>
<section class=" bannerPrincipal pt-4 pb-4"  data-bg-color="#F2F2F5" >
    <div class="container"  data-animate="ts-fadeInLeft" data-ts-delay=".3s">
        <div class="row">
            <div class="col-md-6  imgres">
                <div class="col-md-6" data-animate="ts-fadeInRight" data-ts-delay=".3s">
                    <img class ="movilCovenant" src="assets/img/covenant/header-slide-1.png"  alt="">
                </div>
            </div>
            <div class=" descripCovenant col-md-6 ">
                <div class="row container" >
                    <div class="col-md-12 text-center">
                        <div class="ts-title">
                            <h2 >Covenant Firma</h2>
                        </div>
                    </div>
                    <div class="col-md-12 text-justify">
                        <p class=" text-black " >Es un instrumento de seguridad, diseñado para llevar a cabo de manera digital y
                        segura la firma electrónica de un documento, evitando la presencia física del firmante. La firma se realiza
                        desde un mismo lugar, sin importar que la firma sea Electrónica Avanzada (e.firma / FIEL) o Autógrafa Digital. Contando con los principales beneficios:  
                            <dl  class="vinetas">
                                <ul>
                                <li>Verificación de la identidad del firmante.</li>
                                <li>Tiempo exacto de la transacción de firma. </li>
                                <li>Realiza el proceso vía web o aplicación móvil  desde un celular.</li>
                            </ul>
                            </dl>
                        </p>
                    </div>           
            </div>
        </div>     
    </div>
</section>
<section class="mt-4 mb-4"  data-animate="ts-fadeInLeft" data-ts-delay=".3s">
    <div class="container ">
        <div class="row">
            <div class=" descripCovenant col-md-6 ">
                <div class="row container" >
                    <div class="col-md-12 text-center">
                        <div class="ts-title">
                            <h2 class="">Covenant API</h2>
                        </div>
                    </div>
                    <div class="col-md-12 text-justify">
                        <p class=" text-black">
                        Es un conjunto de métodos y funciones que permite la conexión a nivel servidor con los servicios de Covenant a
                        través de una plataforma cliente. Permitimos la comunicación por medio de un Web Service,
                        logrando así, que el proceso sea transparente para el usuario final. Considerando las siguientes observaciones: 
                            <dl  class="vinetas">
                                <ul>
                                <li>La integración se realiza por parte del cliente.</li>
                                <li>Las peticiones del servicio se realizan por un método REST. </li>
                                <li>Se utiliza el estándar JSON para el consumo de servicios.</li>
                            </ul>
                        </dl>
                        </p>
                    </div>   
                </div>
            </div>
            <div class="col-md-6 imgres ">
                <div class="col-md-6" data-animate="ts-fadeInRight" data-ts-delay=".3s">
                <img class ="movilCovenant" src="assets/img/covenant/mobile5.png" alt="">
                </div>
            </div>
        </div>
    </div>
</section>
    
<section id="features" class="pt-5 pb-5 text-black text-center text-sm-left" data-bg-color="#132035" >  <!--CDE3F7  B7D1E8 C1D6E9 D3E6F7-->
    <div class="container">
        <div class="ts-title text-center mt-5" data-animate="ts-fadeInUp" data-ts-delay=".4s" >
            <h2 style="color:#c56122">Problemáticas que resuelve Covenant</h2>
                    
        </div>  

        <div class="row justify-content-center text-left text-white">
            <div class="col-md-6  mt-5  ">
                <div class="d-flex" ><div class="precio-tabla-check"></div><p class="ml-3"> Evita la presencia física de los firmantes.</p></div>
                <div class="d-flex" ><div class="precio-tabla-check"></div><p class="ml-3" > Reducción de costos y tiempo en procesos administrativos.</p></div>
                <div class="d-flex" ><div class="precio-tabla-check"></div><p class="ml-3"> Garantiza la integridad del contenido del documento, evitando el riesgo físico de traslados.</p></div>           
            </div>
            
            <div class="col-md-6  mt-5 ">
                <div class="d-flex" ><div class="precio-tabla-check"></div><p class="ml-3"> Certeza de que el firmante es quien dice ser. </p></div>
                <div class="d-flex" ><div class="precio-tabla-check"></div><p class="ml-3"> Presunción jurídica y no repudio ante un juez (respaldo legal y tecnológico).</p></div>
                <div class="d-flex" ><div class="precio-tabla-check"></div><p class="ml-3"> Confidencialidad de actos jurídicos o comerciales con firmas seguras mediante un ambiente web y móvil.</p></div>
            </div>

        </div>

    </div>
                    
</section>      

<section id="features" class="pt-5 pb-5 text-black text-center text-sm-left" data-bg-color="#132035">  <!--CDE3F7  B7D1E8 C1D6E9 D3E6F7-->
    <div class="container">
        
            <div class="ts-title text-center mt-5" data-animate="ts-fadeInUp" data-ts-delay=".4s">
                <h2 class="" style="color:#c56122"> Beneficios de utilizar nuestro servicio</h2>
            </div>

            <div class="row text-center iconos-cloud justify-content-center text-justify text-white">
                <div class="col-md-3 col-6" >
                    <img src="assets/img/covenant/ib4.png" alt="">
                        <h5>Reduce riesgos de falsificación</h5>
                </div>
                <div class="col-md-3 col-6">
                    <img src="assets/img/covenant/ib1.png" alt="">
                        <h5>Firma desde cualquier lugar y dispositivo</h5>
                </div>
                <div class="col-md-3 col-6">
                    <img src="assets/img/covenant/ib2.png" alt="">
                     <h5>Reduce costos, tiempos y asociados</h5>
                </div>
                <div class="col-md-3 col-6">
                    <img src="assets/img/covenant/ib3.png" alt="">
                        <h5>Certeza jurídica y validez legal</h5>
                </div>
                </div>
                <div class="row justify-content-center text-left text-white">
                    <div class="col-md-6 mt-5">
                        <div class="d-flex"><div class="precio-tabla-check"></div><p class="ml-3"> Seguro y transparente, cada transacción se registra permitiendo dar integridad al documento sin que intervenga el factor humano.</p></div>
                        <div class="d-flex"><div class="precio-tabla-check"></div><p class="ml-3" > Creación de expedientes digitales y almacenamiento de información en centros de datos certificados.</p></div>
                                    
                    </div>
                        
                    <div class="col-md-6 mt-5">
                        <div class="d-flex"><div class="precio-tabla-check"></div><p class="ml-3"> Firma electrónicamente desde cualquier lugar, obteniendo los mismos efectos jurídicos que con la firma autógrafa.</p></div>
                        <div class="d-flex"><div class="precio-tabla-check"></div><p class="ml-3"> Reducción del riesgo de falsificación en la documentación digital.</p></div>
                        <div class="d-flex"><div class="precio-tabla-check"></div><p class="ml-3"> Regulados por la Secretaría de Economía.</p></div>
                    </div>
                </div>
                   
            </div>
                                        

    </div>

</section>

<section class="signup-step-container" class="py-3" data-bg-color="#fafafa">
    <div class="row justify-content-center text-center">
        <div class="col-md-12 ts-title text-center mt-5" data-animate="ts-fadeInUp" data-ts-delay=".4s">
           <h2><b>¿Cómo lo hacemos?</b></h2>
        </div>
        <div class="col-md-12" >
            <p  style=" font-size:1.1rem;">Puedes enviar a firmar tus documentos digitales mediante dos formas:</p>
        </div>                 
    </div>
    <div class="row justify-content-center text-center" style="display: flex; align-items: center;">
        <div class="col-md-4">
            <a href="Soluciones/Covenant/index.php#step-container-pdf" style=" font-size:1.1rem;" class="btn btn-outline-naranja my-2 video-btn ts-fadeInUp animated">Forma Uno: CARGA DE PDF</a>
        </div>
        <div class="col-md-4">
            <a href="Soluciones/Covenant/index.php#plantilla" style="font-size:1.1rem;" class="btn btn-outline-naranja my-2 video-btn ts-fadeInUp animated">Forma Dos: Creación y asignación de Plantillas</a>
        </div>
    </div>
    <section id="step-container-pdf" class="step-container-pdf" >
        <div class="col-md-12 justify-content-center text-center pb-5" data-animate="ts-fadeInUp" data-ts-delay=".4s" >
            <h3 class="bold" >Opción 1: Carga de PDF</h3>
            <p  style=" font-size:1.3rem;">Puedes enviar a firmar tus documentos digitales siguiendo nuestra guía de 5 sencillos pasos.  </p>
        </div>  
        <div class="container">
        <div class="row d-flex justify-content-center" data-animate="ts-fadeInUp" data-ts-delay=".4s">
            <div class="col-md-10">
            <div class="wizard" data-animate="ts-fadeInUp" data-ts-delay=".3s">
                <div class="wizard-inner">
                <div class="connecting-line"></div>
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                    <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" aria-expanded="true"><span class="round-tab">1 </span> <i class="titlePaso">Paso 1</i></a>
                    </li>
                    <li role="presentation" class="disabled">
                    <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" aria-expanded="false"><span class="round-tab">2</span> <i  class="titlePaso">Paso 2</i></a>
                    </li>
                    <li role="presentation" class="disabled">
                    <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab"><span class="round-tab">3</span> <i  class="titlePaso">Paso 3</i></a>
                    </li>
                    <li role="presentation" class="disabled">
                    <a href="#step4" data-toggle="tab" aria-controls="step4" role="tab"><span class="round-tab text-center">4</span> <i  class="titlePaso">Paso 4</i></a>
                    </li>
                    <li role="presentation" class="disabled">
                    <a href="#step5" data-toggle="tab" aria-controls="step5" role="tab"><span class="round-tab text-center">5</span> <i  class="titlePaso">Paso 5</i></a>
                    </li>
                </ul>
                </div>
    
                <div role="form" class="login-box" style=" padding:30px; ">
                <div class="tab-content" id="main_form">
                    <div class="tab-pane active" role="tabpanel" id="step1">
                    <h4 class="text-center" style="color:#09324A">Cargar el archivo PDF. </h4>
                    <div class="row">
                        <img src="assets/img/steps-pdf/1-cargaPDF.PNG" alt="" style="width:80%; margin-left:10%; margin-right:10%">
                    </div>
                    <ul class="list-inline pull-right">
                        <li><button type="button" class="default-btn next-step">Siguiente</button></li>
                    </ul>
                    </div>
                    <div class="tab-pane" role="tabpanel" id="step2">
                    <h4 class="text-center" style="color:#09324A">Selecciona el tipo de firma (FEA/Autógrafa).</h4>

                    <div class="row">
                        <img src="assets/img/steps-pdf/2-tipoFirma.PNG" alt="" style="width:80%; margin-left:10%; margin-right:10%">
                    </div>
    
                    <ul class="list-inline pull-right">
                        <li><button type="button" class="default-btn prev-step">Anterior</button></li>
                        <li><button type="button" class="default-btn next-step">Siguiente</button></li>
                    </ul>
                    </div>
                    <div class="tab-pane" role="tabpanel" id="step3">
                    <h4 class="text-center" style="color:#09324A">Cambia el nombre del archivo y escribe el asunto del documento.</h4>

                    <div class="row">
                        <img src="assets/img/steps-pdf/3-nombreArchivoAsunto.PNG" alt="" style="width:80%; margin-left:10%; margin-right:10%">
                    </div>
                    <ul class="list-inline pull-right">
                        <li><button type="button" class="default-btn prev-step">Anterior</button></li>
                        <li><button type="button" class="default-btn next-step">Siguiente</button></li>
                    </ul>
                    </div>
                    <div class="tab-pane" role="tabpanel" id="step4">
                    <h4 class="text-center" style="color:#09324A">Agrega los datos de la persona que firmará el documento.</h4>

                    <div class="row pl-3 pr-3">
                        <img src="assets/img/steps-pdf/4-agregarFirmantes.PNG" alt="" style="width:80%; margin-left:10%; margin-right:10%">
                    </div>
    
                    <ul class="list-inline pull-right">
                        <li><button type="button" class="default-btn prev-step">Anterior</button></li>
                        <li><button type="button" class="default-btn next-step">Siguiente</button></li>
                    </ul>
                    </div>
                    <div class="tab-pane" role="tabpanel" id="step5">
                    <h4 class="text-center" style="color:#09324A">Solicita la firma del documento.</h4>
                    <div class="row">
                        <img src="assets/img/steps-pdf/5-solicitudDeFirma.PNG" alt="" style="width:80%; margin-left:10%; margin-right:10%">
                    </div>
    
                    <ul class="list-inline pull-right">
                        <li><button type="button" class="default-btn prev-step">Anterior</button></li>
                    </ul>
                    </div>
                    <div class="clearfix"></div>
                </div>
    
                </div>
            </div>
            </div>
        </div>
        </div>
    </section>

   
    <br>
    <br><br> 
</section>

<section  class="pt-3 pb-5 text-black text-center text-sm-left" data-bg-color="#132035" >  <!--CDE3F7  B7D1E8 C1D6E9 D3E6F7-->
    <div class="container">
    <div id="plantilla" class="pt-5 mt-5">
        <div class="col-md-12 justify-content-center text-center" data-animate="ts-fadeInUp" data-ts-delay=".2s" >
            <h3 class="bold" style="font-size:2rem; color:#c56122"><b>Opción 2: Creación y asignación de plantillas.</b></h3>
            <p  style=" font-size:1.1rem; color:white">Puedes enviar a firma tus documentos digitales utilizando nuestra herramienta para crear y asignar tus propias plantillas personalizadas.</p>
        </div> 
    </div>
    <br>
        <div class="ts-title text-center mt-5" data-animate="ts-fadeInUp" data-ts-delay=".2s" >
            <h2 style="color:#c56122">Algunos beneficios de crear plantillas</h2>
                    
        </div>  

        <div class="row">
            <div class="col-md-6 mt-2  mb-3 text-justify text-white" >
                <div class="d-flex" aling="justify"><div class="precio-tabla-check"></div><p class="ml-3"> Agrega el diseño que necesites.</p></div>
                <div class="d-flex" aling="justify"><div class="precio-tabla-check"></div><p class="ml-3"> Utilizando la Firma Autógrafa Digital se puede elegir el lugar en el documento donde se incorpore el trazo de la firma.</p></div>
                <div class="d-flex" aling="justify"><div class="precio-tabla-check"></div><p class="ml-3" > Asigna la plantilla al área  o áreas que requieran utilizarla.</p></div>
                <div class="d-flex" aling="justify"><div class="precio-tabla-check"></div><p class="ml-3"> El usuario "Checker" (Validador), podrá revisar las plantillas elaboradas para que posteriormente sean asignadas al área que hará uso de ellas.</p></div>
                                  
            </div>

            <div class="col-md-6  mt-2  mb-3 text-justify text-white">
                <div class="d-flex" ><div class="precio-tabla-check"></div><p class="ml-3"> Permite deshabilitar temporalmente el uso de las plantillas para modificar el contenido del documento fuente.</p></div>
                <div class="d-flex"><div class="precio-tabla-check"></div><p class="ml-3"> Los documentos capturados  se pueden configurar para pasar por un proceso de validación antes de enviarse a firma.</p></div>
                <div class="d-flex"><div class="precio-tabla-check"></div><p class="ml-3"> Mantiene la integridad del documento fuente, capturando solamente la información variable (la cual se muestra con el símbolo asterisco "*" por medio de un formulario).</p></div>
                <div class="d-flex" aling="justify"><div class="precio-tabla-check"></div><p class="ml-3"> Previsualiza el documento en cualquier momento del proceso de creación.</p></div>
            </div>
        </div>

    </div>
                    
</section>
<br>
<br>

<section id="documentacion">
    <br>
    <div class="container">
    <?php
    include 'lineaSimple/linea.html';
    ?>


</section>

</div>

<section id="mensaje"  >

    <div class="  mensaje ">
    <div class="col-md-12" data-animate="ts-fadeInUp" data-ts-delay=".3s">
    <div class="ts-title text-center">
        <h2  style="color:#c56122;"><b>"Para poder ver mejor nuestro contenido"</b></h2>
        <br>
        <h5  style="color:#333231;"><b>Vizualizar en una pantalla más grande</b></h5>
    </div>
    </div>
    </section>
        <br>
        <br>


    <br>
    <br><br>





<section class=" ts-background-repeat back-parallax" data-bg-color="#FFFFFF" >
    <h2 class="pt-5 text-center" >Áreas de oportunidad</h2>
        <div class="container ts-promo-numbers text-center">
            <div class="ts-title text-center" data-animate="ts-fadeInLeft" data-ts-delay=".1s"></div>
                <div class="descripCovenant cardcontainer ">

                    <div class="valores">
                         <ul  class="row">   
                            <li class="sector col-md-4">
                                <div class="row ts-feature text-center">
                                    <!-- <h2 class="bold  col-md-12" ><i class="fa fa-money-bill-alt fa-2x"></i></h2> -->
                                    <h2 class="bold  col-md-12" ><i class="fa faa fa-university fa-2x"></i></h2>
                                    <h4 class="bold col-md-12" >Sector Financiero</h4>
                                    <hr  class="areas_opt">
                                    <p style="text-align: justify;" class="col-md-12">Agilizar y optimizar los procesos de documentación, trámites y contratos con la información personalizada y confidencial de tus clientes.</p>
                                </div>
                            </li>
                            <li class="sector col-md-4">
                                <div class="row ts-feature text-center">
                                    <h2 class="bold  col-md-12" ><i class="fa faa fa-gavel fa-2x"></i></h2>
                                    <h4 class="bold col-md-12">Despachos Jurídicos</h4>
                                    <hr  class="areas_opt">
                                    <p style="text-align: justify;" class="col-md-12">Diseñar, crear y gestionar todo tipo de documento de manera inteligente con certeza jurídica y validez legal.</p>
                                </div>
                            </li>
                            <li class="sector col-md-4">
                                <div class="row ts-feature text-center">
                                    <h2 class="bold  col-md-12" ><i class="fa faa fa-home fa-2x"></i></h2>
                                    <h4 class="bold  col-md-12">Sector Inmobiliario</h4>
                                    <hr  class="areas_opt">
                                    <p style="text-align: justify;" class="col-md-12">Lograr ser más eficiente y eficaz con los procesos internos, reduciendo costos y sus asociados firmando electrónicamente los documentos.</p>
                                </div>
                            </li>
                            <li class="sector col-md-4">
                                <div class="row ts-feature text-center">
                                    <h2 class="bold  col-md-12" ><i class="fa faa fa-shield-alt fa-2x"></i></h2>
                                    <h4 class="bold  col-md-12">Aseguradoras</h4>
                                    <hr  class="areas_opt">
                                    <p style="text-align:justify;" class="col-md-12">Agilizar los trámites y servicios que se realizan con los clientes de una manera única, digital y transparente.</p>
                                </div>
                            </li>
                            <li class="sector col-md-4">
                                <div class="row ts-feature text-center">
                                    <h2 class="bold col-md-12" ><i class="fa faa fa-rocket fa-2x"></i></h2>
                                    <h4 class=" bold col-md-12">Startups</h4>
                                    <hr  class="areas_opt">
                                    <p style="text-align: justify;" class="col-md-12">Innovar y gestionar desde el comienzo los procesos internos para lograr hacer crecer tu negocio más rápidamente.</p>
                                </div>
                            </li>
                            <li class="sector col-md-4">
                                <div class="row ts-feature text-center">
                                    <h2 class="bold col-md-12"><i class="fa faa fa-plus-circle fa-2x"></i></h2>
                                    <h4 class=" bold  col-md-12">Otros</h4>
                                    <hr  class="areas_opt">
                                    <p style="text-align: justify;" class="col-md-12">¡Sé parte del futuro hoy! Incrementa tu capacidad como organización líder con nuestra solución tecnológica.</p>
                                </div>
                            </li>
                        </ul>
                      
                    </div>
                </div>
                
            </div>
        </div>
</section>


<br><br><br>


 <section class="pt-5 pb-5 text-black text-center text-sm-left" data-bg-color="#ececf5 ">
    <h2 class="text-center" ><b>Covenant API </b></h2>
    <div class="container" id="procesoAPI">
        <div class="col-md-12">
            <p class="text-center" style="font-size:1.1rem;">Contamos con distintas APIs que pueden ayudar a conectar tu empresa con la nuestra, sin necesidad de utilizar una interfaz web y sin realizar 
                grandes cambios sobre la infraestructura tecnológica con la que ya se cuenta. 
            </p>
            <p class="text-center" style="font-size:1.1rem;"><b>Contamos con dos procesos de conectividad vía API:</b></p>
        </div> 
  
    
            <div class="row cenboton justify-content-center text-center">
                <div class="col-md-4">
            <a href="Soluciones/Covenant/index.php#procesoAPI"  class="btn btnn-outline-naranja my-2 video-btn ts-fadeInUp animated"  >Envío de Documentos PDF</a>
                </div>
                <div class="col-md-4">
                    <a href="Soluciones/Covenant/index.php#procesoPlantilla"  class="btn btnn-outline-naranja my-2 video-btn ts-fadeInUp animated" >Envío de Documentos basados en Plantillas</a>
                </div>
            </div>
            
            
        
     </div>
     
</section>   
<br><br><br>


<!--section id="procesoAPI" class="mt-3" >
        <div class="col-md-12 justify-content-center text-center pb-5" data-animate="ts-fadeInUp" data-ts-delay=".1s" >
            <h3 class="bold " >Opción 1: Envío de Documentos PDF</h3>
            <p  class="text-center" style=" font-size:1.3rem;">Conoce nuestro flujo de trabajo para el envío de documentos PDFa través de una conectividad Web Service (WS). </p>
        </div>  
       

        <div class="container">
            <div class="row d-flex justify-content-center" data-animate="ts-fadeInUp" data-ts-delay=".1s">
                <div class="col-md-10">
                    <div class="wizard" data-animate="ts-fadeInUp" data-ts-delay=".1s">
                        
          
                        <div role="form" class="login-box" style="width: 170%; height: 700px; margin-left:-290px; ">
                            <div class="tab-content" id="main_form">
                                <div class="tab-pane active" role="tabpanel" id="step1">
                                    <div class="row">
                                    <img src="assets/img/api/DiagramaPDFAPI.png" style="width: 94%; margin-top: 20px; margin-left: 15px; margin-right: 15px;" > 
                                    </div>

                
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section-->

<br><br><br>
<section id="procesoAPI" class="mt-3" >
<div class="imgp">

        <div class="col-md-12 justify-content-center text-center pb-5" data-animate="ts-fadeInUp" data-ts-delay=".1s" >
            <h3 class="bold " >Opción 1: Envío de Documentos PDF</h3>
            <p  class="text-center" style=" font-size:1.3rem;">Conoce nuestro flujo de trabajo para el envío de documentos PDFa través de una conectividad Web Service (WS). </p>
        </div>  
        <div class="clic"> <h5 class="bold " >¡DA CLIC!,PARA PODER DESCARGAR</h5> </div>
            <a href="assets/img/api/DiagramaPDFAPI.png" download>  <img  src="assets/img/api/DiagramaPDFAPI.png"  ></a>     
         
        <div class="row justify-content-center text-center clicc" style="display: flex; align-items: center;">
            <div class="col-md-12 clicc ">
                <a  href="assets/img/api/DiagramaPDFAPI.png"  style="font-size:1.1rem;" class="btn btnnn-outline-naranja my-2 video-btn ts-fadeInUp animated"   download>Descargar</a>
            </div>
            
        </div>

</section>
</div>

<br><br><br>
<!--section id="procesoPlantilla"  >
        <div class="col-md-12 justify-content-center text-center pb-5" data-animate="ts-fadeInUp" data-ts-delay=".1s" >
            <h3 class="bold ">Opción 2: Envío de Documentos Basados en Plantillas</h3>
            <p  style=" font-size:1.3rem;">Conoce nuestro proceso de trabajo para el envío de información de los documentos generados
por medio de una plantilla (template) a través de una conectividad Web Service (WS).</p>
        </div>  

        <div class="container">
            <div class="row d-flex justify-content-center" data-animate="ts-fadeInUp" data-ts-delay=".1s">
                <div class="col-md-10">
                    <div class="wizard" data-animate="ts-fadeInUp" data-ts-delay=".1s">
                        
          
                        <div role="form" class="login-box" style=" width: 200%; height: 770px; margin-left: -460px; ">
                            <div class="tab-content" id="main_form">
                                <div class="tab-pane active" role="tabpanel" id="step1">
                                    <div class="row">
                                    <img src="assets/img/api/DiagramaPlantillasAPI.png" style=" width: 98%; height: 680px; margin-top: 20px; margin-left: 15px; margin-right: 15px;" > 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section-->
<br><br><br>
<div class="imgp">
<section id="procesoPlantilla" class="mt-3">
        <div class="col-md-12 justify-content-center text-center pb-5" data-animate="ts-fadeInUp" data-ts-delay=".1s" >
            <h3 class="bold ">Opción 2: Envío de Documentos Basados en Plantillas</h3>
            <p  class="text-center"  style=" font-size:1.3rem;">Conoce nuestro proceso de trabajo para el envío de información de los documentos generados
            por medio de una plantilla (template) a través de una conectividad Web Service (WS).</p>
        </div>  
        <div class="clic"><h5 class="bold " >¡DA CLIC!,PARA PODER DESCARGAR</h5></div>

            <a href="assets/img/api/DiagramaPlantillasAPI.png" download> <img src="assets/img/api/DiagramaPlantillasAPI.png"></a> 
          
        <div class="row justify-content-center text-center clicc" style="display: flex; align-items: center;">
            <div class="col-md-12 clicc" >
                <a  href="assets/img/api/DiagramaPlantillasAPI.png"  style="font-size:1.1rem;" class="btn btnnn-outline-naranja my-2 video-btn ts-fadeInUp animated"   download> Descargar</a>
            </div>
        </div>
                                     
</section>
</div>

<!--section id="partners" class="py-3" data-bg-color="#fafafa">
            <div class="container py-5">
            <h2 class="text-center g1">Características</h2>
                <div class="row">
                    <div class="col-md-6 d-flex align-items-center" data-animate="ts-fadeInLeft" data-ts-delay=".4s">
                        <div class="row container" >
                            <div class="col-md-12">
                                <div class="ts-title">
                                    <h2 >Seguro y Transparente</h2>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <p>Todas las operaciones de firma electrónica avanzada que ocurren en COVENANT son realizadas de forma “local” garantizando que no se exponen datos ni contraseñas de nuestros clientes.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6" data-animate="ts-fadeInRight" data-ts-delay=".4s">
                        <img src="assets/img/covenant/mobile5.png" style="width:100%" alt="">
                    </div>
                    <div class="col-md-6 mt-5" data-animate="ts-fadeInLeft" data-ts-delay=".5s">
                        <img src="assets/img/covenant/mobile6.png" style="width:100%" alt="">
                    </div>
                    <div class="col-md-6 d-flex align-items-center" data-animate="ts-fadeInRight" data-ts-delay=".5s">
                        <div class="row container">
                            <div class="col-md-12">
                                <div class="ts-title">
                                    <h2 >Auténtico e Identificable</h2>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <p>Cada transacción es registrada de forma exacta y permite dar integridad a cada uno de los documentos, gracias a los Sellos Digitales de Tiempo (SDT) y a la huella digital que se incluye en cada uno de ellos.</p>
                            </div>
                        </div>
                    </div>
                </div>
           
            </div>
        </section>
   
       

  
<section class="mt-5 mb-5">
    <div class="container">
        <div class="row justify-content-center text-center">
             <div class="col-md-7">
                <div class="ts-title">
                    <h2 >Documentos</h2>
                </div>
            </div>
            <div class="col-md-7">
                <p>Videos de utilidad sobre el uso de Covenant Firma desde nuestro sitio web</p>
            </div>
                            
        </div>
        <div class="row justify-content-center text-center">
            <div class="col-md-4">
                <button class="btn btn-outline-naranja my-2 video-btn ts-fadeInUp animated" data-toggle="modal" data-target="#ModalPDF">FEA Hoja Membretada</button>
            </div>
            <div class="col-md-4">
                <button class="btn btn-outline-naranja my-2 video-btn ts-fadeInUp animated" data-toggle="modal" data-target="#ModalPDF">Autógrafa Hoja Membretada</button>
            </div>
        </div>
    </div>
</section-->
<br><br><br>
<section id="features" class="pt-5 pb-5 text-black text-center text-sm-left" data-bg-color="#ececf5 ">
                <div class="container descripCovenant">
                <div class="ts-title text-center" >
                    <h2 style="color:#c56122"><b>¡Descarga la aplicación móvil ahora!</b></h2>
                   
                </div>  

                <div class="flex-btnn-social social-btns inicio">
                    <a class="app-btnn blu flex-btnn-social vert" href="https://apps.apple.com/cl/app/covenant-firma/id1499385817" target="_blank">
                        <i class="fab fa-apple"></i>
                        <p>Disponible en <br/> <span class="big-txt">App Store</span></p>
                    </a>

                    <a class="app-btnn blu flex-btnn-social vert" href="https://play.google.com/store/apps/details?id=com.legalexgs.covenant_firma" target="_blank">
                        <i class="fab fa-google-play"></i>
                        <p>Disponible en <br/> <span class="big-txt">Google Play</span></p>
                    </a>
                        
                
                </div>

                <img src="assets/img/mobile-1.png" alt="" class="mt-5 img-fluid" >
                </div>
            </section>



        <section class="mt-5 mb-5 container">
                        <div class="row  justify-content-center text-center">
                            <div class="col-md-7">
                                <div class="ts-title">
                                    <h2 >Tutoriales Web </h2>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <p>Videos de utilidad sobre el uso de Covenant Firma desde nuestro sitio web</p>
                            </div>
                        </div>
            <div id="videos-promo" class="owl-carousel owl-theme">
                <div class="modal-video item">
                
                    <div class="video-tutorial">
                        <img src="assets/img/covenant/lap-tablet-cel.png" alt="Avatar" class="image" style="width:100%">

                        <div class="titulo">
                            <h5 class="video-btn" > <a href="https://www.youtube.com/watch?v=maUjsYD3z4s"  target="_blank"><i class="fa fa-play"></i></a></h5>
                        </div>
                    </div>
                    <p class="text-center">Cómo ingresar por primera vez (Activación de cuenta)</p>
                </div>

                <div class="modal-video item">
                
                    <div class="video-tutorial">
                        <img src="assets/img/covenant/lap-tablet-cel.png" alt="Avatar" class="image" style="width:100%">

                        <div class="titulo">
                            <h5 class="video-btn"> <a href="https://www.youtube.com/watch?v=JnHa9-neTX4"   target="_blank"><i class="fa fa-play"></i></a></h5>
                        </div>
                    </div>
                    <p class="text-center">Recuperar Contraseña (web)</p>
                </div>

                <div class="modal-video item">
                    <div class="video-tutorial">
                        <img src="assets/img/covenant/lap-tablet-cel.png" alt="Avatar" class="image" style="width:100%">

                        <div class="titulo">
                            <h5 class="video-btn"><a href="https://www.youtube.com/watch?v=deSlarOc5UA"  target="_blank"><i class="fa fa-play"></i></a></h5>
                        </div>
                    </div>
                    <p class="text-center">Firma Electrónica Avanzada (FEA) vía web</p>
                </div>

                <div class="modal-video item">
                    
                    <div class="video-tutorial">
                        <img src="assets/img/covenant/lap-tablet-cel.png" alt="Avatar" class="image" style="width:100%">

                        <div class="titulo">
                            <h5 class="video-btn"><a href="https://www.youtube.com/watch?v=T7hiyuo9OkE"  target="_blank"><i class="fa fa-play"></i></a></h5>
                        </div>
                    </div>
                    <p class="text-center">Documento Cancelado por el Firmante (web)</p>
                </div>

            </div>
            
            
            <div class="container mb-3 mt-3">
                <div class="row  justify-content-center">
                    <div class="col-md-1 col-2 left" >
                         <i class="fa fa-arrow-left customNextBtn" aria-hidden="true"></i>
                    </div>
                    <div class="col-md-1 col-2 right" >
                         <i class="fa fa-arrow-right customPrevBtn" aria-hidden="true"></i>
                    </div>
                </div>                
            </div>


            <div class="row container justify-content-center text-center mt-5">
                            <div class="col-md-7">
                                <div class="ts-title">
                                    <h2 >Tutoriales aplicación móvil </h2>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <p>Videos de utilidad sobre el uso de Covenant Firma con nuestra aplicacón</p>
                            </div>
             </div>
            
             <div id="videos-promo2" class="owl-carousel owl-theme">
             <div class="modal-video item">
                    <div class="video-tutorial">
                        <img src="assets/img/covenant/lap-tablet-cel.png" alt="Avatar" class="image" style="width:100%">

                        <div class="titulo">
                            <h5 class="video-btn" ><a href="https://www.youtube.com/watch?v=06eC4gQ1vJI"  target="_blank"><i class="fa fa-play"></i></a></h5>
                        </div>
                    </div>
                    <p class="text-center">¿Cómo instalo la app móvil de Covenant Firma? (App)</p>
                </div>
                <div class="modal-video item">
                <div class="video-tutorial " >
                    <img src="assets/img/covenant/lap-tablet-cel.png" alt="Avatar" class="image" style="width:100%">
                    <div class="titulo">
                        
                        <h5 class="video-btn" > <a  href="https://www.youtube.com/watch?v=QDCCJh7xzuk"  target="_blank"><i class="fa fa-play"></i></a></h5>
                    </div>
                    
                    </div>
                    <p class="text-center">Carga de certificados y firma FEA (App)</p>
                </div>
                <div class="modal-video item">
                <div class="video-tutorial " >
                <img src="assets/img/covenant/lap-tablet-cel.png" alt="Avatar" class="image" style="width:100%">

                    <div class="titulo">
                        
                        <h5 class="video-btn" ><a href="https://www.youtube.com/watch?v=hfLmudWHyEQ"  target="_blank"><i class="fa fa-play"></i></a></h5>
                    </div>
                    
                    
                    </div>
                    <p class="text-center">Firma de Trazo Digital (App)</p>
                </div>
                <div class="modal-video item">
                    <div class="video-tutorial " >
                        <img src="assets/img/covenant/lap-tablet-cel.png" alt="Avatar" class="image" style="width:100%">

                        <div class="titulo">
                            
                            <h5 class="video-btn"><a  href="https://www.youtube.com/watch?v=hraPNy24KAY" target="_blank"><i class="fa fa-play"></i></a></h5>
                        </div>
                    
                    
                    </div>
                    <p class="text-center">Activación de datos Biométricos (App)</p>
                </div>

            </div>

            <div class="container mt-3">
                <div class="row  justify-content-center">
                    <div class="col-md-1  col-2 left">
                         <i class="icon ion-ios-arrow-back customNextBtn2" aria-hidden="true"></i>
                    </div>
                    
                    <div class="col-md-1 col-2 right" >
                         <i class="icon ion-ios-arrow-forward customPrevBtn2" aria-hidden="true"></i>
                    </div>
                </div>                
            </div>
           
        </section>
            <!--Features
            ============================================================================================================
            -->
        


        

       
<?php
include 'inc/pie.php';
?>
<script src="assets/js/steps-cargaPDF.js"></script> 
<script>



var $videoSrc;  
$('.video-btn').click(function() {
    $videoSrc = $(this).data( "src" );
    console.log($videoSrc);
});
console.log($videoSrc);

$('#videoModalbtn').on('shown.bs.modal', function (e) {
    $("#videoModalSrc").attr('src',$videoSrc + "?autoplay=1&amp;modestbranding=1&amp;showinfo=0" ); 
})

$('#videoModalbtn').on('hide.bs.modal', function (e) {
    $("#videoModalSrc").attr('src',$videoSrc); 
}) 

var owlp = $('#videos-promo');
owlp.owlCarousel({
    loop:true,
    autoplay:true,
    autoplayTimeout:4000,
    margin:15,
    center: true,
    autoplaySpeed: 3000,
    smartSpeed: 800,
    autoplayHoverPause:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:3
        }
    }
});
$('.customPrevBtn').click(function() {
    owlp.trigger('next.owl.carousel');
})
// Go to the previous item
$('.customNextBtn').click(function() {
    owlp.trigger('prev.owl.carousel', [300]);
})

var owlp2 = $('#videos-promo2');
owlp2.owlCarousel({
    loop:true,
    autoplay:true,
    margin:15,
    center: true,
    autoplaySpeed: 3000,
    autoplayHoverPause:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:3
        }
    }
});
$('.customPrevBtn2').click(function() {
    owlp2.trigger('next.owl.carousel');
})
// Go to the previous item
$('.customNextBtn2').click(function() {
    owlp2.trigger('prev.owl.carousel');
})

$('#videos-promo2').on('click', function () {
    owlp2.trigger('stop.owl.autoplay');
});
$('#videos-promo').on('click', function () {
    owlp.trigger('stop.owl.autoplay');
});
$(".btn-close-videos-tut").on('click',function(){
    owlp.trigger('play.owl.autoplay');
    owlp2.trigger('play.owl.autoplay');
    owlp.trigger('to.owl.carousel', $(this).data( 'position' ) ); 
    owlp2.trigger('to.owl.carousel', $(this).data( 'position' ) ); 
});

 
</script>