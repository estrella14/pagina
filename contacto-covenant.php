<?php 
$pageLink = "Centro de Recursos";
    include 'inc/cab.php';
?>
    <style>
        .form-covenant{
            background-color:#f5f5f5;
            padding: 1.5rem 2rem;
        }
        .g-recaptcha {
        transform:scale(1);
        
}
    </style>
    <section class="banner-recursos ">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h1 style="font-weight:100;font-size:30px;">Contacta a uno de nuestros agentes de ventas</h1>
                    <p >Legalex GS te ayuda a buscar un presuesto a tu medida. Para más información, contacta con uno de nuestros representantes de ventas.</p>
                    <div style="padding:0 5rem;">
                        <img src="assets/img/covenant/mobile5.png" alt="Covenant Legalex GS " class="img-fluid">    
                    </div>
                   
                </div>
                <div class="col-md-6">
                    <div class="form-covenant">
                        <form action="" id="form-contact" >
                            <h1 style="font-weight:100;font-size:25px;">Habla con un representante de ventas</h1>
                            <p>Nos pondremos en contacto para programar una videollamada o llamada.</p>
                            <div class="form-group">
                                <input type="text" class="form-control" id="servicio" name="solucion" value="Covenant" disabled hidden>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="inputname" name="nombre" placeholder="Nombre *" >
                            </div>
                            <div class="form-group">
                                <input  class="form-control" id="inputapp" name="paterno" placeholder="Apellido Paterno *" >
                            </div>
                            <div class="form-group">
                                <input  class="form-control" id="inputapm" name="materno" placeholder="Apellido Materno">
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" id="inputemail" name="correo" placeholder="Correo *" >
                            </div>
                            <div class="form-group">
                                <input  class="form-control" id="inputtel" name="telefono" placeholder="Teléfono *" >
                            </div>
                            <div class="form-group">
                                <input  class="form-control" id="inputempresa" name="empresa" placeholder="Empresa" >
                            </div>
                            <div class="form-group">
                            <input  class="form-control" id="inputPuesto" name="puesto" placeholder="Puesto" >
                            </div>
                            <div class="form-group">
                            <textarea class="form-control" id="inputmessage" rows="5" name="mensaje" placeholder="Cuéntanos sobre tus necesidades *" ></textarea>
                            </div>

                            <div class="info-form">
                                <small style="font-size:11px">
                                Al enviar este formulario, aceptas que podemos utilizar los datos que nos proporciones para contactar contigo sobre información relacionada con tu solicitud y sobre el producto de Covenant en cuestión. Para más información, consulta el aviso de privacidad de Legalex GS.
                                </small>
                            </div>
                            <div class="form-group clearfix">
                                     <input type="checkbox" name="avprivacidad" id="avp">    
                                     <label for="check">He leído y aceptado el<a href="aviso"> <span style="color:#006699">aviso de privacidad</span>  </a></label>
                                    </div>
                                    <div class="g-recaptcha" data-sitekey="6Lf8mT8cAAAAAE-RDsd0T7nErDHYTRi2W87RvLez"> </div>
                                    <p id="captcha_mensaje" class="text-danger" style="font-size:11px" hidden>
                                Complete el captcha para poder continuar.
                                </p>
                            <div class="form-group clearfix mt-3">
                                <button  type="submit" class="btn btn-naranja float-right" id="form-contact-submit">Enviar Mensaje</button>
                                <div id="btn-gracias-cont" data-toggle="modal" data-target="#GraciasModal"></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php
    include 'inc/pie.php';
?>
<script type="test/javascript" src="assets/js/JQuery.js"></script>
<script src='api/js/ContactoPsp.js'></script>
<script src="assets/js/notify-js.js"></script>
<script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.0.17/dist/sweetalert2.all.min.js"></script>


