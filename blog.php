<?php 
$pageLink = "Blog";
    include 'inc/cab.php';
?>
<style>
    .card-blog{
        display: flex;
        flex-direction: column;
        min-height: 20rem;
        padding: 1rem 1.5rem;
    }
    .card-blog h1{
        margin-left:15px;
    }
   .carta-blog{
    border: 1px solid #212529;
    border-top: 7px solid #212529; 
   }
   .card-blog .content {
    flex: 1 1 auto;
}
.pie-blog{
    display:flex;
    justify-content: space-between;
}
.carta-blog:hover{
    background-color:#051c33;
    color:#fff !important;
    transition: all .4s;
}
.carta-blog:hover a{
    color:#fff;
}
</style>
    <section class="banner-recursos container">
        <div class="header-recursos">
            <h1>Nuestro Blog</h1>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-4 mb-2 carta-blog">
                <div class="card-blog">
                    <div class="content">
                        <h1 class="h4">¿Qué es la firma digital?</h1>
                        <p>las firmas electrónicas son soluciones SaaS completas que autentican, rastrean y almacenan firmas e información del firmante en cada etapa del ciclo de vida del contrato...</p>
                    </div>
                    <div class="pie-blog">
                        <a href="blog-articulo">Leer más</a> <small>15-12-2021</small>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mb-2 carta-blog">
                <div class="card-blog">
                <div class="content">
                    <h1 class="h4">Contratos digitales - El futuro</h1>
                    <p>Desde la implementación del Internet de las Cosas (IoT) en las industrias, las grandes empresas han buscado digitalizar los procesos de contratación, ya sea para adquirir...</p>
                    </div>
                    <div class="pie-blog">
                        <a href="blog-articulo">Leer más</a> <small>15-12-2021</small>
                    </div>
                    
                </div>
            </div>
            <div class="col-md-4 mb-2 carta-blog">
                <div class="card-blog">
                <div class="content">  
                <h1 class="h4">Archivos en la nube</h1>
                    <p>La nube es una tecnología que permite almacenar, acceder y hacer uso de servicios, aplicaciones y contenidos sin necesidad de contar con almacenamiento...</p>
                    </div>
                    <div class="pie-blog">
                        <a href="blog-articulo">Leer más</a> <small>15-12-2021</small>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mb-2 carta-blog">
                <div class="card-blog">
                <div class="content">
                    <h1 class="h4">¿Qué es la firma digital?</h1>
                    <p>las firmas electrónicas son soluciones SaaS completas que autentican, rastrean y almacenan firmas e información del firmante en cada etapa del ciclo de vida del contrato...</p>
                    </div>
                    <div class="pie-blog">
                        <a href="blog-articulo">Leer más</a> <small>15-12-2021</small>
                    </div>
                </div>
            </div>

            
        </div>
    </section>
<?php
    include 'inc/pie.php';
?>