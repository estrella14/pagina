
<footer id="ts-footer" >
  <section id="ts-footer-main">
    <div class="pl-3 pr-3">


      <div class="row">

        <div class="col-md-3 text-center text-justify" style="padding: 0px 0px 11px">
          <img src="assets/img/logo-lg.png" alt="" width="200" style="padding: 0px 15px 0 15px;">

        </div>
        
        <div class="col-md-6 text-center">
          <p>
            <a href="aviso"> Aviso de Privacidad</a> | <a href="terminosyc">Términos y Condiciones</a>
            Copyright © <script>
              document.write(new Date().getFullYear())
            </script> Legalex GS Todos los derechos reservados. <a href="https://www.legalexgs.com/avisoprivacidad.pdf">&nbsp;</a>
          </p>
        </div>
        <div class="col-md-3 text-center">
          <address class="ts-text-muted---white">
            <strong> <i class="fas fa-phone mr-2 "></i></strong> <a href="tel:443 690 68 51">443 690 68 51 y 52 </a> &nbsp | &nbsp
            <strong><i class="fas fa-envelope mr-2 "></i></strong><a href="mailto:contacto@legalexgs.com">contacto@legalexgs.com</a>

          </address>
        </div>
        
        <!--div class="col-sm-6 col-md-3 foot-monb-nav">
          <nav class="nav flex-column">
            <a class="nav-link" href="/pagina">Inicio</a>
            <a class="nav-link" href="nosotros">Nosotros</a>
            <a class="nav-link" href="contacto">Contacto</a>
          </nav>
        </div-->
        <!-- <div class="col-sm-6 text-center">
                            <address class="ts-text-muted---white">
                                Periférico Paseo de la República No. 2650, P2 Int. 3-C Col. Prados del Campestre, CP. 58297, Morelia, Mich. 
                                <br>
                                <br>
                                <strong> <i class="fas fa-phone mr-2 "></i></strong> <a href="tel:443 690 68 51">443 690 68 51 y 52</a>   
                                <br>
                                <strong> <i class="fas fa-envelope mr-2 "></i></strong> <a href="mailto:contacto@legalexgs.com">contacto@legalexgs.com</a>
                                
                            </address>
                        </div> -->
      </div>
    </div>
  </section>

</footer>
</div>
</div>


<!-- Modal -->
<div class="modal fade" id="videoModalbtn" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">

    <div class="modal-content">
      <button type="button" class="close btn-close-videos-tut" data-dismiss="modal" aria-label="Close" style="opacity:1;background-color:#000;">
        <span aria-hidden="true" style="color:#fff">&times;</span>
      </button>
      <div class="embed-responsive embed-responsive-16by9">
        <iframe class="embed-responsive-item" src="" id="videoModalSrc" allowscriptaccess="always" allow="autoplay"></iframe>
      </div>

    </div>
  </div>
</div>
<div class="modal fade " id="GraciasModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered " role="document">

    <div class="modal-content">

      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <i class="ion ion-md-checkmark-circle" style="color:#058205"></i> Hemos recibido tu información
        <br>
        <p class="mt-3">Automatiza el flujo de tus contratos y garantiza su autenticidad con <a href="covenant" class="link-naranja">COVENANT</a> </p>

        <a href="Soluciones/Covenant/index.php" class="mt-3 mb-5 link-azul">Saber más</a>
      </div>

    </div>
  </div>
</div>

</div>

<div class="cookies">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-9">
        <p>Este sitio utiliza cookies para mejorar la experiencia de nuestros Socios. Cada vez que utilizas nuestro sitio web,
          autorizas el uso de cookies para almacenar información y generar estadísticas de audiencia analizando tu navegación.
          Revisa nuestro Aviso de Cookies para más información.</p>
      </div>
      <div class="col-md-3 gbtn" role="group">
        <button type="button" class=" gdpr-accept">
          Acepto</button>
        <a href="cookies">
          <button type="button" class=" gdpr-review">
            Revisar</button></a>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="ModalPDF" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modalWithTop" role="document">
    <!--Content-->
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header">
      </div>

      <!--Body-->
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">&times;</span>
        </button>
        <object class="PDFdoc" width="100%" height="500px" type="application/pdf" data="assets/pdf/psdt.pdf"></object>
      </div>


    </div>
    <!--/.Content-->
  </div>
</div>


<div class="modal fade" id="ModalPDFS" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modalWithTop" role="document">
    <!--Content-->
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header">
      </div>

      <!--Body-->
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">&times;</span>
        </button>
        <object class="PDFdoc" width="100%" height="500px" type="application/pdf" data="assets/pdf/FEA.pdf"></object>
      </div>


    </div>
    <!--/.Content-->
  </div>
</div>



<div class="modal fade" id="ModalPDFD" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modalWithTop" role="document">
    <!--Content-->
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header">
      </div>

      <!--Body-->
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">&times;</span>
        </button>
        <object class="PDFdoc" width="100%" height="500px" type="application/pdf" data="assets/pdf/FAD.pdf"></object>
      </div>


    </div>
    <!--/.Content-->
  </div>
</div>


<div class="modal fade" id="ModalPDFM" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modalWithTop" role="document">
    <!--Content-->
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header">
      </div>

      <!--Body-->
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">&times;</span>
        </button>
        <object class="PDFdoc" width="100%" height="500px" type="application/pdf" data="assets/pdf/msdt.pdf"></object>
      </div>


    </div>
    <!--/.Content-->
  </div>
</div>

<div class="modal fade" id="ModalPDFC" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modalWithTop" role="document">
    <!--Content-->
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header">
      </div>

      <!--Body-->
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">&times;</span>
        </button>
        <object class="PDFdoc" width="100%" height="500px" type="application/pdf" data="assets/pdf/declaracion.pdf"></object>
      </div>


    </div>
    <!--/.Content-->
  </div>
</div>
<div class="modal fade" id="ModalPDFNO" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modalWithTop" role="document">
    <!--Content-->
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header">
      </div>

      <!--Body-->
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">&times;</span>
        </button>
        <object class="PDFdoc" width="100%" height="500px" type="application/pdf" data="assets/pdf/10-Declaración de Prácticas de CCMD_V1_2 VP.pdf"></object>
      </div>


    </div>
    <!--/.Content-->
  </div>
</div>

<div class="modal fade" id="ModalPDFPO" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modalWithTop" role="document">
    <!--Content-->
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header">
      </div>

      <!--Body-->
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">&times;</span>
        </button>
        <object class="PDFdoc" width="100%" height="500px" type="application/pdf" data="assets/pdf/9-Políticas de CCMD 1_2 VP.pdf"></object>
      </div>


    </div>
    <!--/.Content-->
  </div>
</div>

<div class="contacto-fast">
  <a href="nosotros/contacto#formulario-seccion"><i class="fa fa-envelope"></i> <span>contacto</span> </a>
</div>

<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<script type="text/javascript" src="assets/js/navbarmobile.js"></script>
<script src="assets/js/jquery-3.3.1.min.js"></script>
<script src="assets/js/popper.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/js/imagesloaded.pkgd.min.js"></script>
<script src="assets/js/isInViewport.jquery.js"></script>
<script src="assets/js/jquery.magnific-popup.min.js"></script>
<script src="assets/js/scrolla.jquery.min.js"></script>
<script src="assets/js/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/additional-methods.js"></script>
<script src="assets/js/odometer.min.js"></script>
<script src="assets/js/owl.carousel.min.js"></script>
<script src="assets/js/particles.min.js"></script>
<script src="assets/js/leaflet.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>
<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.6.0/mdb.min.js"></script> -->
<script src="assets/js/jquery.vide.min.js"></script>
<script src="assets/js/html2pdf.bundle.min.js"></script>

<script src="assets/js/custom.js"></script>

<?php if (isset($mapaPie)) { ?>
  <script>
    var latitude = 19.6838886;
    var longitude = -101.1653071;
    var markerImage = "assets/img/map-marker.png";
    var mapElement = "map-contact";

    var map = L.map(mapElement).setView([latitude, longitude], 17);

    L.tileLayer('http://{s}.tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png', {
      attribution: '&copy; LEGALEX GS'
    }).addTo(map);

    var markerIcon = L.icon({
      iconUrl: markerImage,
      iconSize: [28, 38],
      iconAnchor: [14, 38]
    });

    L.marker([latitude, longitude], {
        icon: markerIcon
      }).addTo(map)
      .bindPopup('Credix Thrust<br> Morelia,Mich.');
  </script>

<?php } ?>

<script>
  function toggleIcon(e) {

    $(e.target)
      .prev('.faq')
      .find(".more-less")
      .toggleClass('expanded hola');
  }
  $('#accordion').on('hidden.bs.collapse', toggleIcon);
  $('#accordion').on('shown.bs.collapse', toggleIcon);
</script>


</body>

</html>