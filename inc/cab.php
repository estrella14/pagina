<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

if (isset($_GET['prop'])) {
    $_SESSION['prop-ads'] = $_GET['prop'];
} else {
    if (isset($_SESSION['prop-ads'])) {
        $prop = $_SESSION['prop-ads'];
    } else {
        $prop = "organico";
    }
}
$classLogoIndex = "scrolled";
if ($pageLink == "inicio") {
    $classLogoIndex = "";
}
?>
<!doctype html>
<html lang="es">

<head>
    <?php

    $legalex = file_exists("../legalexgs");
    $LEGALEXpagina = file_exists("../../LEGALEX/pagina");
    $Proyectopagina = file_exists("../../proyecto/pagina");
    $pagina = file_exists("../pagina");
    if ($legalex) {
        echo '<base href="/legalexgs/">';
    } else if ($LEGALEXpagina) {
        echo '<base href="/LEGALEX/pagina/">';
    } else if ($Proyectopagina) {
        echo '<base href="/proyecto/pagina/">';
    } else if ($pagina) {
        echo '<base href="/pagina/">';
    } else {
        echo '<base href="/">';
    }


    ?>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="Javier Zamudio">
    <meta name="description" content="Somos una empresa encargada de desarrollar plataformas digitales robustas 
    capaces de mejorar procesos con una eficiencia excepcional.">
    
    <!--Cache-->
    <meta http-equiv='cache-control' content='no-cache'>
    <meta http-equiv='expires' content='0'>
    <meta http-equiv='pragma' content='no-cache'>
      <!------------------------------------------------------------------>

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,600">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css?version=1.0" type="text/css">
    <link rel="stylesheet" href="assets/font-awesome/css/fontawesome-all.min.css?version=1.0">
    <link rel="stylesheet" href="assets/css/magnific-popup.css?version=1.0">
    <link rel="stylesheet" href="assets/css/odometer-theme-default.css?version=1.0">
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css?version=1.0">
    <link rel="stylesheet" href="assets/css/leaflet.css?version=1.0">
    <link rel="stylesheet" href="assets/css/style.css?version=1.0">
    <link rel=icon type=image/png sizes=16x16 href=assets/img/fav.png>
    <link href="https://unpkg.com/ionicons@4.5.10-0/dist/css/ionicons.min.css" rel="stylesheet">
    <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.6.0/mdb.min.css" rel="stylesheet" />-->
    <title>Legalex GS: Eficiencia y claridad administrativa</title>
  
 
</head>

<style>

@media screen and (max-width: 1068px){
.contenedor-nav {
    padding: 0px 0rem;
}
}

</style>
<body class="">

    <div id="screeninicio" class=" has-loading-screen nav-hovered">
        <img src="assets/img/logolg.png" alt="">

    </div>
    </div>

    <div class="ts-page-wrapper">

        <div class="cabecera ">
            <div class="contenedor-nav ">
                <nav>
                    <div class="menu-icons">
                        <i class="icon ion-md-menu"></i>
                        <i class="icon ion-md-close"></i>
                    </div>
                    <?php

                    $legalex = file_exists("../legalexgs");
                    $LEGALEXpagina = file_exists("../../LEGALEX/pagina");
                    $Proyectopagina = file_exists("../../proyecto/pagina");
                    $pagina = file_exists("../pagina");
                    if ($legalex) {
                        echo '<a href="/legalexgs/" class="logo">';
                    } else if ($LEGALEXpagina) {
                        echo '<a href="/LEGALEX/pagina/"  class="logo">';
                    } else if ($Proyectopagina) {
                        echo '<a href="/proyecto/pagina/"  class="logo">';
                    } else if ($pagina) {
                        echo '<a href="/pagina"class="logo">';
                    } else {
                        echo '<a href="/"class="logo">';
                    }


                    ?>
                    <img src="assets/img/legalexblanco.png" alt="">
                    </a>
                    <ul class="nav-list ">

                        <li class="align-self-start">

                            <a class="opPrincipal" onclick="submenu('psc')">Servicios PSC <i class="icon ion-md-arrow-dropdown"></i></a>
                            <div id="menupsc" class="sub-menu">
                                <h4 id="titlemobile">Nuestros Servicios PSC</h4>
                                <div class="row" style="padding-left: 1rem;">
                                    <div class="bloque-nav col-md-5">
                                        <p id="subtitlemobile">Sello Digital de Tiempo</p>
                                        <a href="Servicios/sellos">Información</a>
                                        <a href="Servicios/sellos/validar.jsp">Validar SDT</a>
                                        <a href="Servicios/sellos/documentos">Documentación</a> 
                                        <a href="Servicios/sellos/certificados">Certificado</a>
                                    </div>
                                    <div class="bloque-nav col-md-5">
                                        <p id="subtitlemobile2">Constancia NOM-151 </p>
                                        <a href="Servicios/nom151">Información</a>
                                        <a href="Servicios/nom/validarNOM.jsp">Validar Constancia</a>
                                        <a href="Servicios/nom/documentos">Documentación</a>
                                        <a href="Servicios/nom/certificados">Certificado</a>
                                    </div>
                                </div>

                            </div>
                        </li>
                        <li class="align-self-start">
                            <a class="opPrincipal" onclick="submenu('solutions')">soluciones <i class="icon ion-md-arrow-dropdown"></i></a>
                            <div id="menusolutions" class="sub-menu">
                                <h4 id="titlemobile2">Nuestras Soluciones</h4>
                                <a href="Soluciones/Covenant/index.php">
                                    <div class="item-nav-menu">
                                        <p>COVENANT</p>
                                        <small>La forma más sencilla de firmar documentos </small>
                                    </div>
                                </a>
                                <!--a href="notifimex">
                            <div class="item-nav-menu">
                                <p>Notificamex</p>
                                <small>Recibe notificaciones en tiempo real </small>
                            </div>
                        </a-->
                                <a href="Soluciones/Surety">
                                    <div class="item-nav-menu">
                                        <p>SURETY</p>
                                        <small>Sistema de garantías dinámicas y financiamientos </small>
                                    </div>
                                </a>
                            </div>
                            <ul style="display:none" class="sub-menu">
                                <li>
                                    <a href="Soluciones/Covenant/index.php">Covenant</a>
                                </li>
                                <li>
                                    <a href="notifimex">Notificamex</a>
                                </li>
                                <li>
                                    <a href="Soluciones/Surety">Surety</a>
                                </li>
                            </ul>
                        </li>
                        
                        
                        <!--PRECIÓSSSS
                        <li class="align-self-start">
                            <a href="precio">Precios</a>
                        </li>-->
                        <!------------------------>


                        <li class="align-self-start">
                            <a class="opPrincipal" onclick="submenu('nosotros')">Nosotros <i class="icon ion-md-arrow-dropdown"></i></a>
                            <div id="menunosotros" class="sub-menu">
                                <h4 id="titlemobile3">Legalex GS</h4>
                                <div class="row">
                                    <div class="col-md-12">
                                        <a href="nosotros">
                                            <div class="item-nav-menu">
                                                <p>Somos un Prestador de Servicios de Certificación (PSC) </p>
                                                <small>Más información</small>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-12">
                                        <a href="nosotros/contacto">
                                            <div class="item-nav-menu">
                                                <p>Contacto </p>
                                                <small>Habla con uno de nuestros agentes</small>
                                            </div>
                                        </a>
                                    </div>

                                </div>
                            </div>
                        </li>






                        <li class="align-self-start">
                            <a href="explora">Descargas</a>
                        </li>
                        <li class="align-self-start">
                            <a href="preguntas">Preguntas Frecuentes</a>
                        </li>
                        <li class="mover-derecha align-self-start">
                            <a href="https://www.legalexgs.com/covenant_firma/" class="btn-firmar-covenant">Firmar en Covenant</a>
                        </li>

                    </ul>
                </nav>
            </div>
        </div>