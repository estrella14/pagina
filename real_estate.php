<?php
$pageLink = "real";
include 'inc/cab.php';

?>

    <!--Content
    ====================================================================================================================
    -->
    <div class="ts-content">
        <!--HERO HEADER
        ================================================================================================================
        ================================================================================================================
        -->
        <header id="ts-hero" class=" text-white" >

            <div class="container">
                <div class="row ts-align__vertical">

                    <div class="col-sm-6 col-md-7">
                        <h1 data-animate="ts-reveal">
                            <span class="wrapper">
                                <span>Real Estate</span>
                            </span>
                        </h1>

                        <div  data-animate="ts-fadeInUp" data-ts-delay=".1s">
                            <p>
                            Sistema para la administración de las funciones operativas en la enajenación o arrendamiento de bienes inmuebles.
                            </p>
                        </div>

                        <div class="ts-group d-none d-md-block">
                           
                            <a href="nosotros/contacto" class="btn btn-outline-light my-2" data-animate="ts-fadeInUp" data-ts-delay=".2s">Más Infornmación</a>
                        </div>
                    </div>

                    
                </div>
            </div>

            <div class="ts-background ts-bg-black">
                <div class="ts-img-into-bg ts-background-image ts-parallax-element" style="opacity: 0.5;">
                    <img src="assets/img/clientes.jpg" alt="Real Estate - LEGALEX GS" >
                </div>
            </div>

        </header>
    
       
        <main id="ts-content">

            <section id="blockquote" class="ts-block ts-bg-white ">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 d-flex align-content-center">
                            <center> <img src="assets/img/realestate.png" alt="Real Estate by Legalex GS" style="width:50%" data-animate="ts-fadeInDown" data-ts-delay=".1s"></center>
                            </div>
                            <div class="col-md-12 mt-5">
                                <blockquote class="blockquote">
                                    <h4>Real Estate by Legalex, es una solución informática para la administración de las funciones operativas en la enajenación o arrendamiento de bienes inmuebles durante el proceso y ciclo de venta o renta, así como la gestión de las relaciones con los clientes.</h4>
                                    <footer class="blockquote-footer">the future is now, let's build it together</footer>
                                </blockquote>
                            </div>
                        </div>
                    </div>
            </section>


            <section id="features" class="ts-block text-white text-center text-sm-left" data-bg-color="#14141c">
                <div class="container">
                    <h3 class="text-center">Beneficios</h3>
                    <div class="row">

                        <div class="col-md-6"  data-animate="ts-fadeInLeft" data-ts-delay=".2s">
                            <div class="ts-feature">
                                <div class="row">
                                    <div class="col-md-2">
                                        <img  src="assets/img/icon/11.png" alt="" style="width:60px">
                                    </div>
                                    <p class="col-md-10" data-animate="ts-fadeInLeft" data-ts-delay=".3s"> Administración consolidada de  inmuebles individuales o proyectos inmobiliarios integrales.</p>

                                    <div class="col-md-2">
                                        <img  src="assets/img/icon/5.png" alt="" style="width:60px">
                                    </div>
                                    <p class="col-md-10" data-animate="ts-fadeInLeft" data-ts-delay=".3s"> Mapa Interactivo - Administra todos los estatus del proceso de compra-venta de todo tu proyecto inmobiliario.</p>

                                    <div class="col-md-2">
                                        <img  src="assets/img/icon/8.png" alt="" style="width:60px">
                                    </div>
                                    <p class="col-md-10" data-animate="ts-fadeInLeft" data-ts-delay=".3s"> CRM (Customer Relationship Management) - Gestiona de manera eficiente y en tiempo real las relaciones con tus clientes durante todo el proceso.</p>

                                    <div class="col-md-2">
                                        <img  src="assets/img/icon/9.png" alt="" style="width:60px">
                                    </div>
                                    <p class="col-md-10" data-animate="ts-fadeInLeft" data-ts-delay=".3s"> Notificaciones y alertas vía SMS - Recibe mensajes de texto y correo electrónico.</p>
                                </div>  
                            </div>  
                        </div> 
                        <div class="col-md-6"  data-animate="ts-fadeInLeft" data-ts-delay=".2s">
                                <div class="row">
                                    <div class="col-md-2">
                                        <img  src="assets/img/icon/4.png" alt="" style="width:60px">
                                    </div>
                                    <p class="col-md-10" data-animate="ts-fadeInLeft" data-ts-delay=".3s"> Contratos digitales - Crea, firma y envía todos tu contratos de manera electrónica en cuestión de minutos en unos cuantos clics.</p>
                                   
                                   
                                    <div class="col-md-2">
                                        <img  src="assets/img/icon/2.png" alt="" style="width:60px">
                                    </div>
                                    <p class="col-md-10" data-animate="ts-fadeInLeft" data-ts-delay=".3s"> Certeza jurídica y validez legal - Siéntete totalmente seguro, tranquilo y respaldado en todas tus operaciones electrónicas ya que somos un PSC.</p>
                                    <div class="col-md-2">
                                        <img  src="assets/img/icon/7.png" alt="" style="width:60px">
                                    </div>
                                    <p class="col-md-10" data-animate="ts-fadeInLeft" data-ts-delay=".3s"> Reporting 24/7 - Genera y descarga reportes en tiempo real.</p>
                                    <div class="col-md-2">
                                        <img  src="assets/img/icon/3.png" alt="" style="width:60px">
                                    </div>
                                    <p class="col-md-10" data-animate="ts-fadeInLeft" data-ts-delay=".3s"> Cotizador en tiempo real - Facilita a tu fuerza de ventas una herramienta que les permita cotizar a la medida y necesidades de tus clientes. </p>
                                
                                </div>
                                
                            
                        </div>
                        
                        
                           

                      
                        <!--end Feature-->

                    </div>
                </div>
     
            </section>

            <!--Our Experience
            ============================================================================================================
            -->
            <section class="ts-block  pb-10" data-bg-color="#f5f5f5">
                <div class="container">

                    <div class="ts-title text-center">
                        <h2>Problemáticas que resuelve</h2>
                    </div>

                    <div class="row">

                        <div class="col-md-6 mb-5"  data-animate="ts-fadeInUp" data-ts-delay=".6s">
                            <img src="assets/img/lap-tablet-cel.png" class="mw-100" alt="">
                        </div>

                        <div class="col-md-6">
                            <ul class="list list-text list-dashed">

                                <li data-animate="ts-fadeInRight">
                                    <h4>Consumo excesivo de tiempo y análisis confuso de información.</h4>
                                   
                                </li>
                                <li data-animate="ts-fadeInRight" data-ts-delay=".1s">
                                    
                                    <h4>Proceso lento de ventas y mala gestión de clientes.</h4>
                                </li>
                                <li data-animate="ts-fadeInRight" data-ts-delay=".2s">
                                    <h4>Deficiencias en la comunicación interna y baja productividad.</h4>
                                </li>
                                <li data-animate="ts-fadeInRight" data-ts-delay=".2s">
                                    <h4>Certidumbre y referenciación de los pagos recibídos.</h4>
                                </li>
                                <li data-animate="ts-fadeInRight" data-ts-delay=".2s">
                                    <h4>Acceso limitado y centralizado a la información para la toma de desiciones.</h4>
                                </li>
                            </ul>
                        </div>

                    </div>
                </div>
            </section>


        </main>
    </div>

       
<?php
include 'inc/pie.php';
?>
