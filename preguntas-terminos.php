<?php
$pageLink = "Preguntas frecuentes";
include 'inc/cab.php';
?>
<style>
    .buscador-faqs {
        display: flex;
        justify-content: center;
        margin-bottom: 3rem;
    }

    .inputbuscar {
        display: flex;
        box-shadow: 0px 0px 10px 1px #00000052;
        align-items: center;
        background-color: #fff;
        border-radius: 15px;
        width: 50%;
    }

    .inputbuscar input {
        border: none;
        padding: 1.3rem 0.5rem;
        border-radius: 15px;
        width: 100%;
    }

    .inputbuscar input:focus {
        outline: none;
    }

    .inputbuscar .iconobuscar {
        margin: 0 0.2rem 0 1rem;
    }

    .lateral_faq {
        height: 100vh;
        box-shadow: 4px 0 4px -2px #00000054;
        padding: 0 15px;
        position: fixed;
        left: 0;
        width: 17.7%;
        background-color: #fff;
        font-size: .9375rem;
    }

    .item_nav_faq {
        display: flex;
        padding: 1rem 1.2rem;
        border-radius: 15px;
        margin-bottom: 10px;
    }

    .item_nav_faq.active a div span {
        color: #fff;
    }

    .item_nav_faq.active {
        background-color: #051c33;
        color: #fff;
    }

    .item_nav_faq.active:hover {
        background-color: #051c33;
        cursor: auto;
    }

    .item_nav_faq .icono_nav {
        padding: 0 1.2rem 0 0rem;
    }

    .item_nav_faq:hover {
        background-color: #9ac5f0;
        cursor: pointer;
    }

    .select_mob_faqs {
        display: none;
    }

    @media (max-width:767px) {
        .lateral_faq {
            display: none;
        }

        .select_mob_faqs {
            display: flex;
            justify-content: center;
        }

        .inputbuscar {
            width: 90%;
        }

        .faqs .container-fluid {
            padding-right: 0px;
            padding-left: 0px;
        }

        .faqs .mob-faq {
            padding-right: 15px;
            padding-left: 15px;
        }
    }
</style>
<main style="background-color:#f5f5f5">
    <section class="faqs">
        <div class="container-fluid">
            <div class="row">

                <?php
                include 'menuFaqs.php';
                ?>
                <div class="col-md-10 mt-5 contenedor-mob" style="text-align:justify">

                    <div class="ts-title text-center mt-4">
                        <h2>Preguntas Frecuentes</h2>
                    </div>

                    <center>
                        <p id="textoselect" hidden>Seleccione alguna sección de la cual desee buscar</p>
                    </center>
                    <div class="select_mob_faqs mb-5 form-group container">
                        <select name="" id="whatfaqs" class="form-control" onchange="whatsfaqs()">
                            <option value="Covenant Documentos">Covenant Documentos</option>
                            <option value="Covenant Firmas">Covenant Firmas</option>
                            <option value="Covenant App">Covenant App</option>
                            <option value="Sello Digital de Tiempo">Sello Digital de Tiempo</option>
                            <option value="Constancia NOM-151">Constancia NOM-151</option>
                            <option value="Planes de Covenant">Planes de Covenant</option>
                            <option value="Definiciones" selected>Definiciones</option>
                        </select>
                    </div>
                    <div class="buscador-faqs">
                        <div class="inputbuscar">
                            <div class="iconobuscar">
                                <i class="fa fa-search"></i>
                            </div>
                            <input type="input" placeholder="Escriba algo a buscar..." id="buscador">
                        </div>
                    </div>
                    <div id="accordion" class="row mob-faq justify-content-center mb-5">
                        <div id="" class="accordion col-md-11 ">
                            <div class="card">
                                <div class="card-header faq" id="headingOne">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link  pregunta_search" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            1.- ‍¿Qué es?
                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><i class="expanded  more-less fas fa-plus-circle"></i></span>
                                </div>

                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="card-body">
                                        Es un recurso tecnológico, que permite demostrar que una serie de datos han existido en un determinado momento y no han sido alterados a través del tiempo.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="headingTwo">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            2.- Quién puede emitir un Sello Digital de Tiempo?

                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                    <div class="card-body">
                                        ‍‍Solo un Prestador de Servicios de Certificación acreditado por la Secretaría de Economía.

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="headingThree">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            3.- ¿Diferencia entre el Sello de tiempo y las Constancias de Conservación de Mensajes de Datos (CCMD) conforme la NOM-151-SCFI-2016?

                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        Un sello digital de tiempo brinda la fecha y hora exacta en la que ocurrio una transacción electrónica, mientras que una constancia de conservación de mensajes de datos conforme a la NOM-151-SCFI-2016 otorga presunción de integridad y la obtención de válidez a través del tiempo, ante una autoridad dentro del territorio mexicano.

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p4">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r4" aria-expanded="false" aria-controls="r4">
                                            4.- ‍¿Cuáles son los documentos a los que se les puede aplicar un Sello de Tiempo?


                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r4" aria-expanded="false" aria-controls="r4"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r4" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        Cualquier tipo de documento que requiera de una certeza de fecha y hora especifica en la que este fue emitido o firmado, adicionando que el sello digital de tiempo por si solo puede ser incluido en casi cualquier tipo de archivo por ejemplo de imagen o video hasta un documento editable como es un word, excel, PDF, etc.


                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p5">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r5" aria-expanded="false" aria-controls="r5">
                                            5.- ¿Tiene vigencia, cuánto tiempo?

                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r5" aria-expanded="false" aria-controls="r4"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r5" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        No tiene vigencia, el Sello Digital de Tiempo brinda la existencia de un contenido en una fecha y hora exacta.

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header faq" id="p6">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed pregunta_search" data-toggle="collapse" data-target="#r6" aria-expanded="false" aria-controls="r6">
                                            6.- Cuánto cuesta el servicio de Sellos Digitales de Tiempo?

                                        </button>
                                    </h5>
                                    <span data-toggle="collapse" data-target="#r6" aria-expanded="false" aria-controls="r6"><i class=" more-less fas fa-plus-circle"></i></span>
                                </div>
                                <div id="r6" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        Para más información sobre nuestros servicios, paquetes o planes de venta puede contactarnos en contacto@legalexgs.com o a los teléfonos: <a class="Linkss" href="tel: +52 443 690 6851 p101">+52 443 690 6851 Ext.101</a> y <a class="Linkss" href="tel: +52 443 690 6855">+52 443 690 6855</a>

                                    </div>
                                </div>
                            </div>





                        </div>
                    </div>
                    <?php
                    include 'inc/pie.php';
                    ?>
                </div>
            </div>
        </div>
    </section>
</main>

<script type="text/javascript" src="assets/js/faqs.js"></script>